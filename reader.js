 (function(DOMParser) {
  "use strict";
  
  var proto = DOMParser.prototype,
  nativeParse = proto.parseFromString;
  
  // Firefox/Opera/IE throw errors on unsupported types
  try {
  // WebKit returns null on unsupported types
  if ((new DOMParser()).parseFromString("", "text/html")) {
  // text/html parsing is natively supported
  return;
  }
  } catch (ex) {}
  
  proto.parseFromString = function(markup, type) {
  if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {
  var doc = document.implementation.createHTMLDocument("");
  if (markup.toLowerCase().indexOf('<!doctype') > -1) {
  doc.documentElement.innerHTML = markup;
  } else {
  doc.body.innerHTML = markup;
  }
  return doc;
  } else {
  return nativeParse.apply(this, arguments);
  }
  };
  }(DOMParser));

function init() {
    window.webkit.messageHandlers.onInit.postMessage("");
};

function setupWithConfig(configString) {
    
    var config = JSON.parse(configString);
    window.webkit.messageHandlers.onSetupWithConfig.postMessage(config.shouldCalculateTotalPages);
    
    setup(config.bgColor, config.textColor, config.textColorSecondary,
          config.bookmarkColor, config.fontName, config.fontSize, config.textAlignment,
          config.localPageIndex, config.globalPageIndex, config.showBookmark);
    
    if (config.shouldCalculateTotalPages) {
        // Search for TOC targets also
        var targetIdsPageIndices = {};
        initWith(targetIdsPageIndices, config.tocTargetIds);
        offscreenRender(config.spineElementsCount, targetIdsPageIndices);
    }
    
    // Setup body click
    document.body.addEventListener("click", function(e) {
                                   window.webkit.messageHandlers.onBodyClicked.postMessage("onBodyClicked");
                                   });
    
    // Setup anchor clicks
    var links = getAllAnchorsWithHrefs(getInnerWrapper());
    for (var i = 0; i < links.length; i++) {
        links[i].addEventListener("click", function(e) {
                                  var fragment = getHrefFragment(e.target);
                                  if (fragment) {
                                  window.webkit.messageHandlers.onAnchorClicked.postMessage(fragment);
                                  //                android.onAnchorClicked(fragment);
                                  }
                                  e.preventDefault(); // Dont do native jump-to-link
                                  e.stopPropagation();
                                  });
    }
};

function setup(bgColor, textColor, textColorSecondary, bookmarkColor, fontName, fontSize, textAlignment, localPageIndex, globalPageIndex, showBookmark) {
    adjustOuterWrapperFloatPadding();
    setColorTheme(bgColor, textColor, textColorSecondary, bookmarkColor);
    setFont(fontName);
    setFontSize(fontSize);
    setTextAlignment(textAlignment);
    setCurrentPage(localPageIndex);
    setShowBookmark(showBookmark, false);
    setPageNumber(globalPageIndex);
};

function adjustOuterWrapperFloatPadding() {
    var outerWrapper = document.getElementById("outer-wrapper");
    var floatPaddingTop = parseFloat(window.getComputedStyle(outerWrapper, null).getPropertyValue("padding-top"));
    var floatPaddingRight = parseFloat(window.getComputedStyle(outerWrapper, null).getPropertyValue("padding-right"));
    var floatPaddingLeft = parseFloat(window.getComputedStyle(outerWrapper, null).getPropertyValue("padding-left"));
    var floatPaddingBottom = parseFloat(window.getComputedStyle(outerWrapper, null).getPropertyValue("padding-bottom"));
    
    // As padding could be %, therefore float px values, to prevent rounding errors, floor these values to ints
    // (BTW scrollWidth is only supplied in int value, therefore makes rounding very error-prone)
    outerWrapper.style.padding =
    px(Math.floor(floatPaddingTop)) + " "
    + px(Math.floor(floatPaddingRight)) + " "
    + px(Math.floor(floatPaddingBottom)) + " "
    + px(Math.floor(floatPaddingLeft));
}

function offscreenRender(spineElementsCount, targetIdsPageIndices) {
    
    window.webkit.messageHandlers.onSetupWithConfig.postMessage("off screen render");
    
    //Mock onPagesRenderResultResponse
    
//    window.webkit.messageHandlers.onPagesRenderResult.postMessage('{"anchorTargetIndices":{},"pageCounts":[19,21,18,19,17,21,19,18,18,20,19,19,18,18,18,19,18,18,19,18,18,17,21,18,19,19,19,19,18,18,19,18,19,19,18,18,18,19,19,20,20,20,19,18,18,5]}');
    
    Promise.all(spineElementPromises(spineElementsCount))
    .then(function(htmls) {
          return calculatePageCountPromise(htmls, targetIdsPageIndices)
          })
    .then(function(tuple2) {
          return handleAllSpinesLoaded(tuple2.pageCounts, tuple2.targetIdsPageIndices)
          })
    .catch(function(ex) {
           window.webkit.messageHandlers.onPagesRenderError.postMessage(ex.stack)
           });
};

function calculatePageCountPromise(spineElementHtmls, targetIdsPageIndices) {
    var innerWrapperDiv = getInnerWrapper();
    var parser = new DOMParser();
    var pageCounts = [];
    
    var p = new Promise(function(resolve, reject) {
                        recurseParseAndCalculatePageCounts(resolve, innerWrapperDiv, parser, pageCounts,
                                                           targetIdsPageIndices, spineElementHtmls, 0);
                        });
    
    window.webkit.messageHandlers.onSetupWithConfig.postMessage("calculate page count");
    return p;
}

function recurseParseAndCalculatePageCounts(resolve, innerWrapperDiv, parser, pageCounts, targetIdsPageIndices, htmls, index) {
    if (index < htmls.length) {
        parseAndCalculatePageCounts(innerWrapperDiv, parser, pageCounts, targetIdsPageIndices, htmls[index], index);
        // Notify progress update
        window.webkit.messageHandlers.onPagesRenderProgress.postMessage(((index + 1) / htmls.length)* 100);
        
        // Recurse on the next event loop tick
        // to allow for cancel to pass through if neccessary
        // (for loop would block until finished)
        lastTimeoutId = setTimeout(function() {
                                   recurseParseAndCalculatePageCounts(resolve, innerWrapperDiv, parser, pageCounts, targetIdsPageIndices, htmls, index + 1)
                                   });
    } else {
        resolve({
                pageCounts: pageCounts,
                targetIdsPageIndices: targetIdsPageIndices
                });
    }
}

function parseAndCalculatePageCounts(innerWrapperDiv, parser, pageCounts, targetIdsPageIndices, html, index) {
    // Parse html
    var doc = parser.parseFromString(html, "text/html");
    var loadedInnerWrapperDiv = doc.getElementById("inner-wrapper");
    
    // Clear and move loaded inner-wrapper children to current inner-wrapper
    clearChildren(innerWrapperDiv);
    moveChildren(loadedInnerWrapperDiv, innerWrapperDiv);
    
    // Save result
    pageCounts[index] = calculatePageCount(innerWrapperDiv);
    //    console.log("onAjax pageCount=" + pageCounts[index]);
    
    // Find page indices link targets
    calculatePageIndicesForLinks(innerWrapperDiv, targetIdsPageIndices, index);
}

function spineElementPromises(spineElementsCount) {
    window.webkit.messageHandlers.onSetupWithConfig.postMessage("spine element promises");
    
    var promises = [];
    for (var i = 0; i < spineElementsCount; i++) {
        var p = new Promise(function(resolve, reject) {
                            createLoadHtmlPromise(resolve, i);
                            });
        promises.push(p);
    }
    return promises;
}

function createLoadHtmlPromise(resolve, spineElementIndex) {
    
    window.webkit.messageHandlers.onSetupWithConfig.postMessage("create load html promise");
    window.webkit.messageHandlers.onSetupWithConfig.postMessage(spineElementIndex);
    
    var html = prompt("SpineIndex-" + spineElementIndex)
    resolve(html)
}

var lastTimeoutId;

function cancelOffscreenRendering() {
    clearTimeout(lastTimeoutId);
    // TODO: token cancel nereturnutte promisy este cez token techniku
    // ale zatial pouzivam about:blank na zrusenie vsetkeho
};

function handleAllSpinesLoaded(pageCounts, targetIdsPageIndices) {
    var anchorTargetGlobalIndices = {};
    for (var key in targetIdsPageIndices) {
        anchorTargetGlobalIndices[key] = toGlobalIndex(pageCounts, targetIdsPageIndices[key]);
    }
    var result = {
        anchorTargetIndices : anchorTargetGlobalIndices,
        pageCounts : pageCounts
    }
    
     window.webkit.messageHandlers.onPagesRenderResult.postMessage(JSON.stringify(result));
};

function toGlobalIndex(pageCounts, spineElementIndexLocalIndexPair) {
    if (!spineElementIndexLocalIndexPair) return -1;
    
    var count = 0;
    for (var i = 0; i < spineElementIndexLocalIndexPair.spineElementIndex; i++) {
        count += pageCounts[i];
    }
    count += spineElementIndexLocalIndexPair.localPageIndex;
    return count;
}

function calculatePageCount(innerWrapperDiv) {
    var totalWidth = innerWrapperDiv.scrollWidth;
    var pageWidth = innerWrapperDiv.offsetWidth;
    
    window.webkit.messageHandlers.test.postMessage("Total Width " + totalWidth);
    window.webkit.messageHandlers.test.postMessage("Page Width " + pageWidth);
    
    var pageCount = Math.floor(totalWidth / pageWidth);
    return pageCount;
}

function calculatePageIndex(innerWrapperDiv, element, spineElementIndex) {
    var pageWidth = innerWrapperDiv.offsetWidth;
    var pageIndex = Math.floor(element.offsetLeft / pageWidth);
    return {
        spineElementIndex : spineElementIndex,
        localPageIndex : pageIndex
    }
}

function calculatePageIndicesForLinks(innerWrapperDiv, targetIdsPageIndices, spineElementIndex) {
    findAllInternalLinks(innerWrapperDiv, targetIdsPageIndices);
    calculatePageIndices(innerWrapperDiv, targetIdsPageIndices, spineElementIndex);
}

function findAllInternalLinks(innerWrapperDiv, targetIdsPageIndices) {
    var links = getAllAnchorsWithHrefs(innerWrapperDiv);
    for (var i = 0; i < links.length; i++) {
        var fragment = getHrefFragment(links[i]);
        if (fragment) {
            targetIdsPageIndices[fragment] = null;
        }
    }
}

function getAllAnchorsWithHrefs(div) {
    return div.querySelectorAll("a[href]");
}

function getHrefFragment(anchor) {
    var href = anchor.getAttribute("href");
    var hashIndex = href.indexOf("#");
    if (hashIndex != -1) {
        var fragment = href.substr(hashIndex + 1);
        return fragment;
    }
    return null;
}

function calculatePageIndices(innerWrapperDiv, targetIdsPageIndices, spineElementIndex) {
    for (var key in targetIdsPageIndices) {
        var position = targetIdsPageIndices[key];
        if (!position) { // If not position parsed yet
            key = adjustKey(key);
            if (key) {
                var targetElement = document.querySelector("[id=" + key + "]");
                if (!targetElement) {
                    // Exception, as in some weird books targets are not ids but <a name="foo"></a>
                    targetElement = document.querySelector("[name=" + key + "]");
                }
                if (targetElement) {
                    // Exception: in some epubs, for some reason, Contents hrefs target
                    // empty <a> inside some parent, before the actual <h2> and not the h2 itself,
                    // and we page-break before h2 so the a target ends up on page before h2.
                    // Btw h2 doesn't have to be right next sibling
                    targetElement = handleWeirdAnchorTargetsException(targetElement);
                    
                    position = calculatePageIndex(innerWrapperDiv, targetElement, spineElementIndex);
                    targetIdsPageIndices[key] = position;
                }
            }
        }
    }
}

function adjustKey(elementKey) {
    if (!elementKey) return null;
    elementKey = elementKey.replace(/\./g, "\\."); // replaceAll
    return elementKey;
}

function handleWeirdAnchorTargetsException(targetElement) {
    if (targetElement.tagName == "A"
        && targetElement.hasAttribute("id")
        && !targetElement.hasAttribute("href")) {
        var parent = targetElement.parentElement;
        var probableH2 = searchForNearProbableH2(parent);
        if (probableH2) {
            targetElement = probableH2;
        }
    }
    return targetElement;
}

function searchForNearProbableH2(element) {
    var maxBreadth = 3;
    var breadth = 0;
    while (element && element.tagName != "H2" && !element.hasAttribute("id")) {
        element = element.nextElementSibling;
        if (breadth++ >= maxBreadth) {
            return null;
        }
    }
    return element;
}

function getCurrentPageSnippet() {
    
    window.webkit.messageHandlers.test.postMessage("Get Current Page Snippet")
    
    // Works only in current view port, not with overflowing stuff
    var columnText = getAllTextInColumn(getInnerWrapper().getBoundingClientRect());
    var currentPageSnippet = columnText ? columnText.trim().substring(0, 80).replace("\n", " ") : ""
    window.webkit.messageHandlers.onPageSnippetResult.postMessage(currentPageSnippet);
}

var getAllTextInColumn = function(rect){
    // rect should be the size and x,y of the column
    // { top, left, width, height }
    
    if (document.caretRangeFromPoint){
        var caretRangeStart = document.caretRangeFromPoint(rect.left, rect.top);
        var caretRangeEnd = document.caretRangeFromPoint(rect.left+rect.width-1, rect.top+rect.height-1);
    } else {
        return null;
    }
    
    if (caretRangeStart == null || caretRangeEnd == null) return null;
    
    var range = document.createRange();
    range.setStart(caretRangeStart.startContainer, caretRangeStart.startOffset);
    range.setEnd(caretRangeEnd.endContainer, caretRangeEnd.endOffset);
    
    return range.toString();
};

function injectBookmarkSvg() {
    var bookmarkDiv = document.getElementById("bookmark");
    if (bookmarkDiv.childElementCount == 1) return false;
    
    ajax("bookmark.svg", function(xhttp) {
         if (bookmarkDiv.childElementCount == 1) return;
         var svgElement = xhttp.responseXML.documentElement;
         bookmarkDiv.appendChild(svgElement);
         bookmarkDiv.addEventListener("click", function(ev) {
                                      // Animate
                                      bookmarkDiv.classList.remove("translateDown");
                                      bookmarkDiv.classList.add("translateUp");
                                      
                                      // After animation send click
                                      bookmarkDiv.addEventListener("transitionend", function(ev) {
                                                                   bookmarkDiv.removeEventListener("transitionend", this);
                                                                   //       android.onBookmarkClicked();
                                                                   
                                                                   window.webkit.messageHandlers.onBookmarkClicked.postMessage("onBookmarkClicked");
                                                                   });
                                      // For body underneath not to receive click event
                                      ev.stopPropagation();
                                      });
         });
    return true;
}

function setShowBookmark(showBookmark, animate) {
    var bookmarkDiv = document.getElementById("bookmark");
    bookmarkDiv.classList.remove("translateDown");
    bookmarkDiv.classList.remove("translateUp");
    if (showBookmark) {
        injectBookmarkSvg();
        bookmarkDiv.style.visibility = "visible";
    } else {
        bookmarkDiv.style.visibility = "hidden";
    }
};

function setPageNumber(pageIndex) {
    var pageNumberWrapper = document.getElementById("page-number");
    pageNumberWrapper.innerHTML = pageIndex > 0 ? pageIndex + 1 : "";
}

function setFont(fontName) {
    document.body.style.fontFamily = fontName
};

function setFontSize(fontSize) {
    document.body.style.fontSize = px(fontSize);
};

function setTextAlignment(textAlignment) {
    var align;
    switch(textAlignment) {
        case 0:
            align = "left";
            break;
        case 1:
            align = "justify";
            break;
        case 2:
            align = "right";
            break;
    }
    document.body.style.textAlign = align;
};

function setColorTheme(bgColor, textColor, textSecondaryColor, bookmarkColor) {
    document.body.style.backgroundColor = bgColor; 
    document.body.style.color = textColor;
    document.getElementById("bookmark").style.color = bookmarkColor;
    document.getElementById("page-number-wrapper").style.color = textSecondaryColor;
};

function setCurrentPage(pageIndex) {
    var innerWrapperDiv = getInnerWrapper();
    var columnWidth = innerWrapperDiv.getBoundingClientRect().width;
    window.webkit.messageHandlers.test.postMessage("Column width: " + columnWidth + " page index " + pageIndex);
    window.webkit.messageHandlers.test.postMessage("Scrolling left " + pageIndex*columnWidth + " for page " + pageIndex);

    innerWrapperDiv.scrollLeft = pageIndex * columnWidth;
};

function getInnerWrapper() {
    return document.getElementById("inner-wrapper");
};

function moveChildren(oldParent, newParent) {
    while (oldParent.childNodes.length) {
        newParent.appendChild(oldParent.firstChild);
    }
};

function clearChildren(parent) {
    while (parent.hasChildNodes()) {
        parent.removeChild(parent.lastChild);
    }
};

function ajax(url, onResult) {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            onResult(this);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}

function initWith(map, array) {
    for (var i = 0; i < array.length; i++) {
        var targetId = array[i];
        map[targetId] = null;
    }
}

function px(value) {
    return value + "px";
}
