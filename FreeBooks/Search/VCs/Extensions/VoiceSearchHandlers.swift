//
//  VoiceSearchHandlers.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Lottie
import AVFoundation

extension SearchVC {
    
    @IBAction internal func voiceSearchPressed() {
        searchField.resignFirstResponder()
        showVoiceSearch()
    }
    
    private func showVoiceSearch() {
        voiceSearchContainer.isHidden = false
        setupVoiceSearch()
        UIView.animate(withDuration: 0.5, animations: {
            self.voiceSearchContainer.alpha = 1.0
        })
    }
    
    private func showRecognizingAnimation() {
        //We need to make sure success animation is hidden
        voiceRecognitionSuccessAnimationView.isHidden = true
        voiceRecognitionSuccessAnimationView.subviews.forEach( { $0.removeFromSuperview() })
        
        voiceRecognitionAnimationView.subviews.forEach( { $0.removeFromSuperview() })
        LottieUtils.showAnimation(named: Constants.LottieAnimations.VoiceRecognition, inView: voiceRecognitionAnimationView, completion: { })
    }
    
    internal func showNotRecognizedAnimation() {
        voiceRecognitionAnimationView.subviews.forEach( { $0.removeFromSuperview() })
        LottieUtils.showAnimation(named: Constants.LottieAnimations.VoiceRecognitionFailed, inView: voiceRecognitionAnimationView, startPaused: true, completion: { })
    }
    
    private func showSuccessAnimation(completion: @escaping () -> ()) {
        voiceRecognitionAnimationView.subviews.forEach( { $0.removeFromSuperview() })
        voiceRecognitionSuccessAnimationView.isHidden = false
        LottieUtils.showAnimation(named: Constants.LottieAnimations.Success, inView: voiceRecognitionSuccessAnimationView, playOnce: true, completion: completion)
    }
    
    private func hideVoiceSearch() {
        self.currentVoiceSearchResult = nil
        speechNotRecognizedTimer?.invalidate()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.voiceSearchContainer.alpha = 0
        }, completion: { success in
            self.voiceSearchContainer.isHidden = true
        })
    }
    
    @IBAction private func setupVoiceSearch() {
        showRecognizingAnimation()
        self.voiceRecognitionSuccessAnimationView.gestureRecognizers?.forEach({ self.voiceRecognitionSuccessAnimationView.removeGestureRecognizer($0) })
        self.voiceSearchTryAgain.isHidden = true
        voiceSearchText.text = "Search books or authors..."
        handleNoVoiceRecognized()
        
        //Since there's only a single AVAudioSession per app available and we need to switch between playback and recording (voice search) we need to make sure there's a proper category selected
        try? AVAudioSession.sharedInstance().setCategory(.record, mode: .default, policy: .default, options: .defaultToSpeaker)
        
        VoiceSearchManager.sharedManager.recordAndRecognizeSpeech(onSuccess: { (result) in
            //We need to make sure we're responding only to a single recognized result, otherwise we'd end up with multiple searches/success animations at the same time, having currentVoiceSearchResult defined means we're already processing something. currentVoiceSearchResult is set back to null after we leave current voice search
            guard self.currentVoiceSearchResult == nil else {
                return
            }
            self.currentVoiceSearchResult = result
            
            self.speechNotRecognizedTimer?.invalidate()
            self.setRecognizedText(recognizedText: result)
            
            //We need to bring voice search container to front since there's the "always on top" loading (showLoading from BaseVC) displayed at this time, otherwise it would overlap voice search view
            self.view.bringSubviewToFront(self.voiceSearchContainer)
            self.showSuccessAnimation(completion: {
                self.textFieldDidChange(self.searchField)
                self.hideVoiceSearch()
            })
            
            VoiceSearchManager.sharedManager.stopRecognition()
        }, onFail: { (permissionStatus) in
            let settingsAction = UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            })
            
            switch permissionStatus {
            case .SpeechRecognizer_NotAuthorized:
                AlertUtils.showAlert(title: "Permission required", message: "In order to use voice search, you need to give FreeBooks permissions to recognize your speech. Please do so in your device settings.", actions: [settingsAction], inVC: self)
                break
            case .Microphone_NotAuthorized:
                AlertUtils.showAlert(title: "Permission required", message: "In order to use voice search, you need to give FreeBooks permissions to access your microphone. Please do so in your device settings.", actions: [settingsAction], inVC: self)
                break
            case .SpeechRecognizer_Microphone_NotAuthorized:
                AlertUtils.showAlert(title: "Permission required", message: "In order to use voice search, you need to give FreeBooks permissions to access your microphone and recognize your speech. Please do so in your device settings.", actions: [settingsAction], inVC: self)
                break
            default:
                break
            }
        })
    }

    private func setRecognizedText(recognizedText: String) {
        self.searchField.text = recognizedText
        self.voiceSearchText.text = recognizedText
    }
    
    private func handleNoVoiceRecognized() {
        speechNotRecognizedTimer = Timer(fire: Date(timeIntervalSinceNow: 5), interval: 0, repeats: false, block: {
            _ in
            VoiceSearchManager.sharedManager.stopRecognition()
            self.showNotRecognizedAnimation()
            self.voiceSearchText.text = "Tap to speak"
            self.voiceSearchTryAgain.isHidden = false
            self.voiceRecognitionAnimationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.setupVoiceSearch)))
        })
        RunLoop.main.add(speechNotRecognizedTimer!, forMode: .common)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view == voiceSearchContainer {
            hideVoiceSearch()
        }
    }
}
