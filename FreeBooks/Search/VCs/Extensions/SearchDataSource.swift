//
//  SearchDataSource.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension SearchVC: UICollectionViewDataSource {
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchBook", for: indexPath) as! SearchBookCell
        cell.updateSeparator(color: Constants.Colors.Brown_10, offsetX: self.view.frame.size.width/4)
        
        if let book = self.viewModel?.getBook(index: indexPath.row) {
            cell.initWithSearchTerm(searchTerm: self.searchField.text ?? "", viewModel: BookCellVM(book: book))
        }
        
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel?.getBooksCount() ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SearchHeader", for: indexPath) as! SearchHeader
            headerView.title.text = self.currentState == .previousSearches ? "Recently searched" : "Books"
            return headerView
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SearchFooter", for: indexPath)
            return footerView
        default:
            return UICollectionReusableView()
        }
    }
}
