//
//  SearchDelegate.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension SearchVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    internal func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let searchTerm = searchField.text, searchTerm.isEmptyWithoutWhitespaces == false, let viewModel = self.viewModel, indexPath.row == (viewModel.getBooksCount()-1){
            viewModel.searchNext(searchTerm: searchTerm)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedBook = self.viewModel?.getBook(index: indexPath.row) {
            RealmUtils.storeSearchedBook(searchedBook: selectedBook)
            showBookDetail(book: selectedBook)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return self.currentState == .previousSearches ? CGSize(width: collectionView.frame.size.width, height: 50) : CGSize(width: collectionView.frame.size.width, height: 0)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 150)
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

