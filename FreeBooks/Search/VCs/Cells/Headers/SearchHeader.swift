//
//  SearchHeader.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class SearchHeader: UICollectionReusableView {
    @IBOutlet weak var title: UILabel!
}
