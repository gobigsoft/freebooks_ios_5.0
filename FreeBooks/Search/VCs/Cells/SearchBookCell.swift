//
//  SearchBookCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class SearchBookCell: BookCell {
    func initWithSearchTerm(searchTerm: String, viewModel: BookCellVM) {
        super.initWithViewModel(viewModel: viewModel, iconsTint: Constants.Colors.ReddishBrown, labelsColor: UIColor.black, authorLongNames: true)
        self.author.attributedText = NSMutableAttributedString(fullString: self.author.text ?? "", fullStringColor: UIColor.black, subString: searchTerm, subStringColor: Constants.Colors.ReddishBrown, substringFontSize: self.author.font.pointSize)
        self.title.attributedText = NSMutableAttributedString(fullString: self.title.text ?? "", fullStringColor: UIColor.black, subString: searchTerm, subStringColor: Constants.Colors.ReddishBrown, substringFontSize: self.title.font.pointSize)
    }
}
