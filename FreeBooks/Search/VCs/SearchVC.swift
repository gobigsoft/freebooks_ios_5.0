//
//  SearchVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 02/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Lottie
import MaterialShowcase

enum SearchState {
    case previousSearches
    case searchInfo
    case searching
    case resultsFound
    case noResultsFound
}

class SearchVC: BaseVC {
    private let SearchHeaderOffset = CGFloat(30)
    
    @IBOutlet weak private var foundBooksCollection: UICollectionView!

    @IBOutlet weak internal var searchField: UITextField!
    @IBOutlet weak private var searchFieldContainer: UIView!
    @IBOutlet weak private var searchStatusContainerView: UIView!
    @IBOutlet weak private var searchStatusDescription: UILabel!
    @IBOutlet weak private var searchStatusImage: UIImageView!
    
    @IBOutlet weak private var voiceSearchControlsContainer: UIView!
    @IBOutlet weak internal var voiceSearchContainer: UIView!
    @IBOutlet weak internal var voiceSearchText: UILabel!
    @IBOutlet weak internal var voiceSearchTryAgain: UIButton!
    @IBOutlet weak internal var voiceRecognitionAnimationView: UIView!
    @IBOutlet weak internal var voiceRecognitionSuccessAnimationView: UIView!
    
    @IBOutlet weak internal var searchContextButton: UIButton!
    
    internal var currentState: SearchState = .previousSearches
    internal var viewModel: SearchVM?
    
    internal var speechNotRecognizedTimer: Timer?
    internal var currentVoiceSearchResult: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerScrollToTopNotificationObserver(notificationName: Constants.NotificationConstants.TabBarSelectionNotifications.SearchSelected)

        UIUtils.dropShadow(view: searchFieldContainer)
        searchStatusImage.tintColor = Constants.Colors.GrayLight
        
        UIUtils.addBorder(view: voiceSearchTryAgain, color: voiceSearchTryAgain.tintColor)
        voiceSearchTryAgain.setRoundedCorners(radius: 5)
        voiceSearchControlsContainer.setRoundedCorners(radius: 5)
        searchFieldContainer.setRoundedCorners(radius: 2)
        
        searchField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        searchField.delegate = self
        
        viewModel = SearchVM(successCallback: booksFound, failureCallback: findingBooksFailed)
        viewModel!.getRecentSearchesCount() == 0 ? setSearchState(searchState: .searchInfo) : setSearchState(searchState: .previousSearches)
        
        foundBooksCollection.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        foundBooksCollection.dataSource = self
        foundBooksCollection.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !UserDefaultsUtils.wasSearchHintDisplayed() {
            showSearchHint()
        }
    }
    
    private func showSearchHint() {
        UIUtils.showHint(inView: searchField, title: "Search", subtitle: "Tap here and search for a book, author or category", completion: {
            UserDefaultsUtils.setSearchHintAsDisplayed()
        })
    }
    
    private func setSearchState(searchState: SearchState) {
        self.currentState = searchState
        switch searchState {
        case .previousSearches:
            hideLoading()
            setVoiceSearchButton()
            searchStatusContainerView.isHidden = true
            foundBooksCollection.isHidden = false
            viewModel!.fillWithStoredBooks()
            foundBooksCollection.reloadData()
            break
        case .searchInfo:
            hideLoading()
            setVoiceSearchButton()
            searchStatusImage.image = UIImage(named: "icon_no_search_recents")
            searchStatusDescription.text = "Search for books, audiobooks, categories or authors..."
            searchStatusContainerView.isHidden = false
            foundBooksCollection.isHidden = true
            break
        case .searching:
            showLoading()
            setClearSearchContentsButton()
            //We are preparing collection view display first item from new search results
            scrollToTopIfAvailable()
            searchStatusContainerView.isHidden = true
            foundBooksCollection.isHidden = true
            break
        case .resultsFound:
            setClearSearchContentsButton()
            hideLoading()
            searchStatusContainerView.isHidden = true
            foundBooksCollection.isHidden = false
            foundBooksCollection.reloadData()
            break
        case .noResultsFound:
            setClearSearchContentsButton()
            hideLoading()
            searchStatusImage.image = UIImage(named: "icon_no_search_results")
            searchStatusDescription.text = "No search results found"
            searchStatusContainerView.isHidden = false
            foundBooksCollection.isHidden = true
            break
        }
    }
    
    private func scrollToTopIfAvailable() {
        guard let viewModel = self.viewModel, viewModel.getBooksCount() > 0 else {
            return
        }
        foundBooksCollection.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
        foundBooksCollection.setContentOffset(CGPoint(x: 0, y: -SearchHeaderOffset), animated: false)
    }
    
    @IBAction internal func clearSearchField() {
        searchField.text = ""
        textFieldDidChange(searchField)
    }
    
    @IBAction private func deleteStoredSearchedBooks() {
        guard let viewModel = self.viewModel else {
            return
        }
        viewModel.removeStoredSearchedBooks()
        setSearchState(searchState: .searchInfo)
    }
    
    private func booksFound() {
        guard let viewModel = self.viewModel else {
            return
        }
        viewModel.getBooksCount() == 0 ? setSearchState(searchState: .noResultsFound) : setSearchState(searchState: .resultsFound)
    }
    
    private func findingBooksFailed() {
        
    }
    
    private func setClearSearchContentsButton() {
        searchContextButton.setImage(UIImage(named: "icon_cross"), for: UIControl.State.normal)
        searchContextButton.removeTarget(self, action: #selector(voiceSearchPressed), for: UIControl.Event.allEvents)
        searchContextButton.addTarget(self, action: #selector(clearSearchField), for: UIControl.Event.touchUpInside)
    }
    
    internal func setVoiceSearchButton() {
        searchContextButton.setImage(UIImage(named: "icon_voice_search"), for: UIControl.State.normal)
        searchContextButton.removeTarget(self, action: #selector(clearSearchField), for: UIControl.Event.allEvents)
        searchContextButton.addTarget(self, action: #selector(voiceSearchPressed), for: UIControl.Event.touchUpInside)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let viewModel = self.viewModel, let searchTerm = textField.text else {
            return
        }
        
        if searchTerm.isEmptyWithoutWhitespaces {
            viewModel.getRecentSearchesCount() == 0 ? setSearchState(searchState: .searchInfo) : setSearchState(searchState: .previousSearches)
        } else {
            setSearchState(searchState: .searching)
            viewModel.search(searchTerm: searchTerm)
        }
    }

    override internal func scrollToTop() {
        foundBooksCollection.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
}

extension StringProtocol where Index == String.Index {
    var isEmptyWithoutWhitespaces: Bool {
        return trimmingCharacters(in: .whitespaces) == ""
    }
}



