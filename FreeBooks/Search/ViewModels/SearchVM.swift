//
//  SearchVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 02/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class SearchVM: NSObject {
    
    private var foundBooks = [Book]()
    private var successCallback: (() -> ())!
    private var failureCallback: (() -> ())!
    
    private var displayingStoredBooks = false
    private let searchRequester = SearchRequester()
    
    init(successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    func fillWithStoredBooks() {
        displayingStoredBooks = true
        foundBooks.removeAll()
        self.foundBooks = RealmUtils.getStoredSearchedBooks()
    }
    
    func getRecentSearchesCount() -> Int {
        return RealmUtils.getStoredSearchedBooks().count
    }
    
    func removeStoredSearchedBooks() {
        RealmUtils.removeStoredSearchBooks()
    }
    
    func search(searchTerm: String, offset: Int = 0) {
        displayingStoredBooks = false 
        searchRequester.search(searchTerm: searchTerm, offset: offset, page: 0, completionHandler: {
            success, books in
            
            //Since this is async call, we may receive  responses even after we're already displaying stored searches, this can mess up with the results, we'll prevent it with by simple flag
            guard self.displayingStoredBooks == false else {
                return
            }
            
            if let foundBooks = books, success == true {
                //Starting from offset 0 means there's a new search term in place so we can replace the entire array with our findings, otherwise we append to the existing array
                if offset == 0 {
                    self.foundBooks = foundBooks
                } else {
                    self.foundBooks.append(contentsOf: foundBooks)
                }
                self.successCallback()
            } else {
                self.failureCallback()
            }
        })
    }
    
    func searchNext(searchTerm: String) {
        search(searchTerm: searchTerm, offset: foundBooks.count)
    }
    
    func getBook(index: Int) -> Book? {
        guard foundBooks.indices.contains(index) else {
            return nil
        }
        return foundBooks[index]
    }
    
    func getBooksCount() -> Int {
        return foundBooks.count
    }
}
