//
//  VoiceSearchManager.swift
//  Dohliadac
//
//  Created by Karol Grulling on 24/12/2018.
//  Copyright © 2018 Karol Grulling. All rights reserved.
//

import UIKit
import Speech

enum VoiceSearchPermissionStatus {
    case SpeechRecognizer_Microphone_NotAuthorized
    case SpeechRecognizer_NotAuthorized
    case Microphone_NotAuthorized
    case Ok
    case Unknown
}

class VoiceSearchManager: NSObject {
    static let sharedManager = VoiceSearchManager()
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-GB"))
    private let audioEngine = AVAudioEngine()
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    var successHandler: ((String) -> ())?
    var failureHandler: ((VoiceSearchPermissionStatus) -> ())?
    
    func recordAndRecognizeSpeech(onSuccess: @escaping (_ recognizedText:  String) -> Void, onFail: @escaping (_ failureReason: VoiceSearchPermissionStatus) -> Void) {
        stopRecognition()
        recognitionRequest = generateRecognitionRequest()
        successHandler = onSuccess
        failureHandler = onFail
        setupRecognizer()
        startRecognition()
        setupRecognitionTask()
    }
    
    func generateRecognitionRequest() -> SFSpeechAudioBufferRecognitionRequest {
        return SFSpeechAudioBufferRecognitionRequest()
    }
    
    func setupRecognizer() {
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat, block: { (buffer, _) in
            self.recognitionRequest?.append(buffer)
        })
    }
    
    func startRecognition() {
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            return print(error)
        }
    }
     
    @objc func stopRecognition() {
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
    }
    
    func resetRecognizer() {
        audioEngine.stop()
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        audioEngine.inputNode.removeTap(onBus: 0)
        setupRecognizer()
        startRecognition()
    }
    
    func isRecognizerAvailable() -> Bool {
        guard let recognizer = speechRecognizer else {
            return false
        }
        return recognizer.isAvailable
    }
    
    func isSpeechRecognizerAuthorized() -> Bool {
        return SFSpeechRecognizer.authorizationStatus() == .authorized || SFSpeechRecognizer.authorizationStatus() == .notDetermined
    }
    
    func isMicrophoneAccessEnabled() -> Bool {
        return AVAudioSession.sharedInstance().recordPermission == .granted || AVAudioSession.sharedInstance().recordPermission == .undetermined
    }
    
    func isVoiceSearchAuthorizedAndReady() -> Bool {
        return getVoiceSearchPermissionsStatus() == .Ok
    }
    
    func setupRecognitionTask() {
        let permissionStatus = getVoiceSearchPermissionsStatus()
        if permissionStatus == .Ok {
            recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest!, resultHandler: { (result, error) in
                if let result = result {
                    self.successHandler!(result.bestTranscription.formattedString)
                } else if let _ = error {
                    self.failureHandler!(.Unknown)
                }
            })
        } else {
            self.failureHandler!(permissionStatus)
        }
    }
    
    func getVoiceSearchPermissionsStatus() -> VoiceSearchPermissionStatus {
        if isSpeechRecognizerAuthorized() == false && isMicrophoneAccessEnabled() == false {
            return .SpeechRecognizer_Microphone_NotAuthorized
        } else {
            if isSpeechRecognizerAuthorized() == false {
                return .SpeechRecognizer_NotAuthorized
            } else if isMicrophoneAccessEnabled() == false {
                return .Microphone_NotAuthorized
            } else {
                return .Ok
            }
        }
    }
}
