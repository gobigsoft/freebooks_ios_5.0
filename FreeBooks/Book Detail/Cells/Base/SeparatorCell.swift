//
//  BaseBookDetailCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 31/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class SeparatorCell: BaseSeparatorCell {

    override func awakeFromNib() {
        addSeparator()
    }
}
