//
//  BookDetailMatchingAudiobookCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BookDetailMatchingBookCell: SeparatorCell {
    
    @IBOutlet weak private var matchingAudiobookDesc: UIView!
    @IBOutlet weak private var matchingBookTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        matchingAudiobookDesc.setRoundedCorners(radius: 2)
        matchingAudiobookDesc.layer.borderWidth = 1
        matchingAudiobookDesc.layer.borderColor = UIColor.init(white: 1, alpha: 0.2).cgColor
    }
}
