//
//  BookDetailAboutCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AboutCell: SeparatorCell {
    @IBOutlet weak private var about: UILabel!
    
    func initWithAbout(aboutText: String, isExpandable: Bool) {
        let attributedString = NSMutableAttributedString(string: aboutText)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        about.attributedText = attributedString
        
        if isExpandable {
            UIUtils.addBottomFadeOutGradient(view: about)
            removeSeparator()
        }
    }
}
