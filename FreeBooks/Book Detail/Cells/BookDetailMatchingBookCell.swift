//
//  BookDetailMatchingBookCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BookDetailMatchingBookCell: SeparatorCell {
    
    @IBOutlet weak private var matchingBookDescriptionContainer: UIView!
    @IBOutlet weak private var matchingBookTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        matchingBookDescriptionContainer.setRoundedCorners(radius: 2)
        matchingBookDescriptionContainer.layer.borderWidth = 1
        matchingBookDescriptionContainer.layer.borderColor = UIColor.init(white: 1, alpha: 0.2).cgColor
    }
    
    func initWithViewModel(viewModel: BookDetailVM) {
        matchingBookTitle.text = viewModel.isAudiobook() ? "Matching book also available" : "Matching audiobook also available"
    }
}
