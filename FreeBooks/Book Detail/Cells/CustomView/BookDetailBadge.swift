//
//  BookDetailBadge.swift
//  FreeBooks
//
//  Created by Karol Grulling on 31/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

@IBDesignable class BookDetailBadge: UIView {
    
    private let BookDetailBadgeNibName = "BookDetailBadge"
    private var view: UIView!

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var icon: UIImageView!

    @IBInspectable fileprivate var badgeText: String? {
        get {
            return label.text
        }
        set(badgeText) {
            label.text = badgeText
        }
    }

    @IBInspectable fileprivate var badgeIcon: UIImage? {
        get {
            return icon.image
        }
        set(badgeIcon) {
            icon.image = badgeIcon
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        UIUtils.tintImageView(imageView: icon, tintColor: Constants.Colors.Gold)
        view.layer.borderWidth = 0.5
        view.layer.borderColor = Constants.Colors.Gold.cgColor
        view.setRoundedCorners(radius: 5)
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: BookDetailBadgeNibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func setBoldFont() {
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
    }
}
