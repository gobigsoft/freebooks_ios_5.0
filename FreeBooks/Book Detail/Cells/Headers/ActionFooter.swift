//
//  BookDetailActionFooter.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class ActionFooter: SeparatorCell {
    @IBOutlet weak private var actionButton: UIButton!
    
    func initWithAction(title: String, target: Any?, action: Selector) {
        actionButton.setTitle(title, for: UIControl.State.normal)
        actionButton.addTarget(target, action: action, for: UIControl.Event.touchUpInside)
        //Make sure action button is always accessible
        self.bringSubviewToFront(actionButton)
    }
}
