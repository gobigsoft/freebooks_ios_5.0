//
//  ImageHeader.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class ImageHeader: UICollectionReusableView {
    @IBOutlet weak private var image: UIImageView!
    @IBOutlet weak private var backButtonTopConstraint: NSLayoutConstraint!
    
    func initWithImageUrl(imageUrl: String) {
        UIUtils.blurImageView(imageView: image)
        image.kf.setImage(with: URL(string: imageUrl), options: [.transition(.fade(0.2))])
        
        if DeviceUtils.isBezelessDevice() {
            backButtonTopConstraint.constant = backButtonTopConstraint.constant+10
        }
    }
}
