//
//  BookDetailAuthorCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AuthorCell: SeparatorCell {
    @IBOutlet weak private var authorImage: UIImageView!
    @IBOutlet weak private var authorName: UILabel!
    
    func initWithViewModel(viewModel: BookDetailVM) {
        setupContents(name: viewModel.getAuthorShortName(), imageUrl: viewModel.getAuthorImageUrl())
    }
    
    func initWithViewModel(viewModel: AuthorDetailVM) {
        setupContents(name: viewModel.getAuthorName(), imageUrl: viewModel.getAuthorImageUrl())
    }
    
    func initWithImageUrl(imageUrl: String, name: String) {
        removeSeparator()
        setupContents(name: name, imageUrl: imageUrl)
    }
    
    private func setupContents(name: String, imageUrl: String) {
        authorImage.setRoundedCorners(radius: authorImage.frame.height/2)
        if imageUrl.count > 0 {
            loadAuthorImage(imageUrl: imageUrl)
        }
        authorName.text = name
    }
    
    private func loadAuthorImage(imageUrl: String) {
        authorImage.contentMode = UIView.ContentMode.scaleAspectFit
        authorImage.kf.setImage(with: URL(string: imageUrl), options: [.transition(.fade(0.2))])
    }
}
