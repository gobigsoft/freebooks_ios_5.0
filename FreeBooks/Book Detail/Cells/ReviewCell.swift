//
//  ReviewCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class ReviewCell: BaseSeparatorCell {
    
    @IBOutlet weak private var author: UILabel!
    @IBOutlet weak private var review: UILabel?
    @IBOutlet weak private var contentStackView: UIStackView!
    @IBOutlet weak private var ratingStackView: UIStackView!
    @IBOutlet weak private var rating: UILabel!
    
    @IBOutlet private var maxWidthConstraint: NSLayoutConstraint! {
        didSet {
            maxWidthConstraint.isActive = false
        }
    }
    
    var maxWidth: CGFloat? = nil {
        didSet {
            guard let maxWidth = maxWidth else {
                return
            }
            maxWidthConstraint.isActive = true
            maxWidthConstraint.constant = maxWidth
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
    }
    
    func initWithViewModel(viewModel: ReviewCellVM, ratingTintColor: UIColor, emptyRatingTintColor: UIColor = Constants.Colors.Gold_10) {
        self.author.text = viewModel.getReviewAuthor()

        if let review = self.review {
            review.text = viewModel.getReviewText()
        }
        
        let reviewRating = viewModel.getRating()
        self.rating.text = reviewRating
        
        if reviewRating.count > 0 {
            if let ratingInt = Int(reviewRating) {
                for i in 0..<Constants.Misc.MaxRating {
                    if i<ratingInt {
                        UIUtils.tintImageView(imageView: ratingStackView.arrangedSubviews[i] as! UIImageView, tintColor: ratingTintColor)
                    } else {
                        UIUtils.tintImageView(imageView: ratingStackView.arrangedSubviews[i] as! UIImageView, tintColor: emptyRatingTintColor)
                    }
                }
            }
        }
    }
    
    func hideReviewsTitle() {
        contentStackView.arrangedSubviews[0].isHidden = true
    }
    
    func showReviewsTitle() {
        contentStackView.arrangedSubviews[0].isHidden = false
    }
}
