//
//  BookDetailInfoCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

enum BadgeType: Int {
    case Rating = 0
    case TopBook = 1
    case Downloads = 2
    case Audio = 3
}

enum DownloadButtonState {
    case Download
    case Downloading
    case Open
    case Play
    case Subscribe
}

class DownloadButton: UIButton {
    private var widthConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setWidthConstraint(constraint: NSLayoutConstraint) {
        self.widthConstraint = constraint
    }
    
    func setDownloadState(downloadState: DownloadButtonState, animate: Bool) {
        switch downloadState {
        case .Download:
            setTitle("DOWNLOAD", for: UIControl.State.normal)
            widthConstraint.constant = 80
        case .Downloading:
            setTitle("DOWNLOADING", for: UIControl.State.normal)
            widthConstraint.constant = 100
        case .Open:
            setTitle("OPEN", for: UIControl.State.normal)
            widthConstraint.constant = 60
        case .Play:
            setTitle("PLAY", for: UIControl.State.normal)
            widthConstraint.constant = 60
        case .Subscribe:
            setTitle("SUBSCRIBE", for: UIControl.State.normal)
            widthConstraint.constant = 100
        }
        
        if animate {
            UIView.animate(withDuration: 0.5, animations: { self.layoutIfNeeded() })
        }
    }
}

class BookDetailInfoCell: SeparatorCell {
    @IBOutlet weak private var title: UILabel!
    @IBOutlet weak private var authorName: UILabel!
    @IBOutlet weak private var coverButtonImage: UIButton!
    @IBOutlet weak private var badgesStackView: UIStackView!
    
    @IBOutlet weak private var downloadButton: DownloadButton!
    @IBOutlet weak private var downloadingStatusContainer: UIView!
    @IBOutlet weak private var progressBarContainer: UIView!
    @IBOutlet weak private var progressView: UIProgressView!
    @IBOutlet weak private var progress: UILabel!
    @IBOutlet weak private var estimatedSize: UILabel!
    
    @IBOutlet weak private var downloadButtonWidth: NSLayoutConstraint!
    
    private var viewModel: BookDetailVM?
    private var progressBar: LinearProgressBar?
    
    func initWithViewModel(viewModel: BookDetailVM) {
        self.viewModel = viewModel
        self.title.text = viewModel.getBookTitle()
        self.authorName.text = viewModel.getAuthorFullName()
        self.coverButtonImage.kf.setImage(with: URL(string: viewModel.getBookCoverImageUrl()), for: .normal, options: [.transition(.fade(0.2))])
        handleBadges(viewModel: viewModel)
        
        downloadButton.setRoundedCorners(radius: 2)
        UIUtils.dropButtonShadow(view: downloadButton)
        
        if viewModel.hasMatchingBook() {
            removeSeparator()
        }
        
        downloadButton.setWidthConstraint(constraint: downloadButtonWidth)
        
        self.progressView.transform = self.progressView.transform.scaledBy(x: 1, y: 2)
    }
    
    func showLoading() {
        //Loading state expands the cell, therefore it's separator position needs to be updated accordingly
        updateSeparator()
        downloadingStatusContainer.isHidden = false
        
        if progressBar == nil {
            progressBar = LinearProgressBar()
        }
        
        progressBar!.progressBarBackgroundColor = Constants.Colors.Gold_10
        progressBar!.progressBarColor = Constants.Colors.Gold
        progressBar!.startAnimating()
        progressBarContainer.addSubview(progressBar!)
    }
    
    func hideLoading() {
        //Hiding loading shrinks the cell, therefore it's separator position needs to be updated accordingly
        updateSeparator()
        downloadingStatusContainer.isHidden = true
        
        progressBar?.removeFromSuperview()
        progressBar = nil
        
        progressView.setProgress(0, animated: true)
    }
    
    func setDownloadButtonState(downloadState: DownloadButtonState, animate: Bool) {
        downloadButton.setDownloadState(downloadState: downloadState, animate: animate)
    }
    
    func removeDownloadButtonAction(target: Any) {
        downloadButton.removeTarget(target, action: nil, for: UIControl.Event.allEvents)
    }
    
    func setDownloadButtonAction(target: Any, selector: Selector) {
        downloadButton.addTarget(target, action: selector, for: UIControl.Event.touchUpInside)
    }
    
    func setProgress(progress: Int) {
        //Progress bar is our temporarily animated progress, therefore once we receive first real progress data we need to remove it and use regular progress view instead
        if progressBar != nil {
            progressBar?.removeFromSuperview()
            progressBar = nil
        }
        
        self.progress.text = "\(progress)%"
        self.progressView.progress = Float(Float(progress)/100)
    }
    
    func setTotalSize(size: Double) {
        if self.estimatedSize.isHidden == true {
            self.estimatedSize.isHidden = false
        }
        self.estimatedSize.text = "\(size) MB"
    }
    
    override internal func updateSeparator(color: UIColor = UIColor.init(white: 1, alpha: 0.05), offsetX: CGFloat = 0)  {
        //Cell doesn't have any separator in case when matching audiobook exists
        if let bookDetailVM = self.viewModel, !bookDetailVM.hasMatchingBook() {
            super.updateSeparator(color: color, offsetX: offsetX)
        }
    }
    
    func getCoverImage() -> UIImage? {
        return self.coverButtonImage.imageView?.image
    }
    
    private func handleBadges(viewModel: BookDetailVM) {
        (badgesStackView.arrangedSubviews[BadgeType.Rating.rawValue] as! BookDetailBadge).label.text = viewModel.getRating()
        
        if viewModel.isTop()  {
            (badgesStackView.arrangedSubviews[BadgeType.TopBook.rawValue] as! BookDetailBadge).setBoldFont()
        } else {
            badgesStackView.arrangedSubviews[BadgeType.TopBook.rawValue].isHidden = true
        }
        
        (badgesStackView.arrangedSubviews[BadgeType.Downloads.rawValue] as! BookDetailBadge).label.text = viewModel.getDownloads()
        
        if viewModel.getBookType() != BookType.Audiobook {
            badgesStackView.arrangedSubviews[BadgeType.Audio.rawValue].isHidden = true
        }
    }
}


