//
//  ReviewCellVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class ReviewCellVM: NSObject {
    
    private let review: Review
    
    init(review: Review) {
        self.review = review
    }
    
    func getReviewAuthor() -> String {
        return self.review.user ?? ""
    }
    
    func getRating() -> String {
        return self.review.rating ?? ""
    }

    func getReviewDate() -> String {
        return self.review.addedDate ?? ""
    }
    
    func getReviewText() -> String {
        return self.review.review ?? ""
    }
}
