//
//  BookReviewsVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 08/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BookReviewsVM: BaseBookVM {
    
    private var reviews = [Review]()
    
    private var successCallback: (() -> ())!
    private var failureCallback: (() -> ())!
    
    init(book: Book, successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        super.init(book: book)
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    func getBookReviews() {
        BookDetailRequester().getBookReviews(bookId: book.bookId, completionHandler: { success, reviews in
            if let bookReviews = reviews, success {
                self.reviews = bookReviews
                self.successCallback()
            } else {
                self.failureCallback()
            }
        })
    }
    
    func getReview(index: Int) -> Review {
        return self.reviews[index]
    }
    
    func getReviewCount() -> Int {
        return self.reviews.count
    }
    
    func isReviewEmpty(index: Int) -> Bool {
        guard let review = getReview(index: index).review else {
            return false
        }
        return review.isEmpty
    }
}
