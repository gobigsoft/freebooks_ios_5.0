//
//  MatchingAudiobookDetailVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 20/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class MatchingBookDetailVM: NSObject {
    
    private let bookId: Int
    
    private var successCallback: ((BookDetail) -> ())!
    private var failureCallback: (() -> ())!
    
    init(bookId: Int, successCallback: @escaping (BookDetail) -> (), failureCallback: @escaping () -> ()) {
        self.bookId = bookId
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    func getBookDetail() {
        BookDetailRequester().getBookDetail(bookId: bookId, completionHandler: { success, bookDetail in
            if success {
                self.successCallback(bookDetail!)
            } else {
                self.failureCallback()
            }
        })
    }
}
