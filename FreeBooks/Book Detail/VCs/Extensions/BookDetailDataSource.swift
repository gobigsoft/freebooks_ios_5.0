//
//  BookDetailDataSource.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit


internal enum BookDetailSections: String {
    case Info = "Info"
    case MatchingBook = "MatchingBook"
    case About = "About"
    case Reviews = "Reviews"
    case Author = "Author"
    case Related = "Related"
    
    static let allValues = [Info, MatchingBook, About, Reviews, Author, Related]
}

extension BookDetailVC : UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bookDetailSection = BookDetailSections.allValues[indexPath.section]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: bookDetailSection.rawValue, for: indexPath)
        
        guard let bookDetailViewModel = self.viewModel else {
            return cell
        }
        
        switch bookDetailSection {
        case .Info:
            if bookInfoCell == nil {
                bookInfoCell = cell as? BookDetailInfoCell
            }
            bookInfoCell?.initWithViewModel(viewModel: bookDetailViewModel)
            
            //We don't want to animate the download button at the first opening of the screen - which is the moment when cellForItem is called
            //we will animate it after the user's actions - as the screen state changes
            if state == .notDownloaded {
                self.setDownloadButtonStateToDownload(animate: false)
            } else if state == .subscriptionRequired {
                //If there's no active audiobooks subscription we set the button action to "SUBSCRIBE", applies only for audiobooks
                self.setDownloadButtonStateToSubscribe(animate: false)
            } else if state == .obtainedFromRealm {
                //If we have the book already stored in Realm, we need to set download button to "Open" or "Play", so we don't download it again
                bookDetailViewModel.isAudiobook() ? self.setDownloadButtonStateToPlay(animate: false) : self.setDownloadButtonStateToOpen(animate: false)
            }
    
            return bookInfoCell!
        case .MatchingBook:
            (cell as! BookDetailMatchingBookCell).initWithViewModel(viewModel: bookDetailViewModel)
            return cell
        case .About:
            (cell as! AboutCell).initWithAbout(aboutText: bookDetailViewModel.getAbout(), isExpandable: bookDetailViewModel.hasExpandableDescription())
            return cell
        case .Author:
            (cell as! AuthorCell).initWithViewModel(viewModel: bookDetailViewModel)
            return cell
        case .Reviews:
            if let review = bookDetailViewModel.getReview(index: indexPath.row) {
                (cell as! ReviewCell).initWithViewModel(viewModel: ReviewCellVM(review: review), ratingTintColor: Constants.Colors.Gold)
                indexPath.row == 0 ? (cell as! ReviewCell).showReviewsTitle() : (cell as! ReviewCell).hideReviewsTitle() 
            }
            return cell
        case .Related:
            (cell as! BaseBookSectionCell).initWithViewModel(viewModel: BaseBookSectionVM(books: bookDetailViewModel.getRelated()), delegate: self, bookCellIconsTintColor: Constants.Colors.Gold, bookCellLabelsColor: UIColor.white)
            return cell
        }
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return BookDetailSections.allValues.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch BookDetailSections.allValues[section] {
        case .Reviews:
            guard let reviewCount = viewModel?.getReviews().count else {
                return 0
            }
            return min(reviewCount, Constants.Misc.MaxReviewsInBookDetailsCount)
        default:
            return 1
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ImageHeader", for: indexPath) as! ImageHeader
            if let bookDetailViewModel = viewModel {
                headerView.initWithImageUrl(imageUrl: bookDetailViewModel.getBookCoverImageUrl())
            }
            return headerView
        case UICollectionView.elementKindSectionFooter:
            let sectionFooter = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "BookDetailActionFooter", for: indexPath) as! ActionFooter
            switch BookDetailSections.allValues[indexPath.section] {
            case .Reviews:
                sectionFooter.initWithAction(title: "VIEW ALL REVIEWS", target: self, action: #selector(seeAllReviewsPressed))
            case .About:
                sectionFooter.initWithAction(title: "SHOW MORE", target: self, action: #selector(showMoreAboutPressed))
            default:
                assert(false, "Invalid section type")
            }
            return sectionFooter
        default:
            return UICollectionReusableView()
        }
    }
}
