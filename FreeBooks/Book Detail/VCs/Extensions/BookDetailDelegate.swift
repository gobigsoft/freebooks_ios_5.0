//
//  BookDetailDelegate.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension BookDetailVC: UICollectionViewDelegate {
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch BookDetailSections.allValues[indexPath.section] {
        case .Author:
            guard let author = viewModel?.getAuthor() else {
                return
            }
            showAuthorDetail(author: author)
        default:
            break
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: heightForBookDetailIndexPath(indexPath: indexPath))
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let bookDetailSection = BookDetailSections.allValues[section]
        switch bookDetailSection {
        case .Info:
            return CGSize(width: collectionView.frame.size.width, height: CGFloat(168))
        default:
            return CGSize(width: collectionView.frame.size.width, height: CGFloat(0))
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let bookDetailSection = BookDetailSections.allValues[section]
        switch bookDetailSection {
        case .Reviews:
            if let bookDetailViewModel = viewModel, bookDetailViewModel.getReviews().count > Constants.Misc.MaxReviewsInBookDetailsCount {
                return CGSize(width: self.view.frame.size.width, height: CGFloat(60))
            } else {
                return CGSize(width: self.view.frame.size.width, height: CGFloat(0))
            }
        case .About:
            if let bookDetailViewModel = viewModel, bookDetailViewModel.hasExpandableDescription() {
                return CGSize(width: self.view.frame.size.width, height: CGFloat(60))
            } else {
                return CGSize(width: self.view.frame.size.width, height: CGFloat(0))
            }
        default:
            return CGSize(width: self.view.frame.size.width, height: CGFloat(0))
        }
    }
    
    private func heightForBookDetailIndexPath(indexPath: IndexPath) -> CGFloat {
        //Make sure viewmodel is available for determining contents of book detail screen
        guard let bookDetailViewModel = viewModel else {
            return 1
        }
        
        switch BookDetailSections.allValues[indexPath.section] {
        case .Info:
            return bookDetailViewModel.isDownloading() ? 245.0 : 184.0
        case .MatchingBook:
            return bookDetailViewModel.hasMatchingBook() ? 62.0 : 1
        case .About:
            return bookDetailViewModel.getAbout().count > 0 ? 220.0 : 1
        case .Author:
            return bookDetailViewModel.hasAuthorData() ? 130.0 : 1
        case .Reviews:
            //First row contains "Reviews" title
            return indexPath.row == 0 ? 117.0 : 80.0
        case .Related:
            return bookDetailViewModel.getRelated().count > 0 ? 255.0 : 1
        }
    }
}
