//
//  BookReviewsVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 08/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BookReviewsVC: BaseFakeNavigationBarVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let ReviewCellIdentifier = "ReviewCell"
    let ReviewCellEmptyIdentifier = "ReviewCellEmpty"
    
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout! {
        didSet {
            collectionLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    
    @IBOutlet private weak var bookReviewsCollection: UICollectionView!
    @IBOutlet private weak var rating: UILabel!
    private var viewModel: BookReviewsVM?
    
    func initWithBook(book: Book) {
        viewModel = BookReviewsVM(book: book, successCallback: bookReviewsObtained, failureCallback: obtainingBookReviewsFailed)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel?.getBookReviews()
        if let reviewViewModel = viewModel {
            rating.text = "RATING " + reviewViewModel.getRating()
        }
        showLoading()
        
        bookReviewsCollection.collectionViewLayout = collectionLayout
    }
    
    private func bookReviewsObtained() {
        hideLoading()
        self.bookReviewsCollection.delegate = self
        self.bookReviewsCollection.dataSource = self
    }
    
    private func obtainingBookReviewsFailed() {
        hideLoading()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var reviewCellIdentifier = ""
        if let bookReviewsVM = viewModel {
            reviewCellIdentifier = !bookReviewsVM.isReviewEmpty(index: indexPath.row) ? ReviewCellIdentifier : ReviewCellEmptyIdentifier
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reviewCellIdentifier, for: indexPath) as! ReviewCell
        if let bookReviewsViewModel = viewModel {
            cell.initWithViewModel(viewModel: ReviewCellVM(review: bookReviewsViewModel.getReview(index: indexPath.row)), ratingTintColor: Constants.Colors.ReddishBrown, emptyRatingTintColor: Constants.Colors.ReddishBrown_33)
            cell.maxWidth = collectionView.bounds.width
        }
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getReviewCount() ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let bookReviewsVM = viewModel, !bookReviewsVM.isReviewEmpty(index: indexPath.row) {
            return CGSize(width: collectionView.frame.size.width, height: 90)
        } else {
            return CGSize(width: collectionView.frame.size.width, height: 58)
        }
    }
}
