//
//  CoverDetailVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 23/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class CoverDetailVC: BaseVC, UIScrollViewDelegate {
    
    private var cover: UIImage?
    
    @IBOutlet weak var navigationBarContainer: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var coverImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let cover = self.cover {
            self.coverImage.image = cover
        }
        
        scrollView.delegate = self
        
        scrollView.zoomScale = 1
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 5
        
        setupGestureRecognizer()
        addBlackToTransparentGradient()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func initWithImage(image: UIImage) {
       self.cover = image
    }
    
    private func addBlackToTransparentGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.black.withAlphaComponent(1.0).cgColor,
                                UIColor.black.withAlphaComponent(0.0).cgColor]

        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: navigationBarContainer.frame.size.height)
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        navigationBarContainer.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    private func setupGestureRecognizer() {
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTapGest)
    }
    
    internal func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return coverImage
    }
    
    @objc private func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        scrollView.zoomScale == scrollView.maximumZoomScale ? scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true) : scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
    }
}
 
