//
//  BookDetailVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import EPUBKit
import RealmSwift

enum BookDetailState {
    case obtainedFromRealm
    case notDownloaded
    case subscriptionRequired
}

class BookDetailVC: BaseDetailVC, BookSectionSelectionDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet private weak var bookDetailCollection: UICollectionView!
    private var book: Book?
    
    internal var viewModel: BookDetailVM?
    internal var bookInfoCell: BookDetailInfoCell?
    internal var state: BookDetailState = .notDownloaded
    
    private var ePUB: EPUBDocument?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Put collectionview contents below status bar
        bookDetailCollection.contentInset.top = -UIApplication.shared.statusBarFrame.height
        showLoading(color: Constants.Colors.Gold)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupScreen()
    }
    
    func initWithBook(book: Book) {
        self.book = book
    }
    
    private func setupScreen() {
        guard let book = self.book else {
            return
        }
        
        //If the current book is an audiobook and audiobooks subscription isn't active yet we create the observer to handle eventual subscription activation
        if book.isAudiobook() && !UserDefaultsUtils.isAudiobooksSubscriptionActive() {
            NotificationCenter.default.addObserver(self, selector: #selector(handleAudiobooksSubscriptionPurchase), name: NSNotification.Name(rawValue: Constants.NotificationConstants.AudiobooksSubscriptionActive), object: nil)
        }
        
        if let storedBookDetail = RealmUtils.getStoredBookDetail(bookId: book.bookId) {
            viewModel = BookDetailVM(bookDetail: storedBookDetail, successCallback: bookDetailsObtained, failureCallback: obtainingBookDetailsFailed)
            self.state = .obtainedFromRealm
            if !storedBookDetail.isAudiobook() {
                self.ePUB = self.viewModel!.getStoredEPUB()
            }
            bookDetailsObtained()
        } else {
            self.state = .notDownloaded
            viewModel = BookDetailVM(book: book, successCallback: bookDetailsObtained, failureCallback: obtainingBookDetailsFailed)
            viewModel?.downloadBookDetail()
        }
    }
    
    //Used for matching audiobooks for which there isn't a book object yet
    func initWithBookId(bookId: Int) {
        //There's a separate VM for matching audiobooks, which is not a subclass of "Base Book VM", which requires book object for it's initialisation
        MatchingBookDetailVM(bookId: bookId, successCallback: matchingBookDetailsObtained, failureCallback: obtainingBookDetailsFailed).getBookDetail()
    }
    
    private func matchingBookDetailsObtained(bookDetail: BookDetail) {
        viewModel = BookDetailVM(bookDetail: bookDetail, successCallback: bookDetailsObtained, failureCallback: obtainingBookDetailsFailed)
        bookDetailsObtained()
    }
    
    private func bookDetailsObtained() {
        hideLoading()
        
        //We need to check the current subscription status and in case there isn't any active subscription we set the screen to "subscriptionRequired" state which disables audiobook playback
        if let book = self.book, book.isAudiobook() && !UserDefaultsUtils.isAudiobooksSubscriptionActive() {
            self.state = .subscriptionRequired
        }
        
        bookDetailCollection.dataSource = self
        bookDetailCollection.delegate = self
    }
    
    private func obtainingBookDetailsFailed() {
        hideLoading()
        
        showNoConnectionView()
    }
    
    @IBAction private func retryPressed() {
        hideNoConnectionView()
        showLoading()
        
        viewModel?.downloadBookDetail()
    }
    
    @objc internal func showMoreAboutPressed() {
        if let bookDetailViewModel = viewModel {
            showAbout(aboutTitle: bookDetailViewModel.getBookTitle(), about: bookDetailViewModel.getAbout())
        }
    }
    
    @objc internal func seeAllReviewsPressed() {
        if let bookDetailViewModel = viewModel {
            let bookReviewsVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookReviewsVC) as! BookReviewsVC
            bookReviewsVC.initWithBook(book: bookDetailViewModel.getBook())
            self.present(bookReviewsVC, animated: true, completion: nil)
        }
    }
    
    internal func bookSelected(book: Book) {
        showBookDetail(book: book)
    }
    
    @IBAction private func showMatchingBook() {
        if let matchingBookId = viewModel?.getMatchingBookId() {
            showBookDetail(bookId: matchingBookId)
        }
    }
    
    @IBAction private func sharePressed() {
        if let bookDetailViewModel = viewModel {
            shareBook(title: bookDetailViewModel.getBookTitle(), authorName: bookDetailViewModel.getAuthorFullName())
        }
    }
    
    //MARK: Book Download
    @IBAction private func downloadBook() {
        
        //Audiobook triggers mp3 download of the first chapter, Regular book downloads EPUB
        viewModel?.isAudiobook() ?? false ? viewModel?.downloadAudiobook(progressHandler: updateProgress, successCallback: audiobookDownloaded, failureCallback: downloadingBookFailed) : viewModel?.downloadBook(progressHandler: updateProgress, successCallback: bookDownloaded, failureCallback: downloadingBookFailed)
        
        bookDetailCollection.performBatchUpdates({ })
        self.bookInfoCell?.showLoading()
        self.bookInfoCell?.setDownloadButtonState(downloadState: .Downloading, animate: true)
    }
    
    private func stopCellLoading() {
        self.bookDetailCollection.performBatchUpdates({ })
        self.bookInfoCell?.hideLoading()
    }
    
    private func bookDownloaded(bookEPUB: EPUBDocument) {
        stopCellLoading()
        setDownloadButtonStateToOpen(animate: true)
        storeBookToRealm()
        
        self.ePUB = bookEPUB
    }
    
    internal func setDownloadButtonStateToOpen(animate: Bool = false) {
        self.bookInfoCell?.removeDownloadButtonAction(target: self)
        self.bookInfoCell?.setDownloadButtonAction(target: self, selector: #selector(openBook))
        self.bookInfoCell?.setDownloadButtonState(downloadState: .Open, animate: animate)
    }
    
    private func storeBookToRealm() {
        if let bookDetailViewModel = viewModel, let bookDetail = bookDetailViewModel.getBookDetail() {
            RealmUtils.storeBookDetail(bookDetail: bookDetail)
            if bookDetailViewModel.isAudiobook() {
                RealmUtils.setChapterAsDownloaded(chapterId: bookDetail.audiobookChapters[0].chapterId)
            }
        }
    }
    
    private func audiobookDownloaded() {
        stopCellLoading()
        setDownloadButtonStateToPlay(animate: true)
        storeBookToRealm()
    }
    
    internal func setDownloadButtonStateToPlay(animate: Bool = false) {
        self.bookInfoCell?.removeDownloadButtonAction(target: self)
        self.bookInfoCell?.setDownloadButtonAction(target: self, selector: #selector(playBook))
        self.bookInfoCell?.setDownloadButtonState(downloadState: .Play, animate: animate)
    }
    
    internal func setDownloadButtonStateToDownload(animate: Bool = false) {
        self.bookInfoCell?.removeDownloadButtonAction(target: self)
        self.bookInfoCell?.setDownloadButtonAction(target: self, selector: #selector(downloadBook))
        self.bookInfoCell?.setDownloadButtonState(downloadState: .Download, animate: animate)
    }
    
    internal func setDownloadButtonStateToSubscribe(animate: Bool = false) {
        self.bookInfoCell?.removeDownloadButtonAction(target: self)
        self.bookInfoCell?.setDownloadButtonAction(target: self, selector: #selector(subscribe))
        self.bookInfoCell?.setDownloadButtonState(downloadState: .Subscribe, animate: animate)
    }
    
    private func downloadingBookFailed() {
        stopCellLoading()
        self.bookInfoCell?.setDownloadButtonState(downloadState: .Download, animate: true)
    }
    
    @IBAction private func cancelDownload() {
        viewModel?.stopDownloadingBook()
        bookDetailCollection.performBatchUpdates({ })
        self.bookInfoCell?.hideLoading()
        
        self.bookInfoCell?.setDownloadButtonState(downloadState: .Download, animate: true)
    }
    
    private func updateProgress(progress: Int, estimatedSize: Double) {
        self.bookInfoCell?.setProgress(progress: progress)
        
        //Audiobook download should display also the estimated size of a chapter
        if self.viewModel?.isAudiobook() ?? false {
            self.bookInfoCell?.setTotalSize(size: estimatedSize)
        }
    }
    
    @objc internal func openBook() {
        if let bookDetailViewModel = viewModel, let ePUB = self.ePUB, let bookDetail = bookDetailViewModel.getBookDetail() {
            let readerVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.ReaderVC) as! ReaderVC
            
            //Load stored style
            ReaderStyleProvider.sharedProvider.loadStyleFromRealm()
            
            //Init engine
            let readerEngine = ReaderEngine(bookDetail: bookDetail, ePUB: ePUB)
            
            //If we have already cached page counts for current text size, let's setup engine with them so we skip page counts calculation
            if let cache = readerEngine.getCache(textSize: ReaderStyleProvider.sharedProvider.getCurrentTextSize()) {
                readerEngine.setupWithPageCounts(pageCounts: cache.getPageCounts(), tableOfContentsIndexItems: cache.getTableOfContentsIndexItems())
            }
            
            readerVC.initWithReaderEngine(readerEngine: readerEngine)

            readerVC.interstitialAdDelegate = self
            
            self.present(readerVC, animated: true, completion: nil)
        }
    }
    
    @objc internal func playBook() {
        if let bookDetail = viewModel?.getBookDetail() {
            openPlayer(bookDetail: bookDetail)
            InterstitialAdProvider.sharedProvider.showInterstitial(inVC: self)
        }
    }
    
    @objc private func subscribe() {
        let subscribeToAudiobooksAction = UIAlertAction.init(title: "Subscribe", style: UIAlertAction.Style.default, handler: { action in
            PurchaseEngine.sharedEngine.purchaseSubscription()
        })
        
        let termsOfUseAction = UIAlertAction.init(title: "Terms of Use", style: UIAlertAction.Style.default, handler: { action in
            guard let url = URL(string: Constants.AboutApp.URLs.WebTermsOfUse) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
        
        let privacyPolicyAction = UIAlertAction.init(title: "Privacy Policy", style: UIAlertAction.Style.default, handler: { action in
            guard let url = URL(string: Constants.AboutApp.URLs.WebPrivacyPolicy) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
        
        AlertUtils.showAlert(title: "Subscribe to audiobooks", message: Constants.Misc.SubscriptionInfoText, actions: [subscribeToAudiobooksAction, termsOfUseAction, privacyPolicyAction], cancelAction: UIAlertAction(title: "Cancel", style: .cancel, handler: nil), inVC: self)
    }
    
    @objc private func handleAudiobooksSubscriptionPurchase() {
        guard let book = self.book else {
            return
        }
        
        if let _ = RealmUtils.getStoredBookDetail(bookId: book.bookId) {
            self.state = .obtainedFromRealm
            setDownloadButtonStateToPlay(animate: true)
        } else {
            self.state = .notDownloaded
            setDownloadButtonStateToDownload(animate: true)
        }
    }
    
    @IBAction private func coverPressed() {
        if let bookInfoCell = self.bookInfoCell, let coverImage = bookInfoCell.getCoverImage() {
            let coverDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.CoverDetailVC) as! CoverDetailVC
            coverDetailVC.initWithImage(image: coverImage)
            self.present(coverDetailVC, animated: true, completion: nil)
        }
    }
}

extension BookDetailVC: ReaderInterstitialAdDelegate {
    internal func readerClosed() {
        InterstitialAdProvider.sharedProvider.showInterstitial(inVC: self)
    }
}
