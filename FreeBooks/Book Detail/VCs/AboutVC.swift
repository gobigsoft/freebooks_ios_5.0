//
//  AboutVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 05/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AboutVC: BaseFakeNavigationBarVC {
    
    @IBOutlet private weak var aboutText: UITextView!
    
    private var aboutTitle: String?
    private var about: String?
    
    func initWithContents(aboutTitle: String, about: String) {
        self.aboutTitle = aboutTitle
        self.about = about
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenTitle.text = self.aboutTitle
        aboutText.text = self.about
    }
}
