//
//  PlayerAnimator.swift
//  FreeBooks
//
//  Created by Karol Grulling on 23/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

enum PlayerAnimationState {
    case closed
    case open
}

extension PlayerAnimationState {
    var opposite: PlayerAnimationState {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

class InstantPanGestureRecognizer: UIPanGestureRecognizer {
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
//        if (self.state == UIGestureRecognizer.State.began) { return }
//        super.touchesBegan(touches, with: event)
//        self.state = UIGestureRecognizer.State.began
//    }
}

protocol PlayerAnimatorDelegate {
    func playerOpened()
    func playerClosed()
}

class PlayerAnimator: NSObject {
    
    private let playerContainer: UIView
    private let parentView: UIView
    private let playerMiniature: UIView
    private var tabBar: UITabBar?

    private let offset: CGFloat
    
    /// The progress of each animator. This array is parallel to the `runningAnimators` array.
    private var runningAnimators = [UIViewPropertyAnimator]()
    private var animationProgress = [CGFloat]()
    
    private var currentState: PlayerAnimationState = .closed
    
    var delegate: PlayerAnimatorDelegate?
    
    init(playerContainer: UIView, parentView: UIView, playerMiniature: UIView, offset: CGFloat, tabBar: UITabBar?) {
        self.playerContainer = playerContainer
        self.parentView = parentView
        self.playerMiniature = playerMiniature
        self.offset = offset
        self.tabBar = tabBar
        super.init()
    }
    
    func getCurrentState() -> PlayerAnimationState {
        return currentState
    }
    
    func animateTransitionIfNeeded(to state: PlayerAnimationState, duration: TimeInterval) {
        let state = currentState.opposite
        
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.playerContainer.frame.origin.y = 0
                if let tabBar = self.tabBar {
                    tabBar.frame.origin.y = self.parentView.frame.size.height + tabBar.frame.size.height
                }
                self.playerMiniature.alpha = 0
            case .closed:
                self.playerContainer.frame.origin.y = self.parentView.frame.size.height-self.offset
                if let tabBar = self.tabBar {
                    tabBar.frame.origin.y = self.parentView.frame.size.height - tabBar.frame.size.height
                }
                self.playerMiniature.alpha = 1
            }
            self.parentView.layoutIfNeeded()
        })
        
        transitionAnimator.addCompletion { position in
            
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
                //Animation has ended, user can freely interact with view
                self.playerContainer.isUserInteractionEnabled = true
                
                if self.currentState == .closed {
                    self.delegate?.playerClosed()
                } else {
                    self.delegate?.playerOpened()
                }
            case .current:
                ()
            }
            switch self.currentState {
            case .open:
                if let tabBar = self.tabBar {
                    tabBar.frame.origin.y = self.parentView.frame.size.height + tabBar.frame.size.height
                }
                self.playerContainer.frame.origin.y = 0
            case .closed:
                if let tabBar = self.tabBar {
                    tabBar.frame.origin.y = self.parentView.frame.size.height - tabBar.frame.size.height
                }
                self.playerContainer.frame.origin.y = self.parentView.frame.size.height-self.offset
            }
            self.runningAnimators.removeAll()
        }
        
        transitionAnimator.startAnimation()
        runningAnimators.append(transitionAnimator)
    }
    
    @objc func playerViewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            // start the animations
            animateTransitionIfNeeded(to: currentState.opposite, duration: Constants.Misc.PlayerDisplayAnimationDuration)
            
            // pause all animations, since the next event may be a pan changed
            runningAnimators.forEach { $0.pauseAnimation() }
            
            // keep track of each animator's progress
            animationProgress = runningAnimators.map { $0.fractionComplete }
        case .changed:
            // variable setup
            let translation = recognizer.translation(in: playerContainer)
            var fraction = -translation.y / (parentView.frame.size.height-offset)

            // adjust the fraction for the current state and reversed state
            if currentState == .open { fraction *= -1 }
            if runningAnimators.count > 0 && runningAnimators[0].isReversed { fraction *= -1 }

            // apply the new fraction
            for (index, animator) in runningAnimators.enumerated() {
                animator.fractionComplete = fraction + animationProgress[index]
            }
            
        case .ended:
            // variable setup
            let yVelocity = recognizer.velocity(in: playerContainer).y
            let shouldClose = yVelocity > 0
            
            // if there is no motion, continue all animations and exit early
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break
            }
        
            // reverse the animations based on their current state and pan motion
            if runningAnimators.count > 0 {
                switch currentState {
                case .open:
                    if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                    if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                case .closed:
                    if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                    if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                }
                
                //We don't want to allow user to interact with the view until the animation ends
                //Recognizer states keep changing even with slight motions, we need to make sure interaction is disabled only when we're really closing/opening
                if (currentState == .open && shouldClose) || (currentState == .closed && !shouldClose) {
                    self.playerContainer.isUserInteractionEnabled = false
                }
            }
            
            // continue all animations
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
        default:
            ()
        }
    }
    
    func hide() {
        let hideAnimator = UIViewPropertyAnimator(duration: 0.5, curve: .easeIn) {
            //On some iPhones safe area constraint messes up with total frame height, adding status bar height" makes sure we always end up beneath the visible area
            self.playerContainer.frame.origin.y = self.playerContainer.frame.size.height+UIApplication.shared.statusBarFrame.height
            self.currentState = .closed
        }
        hideAnimator.startAnimation()
    }
    
    func showTabBarIfRequired() {
        if let tabBar = self.tabBar {
            let tabBarPosition = self.parentView.frame.size.height - tabBar.frame.size.height
            if tabBar.frame.origin.y != tabBarPosition {
                tabBar.frame.origin.y = self.parentView.frame.size.height - tabBar.frame.size.height
            }
        }
    }
}
