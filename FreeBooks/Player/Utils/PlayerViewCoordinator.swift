//
//  PlayerViewCoordinator.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class PlayerViewCoordinator: NSObject, UIGestureRecognizerDelegate, PlayerVCDelegate {
    
    private lazy var PlayerOffset: CGFloat = {
        var offset: CGFloat
        if DeviceUtils.DeviceType.IS_IPHONE_XR || DeviceUtils.DeviceType.IS_IPHONE_XS_MAX || DeviceUtils.DeviceType.IS_IPHONE_X {
            offset = 135
        } else if DeviceUtils.DeviceType.IS_IPHONE_6_7 || DeviceUtils.DeviceType.IS_IPHONE_5 {
            offset = 100
        } else {
            offset = 95
        }
        
        return offset
    }()
    
    let parentVC: UIViewController
    let playerContainer: UIView
    let playerVC: PlayerVC

    var tabBar: UITabBar?
    var playerAnimator: PlayerAnimator?
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(playerPanned(recognizer:)))
        recognizer.delegate = self
        return recognizer
    }()
    
    init(playerContainer: UIView, parentVC: UIViewController, tabBar: UITabBar? = nil) {
        self.playerContainer = playerContainer
        self.parentVC = parentVC
        self.tabBar = tabBar
        self.playerVC = UIStoryboard.init(name: Constants.StoryboardConstants.StoryboardMain, bundle: nil).instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.PlayerVC) as! PlayerVC
        super.init()

        playerVC.delegate = self
        initPlayerView()
    }
    
    func showPlayerMiniature() {
        playerVC.setup()
        playerVC.setMiniatureVisible()
    }
    
    func hidePlayer() {
        playerAnimator?.hide()
    }
    
    func showPlayer(bookDetail: BookDetail) {
        playerAnimator?.animateTransitionIfNeeded(to: .open, duration: Constants.Misc.PlayerDisplayAnimationDuration)
        
        PlayerEngine.sharedEngine.setupWithAudiobookDetail(audiobookDetail: bookDetail)
        PlayerEngine.sharedEngine.continuePlayback()
    }
    
    
    private func initPlayerView() {
        playerContainer.isOpaque = true
        
        //Add gesture recognizer
        playerContainer.addGestureRecognizer(panRecognizer)
        
        //Put PlayerVC its view into container
        playerContainer.addSubview(playerVC.view)
        
        //Add container with Player into parent view
        parentVC.view.addSubview(playerContainer)
        
        //Make sure tab bar is visible
        if let tabBar = self.tabBar {
            parentVC.view.bringSubviewToFront(tabBar)
        }
        
        playerAnimator = PlayerAnimator(playerContainer: self.playerContainer, parentView: parentVC.view, playerMiniature: self.playerVC.playerMiniature, offset: PlayerOffset, tabBar: self.tabBar)
    }

    @objc private func playerPanned(recognizer: UIPanGestureRecognizer) {
        playerAnimator?.playerViewPanned(recognizer: recognizer)
    }
    
    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIControl {
            return false
        }
        return true
    }
    
    internal func showChaptersPressed() {
        if let audiobookDetail = PlayerEngine.sharedEngine.getAudiobookDetail() {
            let audiobookChaptersVC = parentVC.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.AudiobookChaptersVC) as! AudiobookChaptersVC
            audiobookChaptersVC.initWithChapters(audiobookChapters: audiobookDetail.audiobookChapters, audiobookDetail: audiobookDetail)
            parentVC.present(audiobookChaptersVC, animated: true, completion: nil)
        }
    }
    
    internal func showAudiobookDetailPressed() {
        if let audiobookDetail = PlayerEngine.sharedEngine.getAudiobookDetail() {
            let bookDetailVC = parentVC.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookDetailVC) as! BookDetailVC
            bookDetailVC.initWithBook(book: audiobookDetail)
            parentVC.present(bookDetailVC, animated: true, completion: nil)
        }
    }
    
    internal func cancelPlayerPressed() {
         NotificationCenter.default.post(name: NSNotification.Name(Constants.NotificationConstants.PlayerCancelled), object: nil)
    }
    
    func hidePlayerPressed() {
        playerAnimator?.animateTransitionIfNeeded(to: .closed, duration: Constants.Misc.PlayerDisplayAnimationDuration)
    }
}
