//
//  ChapterDownloader.swift
//  FreeBooks
//
//  Created by Karol Grulling on 25/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol ChapterDownloaderDelegate {
    func chapterDownloadUpdated(percentageProgress: Int)
    func chapterDownloaded()
    func chapterDownloadFailed()
}

class ChapterDownloader: NSObject {
    
    let chapter: AudiobookChapter
    let bookId: Int
    
    var delegate: ChapterDownloaderDelegate?
    let audiobookContentRequester = AudiobookContentRequester()
    
    init(bookId: Int, chapter: AudiobookChapter) {
        self.bookId = bookId
        self.chapter = chapter
    }
    
    func downloadChapter() {
        LoggingUtils.logMessage(message: "Downloading chapter \(self.chapter.chapterId)")
        
        audiobookContentRequester.downloadChapter(chapterId: self.chapter.chapterId, progressHandler: { progress, estimatedSize in
            let percentageProgress = Int(progress*100)
            self.delegate?.chapterDownloadUpdated(percentageProgress: percentageProgress)
        }, completionHandler: { success, responseData in
            if let returnedChapterData = responseData, success {
                PlayerUtils.storeChapter(chapterData: returnedChapterData, chapterId: self.chapter.chapterId, bookId: self.bookId)
                PlayerEngine.sharedEngine.setChapterAsDownloaded(chapterId: self.chapter.chapterId)
        
                LoggingUtils.logMessage(message: "Chapter \(self.chapter.chapterId) downloaded")
                self.delegate?.chapterDownloaded()
            } else {
                self.delegate?.chapterDownloadFailed()
            }
        })
    }
    
    func stopDownloading() {
        audiobookContentRequester.cancelDownload()
    }
}
