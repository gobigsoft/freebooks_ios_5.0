//
//  PlayerUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class PlayerUtils: NSObject {
    
    public static func storeChapter(chapterData: Data, chapterId: Int, bookId: Int) {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let bookURL = documentsURL.appendingPathComponent(String(describing: bookId), isDirectory: true)
        let chapterUrl = bookURL.appendingPathComponent(String(describing: chapterId)).appendingPathExtension("mp3")
        do {
            try FileManager.default.createDirectory(at: bookURL, withIntermediateDirectories: true, attributes: nil)
            try chapterData.write(to: chapterUrl)
        } catch {
            print("Couldn't store chapter \(chapterId) for book \(bookId).")
        }
    }
    
    public static func getURLForChapter(bookId: Int, chapterId: Int) -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let bookURL = documentsURL.appendingPathComponent(String(describing: bookId), isDirectory: true)
        let chapterUrl = bookURL.appendingPathComponent(String(describing: chapterId)).appendingPathExtension("mp3")
        return chapterUrl
    }
    
    public static func getPlaybackTimeString(seconds: Double) -> String {
        let secs = Int(seconds)
        let hours = secs / 3600
        let minutes = ((secs % 3600) / 60) + (hours*60)
        let seconds = (secs % 3600) % 60
        
        let minutesString = minutes < 10 ? "0\(minutes)" : "\(minutes)"
        let secondsString = seconds < 10 ? "0\(seconds)" : "\(seconds)"
        
        return "\(minutesString):\(secondsString)"
    }
}
