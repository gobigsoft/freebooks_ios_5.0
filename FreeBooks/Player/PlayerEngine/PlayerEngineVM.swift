//
//  PlayerEngineVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension PlayerEngine {
    func getBookTitle() -> String {
        return self.audiobookDetail?.title ?? ""
    }
    
    func getBookCover() -> String {
        return self.audiobookDetail?.coverUrl ?? ""
    }
    
    func getCurrentChapterLabel() -> String {
        guard let audiobookDetail = self.audiobookDetail else {
            return ""
        }
        
        let authorName = (audiobookDetail.authorsFull.count > 0) ? Utils.getAuthorLongNames(authors: audiobookDetail.authorsFull) : Utils.getAuthorLongNames(book: audiobookDetail)
        return "Chapter \(currentChapterIndex+1)/\(audiobookDetail.audiobookChapters.count) · \(authorName)"
    }
    
    func getAudiobookDetail() -> BookDetail? {
        return self.audiobookDetail
    }
    
    func getCurrentChapterIndex() -> Int {
        return self.currentChapterIndex
    }
    
    func getChapterCount() -> Int {
        return self.audiobookDetail?.audiobookChapters.count ?? 0
    }
}
