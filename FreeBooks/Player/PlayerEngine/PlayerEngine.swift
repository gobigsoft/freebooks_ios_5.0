//
//  PlayerEngine.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import AVFoundation

enum PlayerState {
    case playing
    case paused
    case closed
}

final class PlayerEngine: NSObject {
    static let sharedEngine = PlayerEngine()
    
    let SeekStepSeconds = Double(10)
    internal var audiobookDetail: BookDetail?
    internal var currentChapterIndex: Int = 0
    internal var chapterDownloaders = [ChapterDownloader]()
    
    internal var currentPlaybackSpeed: Float = 1.0
    
    internal var coverImage: UIImage?
    
    internal lazy var player: AVPlayer = {
        let player = AVPlayer()
        self.setupRemoteTransportControls()
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        return player
    }()
        
    private var timeObserverToken: Any?
    internal var currentPlaybackProgressSeconds: Double = 0
    private var state: PlayerState = .closed
    
    func setupWithAudiobookDetail(audiobookDetail: BookDetail, startingState: PlayerState = .playing) {
        var newAudiobookSelected = false
        //If there's a new audiobook selected, notify all of the Player VCs to update their contents
        if let currentAudiobook = self.audiobookDetail, currentAudiobook.bookId != audiobookDetail.bookId {
            newAudiobookSelected = true
        } else if self.audiobookDetail == nil {
            newAudiobookSelected = true
        }
        
        //Setting up a player engine means it's going to play immediately
        self.audiobookDetail = audiobookDetail
        setState(state: startingState)
        
        if newAudiobookSelected {
            sendBookChangedNotification()
        }
    }
    
    //Function for recovering user's last audio session in the app after terminating. We're storing last audiobook id when the app goes to background. Recover method is called when Tab Bar is loaded - so after the app's startup. We'll retrieve audiobook detail, continue playback and set the current progress values which are then picked by PlayerVC in its setup() method
    func recoverFromBookId(bookId: Int) {
        if let book = RealmUtils.getStoredBookDetail(bookId: bookId) {
            setupWithAudiobookDetail(audiobookDetail: book, startingState: .paused)
            continuePlayback()
            setCurrentProgressSeconds()
        }
    }
    
    func continuePlayback() {
        if let audiobookDetail = self.audiobookDetail {
            setChapter(chapterIndex: audiobookDetail.currentAudiobookChapter ?? 0)
            seekPlayerTo(progress: Double(audiobookDetail.progress))
        }
    }
    
    private func sendBookChangedNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(Constants.NotificationConstants.AudiobookChanged), object: nil)
    }
    
    private func sendAudiobookProgressUpdatedNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(Constants.NotificationConstants.AudiobookProgressUpdated), object: nil)
    }
    
    func setChapter(chapterIndex: Int) {
        guard let chapter = getChapterForIndex(index: chapterIndex), chapter.isDownloaded, let audiobookDetail = self.audiobookDetail else {
            return
        }
        currentChapterIndex = chapterIndex
        downloadNextChapterIfRequired()
        sendBookChangedNotification()
        
        setNewContent(url: PlayerUtils.getURLForChapter(bookId: audiobookDetail.bookId, chapterId: chapter.chapterId))
        
        //If we're in state of "paused" we just set a new content and don't start playback immediately - we wait for user to press PLAY
        if getState() == .playing {
            startPlaying()
        }
    }
    
    internal func getChapterForIndex(index: Int) -> AudiobookChapter? {
        guard let audiobookDetail = self.audiobookDetail else {
            return nil
        }
        guard audiobookDetail.audiobookChapters.indices.contains(index) else {
            return nil
        }
        
        return audiobookDetail.audiobookChapters[index]
    }
    
    private func setNewContent(url: URL) {
        player.replaceCurrentItem(with: AVPlayerItem(url: url))
    }
    
    @objc private func playerDidReachEnd() {
        playNextChapter()
    }
    
    func getState() -> PlayerState {
        return state
    }
    
    func setState(state: PlayerState) {
        self.state = state
    }
    
    internal func addPeriodicTimeObserver() {
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 1, preferredTimescale: timeScale)
        timeObserverToken = player.addPeriodicTimeObserver(forInterval: time,
                                                           queue: .main) {
                                                            time in
                                                            self.currentPlaybackProgressSeconds = time.seconds
                                                            self.sendAudiobookProgressUpdatedNotification()
                                                            //We need to update the progress to Realm so user can continue listening when he gets back into the app
                                                            self.setCurrentListeningProgress()
                                                            
                                                            self.setupNowPlayingInfo()
        }
    }
    
    private func setCurrentListeningProgress() {
        if let audiobookDetail = self.audiobookDetail {
            audiobookDetail.progress = getCurrentProgress()
            RealmUtils.updateProgress(progress: getCurrentProgress(), bookId: audiobookDetail.bookId, currentAudiobookChapter: self.getCurrentChapterIndex())
        }
    }
    
    internal func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            player.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }
}
