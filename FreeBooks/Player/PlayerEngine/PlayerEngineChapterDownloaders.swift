//
//  PlayerEngineChapterHandlers.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension PlayerEngine {
    internal func downloadNextChapterIfRequired() {
        if let audiobookDetail = self.audiobookDetail, let chapter = getChapterForIndex(index: currentChapterIndex+1), chapter.isDownloaded == false {
            let chapterDownloader = ChapterDownloader(bookId: audiobookDetail.bookId, chapter: chapter)
            chapterDownloader.downloadChapter()
            self.chapterDownloaders.append(chapterDownloader)
        }
    }
    
    func getChapterDownloader(chapterId: Int) -> ChapterDownloader? {
        return chapterDownloaders.filter({ $0.chapter.chapterId == chapterId }).first
    }
    
    func getChapterDownloader(bookId: Int) -> ChapterDownloader? {
        return chapterDownloaders.filter({ $0.bookId == bookId }).first
    }
    
    internal func removeChapterDownloader(chapterId: Int) {
        if let chapterDownloaderIndex = chapterDownloaders.firstIndex(where: { $0.chapter.chapterId == chapterId }) {
            chapterDownloaders.remove(at: chapterDownloaderIndex)
        }
    }
    
    func setChapterAsDownloaded(chapterId: Int) {
        removeChapterDownloader(chapterId: chapterId)
        RealmUtils.setChapterAsDownloaded(chapterId: chapterId)
        guard let audiobookDetail = self.audiobookDetail else {
            return
        }
        
        audiobookDetail.audiobookChapters.filter({ $0.chapterId == chapterId }).first?.isDownloaded = true
    }
}
