//
//  MediaPlayerUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import MediaPlayer


extension PlayerEngine {
    
    internal func disableCommandCenterNext() {
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.nextTrackCommand.isEnabled = false
    }
    
    internal func enableCommandCenterNext() {
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.nextTrackCommand.isEnabled = true
    }
    
    internal func disableCommandCenterPrevious() {
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.previousTrackCommand.isEnabled = false
    }
    
    internal func enableCommandCenterPrevious() {
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.previousTrackCommand.isEnabled = true
    }
    
    internal func setupRemoteTransportControls() {
        let commandCenter = MPRemoteCommandCenter.shared()
        
        commandCenter.playCommand.addTarget { [unowned self] event in
            self.resumePlayback()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotificationConstants.PlaybackStateChangedFromCommandCenter), object: nil)
            return .success
        }
        
        commandCenter.pauseCommand.addTarget { [unowned self] event in
            self.pausePlayback()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotificationConstants.PlaybackStateChangedFromCommandCenter), object: nil)
            return .success
        }
        
        commandCenter.nextTrackCommand.addTarget { [unowned self] event in
            self.playNextChapter()
            return .success
        }
        
        commandCenter.previousTrackCommand.addTarget(handler: { [unowned self] event in
            self.playPreviousChapter()
            return .success
        })
        
        commandCenter.changePlaybackPositionCommand.isEnabled = true
        commandCenter.changePlaybackPositionCommand.addTarget { [unowned self] event in
            
            if let positionEvent = event as? MPChangePlaybackPositionCommandEvent {
                self.seekPlayerTo(seconds: positionEvent.positionTime)
                return MPRemoteCommandHandlerStatus.success;
            }
            
            return MPRemoteCommandHandlerStatus.commandFailed;
        }
        UIApplication.shared.beginReceivingRemoteControlEvents()
    }
    
    func setupNowPlayingInfo() {
        var nowPlayingInfo = [String : Any]()
        nowPlayingInfo[MPMediaItemPropertyTitle] = self.getBookTitle()
 
        if let image = self.coverImage {
            nowPlayingInfo[MPMediaItemPropertyArtwork] =
                MPMediaItemArtwork(boundsSize: image.size) { size in
                    return image
            }
        }
        
        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = player.currentTime().seconds
        nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = player.currentItem?.duration.seconds
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = player.rate
        nowPlayingInfo[MPNowPlayingInfoPropertyChapterCount] = getChapterCount()
        nowPlayingInfo[MPNowPlayingInfoPropertyChapterNumber] = getCurrentChapterIndex()
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    func setCoverImage(image: UIImage) {
        self.coverImage = image 
    }
}
