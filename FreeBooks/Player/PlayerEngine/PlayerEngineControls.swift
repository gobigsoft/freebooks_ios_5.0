//
//  PlayerEngineControls.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

extension PlayerEngine {
    func startPlaying() {        
        //Since there's only a single AVAudioSession per app available and we need to switch between playback and recording (voice search) we need to make sure there's a proper category selected
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, policy: .default, options: .defaultToSpeaker)
        
        player.play()
        setState(state: .playing)
        
        removePeriodicTimeObserver()
        addPeriodicTimeObserver()
    }
    
    func resumePlayback() {
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, policy: .default, options: .defaultToSpeaker)
        
        player.play()
        player.rate = currentPlaybackSpeed
        setState(state: .playing)
        
        removePeriodicTimeObserver()
        addPeriodicTimeObserver()
    }
    
    func pausePlayback() {
        player.pause()
        setState(state: .paused)
    }
    
    func stopPlayback() {
        player.pause()
        player.replaceCurrentItem(with: nil)
        removePeriodicTimeObserver()
        setState(state: .closed)
    }
    
    func playNextChapter() {
        setChapter(chapterIndex: currentChapterIndex+1)
    }
    
    func playPreviousChapter() {
        setChapter(chapterIndex: currentChapterIndex-1)
    }
    
    func playChapter(index: Int) {
        setChapter(chapterIndex: index)
    }
    
    func seekForward() {
        if getRemainingTimeSeconds() > SeekStepSeconds {
            seekPlayerTo(seconds: getElapsedTimeSeconds()+SeekStepSeconds)
        }
    }
    
    func seekBackwards() {
        if getElapsedTimeSeconds() > SeekStepSeconds {
            seekPlayerTo(seconds: getElapsedTimeSeconds()-SeekStepSeconds)
        }
    }
    
    func seekPlayerTo(seconds: Double) {
        player.seek(to: CMTimeMakeWithSeconds(seconds, preferredTimescale: CMTimeScale(NSEC_PER_SEC)))
    }
    
    func seekPlayerTo(progress: Double) {
        seekPlayerTo(seconds: getElapsedSecondsForProgress(progress: progress))
    }
    
    func setPlaybackSpeed(speed: Float) {
        currentPlaybackSpeed = speed
        if getState() == .playing {
            player.rate = speed
        }
    }
    
    func getPlaybackSpeed() -> Float {
        return player.rate
    }
}
