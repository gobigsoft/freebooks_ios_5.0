//
//  PlayerEngineProgressProviders.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension PlayerEngine {
    func getElapsedSecondsForProgress(progress: Double) -> Double {
        return getTotalPlaybackTime()*progress
    }
    
    func getRemainingTimeForProgress(progress: Double) -> Double {
        return getTotalPlaybackTime()-getElapsedSecondsForProgress(progress:progress)
    }
    
    func getTotalPlaybackTime() -> Double {
        return player.currentItem?.asset.duration.seconds ?? 0
    }
    
    func getRemainingTimeSeconds() -> Double {
        return getTotalPlaybackTime()-currentPlaybackProgressSeconds
    }
    
    func getElapsedTimeSeconds() -> Double {
        return currentPlaybackProgressSeconds
    }
    
    func getCurrentProgress() -> Float {
        return getTotalPlaybackTime() == 0 ? 0 : Float(getElapsedTimeSeconds())/Float(getTotalPlaybackTime())
    }
    
    //When we need to calculate progress seconds from total time * progress in %
    internal func setCurrentProgressSeconds() {
        if let currentProgress = self.getAudiobookDetail()?.progress {
            self.currentPlaybackProgressSeconds = getTotalPlaybackTime()*Double(currentProgress)
        } else {
            self.currentPlaybackProgressSeconds = 0
        }
    }
}
