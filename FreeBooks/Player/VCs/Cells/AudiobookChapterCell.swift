//
//  ChapterCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import MBCircularProgressBar

enum DownloadState {
    case stopped
    case downloading
    case downloaded
}

class AudiobookChapterCell: BaseSeparatorCell, ChapterDownloaderDelegate {
    
    private var bookId: Int?
    private var chapter: AudiobookChapter?
    private var chapterDownloader: ChapterDownloader?

    private var downloadInProgress = false
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var order: UILabel!

    @IBOutlet weak private var downloadButton: UIButton!
    @IBOutlet weak private var circularProgressBar: MBCircularProgressBarView!
    
    func initWithBookId(bookId: Int, chapterIndex: Int, chapter: AudiobookChapter) {
        self.bookId = bookId
        self.chapter = chapter
        
        self.name.text = chapter.name
        self.order.text = "\(chapterIndex+1)"
        
        if let chapterDownloader = PlayerEngine.sharedEngine.getChapterDownloader(chapterId: chapter.chapterId) {
            DispatchQueue.main.async {
                self.setDownloadingState()
            }
            self.chapterDownloader = chapterDownloader
            self.chapterDownloader?.delegate = self
        }
    }
    
   @IBAction private func downloadPressed() {
        if let bookId = self.bookId, let chapter = self.chapter {
            setDownloadingState()
            showDownloadPreparationLoading()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                self.chapterDownloader = ChapterDownloader(bookId: bookId, chapter: chapter)
                self.chapterDownloader?.delegate = self
                self.chapterDownloader?.downloadChapter()
            })
        }
    }

    private func showDownloadPreparationLoading() {
        if downloadInProgress == false {
            UIView.animate(withDuration: 1, animations: {
                self.circularProgressBar.value = 100
            }, completion: { success in
                self.circularProgressBar.value = CGFloat(0)
                DispatchQueue.main.async {
                    self.showDownloadPreparationLoading()
                }
            })
        }
    }

    @objc private func stopDownloading() {
        downloadInProgress = false
        chapterDownloader?.stopDownloading()
        
        //Let's turn off the deleagate so we don't receive any progress callback - we could receive some late callbacks until the request is cancelled, which is not desired anymore
        self.chapterDownloader?.delegate = nil
        circularProgressBar.value = 0
        
        setDownloadState()
        
        if let chapter = self.chapter {
            PlayerEngine.sharedEngine.removeChapterDownloader(chapterId: chapter.chapterId)
        }
    }
    
    func handleCurrentStateVisual() {
        if let chapter = self.chapter, chapter.isDownloaded == true {
            setDownloadedVisual()
        } else if downloadInProgress == true {
            setDownloadingState()
        } else {
            setDownloadState()
        }
    }
    
    private func setDownloadingState() {
        circularProgressBar.isHidden = false
        downloadButton.setImage(UIImage(named: "icon_stop_download"), for: UIControl.State.normal)
        downloadButton.removeTarget(self, action: #selector(downloadPressed), for: UIControl.Event.allEvents)
        downloadButton.addTarget(self, action: #selector(stopDownloading), for: UIControl.Event.touchUpInside)
    }

    private func setDownloadState() {
        name.textColor = UIColor.lightGray
        order.textColor = UIColor.lightGray
        
        circularProgressBar.isHidden = false
        downloadButton.setImage(UIImage(named: "icon_download"), for: UIControl.State.normal)
        downloadButton.removeTarget(self, action: #selector(stopDownloading), for: UIControl.Event.allEvents)
        downloadButton.addTarget(self, action: #selector(downloadPressed), for: UIControl.Event.touchUpInside)
    }
    
    private func setDownloadedVisual() {
        name.textColor = Constants.Colors.Brown
        order.textColor = Constants.Colors.Brown
        circularProgressBar.isHidden = true
    }
    
    internal func chapterDownloadUpdated(percentageProgress: Int) {
        downloadInProgress = true
        circularProgressBar.value = CGFloat(percentageProgress)
    }
    
    internal func chapterDownloaded() {
        downloadInProgress = false
        chapter?.isDownloaded = true
        setDownloadedVisual()
    }
    
    internal func chapterDownloadFailed() {
        downloadInProgress = false
        chapter?.isDownloaded = false
        setDownloadState()
    }
}


