//
//  PlayerVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import MBCircularProgressBar

protocol PlayerVCDelegate {
    func showChaptersPressed()
    func showAudiobookDetailPressed()
    func cancelPlayerPressed()
    func hidePlayerPressed()
}

class PlayerVC: BaseVC {
    
    @IBOutlet weak private var cover: UIImageView!
    @IBOutlet weak private var coverBackground: UIImageView!
    @IBOutlet weak private var bookName: UILabel!
    @IBOutlet weak private var miniatureBookName: UILabel!
    @IBOutlet weak private var currentChapter: UILabel!
    @IBOutlet weak private var miniatureCurrentChapter: UILabel!
    @IBOutlet weak private var elapsedTime: UILabel!
    @IBOutlet weak private var remainingTime: UILabel!
    @IBOutlet weak private var progressSlider: UISlider!
    
    @IBOutlet weak private var previousButton: UIButton!
    @IBOutlet weak private var nextButton: UIButton!
    private var playPauseButton: RSPlayPauseButton?
    @IBOutlet weak private var playPauseButtonContainer: UIView!
    
    @IBOutlet weak var playerMiniature: UIView!
    @IBOutlet weak private var playerMiniatureHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var miniatureCircularProgressBar: MBCircularProgressBarView!
    @IBOutlet weak private var miniaturePlayPauseButtonContainer: UIView!
    private var miniaturePlayPauseButton: RSPlayPauseButton?
    
    @IBOutlet weak var playbackSpeedDarkView: UIView!
    @IBOutlet weak private var playbackSpeedContainer: UIView!
    @IBOutlet weak private var playbackSpeedSlider: UISlider!
    @IBOutlet weak private var playbackSpeed: UILabel!
    
    @IBOutlet weak private var speedButton: ButtonWithBottomTitle!
    @IBOutlet weak private var chaptersButton: ButtonWithBottomTitle!
    @IBOutlet weak private var detailsButton: ButtonWithBottomTitle!
    
    private var ignoreCallbacks = false
    var delegate: PlayerVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMiniatureHeightConstraint()
        
        UIUtils.blurImageView(imageView: coverBackground)
        setShadows()
        playbackSpeedContainer.layer.cornerRadius = 5
        
        progressSlider.isContinuous = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(setup), name: Notification.Name(rawValue: Constants.NotificationConstants.AudiobookChanged), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setupPlaybackTimeValues), name: Notification.Name(rawValue: Constants.NotificationConstants.AudiobookProgressUpdated), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setButtonForPlayerState), name: Notification.Name(rawValue: Constants.NotificationConstants.PlaybackStateChangedFromCommandCenter), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelPlayerPressed), name: Notification.Name(rawValue: Constants.NotificationConstants.AudiobooksSubscriptionInactive), object: nil)
        
        initPlayPauseButtons()
        initTitleButtons()
    }
    
    private func setMiniatureHeightConstraint() {
        if DeviceUtils.isBezelessDevice() {
            playerMiniatureHeightConstraint.constant = Constants.Misc.PlayerMiniatureBezelessDeviceSizeHeight
        }
    }
    
    private func setShadows() {
        cover.layer.shadowRadius = 3
        cover.layer.shadowOffset = CGSize(width: 0, height: 0)
        cover.layer.shadowOpacity = 0.7
        cover.layer.masksToBounds = false
        
        playerMiniature.layer.shadowRadius = 2
        playerMiniature.layer.shadowOffset = CGSize(width: 0, height: -1)
        playerMiniature.layer.shadowOpacity = 0.15
        playerMiniature.layer.masksToBounds = false
    }
    
    private func initPlayPauseButtons() {
        playPauseButton = RSPlayPauseButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50), tintColor: UIColor.white, hasCircleBorder: true)
        playPauseButton?.center = CGPoint(x: 25, y: 25)
        playPauseButton?.animationStyle = RSPlayPauseButtonAnimationStyle.splitAndRotate
        playPauseButtonContainer.addSubview(playPauseButton!)
        
        miniaturePlayPauseButton = RSPlayPauseButton(frame: CGRect(x: 0, y: 0, width: 38, height: 38), tintColor: Constants.Colors.DarkerBrown, hasCircleBorder: false)
        miniaturePlayPauseButton?.center = CGPoint(x: 19, y: 19)
        miniaturePlayPauseButton?.animationStyle = RSPlayPauseButtonAnimationStyle.splitAndRotate
        miniaturePlayPauseButtonContainer.addSubview(miniaturePlayPauseButton!)
    }
    
    private func initTitleButtons() {
        speedButton.setBottomTitle(title: "SPEED")
        chaptersButton.setBottomTitle(title: "CHAPTERS")
        detailsButton.setBottomTitle(title: "DETAILS")
    }
    
    @objc func setup() {
        coverBackground.kf.setImage(with: URL(string: PlayerEngine.sharedEngine.getBookCover()), options: [.transition(.fade(0.2))])
        cover.kf.setImage(with: URL(string: PlayerEngine.sharedEngine.getBookCover()), placeholder: nil, options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: { image, error, cacheType, url in
            if let image = image {
                self.cover.image = image
                
                //We want to store downloaded image in PlayerEngine so we can reuse the same image for Media Player Command Center when app goes into the background
                PlayerEngine.sharedEngine.setCoverImage(image: image)
            }
        })
        bookName.text = PlayerEngine.sharedEngine.getBookTitle()
        miniatureBookName.text = PlayerEngine.sharedEngine.getBookTitle()
        setCurrentChapterLabel()
        
        setButtonForPlayerState()
        setupPlaybackTimeValues()
        handlePreviousNextButtonDisabling()
    }
    
    func setMiniatureVisible() {
        playerMiniature.alpha = 1
    }
    
    private func updatePlaybackTime(elapsedTime: Double, remainingTime: Double) {
        self.elapsedTime.text = PlayerUtils.getPlaybackTimeString(seconds: elapsedTime)
        self.remainingTime.text = PlayerUtils.getPlaybackTimeString(seconds: remainingTime)
    }
    
    private func setCurrentChapterLabel() {
        currentChapter.text = PlayerEngine.sharedEngine.getCurrentChapterLabel()
        miniatureCurrentChapter.text = PlayerEngine.sharedEngine.getCurrentChapterLabel()
    }
    
    @objc private func setupPlaybackTimeValues() {
        let elapsedTime = PlayerEngine.sharedEngine.getElapsedTimeSeconds()
        let remainingTime = PlayerEngine.sharedEngine.getRemainingTimeSeconds()
        
        if ignoreCallbacks == false {
            progressSlider.value = PlayerEngine.sharedEngine.getCurrentProgress()
            miniatureCircularProgressBar.value = CGFloat(PlayerEngine.sharedEngine.getCurrentProgress())
            updatePlaybackTime(elapsedTime: elapsedTime, remainingTime: remainingTime)
        }
    }
    
    @IBAction private func valueChanged(_ slider: UISlider, _ event: UIEvent) {
        guard let touch = event.allTouches?.first, touch.phase != .ended else {
            
            //We need to ignore callbacks for a while after the seeking so we don't end up with choppy slider/values animation changes caused by immediate handling of callback
            ignoreCallbacksBriefly()
            PlayerEngine.sharedEngine.seekPlayerTo(progress: Double(progressSlider.value))
            return
        }
        
        let progress = Double(progressSlider.value)
        updatePlaybackTime(elapsedTime: PlayerEngine.sharedEngine.getElapsedSecondsForProgress(progress: progress), remainingTime: PlayerEngine.sharedEngine.getRemainingTimeForProgress(progress: progress))
    }
    
    private func ignoreCallbacksBriefly() {
        ignoreCallbacks = true
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500), execute: {
            self.ignoreCallbacks = false
        })
    }
    
    @IBAction private func nextPressed() {
        PlayerEngine.sharedEngine.playNextChapter()
        handlePreviousNextButtonDisabling()
    }
    
    @IBAction private func previousPressed() {
        //If we're already listening to the book, button "previous" should take us by default to the beginning of the chapter, subsequent "previous" should move us to the previous chapter
        if PlayerEngine.sharedEngine.getElapsedTimeSeconds() >= 1 {
            PlayerEngine.sharedEngine.seekPlayerTo(seconds: 0)
        } else {
            PlayerEngine.sharedEngine.playPreviousChapter()
            handlePreviousNextButtonDisabling()
        }
    }
    
    @IBAction private func seekForward() {
        PlayerEngine.sharedEngine.seekForward()
    }
    
    @IBAction private func seekBackwards() {
        PlayerEngine.sharedEngine.seekBackwards()
    }
    
    @objc @IBAction private func playPressed() {
        setPauseButton()
        PlayerEngine.sharedEngine.resumePlayback()
    }
    
    @objc @IBAction private func pausePressed() {
        setPlayButton()
        PlayerEngine.sharedEngine.pausePlayback()
    }
    
    @IBAction private func cancelPlayerPressed() {
        PlayerEngine.sharedEngine.stopPlayback()
        self.delegate?.cancelPlayerPressed()
    }
    
    @IBAction private func hidePlayerPressed() {
        self.delegate?.hidePlayerPressed()
    }
    
    private func handlePreviousNextButtonDisabling() {
        let nextChapter = PlayerEngine.sharedEngine.getChapterForIndex(index: PlayerEngine.sharedEngine.getCurrentChapterIndex()+1)
        nextButton.isEnabled = (nextChapter != nil)
        (nextChapter != nil) ? PlayerEngine.sharedEngine.enableCommandCenterNext() :  PlayerEngine.sharedEngine.disableCommandCenterNext()
        
        let previousChapter = PlayerEngine.sharedEngine.getChapterForIndex(index: PlayerEngine.sharedEngine.getCurrentChapterIndex()-1)
        previousButton.isEnabled = (previousChapter != nil)
        (previousChapter != nil) ? PlayerEngine.sharedEngine.enableCommandCenterPrevious() : PlayerEngine.sharedEngine.disableCommandCenterPrevious()
    }
    
    @objc private func setButtonForPlayerState() {
        PlayerEngine.sharedEngine.getState() == .playing ? setPauseButton() : setPlayButton()
    }
    
    private func setPlayButton() {
        playPauseButton?.setPaused(true, animated: true)
        playPauseButton?.removeTarget(self, action: #selector(pausePressed), for: UIControl.Event.allEvents)
        playPauseButton?.addTarget(self, action: #selector(playPressed), for: UIControl.Event.touchUpInside)
        
        miniaturePlayPauseButton?.setPaused(true, animated: true)
        miniaturePlayPauseButton?.removeTarget(self, action: #selector(pausePressed), for: UIControl.Event.allEvents)
        miniaturePlayPauseButton?.addTarget(self, action: #selector(playPressed), for: UIControl.Event.touchUpInside)
    }
    
    private func setPauseButton() {
        playPauseButton?.setPaused(false, animated: true)
        playPauseButton?.removeTarget(self, action: #selector(playPressed), for: UIControl.Event.allEvents)
        playPauseButton?.addTarget(self, action: #selector(pausePressed), for: UIControl.Event.touchUpInside)
        
        miniaturePlayPauseButton?.setPaused(false, animated: true)
        miniaturePlayPauseButton?.removeTarget(self, action: #selector(playPressed), for: UIControl.Event.allEvents)
        miniaturePlayPauseButton?.addTarget(self, action: #selector(pausePressed), for: UIControl.Event.touchUpInside)
    }
    
    @IBAction private func showPlaybackSpeed() {
        self.playbackSpeedDarkView.isHidden = false
        UIView.animate(withDuration: 0.7, animations: {
            self.playbackSpeedDarkView.alpha = 1
        })
    }
    
    private func hidePlaybackSpeed() {
        UIView.animate(withDuration: 0.7, animations: {
            self.playbackSpeedDarkView.alpha = 0
        }, completion: { success in
            self.playbackSpeedDarkView.isHidden = true
        })
    }
    
    //Hide playback speed selection at any touch outside the selection
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view == playbackSpeedDarkView {
            hidePlaybackSpeed()
        }
    }
    
    @IBAction func playbackSpeedValueChanged(_ slider: UISlider, _ event: UIEvent) {
        let step: Float = 0.1
        let roundedValue = round(slider.value / step) * step
        slider.value = roundedValue
        playbackSpeed.text = String.init(format: "%.1fx", roundedValue)
        PlayerEngine.sharedEngine.setPlaybackSpeed(speed: roundedValue)
    }
    
    @IBAction private func showChapters() {
        delegate?.showChaptersPressed()
    }
    
    @IBAction private func showAudiobookDetail() {
        delegate?.showAudiobookDetailPressed()
    }
}

class ButtonWithBottomTitle: UIButton {
    
    private var bottomTitle: UILabel?
    let color = UIColor.init(white: 1, alpha: 0.45)
    let highlightedColor = UIColor(red: 111/255, green: 106/255, blue: 102/255, alpha: 1)
    
    func setBottomTitle(title: String) {
        bottomTitle = UILabel(frame: CGRect(x: -19, y: self.frame.size.height+2, width: self.frame.size.width+40, height: 20))
        bottomTitle?.textAlignment = NSTextAlignment.center
        bottomTitle?.textColor = color
        bottomTitle?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        bottomTitle?.text = title
        
        self.addSubview(bottomTitle!)
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted == true {
                bottomTitle?.textColor = highlightedColor
            } else {
                bottomTitle?.textColor = color
            }
        }
    }
}


