//
//  ChaptersVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AudiobookChaptersVC: BaseFakeNavigationBarVC {
    
    let AudiobookChapterCellIdentifier = "AudiobookChapter"
    
    @IBOutlet weak private var chaptersCollection: UICollectionView!
    @IBOutlet weak private var bookName: UILabel!
    
    private var audiobookChapters = [AudiobookChapter]()
    private var audiobookDetail: BookDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chaptersCollection.delegate = self
        self.chaptersCollection.dataSource = self
        
        self.bookName.text = audiobookDetail?.title.uppercased()
    }

    override func viewWillAppear(_ animated: Bool) {        
        DispatchQueue.main.async {
            self.chaptersCollection.scrollToItem(at: IndexPath(row: PlayerEngine.sharedEngine.currentChapterIndex, section: 0), at: .centeredVertically, animated: false)
        }
    }
    
    func initWithChapters(audiobookChapters: [AudiobookChapter], audiobookDetail: BookDetail) {
        self.audiobookChapters = audiobookChapters
        self.audiobookDetail = audiobookDetail
    }
}

extension AudiobookChaptersVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if audiobookChapters[indexPath.row].isDownloaded == true {
            PlayerEngine.sharedEngine.playChapter(index: indexPath.row)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 60)
    }
}

extension AudiobookChaptersVC: UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let chapterCell = collectionView.dequeueReusableCell(withReuseIdentifier: AudiobookChapterCellIdentifier, for: indexPath) as! AudiobookChapterCell
        if let audiobookDetail = self.audiobookDetail {
            chapterCell.initWithBookId(bookId: audiobookDetail.bookId, chapterIndex: indexPath.row, chapter: audiobookChapters[indexPath.row])
            //Adjusting cells from VC prevents incorrect displaying caused by OS reusing cells
            chapterCell.handleCurrentStateVisual()
            if (indexPath.row == PlayerEngine.sharedEngine.currentChapterIndex) {
                chapterCell.name.textColor = Constants.Colors.ReddishBrown
                chapterCell.order.textColor = Constants.Colors.ReddishBrown
            }
        }
        chapterCell.updateSeparator(color: Constants.Colors.Brown_10, offsetX: self.view.frame.size.width/5)
        return chapterCell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return audiobookChapters.count
    }
}
