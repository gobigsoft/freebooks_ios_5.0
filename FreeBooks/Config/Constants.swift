//
//  Constants.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

struct Constants {
    
    struct StoryboardConstants {
        static let StoryboardMain = "Main"
        struct VCIdentifiers {
            static let WalkthroughContainerVC = "WalkthroughContainerVC"
            static let WalkthroughVC = "WalkthroughVC"
            static let FeaturedVC = "FeaturedVC"
            static let BookDetailVC = "BookDetailVC"
            static let AboutVC = "AboutVC"
            static let BookReviewsVC = "BookReviewsVC"
            static let BrowseVC = "BrowseVC"
            static let AudiobooksVC = "AudiobooksVC"
            static let AuthorDetailVC = "AuthorDetailVC"
            static let ReaderVC = "ReaderVC"
            static let ReaderSinglePageVC = "ReaderSinglePageVC"
            static let BookMetaContentsVC = "BookMetaContentsVC"
            static let TableOfContentsVC = "TableOfContentsVC"
            static let BookmarksVC = "BookmarksVC"
            static let MyLibraryBooksVC = "MyLibraryBooksVC"
            static let PlayerVC = "PlayerVC"
            static let AudiobookChaptersVC = "AudiobookChaptersVC"
            static let CategoryDetailVC = "CategoryDetailVC"
            static let CategoryDetailBooksVC = "CategoryDetailBooksVC"
            static let SearchVC = "SearchVC"
            static let BookAdVC = "BookAdVC"
            static let AboutAppVC = "AboutAppVC"
            static let OpenSourceLicensesVC = "OpenSourceLicensesVC"
            static let SettingsVC = "SettingsVC"
            static let CoverDetailVC = "CoverDetailVC"
        }
        
        struct TabControllersIdentifiers {
            static let MainTabController = "MainTabController"
        }
    }
    
    struct Colors {
        static let AppleRed = UIColor(red: 156/255, green: 79/255, blue: 53/255, alpha: 1)
        static let BlueViolet = UIColor(red: 111/255, green: 78/255, blue: 96/255, alpha: 1)
        static let Brown = UIColor(red: 95/255, green: 82/255, blue: 74/255, alpha: 1)
        static let HuskGreen = UIColor(red: 156/255, green: 123/255, blue: 52/255, alpha: 1)
        static let AppleGreen = UIColor(red: 111/255, green: 123/255, blue: 52/255, alpha: 1)
        static let HippieBlue = UIColor(red: 111/255, green: 123/255, blue: 96/255, alpha: 1)
        static let FuchsiaPink = UIColor(red: 156/255, green: 78/255, blue: 96/255, alpha: 1)
        static let PrimaryBrownBg = UIColor(red: 248/255, green: 244/255, blue: 231/255, alpha: 1)
        static let ReddishBrown = UIColor(red: 151/255, green: 78/255, blue: 0/255, alpha: 1)
        static let ReddishBrown_33 = UIColor(red: 151/255, green: 78/255, blue: 0/255, alpha: 0.33)
        static let DarkerBrown = UIColor(red: 74/255, green: 64/255, blue: 58/255, alpha: 1)
        static let Gold = UIColor(red: 255/255, green: 193/255, blue: 104/255, alpha: 1)
        static let Gold_10 = UIColor(red: 255/255, green: 193/255, blue: 104/255, alpha: 0.1)
        static let PlaceholderBrown = UIColor(red: 232/255, green: 229/255, blue: 219/255, alpha: 1)
        static let Brown_10 = UIColor(red: 95/255, green: 82/255, blue: 74/255, alpha: 0.1)
        static let Brown_22 = UIColor(red: 95/255, green: 82/255, blue: 74/255, alpha: 0.22)
        static let Brown_60 = UIColor(red: 95/255, green: 82/255, blue: 74/255, alpha: 0.60)
        static let Gray_10 = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        static let Gray_70 = UIColor(red: 97/255, green: 97/255, blue: 97/255, alpha: 1)
        static let Gray_80 = UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1)
        static let Gray_95 = UIColor(red: 16/255, green: 16/255, blue: 16/255, alpha: 1)
        static let GrayLight = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 0.50)
    }
    
    struct ThemeConstants {
        struct ColorsHex {
            static let Sepia = "F2EDE6"
            static let SepiaDark = "#1a0f01"
            static let SepiaDarker = "#c7c3be"
            static let SepiaBookmarkColor = "974E00"
            
            static let White = "#FFFFFF"
            static let White_Gray90 = "#212121"
            static let White_Gray20 = "#EEEEEE"
            static let WhiteBookmarkColor = "#974E00"
            
            static let Dark_Gray20 = "#EEEEEE"
            static let Dark_Gray80 = "#424242"
            static let Dark_Gray90 = "#212121"
            static let DarkBookmarkColor = "#FFC168"
            
            static let Sunset_RedDark = "#351611"
            static let Sunset_RedDarker = "#8a716d"
            static let Sunset_Red = "#a88984"
            static let SunsetBookmarkColor = "#FFC168"
        }
    }
    
    struct NotificationConstants {
        static let ThemeChanged = "ThemeChanged"
        static let AudiobookChanged = "AudiobookChanged"
        static let AudiobookProgressUpdated = "AudiobookProgressUpdated"
        static let PlaybackStateChangedFromCommandCenter = "PlaybackStateChangedFromCommandCenter"
        static let PlayerCancelled = "PlayerCancelled"
        static let PlayerClosed = "PlayerClosed"
        static let AdsRemoved = "AdsRemoved"
        static let AudiobooksSubscriptionInactive = "AudiobooksSubscriptionInactive"
        static let AudiobooksSubscriptionActive = "AudiobooksSubscriptionActive"
        static let ReaderAppearanceSettingsDisplayed = "ReaderAppearanceSettingsDisplayed"
        
        struct TabBarSelectionNotifications {
            static let FeaturedSelected = "FeaturedSelected"
            static let AudiobooksSelected = "AudiobooksSelected"
            static let BrowseSelected = "BrowseSelected"
            static let MyLibrarySelected = "MyLibrarySelected"
            static let SearchSelected = "SearchSelected"
        }
    }

    struct Misc {
        static let DefaultRating = 4.8
        static let MaxRating = 5
        static let MaxReviewsInBookDetailsCount = 3
        static let MaxBookDetailAboutVisibleCharacters = 450
        static let PlayerDisplayAnimationDuration = Double(1)
        static let PlayerMiniatureSizeHeight = CGFloat(49)
        static let PlayerMiniatureBezelessDeviceSizeHeight = CGFloat(85)
        static let DefaultSearchResultsPageCount = 50
        static let AppStoreUrl = "https://itunes.apple.com/us/app/my-books-unlimited-library/id364612911?mt=8"
        static let ManageSubscriptionsUrl = "https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions"
        static let SubscriptionInfoText = "Get unlimited access to the whole audiobooks library consisting of 5,199 audiobooks. \n\n  • A 1,99 USD/month purchase will be applied to your iTunes account on confirmation. \n • Subscriptions will automatically renew unless canceled within 24-hours before the end of the current period. \n • You can cancel anytime with your iTunes account settings. \n • For more information, see our Terms of Service and Privacy Policy."

        //Constant used for saying there isn't any unfinished playback 
        static let NoUnfinishedPlaybackAudiobookId = 0
    }
    
    struct AboutApp {
        static let LicensesFileName = "open_source_licenses"
        static let SupportEmail = "support@digiply.io"

        struct URLs {
            static let WebKrl = "http://www.krl.sk"
            static let WebDigiply = "http://digiply.io/"
            static let WebForum = "http://digiply.io/support/category/free-books-for-android/"
            static let WebPrivacyPolicy = "http://www.freebooksapp.com/privacy-policy.html"
            static let WebTermsOfUse = "http://www.freebooksapp.com/terms-of-use.html"
        }
    }
    
    struct LottieAnimations {
        static let VoiceRecognition = "voice_recognition"
        static let VoiceRecognitionFailed = "voice_recognition_failed"
        static let Success = "success"
    }
    
    struct UserDefaultsKeys {
        static let WalkthroughDisplayed = "WalkthroughDisplayed"
        static let SearchHintDisplayed = "SearchHintDisplayed"
        static let ReaderControlsHintDisplayed = "ReaderControlsHintDisplayed"
        static let AdsRemoved = "AdsRemoved"
        static let AudiobooksPurchased = "AudiobooksPurchased"
        static let AudiobooksSubscriptionActive = "AudiobooksSubscriptionActive"
        
        static let AnalyticsEnabled = "AnalyticsEnabled"
        static let PushNotificationsEnabled = "PushNotificationEnabled"
        
        //Used to determine whether user has already disabled the analytics at least once, by default analytics are set to true
        static let AnalyticsSettingsOverridenByUser = "AnalyticsSettingsOverridenByUser"
        
        static let UnfinishedPlaybackAudiobookId = "UnfinishedPlaybackAudiobookId"
    }
}
