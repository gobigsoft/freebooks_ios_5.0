//
//  Config.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

struct Config {
    
    struct AppMode {
        static let Debug = true
        static let ProductionAPI = true
    }
    
    struct API {
        struct BaseUrl {
            static let Testing = "http://ec2-18-195-226-222.eu-central-1.compute.amazonaws.com"
            static let Production = "http://api.freebooksapp.com"
        }
        
        struct Routes {
            static let Featured = "/3.2/book/featured"
            static let Audiobooks = "/3.2/audiobooks/top"
            static let Browse = "/3.0/author/browse"
            static let Search = "/3.2/book/search"
            static let Synchronize = "/v2/synchronize"
            static let BookDetail = "/3.2/book/%d/detail"
            static let BookReviews = "/3.0/book/%d/reviews/0"
            static let UserReview = "/userReview/%@"
            static let AuthorDetail = "/3.0/author/%d/detail"
            static let Category = "/3.0/category"
            static let Login = "/login"
            static let FacebookLogin = "/login/facebook"
            static let Register = "/register"
            static let Logout = "/logout"
            static let RecoverPassword = "/recoverPassword"
            static let Statistics = "/statistics"
            static let DownloadBook = "/3.0/book/%d/download"
            static let DownloadAudiobookChapter = "/3.0/book/chapter/%d"
            static let OldBooks = "/3.0/oldbooks"
            static let FeelingLucky = "/3.2/feelingLucky"
            static let Archive = "/3.0/archive/%@"
            static let LegacyBooks = "/3.2/book_legacy/details?array=%d"
            static let Timestamp = "/3.2/timestamp"
        }
        
        struct StatusCode {
            static let Success = 200
            static let UnknownError = 9999
        }
    }
    
    struct Realm {
        static let SchemaVersion: UInt64 = 12
    }
    
    struct Ads {
        static let MinimumSpanBetweenInterstitialsSeconds = 120
        static let ReaderAdFrequencyPages = 16
        struct AdMob {
            static let AppId = "ca-app-pub-4973949849980360~8723754535"
            struct AdIdentifiers {
                static let Featured = "ca-app-pub-4973949849980360/8020504480"
                static let MyLibrary = "ca-app-pub-4973949849980360/7618724394"
                static let ReaderInterstitial = "ca-app-pub-4973949849980360/7058776607"
            }
        }
    }
    
    struct InAppPurchases {
        static let PurchaseItemRemoveAds = "com.spreadsong.freebooks.ipad.adFreeModel"
        static let PurchaseItemAudiobooks = "com.spreadsong.freebooks.ipad.audiobooksModel"
        static let PurchaseItemUnlockAudiobooks = "com.spreadsong.freebooks.ipad.unlockaudiobooks"
        static let PurchaseItemAudiobooksSubscription = "com.spreadsong.freeboks.ipad.audiobooks.subscription"
    }
}

