//
//  Author.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class Author: NSObject {
    let authorId: Int 
    let name: String
    let image: String
    
    init(json: JSON) throws {
        self.authorId = json["id"].intValue
        self.name = json["name"].stringValue
        self.image = json["image"].stringValue
    }
    
    init(realmAuthor: RealmAuthor) {
        self.authorId = realmAuthor.authorId
        self.name = realmAuthor.name
        self.image = realmAuthor.image
    }
    
    func getRealmObject() -> RealmAuthor {
        return RealmAuthor(value: ["authorId": authorId, "name": name, "image": image])
    }
}

class RealmAuthor: Object {
    @objc dynamic var authorId: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var image: String = ""
}

