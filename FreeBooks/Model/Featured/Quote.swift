//
//  Quote.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON

class Quote: NSObject {
    let quote: String
    var author: Author?
    
    init(json: JSON) throws {
        self.quote = json["quote"].stringValue
        do {
            self.author = try Author(json: json["author"])
        } catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't parse quote, error: \(error.description))")
        }
    }
}


