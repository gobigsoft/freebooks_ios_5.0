//
//  Book.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

enum BookType: String {
    case Book = "book"
    case Audiobook = "audiobook"
}

class Book: NSObject {
    let bookId: Int
    let title: String
    let type: String 
    let coverUrl: String
    let rating: String
    let isTop: Bool
    var authors = [String]()
    
    init(json: JSON) throws {
        self.bookId = json["id"].intValue
        self.title = json["title"].stringValue
        self.type = json["type"].stringValue
        self.coverUrl = json["cover"].stringValue
        self.rating = json["rating"].stringValue
        self.isTop = json["is_top"].boolValue
        for author in json["authors"].arrayValue {
            authors.append(author.stringValue)
        }
    }
    
    init(realmBook: RealmBook) {
        self.bookId = realmBook.id
        self.title = realmBook.title
        self.type = realmBook.type
        self.coverUrl = realmBook.coverUrl
        self.rating = realmBook.rating
        self.isTop = realmBook.isTop
        super.init()
        realmBook.authors.forEach({
            self.authors.append($0.name)
        })
    }
    
    func getRealmObject() -> RealmBook {
        return RealmBook(value: getRealmValues())
    }
    
    func getSearchedBookRealmobject() -> RealmSearchedBook {
        let realmSearchedBook = RealmSearchedBook(value: getRealmValues())
        authors.forEach({
            realmSearchedBook.authors.append(RealmStringAuthor(value: ["name": $0]))
        })
        return realmSearchedBook
    }
    
    func getRealmValues() -> [String: Any] {
        return ["id" : bookId, "title": title, "type": type, "coverUrl": coverUrl, "rating": rating, "isTop": isTop]
    }
    
    func isAudiobook() -> Bool {
        return self.type == BookType.Audiobook.rawValue
    }
}

class RealmBook: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var type: String = ""
    @objc dynamic var coverUrl: String = ""
    @objc dynamic var rating: String = ""
    @objc dynamic var isTop: Bool = false
    let authors = List<RealmStringAuthor>()

    override static func primaryKey() -> String? {
        return "id"
    }
}

class RealmSearchedBook: RealmBook {
    
}

class RealmStringAuthor: Object {
    @objc dynamic var name: String = ""
}

