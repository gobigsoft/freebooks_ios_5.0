//
//  Featured.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON

class Featured: NSObject {
    private let KwTypeFeaturedBooks = "featured_books"
    private let KwTypeBooksWithImage = "books_with_image"
    private let KwTypeQuotes = "quotes"
    private let KwTypeBooks = "books"
    
    var featuredBooks: FeaturedBooks?
    var audiobooks: FeaturedAudiobooks?
    var booksWithImage: FeaturedBooksWithImage?
    var quotes: FeaturedQuotes?
    
    init(json: JSON) throws {
        for featuredJson in json["items"].arrayValue {
            let featuredType = featuredJson["type"].stringValue
            if featuredType == KwTypeFeaturedBooks {
                featuredBooks = try FeaturedBooks(json: featuredJson)
            } else if featuredType == KwTypeBooksWithImage {
                booksWithImage = try FeaturedBooksWithImage(json: featuredJson)
            } else if featuredType == KwTypeQuotes {
                quotes = try FeaturedQuotes(json: featuredJson)
            } else if featuredType == KwTypeBooks {
                audiobooks = try FeaturedAudiobooks(json: featuredJson)
            }
        }
    }
}

//MARK: Featured entities

internal class FeaturedItems: NSObject {
    var type: String?
    var title: String?
    init(json: JSON) throws {
        self.type = json["type"].stringValue
        self.title = json["title"].stringValue
    }
}

class FeaturedQuotes: FeaturedItems {
    var quotes = [Quote]()
    
    override init(json: JSON) throws {
        try super.init(json: json)
        for quote in json["quotes"].arrayValue {
            do {
                try quotes.append(Quote(json: quote))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse featured quote, error: \(error.description)")
            }
        }
    }
}

class FeaturedAudiobooks: FeaturedItems {
    var books = [Book]()
    override init(json: JSON) throws {
        try super.init(json: json)
        for book in json["books"].arrayValue {
            do {
                try books.append(Book(json: book))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse featured book, error: \(error.description)")
            }
        }
    }
}

class FeaturedBooksWithImage: FeaturedAudiobooks {
    var subtitle: String?
    var imageUrl: String?
    override init(json: JSON) throws {
        try super.init(json: json)
        self.subtitle = json["subtitle"].stringValue
        self.imageUrl = json["image"].stringValue
    }
}

class FeaturedBooks: FeaturedBooksWithImage {
    var bgImageUrl: String?
    override init(json: JSON) throws {
        try super.init(json: json)
        self.bgImageUrl = json["bg_image"].stringValue
    }
}



