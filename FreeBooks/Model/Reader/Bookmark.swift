//
//  Bookmark.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import RealmSwift

class Bookmark: NSObject {
    let progress: Float
    let snippet: String
    let dateAdded: Date
    
    init(progress: Float, snippet: String) {
        self.progress = progress
        self.snippet = snippet
        self.dateAdded = Date()
    }
    
    init(realmBookmark: RealmBookmark) {
        self.progress = realmBookmark.progress
        self.snippet = realmBookmark.snippet ?? ""
        self.dateAdded = realmBookmark.dateAdded
    }
    
    func getRealmObject() -> RealmBookmark {
        return RealmBookmark(value: ["progress": progress, "snippet": snippet, "dateAdded": dateAdded])
    }
}

class RealmBookmark: Object {
    @objc dynamic var progress: Float = 0.0
    @objc dynamic var snippet: String?
    @objc dynamic var dateAdded: Date = Date()
}


