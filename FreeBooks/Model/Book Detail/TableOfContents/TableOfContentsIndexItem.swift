//
//  TableOfContentsIndexItem.swift
//  FreeBooks
//
//  Created by Karol Grulling on 14/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import RealmSwift

//Chapter Index Item is a product of reader.js anchor target indices result 
class TableOfContentsIndexItem: NSObject {
  
    internal let chapterId: String
    internal let pageIndex: Int
    
    init(chapterId: String, pageIndex: Int) {
        self.chapterId = chapterId
        self.pageIndex = pageIndex
    }
    
    func getChapterId() -> String {
        return self.chapterId
    }
    
    func getPageIndex() -> Int {
        return pageIndex
    }
    
    init(realmTableOfContentsIndexItem: RealmTableOfContentsIndexItem) {
        self.chapterId = realmTableOfContentsIndexItem.chapterId
        self.pageIndex = realmTableOfContentsIndexItem.pageIndex
    }
    
    func getRealmObject() -> RealmTableOfContentsIndexItem {
        let realmTableOfContentsIndexItem = RealmTableOfContentsIndexItem(value: ["chapterId": chapterId, "pageIndex": pageIndex])
        return realmTableOfContentsIndexItem
    }
}

class RealmTableOfContentsIndexItem: Object {
    @objc dynamic var chapterId: String = ""
    @objc dynamic var pageIndex: Int = 0
}
