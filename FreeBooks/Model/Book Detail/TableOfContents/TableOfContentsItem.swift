//
//  TableOfContentItem.swift
//  FreeBooks
//
//  Created by Karol Grulling on 14/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class TableOfContentsItem: NSObject {
    
    private let chapterName: String
    private let chapterId: String
    private var pageIndex: Int?
    
    init(chapterName: String, chapterId: String) {
        self.chapterName = chapterName
        self.chapterId = chapterId
    }
    
    func setPageIndex(index: Int) {
        self.pageIndex = index
    }
    
    func getChapterId() -> String {
        return self.chapterId
    }
    
    func getPageIndex() -> Int {
        return pageIndex ?? 0
    }
    
    func getChapterName() -> String {
        return self.chapterName
    }
}
