//
//  ReaderCache.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import RealmSwift

class ReaderCache: NSObject {
    
    private var pageCounts = [Int]()
    private let totalPages: Int
    private let fontSize: Int
    private var tableOfContentsIndexItems = [TableOfContentsIndexItem]()
    
    init(realmReaderCache: RealmReaderCache) {
        self.totalPages = realmReaderCache.totalPages
        self.fontSize = realmReaderCache.fontSize
        
        //Split stored pagecounts string into an array and turn it into int array
        if let pageCountsString = realmReaderCache.pageCounts?.split(separator: ",") {
            pageCounts = pageCountsString.compactMap { Int($0)}
        }
        super.init()

        realmReaderCache.tableOfContentsIndexItems.forEach({
            self.tableOfContentsIndexItems.append(TableOfContentsIndexItem(realmTableOfContentsIndexItem: $0))
        })
    }
    
    func getPageCounts() -> [Int] {
        return self.pageCounts
    }
    
    func getTotalPages() -> Int {
        return self.totalPages
    }
    
    func getFontSize() -> Int {
        return self.fontSize
    }
    
    func getTableOfContentsIndexItems() -> [TableOfContentsIndexItem] {
        return self.tableOfContentsIndexItems
    }
}

class RealmReaderCache: Object {
    @objc dynamic var pageCounts: String?
    @objc dynamic var totalPages: Int = 0
    @objc dynamic var fontSize: Int = 100
    
    let tableOfContentsIndexItems = List<RealmTableOfContentsIndexItem>()
}
