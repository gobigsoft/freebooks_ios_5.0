//
//  AudiobookChapter.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class AudiobookChapter: NSObject {
    
    let chapterId: Int
    let name: String
    var isDownloaded: Bool
    
    init(json: JSON) throws {
        self.chapterId = json["id"].intValue
        self.name = json["name"].stringValue
        self.isDownloaded = false
    }
    
    init(realmAudiobookChapter: RealmAudiobookChapter) {
        self.chapterId = realmAudiobookChapter.id
        self.name = realmAudiobookChapter.name ?? ""
        self.isDownloaded = realmAudiobookChapter.isDownloaded
    }
    
    func getRealmObject() -> RealmAudiobookChapter {
        return RealmAudiobookChapter(value: ["id": chapterId, "name": name, "isDownloaded" : isDownloaded])
    }
}

class RealmAudiobookChapter: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var isDownloaded: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
