//
//  BookDetailVCVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import EPUBKit

class BookDetailVM: BaseBookVM, ExpandableContentsViewModel {
    
    private var bookDetail: BookDetail?
    
    private var successCallback: (() -> ())!
    private var failureCallback: (() -> ())!
    
    private var downloadInProgress: Bool = false
    private var bookContentRequester = BookContentRequester()
    private var audiobookContentRequester = AudiobookContentRequester()
    
    private var ePUB: EPUBDocument?
    
    init(book: Book, successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        super.init(book: book)
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    //Separate constructor for cases when there's already Book Detail object available
    init(bookDetail: BookDetail, successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        super.init(book: bookDetail)
        self.bookDetail = bookDetail
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    func downloadBookDetail() {
        BookDetailRequester().getBookDetail(bookId: book.bookId, completionHandler: { success, bookDetail in
            if success {
                self.bookDetail = bookDetail
                self.successCallback()
            } else {
                self.failureCallback()
            }
        })
    }
    
    func getBookTitle() -> String {
        return self.bookDetail?.title ?? ""
    }
    
    func getAuthorFullName() -> String {
        //If view model is not available, return data from book passed during init
        guard let bookDetail = self.bookDetail else {
            return Utils.getAuthorLongNames(book: book)
        }
        
        if bookDetail.authorsFull.count > 0 {
            return Utils.getAuthorLongNames(authors: bookDetail.authorsFull)
        } else {
            return Utils.getAuthorLongNames(book: bookDetail)
        }
    }
    
    func getBookCoverImageUrl() -> String {
        return self.bookDetail?.coverUrl ?? ""
    }
    
    func getAbout() -> String {
        return self.bookDetail?.about ?? ""
    }
    
    func getAuthorShortName() -> String {
        guard let bookDetail = self.bookDetail else {
            return ""
        }
        
        return (bookDetail.authorsFull.count > 0) ? Utils.getAuthorNamesWithFirstNameInitials(authors: bookDetail.authorsFull) : Utils.getAuthorNamesWithFirstNameInitials(book: bookDetail)
    }
    
    func getAuthorImageUrl() -> String {
        return self.bookDetail?.authorsFull.first?.image ?? ""
    }
    
    func getReviews() -> [Review] {
        return self.bookDetail?.reviews ?? [Review]()
    }
    
    func getRelated() -> [Book] {
        return self.bookDetail?.relatedBooks ?? [Book]()
    }
    
    func hasAuthorData() -> Bool {
        return self.bookDetail?.authorsFull.count ?? 0 > 0
    }
    
    func getAuthor() -> Author? {
        return self.bookDetail?.authorsFull[0]
    }
    
    func getMatchingBookId() -> Int {
        return self.bookDetail?.matchingBookId ?? 0
    }
    
    func hasMatchingBook() -> Bool {
        return getMatchingBookId() != 0
    }
    
    func getReview(index: Int) -> Review? {
        return self.bookDetail?.reviews[index]
    }
    
    func getDownloads() -> String {
        guard let downloadsCount = self.bookDetail?.downloadsCount else {
            return ""
        }
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter.string(for: downloadsCount) ?? ""
    }
    
    func hasExpandableDescription() -> Bool {
        return getAbout().count > Constants.Misc.MaxBookDetailAboutVisibleCharacters
    }
    
    func getBookDetail() -> BookDetail? {
        return self.bookDetail
    }
    
    //MARK: Book
    func downloadBook(progressHandler: @escaping (Int, Double) -> (), successCallback: @escaping (EPUBDocument) -> (), failureCallback: @escaping () -> ()) {
        self.downloadInProgress = true
        
        //Little delay before actual download makes animations recognisable
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.bookContentRequester.downloadBook(bookId: self.book.bookId, progressHandler: { progress, estimatedSize in
                let percentageProgress = Int(progress*100)
                let estimatedSizeInMB = Double(estimatedSize/1000000)
                progressHandler(percentageProgress, estimatedSizeInMB)
            }, completionHandler: { success, responseData in
                self.downloadInProgress = false
                if let returnedBookData = responseData, success, let document = FileUtils.parseEPUBFromData(bookData: returnedBookData, bookId: self.book.bookId) {
                    self.ePUB = document
                    successCallback(document)
                } else {
                    failureCallback()
                }
            })
        })
    }
    
    func getStoredEPUB() -> EPUBDocument? {
        return FileUtils.getStoredEPUB(bookId: self.book.bookId)
    }
    
    //MARK: Audiobook
    func downloadAudiobook(progressHandler: @escaping (Int, Double) -> (), successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        
        //We download the first chapter 
        guard let chapterId = self.bookDetail?.audiobookChapters[0].chapterId else {
            failureCallback()
            return
        }
        
        self.downloadInProgress = true
        
        //Little delay before actual download makes animations recognisable
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.audiobookContentRequester.downloadChapter(chapterId: chapterId, progressHandler: { progress, estimatedSize in
                let percentageProgress = Int(progress*100)
                let estimatedSizeInMB = Double(estimatedSize/1000000)
                progressHandler(percentageProgress, estimatedSizeInMB)
            }, completionHandler: { success, responseData in
                self.downloadInProgress = false
                if let returnedChapterData = responseData, success {
                    PlayerUtils.storeChapter(chapterData: returnedChapterData, chapterId: chapterId, bookId: self.book.bookId)
                    self.bookDetail?.audiobookChapters[0].isDownloaded = true
                    successCallback()
                } else {
                    failureCallback()
                }
            })
        })
    }
    
    func stopDownloadingBook() {
        self.downloadInProgress = false
        self.book.isAudiobook() ? audiobookContentRequester.cancelDownload() : bookContentRequester.cancelDownload()
    }
    
    func isAudiobook() -> Bool {
        return self.book.isAudiobook()
    }
    
    func isDownloading() -> Bool {
        return downloadInProgress
    }
}
