//
//  BookDetail.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class BookDetail: Book {
    
    var about: String?
    var authorId: String?
    var matchingBookId: Int?
    var downloadsCount: Int?
    var reviewCount: Int?
    
    //For audiobooks, progress represents progress within a given chapter, for regular books this is progress within whole book
    var progress: Float = 0
    var currentAudiobookChapter: Int?
    var authorsFull = [Author]()
    var reviews = [Review]()
    var relatedBooks = [Book]()
    var bookmarks = [Bookmark]()
    var readerCache = [ReaderCache]()
    var audiobookChapters = [AudiobookChapter]()
    
    override init(json: JSON) throws {
        try super.init(json: json)
        self.about = json["about"].stringValue
        self.matchingBookId = json["matching_book"].intValue
        self.downloadsCount = json["downloads_count"].intValue
        self.authorId = json["author_id"].stringValue
        self.reviewCount = json["reviewCount"].intValue
        
        for author in json["authors_full"].arrayValue {
            do {
                try self.authorsFull.append(Author(json: author))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse author from book detail, error: \(error.description)")
            }
        }
        
        for review in json["reviews"].arrayValue {
            do {
                try self.reviews.append(Review(json: review))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse review from book detail, error: \(error.description)")
            }
        }
        
        for book in json["relatedBooks"].arrayValue {
            do {
                try self.relatedBooks.append(Book(json: book))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse related book from book detail, error: \(error.description)")
            }
        }
        
        for chapter in json["chapters"].arrayValue {
            do {
                try self.audiobookChapters.append(AudiobookChapter(json: chapter))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse audiobook chapter from book detail, error: \(error.description)")
            }
        }
    }
    
    init(realmBookDetail: RealmBookDetail) {
        super.init(realmBook: realmBookDetail.book!)
        self.about = realmBookDetail.about
        self.authorId = realmBookDetail.authorId
        self.matchingBookId = realmBookDetail.matchingBookId
        self.downloadsCount = realmBookDetail.downloadsCount
        self.reviewCount = realmBookDetail.reviewCount
        self.progress = realmBookDetail.progress
        self.currentAudiobookChapter = realmBookDetail.currentAudiobookChapter
        
        realmBookDetail.reviews.forEach({
            self.reviews.append(Review(realmReview: $0))
        })
        
        realmBookDetail.relatedBooks.forEach({
            self.relatedBooks.append(Book(realmBook: $0))
        })
        
        realmBookDetail.authors.forEach({
            self.authorsFull.append(Author(realmAuthor: $0))
        })
        
        realmBookDetail.bookmarks.forEach({
            self.bookmarks.append(Bookmark(realmBookmark: $0))
        })
        
        realmBookDetail.cache.forEach({
            self.readerCache.append(ReaderCache(realmReaderCache: $0))
        })
        
        realmBookDetail.audiobookChapters.forEach({
            self.audiobookChapters.append(AudiobookChapter(realmAudiobookChapter: $0))
        })
    }
    
    func getRealmBookDetailObject() -> RealmBookDetail {
        
        let realmBookDetail = RealmBookDetail(value: ["id": bookId, "about": about ?? "", "authorId": authorId ?? "", "type": type, "matchingBookId": matchingBookId ?? 0, "downloadsCount": downloadsCount ?? 0, "reviewCount": reviewCount ?? 0, "book": super.getRealmObject()])
        self.reviews.forEach({
            realmBookDetail.reviews.append($0.getRealmObject())
        })
        self.relatedBooks.forEach({
            realmBookDetail.relatedBooks.append($0.getRealmObject())
        })
        
        if self.authorsFull.count > 0 {
            self.authorsFull.forEach({
                realmBookDetail.authors.append($0.getRealmObject())
            })
        } else {
            self.authors.forEach({
                realmBookDetail.authors.append(RealmAuthor(value: ["name": $0]))
            })
        }
        
        self.audiobookChapters.forEach({
            realmBookDetail.audiobookChapters.append($0.getRealmObject())
        })
        
        return realmBookDetail
    }
}

class RealmBookDetail: Object {
    @objc dynamic var id: Int = 0    
    @objc dynamic var about: String?
    @objc dynamic var authorId: String?
    @objc dynamic var type: String = BookType.Book.rawValue
    @objc dynamic var matchingBookId: Int = 0
    @objc dynamic var downloadsCount: Int = 0
    @objc dynamic var reviewCount: Int = 0
    @objc dynamic var progress: Float = 0.0
    @objc dynamic var currentAudiobookChapter: Int = 0
    @objc dynamic var book: RealmBook? = nil
    let reviews = List<RealmReview>()
    let relatedBooks = List<RealmBook>()
    let authors = List<RealmAuthor>()
    let bookmarks = List<RealmBookmark>()
    let cache = List<RealmReaderCache>()
    let audiobookChapters = List<RealmAudiobookChapter>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

