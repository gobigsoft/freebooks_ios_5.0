//
//  Review.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class Review: NSObject {
    
    var user: String?
    var review: String?
    var addedDate: String?
    var rating: String?
    
    init(json: JSON) throws {
        self.user = json["user"].stringValue
        self.review = json["review"].stringValue
        self.addedDate = json["added"].stringValue
        self.rating = json["rating"].stringValue
    }
    
    init(realmReview: RealmReview) {
        self.user = realmReview.user
        self.review = realmReview.review
        self.addedDate = realmReview.addedDate
        self.rating = realmReview.rating
    }
    
    func getRealmObject() -> RealmReview {
        return RealmReview(value: ["user": user ?? "", "review": review ?? "", "addedDate": addedDate ?? "", "rating": rating])
    }
}

class RealmReview: Object {
    @objc dynamic var user: String?
    @objc dynamic var review: String?
    @objc dynamic var addedDate: String?
    @objc dynamic var rating: String?
}
