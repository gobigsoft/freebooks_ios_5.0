//
//  AuthorDetail.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuthorDetail: Author {
    
    var birthDate: String?
    var deathDate: String?
    var about: String?
    var books = [Book]()
    var audiobooks = [Book]()
    
    override init(json: JSON) throws {
        try super.init(json: json)
        self.birthDate = json["birthDate"].stringValue
        self.deathDate = json["deathDate"].stringValue
        self.about = json["about"].stringValue
        
        for book in json["books"].arrayValue {
            do {
                books.append(try Book(json: book))
            } catch let error as NSError {
                print("Couldn't book detail from author detail, error: \(error.description)")
            }
        }
        
        for audiobook in json["audiobooks"].arrayValue {
            do {
                audiobooks.append(try Book(json: audiobook))
            } catch let error as NSError {
                print("Couldn't audiobook detail from author detail, error: \(error.description)")
            }
        }
    }
}
