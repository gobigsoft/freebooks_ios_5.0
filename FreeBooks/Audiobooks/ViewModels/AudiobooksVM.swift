//
//  AudiobooksViewModel.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AudiobooksVM: NSObject {
    
    private var audiobooks: [Book]?
    
    private var successCallback: (() -> ())!
    private var failureCallback: (() -> ())!
    
    init(successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    func getAudiobooks() {
        AudiobooksRequester().getAudiobooks(completionHandler: { success, audiobooks in
            if success {
                self.audiobooks = audiobooks
                self.successCallback()
            } else {
                self.failureCallback()
            }
        })
    }
    
    func getAudiobooksCount() -> Int {
        return audiobooks?.count ?? 0
    }
    
    func getAudiobook(index: Int) -> Book? {
        return audiobooks?[index]
    }
}
