//
//  AudiobooksVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AudiobooksVC: BaseVC {
    
    @IBOutlet weak private var audiobooksCollection: UICollectionView!
    internal var viewModel: AudiobooksVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerScrollToTopNotificationObserver(notificationName: Constants.NotificationConstants.TabBarSelectionNotifications.AudiobooksSelected)

        showLoading()
        viewModel = AudiobooksVM(successCallback: audiobooksObtained, failureCallback: obtainingAudiobooksFailed)
        viewModel?.getAudiobooks()
    }
    
    private func audiobooksObtained() {
        hideLoading()
        audiobooksCollection.delegate = self
        audiobooksCollection.dataSource = self
    }
    
    private func obtainingAudiobooksFailed() {
        hideLoading()
        
        showNoConnectionView()
    }
    
    @IBAction private func retryPressed() {
        hideNoConnectionView()
        showLoading()
        
        viewModel?.getAudiobooks()
    }
    
    @objc internal func showMorePressed() {
        goToSearch()
    }
    
    override internal func scrollToTop() {
        audiobooksCollection.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
}
