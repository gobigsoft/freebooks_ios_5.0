//
//  AudiobooksDataSource.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension AudiobooksVC: UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Audiobook", for: indexPath) as! BookCell
        if let audiobook = viewModel?.getAudiobook(index: indexPath.row) {
            cell.initWithViewModel(viewModel: BookCellVM(book: audiobook), iconsTint: Constants.Colors.ReddishBrown, labelsColor: UIColor.black, authorLongNames: true)
        }
        cell.updateSeparator(color: Constants.Colors.Brown_10, offsetX: self.view.frame.size.width/4)
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getAudiobooksCount() ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AudiobooksHeader", for: indexPath) as! AudiobooksHeader
            return headerView
        case UICollectionView.elementKindSectionFooter:
            let sectionFooter = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AudiobooksFooter", for: indexPath) as! ActionFooter
            sectionFooter.initWithAction(title: "SHOW MORE", target: self, action: #selector(showMorePressed))
            return sectionFooter
        default:
            return UICollectionReusableView()
        }
    }
}
