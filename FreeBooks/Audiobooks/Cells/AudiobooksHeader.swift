//
//  AudiobooksHeader.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AudiobooksHeader: UICollectionReusableView {
    
    @IBOutlet weak private var headerContainer: UIView!
    @IBOutlet private var icons: [UIImageView]!
    
    override func awakeFromNib() {
        headerContainer.setRoundedCorners(radius: 5)
        headerContainer.layer.borderWidth = 0.5
        headerContainer.layer.borderColor = UIColor.lightGray.cgColor
        setTintColors()
    }
    
    private func setTintColors() {
        for icon in icons {
            UIUtils.tintImageView(imageView: icon, tintColor: Constants.Colors.ReddishBrown_33)
        }
    }
}
