//
//  AuthorDetailVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol ExpandableContentsViewModel {
    func hasExpandableDescription() -> Bool
}

class AuthorDetailVM: NSObject, ExpandableContentsViewModel {
    
    private var authorDetail: AuthorDetail?
    private let author: Author
    
    private var successCallback: (() -> ())!
    private var failureCallback: (() -> ())!
    
    init(author: Author, successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        self.author = author
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    func getAuthorDetail() {
        AuthorDetailRequester().getAuthorDetail(authorId: self.author.authorId, completionHandler: { success, authorDetail in
            if success {
                self.authorDetail = authorDetail
                self.successCallback()
            } else {
                self.failureCallback()
            }
        })
    }
    
    func getAuthorName() -> String {
        return author.name
    }
    
    func getAuthorImageUrl() -> String {
        return author.image
    }
    
    func getAuthorBirthDate() -> String {
        guard let birthDate = self.authorDetail?.birthDate else {
            return ""
        }
        return "* " + birthDate
    }
    
    func getAuthorDeathDate() -> String {
        guard let deathDate = self.authorDetail?.deathDate else {
            return ""
        }
        return "† " + deathDate
    }
    
    func getAuthorAbout() -> String {
        return self.authorDetail?.about ?? ""
    }
    
    func getAuthorBooks() -> [Book] {
        return self.authorDetail?.books ?? [Book]()
    }
    
    func getAuthorAudiobooks() -> [Book] {
        return self.authorDetail?.audiobooks ?? [Book]()
    }
    
    func hasExpandableDescription() -> Bool {
        return getAuthorAbout().count > Constants.Misc.MaxBookDetailAboutVisibleCharacters
    }
}
