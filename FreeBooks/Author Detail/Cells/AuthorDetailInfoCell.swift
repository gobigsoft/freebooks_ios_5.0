//
//  AuthorInfoCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AuthorDetailInfoCell: AuthorCell {
    
    @IBOutlet weak private var birthDate: UILabel!
    @IBOutlet weak private var deathDate: UILabel!
    
    override func initWithViewModel(viewModel: AuthorDetailVM) {
        super.initWithViewModel(viewModel: viewModel)
        birthDate.text = viewModel.getAuthorBirthDate()
        deathDate.text = viewModel.getAuthorDeathDate()
    }
}
