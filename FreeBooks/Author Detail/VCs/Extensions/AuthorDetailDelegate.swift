//
//  AuthorDetailDelegate.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension AuthorDetailVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: heightForAuthorDetailIndexPath(indexPath: indexPath))
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let authorDetailSection = AuthorDetailSections.allValues[section]
        switch authorDetailSection {
        case .Info:
            return CGSize(width: collectionView.frame.size.width, height: CGFloat(168))
        default:
            return CGSize(width: collectionView.frame.size.width, height: CGFloat(0))
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let authorDetailSection = AuthorDetailSections.allValues[section]
        switch authorDetailSection {
        case .About:
            if let authorDetailViewModel = viewModel, authorDetailViewModel.hasExpandableDescription() {
                return CGSize(width: collectionView.frame.size.width, height: CGFloat(60))
            } else {
                return CGSize(width: collectionView.frame.size.width, height: CGFloat(0))
            }
        default:
            return CGSize(width: collectionView.frame.size.width, height: CGFloat(0))
        }
    }
    
    private func heightForAuthorDetailIndexPath(indexPath: IndexPath) -> CGFloat {
        //Make sure viewmodel is available for determining contents of author detail screen
        guard let authorDetailViewModel = viewModel else {
            return 1
        }
        
        switch AuthorDetailSections.allValues[indexPath.section] {
        case .Info:
            return 105.0
        case .About:
            return authorDetailViewModel.getAuthorAbout().count > 0 ? 220.0 : 1
        case .Books:
            return authorDetailViewModel.getAuthorBooks().count > 0 ? 255.0 : 1
        case .Audiobooks:
            return authorDetailViewModel.getAuthorAudiobooks().count > 0 ? 255.0 : 1
        }
    }
}
