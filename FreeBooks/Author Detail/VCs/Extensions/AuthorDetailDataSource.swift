//
//  AuthorDetailDataSource.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

internal enum AuthorDetailSections: String {
    case Info = "Info"
    case About = "About"
    case Books = "Books"
    case Audiobooks = "Audiobooks"
    
    static let allValues = [Info, About, Books, Audiobooks]
}

extension AuthorDetailVC: UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let authorDetailSection = AuthorDetailSections.allValues[indexPath.section]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: authorDetailSection.rawValue, for: indexPath)
        
        guard let authorDetailViewModel = self.viewModel else {
            return cell
        }
        
        switch authorDetailSection {
        case .Info:
            (cell as! AuthorDetailInfoCell).initWithViewModel(viewModel: authorDetailViewModel)
            return cell
        case .About:
            (cell as! AboutCell).initWithAbout(aboutText: authorDetailViewModel.getAuthorAbout(), isExpandable: authorDetailViewModel.hasExpandableDescription())
            return cell
        case .Books:
            (cell as! BaseBookSectionCell).initWithViewModel(viewModel: BaseBookSectionVM(books: authorDetailViewModel.getAuthorBooks()), delegate: self, bookCellIconsTintColor: Constants.Colors.Gold, bookCellLabelsColor: UIColor.white)
            return cell
        case .Audiobooks:
            (cell as! BaseBookSectionCell).initWithViewModel(viewModel: BaseBookSectionVM(books: authorDetailViewModel.getAuthorAudiobooks()), delegate: self, bookCellIconsTintColor: Constants.Colors.Gold, bookCellLabelsColor: UIColor.white)
            return cell
        }
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return AuthorDetailSections.allValues.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ImageHeader", for: indexPath) as! ImageHeader
            if let authorDetailViewModel = viewModel {
                headerView.initWithImageUrl(imageUrl: authorDetailViewModel.getAuthorImageUrl())
            }
            return headerView
        case UICollectionView.elementKindSectionFooter:
            let sectionFooter = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AuthorDetailActionFooter", for: indexPath) as! ActionFooter
            switch AuthorDetailSections.allValues[indexPath.section] {
            case .About:
                sectionFooter.initWithAction(title: "SHOW MORE", target: self, action: #selector(showMoreAboutPressed))
                return sectionFooter
            default:
                assert(false, "Invalid section type")
            }
            return sectionFooter
        default:
            return UICollectionReusableView()
        }
    }
}
