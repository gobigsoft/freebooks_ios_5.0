//
//  AuthorDetailVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AuthorDetailVC: BaseDetailVC, BookSectionSelectionDelegate {
    
    @IBOutlet weak private var authorDetailCollection: UICollectionView!
    internal var viewModel: AuthorDetailVM?

    override func viewDidLoad() {
        super.viewDidLoad()
        //Put collectionview contents below status bar
        authorDetailCollection.contentInset.top = -UIApplication.shared.statusBarFrame.height
        showLoading(color: Constants.Colors.Gold)
    }
    
    func initWithAuthor(author: Author) {
        viewModel = AuthorDetailVM(author: author, successCallback: authorDetailsObtained, failureCallback: obtainingAuthorDetailsFailed)
        viewModel?.getAuthorDetail()
    }
    
    private func authorDetailsObtained() {
        hideLoading()
        authorDetailCollection.delegate = self
        authorDetailCollection.dataSource = self
    }
    
    private func obtainingAuthorDetailsFailed() {
        hideLoading()
        
        showNoConnectionView()
    }
    
    @IBAction private func retryPressed() {
        hideNoConnectionView()
        showLoading(color: Constants.Colors.Gold)
        
        viewModel?.getAuthorDetail()
    }
    
    @objc internal func showMoreAboutPressed() {
        if let authorDetailViewModel = viewModel {
            showAbout(aboutTitle: authorDetailViewModel.getAuthorName(), about: authorDetailViewModel.getAuthorAbout())
        }
    }
    
    internal func bookSelected(book: Book) {
        showBookDetail(book: book)
    }
}
