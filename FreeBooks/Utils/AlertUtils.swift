//
//  AlertUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AlertUtils: NSObject {

    //inRect is needed for iPad which require a precise location of where the popover controller should be displayed
    public static func showActionSheet(title: String? = nil, message: String? = nil, actions: [UIAlertAction], inVC: UIViewController, inRect: CGRect? = nil) {
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        actions.forEach({ actionSheet.addAction($0) })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(cancel)
        
        if let popoverController = actionSheet.popoverPresentationController, let rect = inRect {
            popoverController.sourceView = inVC.view
            popoverController.sourceRect = rect
            popoverController.permittedArrowDirections = [.up]
        }
 
        inVC.present(actionSheet, animated: true, completion: nil)
    }
    
    public static func showAlert(title: String? = nil, message: String? = nil, actions: [UIAlertAction], cancelAction: UIAlertAction? = nil, inVC: UIViewController) {
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach({ actionSheet.addAction($0) })
    
        if let customCancelAction = cancelAction {
            actionSheet.addAction(customCancelAction)
        } else {
            let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            actionSheet.addAction(okAction)
        }

        inVC.present(actionSheet, animated: true, completion: nil)
    }
    
    public static func createMigrationLoadingAlert(booksToMigrateCount: Int) -> UIAlertController {
        let alert = UIAlertController(title: "Migrating from the older version", message: "Please don't turn off the network. \n 1 of \(booksToMigrateCount)", preferredStyle: .alert)
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        
        alert.view.addSubview(activityIndicator)
        alert.view.heightAnchor.constraint(equalToConstant: 155).isActive = true
        
        activityIndicator.centerXAnchor.constraint(equalTo: alert.view.centerXAnchor, constant: 0).isActive = true
        activityIndicator.bottomAnchor.constraint(equalTo: alert.view.bottomAnchor, constant: -25).isActive = true
        
        return alert
    }
}
