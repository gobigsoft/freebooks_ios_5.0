//
//  UIUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 27/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import MaterialShowcase

class UIUtils: NSObject {
    
    public static func blurImageView(imageView: UIImageView) {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = imageView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.addSubview(blurEffectView)
    }
    
    public static func tintImageView(imageView: UIImageView, tintColor: UIColor) {
        imageView.image = imageView.image!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = tintColor
    }
    
    public static func addBottomFadeOutGradient(view: UIView) {
        let gradient = CAGradientLayer()
        //iPhone 7,8+ and XS MAX sizes have trouble with the gradient's width in cases where it equals the view's bounds, by doubling it we ensure it displays correctly on every screen
        gradient.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width*2, height: view.bounds.size.height)
        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0.7, 1]
        view.layer.mask = gradient
    }
    
    public static func dropButtonShadow(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view.layer.shadowOpacity = 0.2
        view.layer.masksToBounds = false
    }
    
    public static func dropBottomShadow(view: UIView) {
        dropShadowWithOffset(view: view, shadowOffset: CGSize(width: 0, height: 1))
    }
    
    public static func dropTopShadow(view: UIView) {
        dropShadowWithOffset(view: view, shadowOffset: CGSize(width: 0, height: -1))
    }
    
    public static func dropShadowWithOffset(view: UIView, shadowOffset: CGSize, radius: CGFloat = 1) {
        view.layer.shadowOffset = shadowOffset
        view.layer.shadowRadius = radius
        view.layer.shadowOpacity = 0.3
        view.layer.masksToBounds = false
    }
    
    public static func dropShadow(view: UIView) {
        dropShadowWithOffset(view: view, shadowOffset: CGSize(width: 0, height: 0), radius: 3)
    }
    
    public static func addBorder(view: UIView, color: UIColor) {
        view.layer.borderWidth = 1
        view.layer.borderColor = color.cgColor
    }
    
    public static func showHint(inView: UIView, title: String, subtitle: String, completion: @escaping () -> ()) {
        let searchHint = MaterialShowcase()
        searchHint.setTargetView(view: inView)
        searchHint.primaryText = title
        searchHint.secondaryText = subtitle
        searchHint.backgroundPromptColor = Constants.Colors.Brown
        searchHint.secondaryTextSize = 18
        searchHint.primaryTextSize = 23
        searchHint.secondaryTextColor =  UIColor.init(white: 1, alpha: 0.45)
        searchHint.show(completion: completion)
    }
}
