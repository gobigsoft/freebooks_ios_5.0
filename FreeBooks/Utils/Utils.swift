//
//  Utils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    public static func getAuthorNamesWithFirstNameInitials(book: Book) -> String {
        return processNamesArray(names: book.authors)
    }
    
    public static func getAuthorNamesWithFirstNameInitials(author: Author) -> String {
        return processNamesArray(names: [author.name])
    }
    
    public static func getAuthorNamesWithFirstNameInitials(authors: [Author]) -> String {
        var authorNames = ""
        for i in 0..<authors.count {
            authorNames.append(getAuthorNamesWithFirstNameInitials(author: authors[i]))
            guard i != authors.count-1 else {
                continue
            }
            authorNames.append(", ")
        }
        return authorNames
    }
    
    private static func processNamesArray(names: [String]) -> String {
        var authorsWithFirstNameInitials = ""
        for author in names {
            let authorNames = author.split(separator: " ")
            if authorNames.count == 1 {
                authorsWithFirstNameInitials.append(String(authorNames[0]))
            } else if authorNames.count > 1 {
                let firstName = authorNames.first
                guard let initial = firstName?.first else {
                    break
                }
                authorsWithFirstNameInitials.append(String(initial)+".")
                for i in 1..<authorNames.count {
                    authorsWithFirstNameInitials.append(" " + authorNames[i])
                }
            }
        }
        return authorsWithFirstNameInitials
    }
    
    public static func getAuthorLongNames(book: Book) -> String {
        var authorNames = ""
        for i in 0..<book.authors.count {
            i == 0 ? authorNames.append(book.authors[i]) : authorNames.append(String(format: ", %@", book.authors[i]))
        }
        return authorNames
    }
    
    public static func getAuthorLongNames(authors: [Author]) -> String {
        var authorNames = ""
        for i in 0..<authors.count {
            i == 0 ? authorNames.append(authors[i].name) : authorNames.append(String(format: ", %@", authors[i].name))
        }
        return authorNames
    }
    
    public static func getBookRating(book:Book) -> Double {
        guard let rating = Double(book.rating) else {
            return 0.0
        }
        if rating == 0 {
            return Constants.Misc.DefaultRating 
        } else {
            return rating
        }
    }
}

extension NSMutableAttributedString {
    convenience init (fullString: String, fullStringColor: UIColor, subString: String, subStringColor: UIColor, substringFontSize: CGFloat) {
        let rangeOfSubString = (fullString.lowercased() as NSString).range(of: subString.lowercased())
        let rangeOfFullString = NSRange(location: 0, length: fullString.lowercased().count)
        let attributedString = NSMutableAttributedString(string:fullString)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: fullStringColor, range: rangeOfFullString)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: subStringColor, range: rangeOfSubString)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: substringFontSize, weight: UIFont.Weight.bold), range: rangeOfSubString)
        self.init(attributedString: attributedString)
    }
}

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    var releaseVersionNumberPretty: String {
        return "Version \(releaseVersionNumber ?? "1.0.0")"
    }
}
