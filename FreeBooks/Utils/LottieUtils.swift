//
//  LottieUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Lottie

class LottieUtils: NSObject {
    public static func showAnimation(named: String, inView: UIView, playOnce: Bool = false, startPaused: Bool = false, completion: @escaping () -> ()) {
        let animationView = AnimationView(name: named)
        animationView.frame = CGRect(x: 0, y: 0, width: inView.frame.size.width, height: inView.frame.size.height)
        animationView.loopMode = playOnce == true ? LottieLoopMode.playOnce : LottieLoopMode.loop
        
        startPaused == false ? animationView.play(completion: { finished in
            if finished == true {
                completion()
            }
        }) : animationView.pause()
        
        inView.addSubview(animationView)
    }
}


