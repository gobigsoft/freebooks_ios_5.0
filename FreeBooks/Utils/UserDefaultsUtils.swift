//
//  UserDefaultsUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class UserDefaultsUtils: NSObject {
    
    public static func setReaderControlsHintAsDisplayed() {
        setTrueToDefaults(actionKey: Constants.UserDefaultsKeys.ReaderControlsHintDisplayed)
    }
    
    public static func wasReaderControlsHintDisplayed() -> Bool {
        return getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.ReaderControlsHintDisplayed)
    }
    
    //MARK: Walkthrough
    public static func setWalkthroughAsDisplayed() {
        setTrueToDefaults(actionKey: Constants.UserDefaultsKeys.WalkthroughDisplayed)
    }
    
    public static func wasWalkthroughDisplayed() -> Bool {
        return getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.WalkthroughDisplayed)
    }
    
    //MARK: Search hint
    public static func setSearchHintAsDisplayed() {
        setTrueToDefaults(actionKey: Constants.UserDefaultsKeys.SearchHintDisplayed)
    }
    
    public static func wasSearchHintDisplayed() -> Bool {
        return getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.SearchHintDisplayed)
    }

    //MARK: Ad removal
    public static func setAdsRemovedPurchased() {
        setTrueToDefaults(actionKey: Constants.UserDefaultsKeys.AdsRemoved)
    }
    
    public static func areAdsRemoved() -> Bool {
        return getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.AdsRemoved)
    }

    //MARK: Audiobooks purchase from the former app
    public static func setAudiobooksPurchased() {
        setTrueToDefaults(actionKey: Constants.UserDefaultsKeys.AudiobooksPurchased)
    }
    
    public static func areAudiobooksPurchased() -> Bool{
        return getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.AudiobooksPurchased)
    }
    
    //MARK: Audiobooks subscription
    public static func setAudiobooksSubscriptionActive() {
        LoggingUtils.logMessage(message: "Setting audiobooks subscription as active")
        setTrueToDefaults(actionKey: Constants.UserDefaultsKeys.AudiobooksSubscriptionActive)
    }
    
    public static func setAudiobooksSubscriptionInactive() {
        LoggingUtils.logMessage(message: "Setting audiobooks subscription as inactive")
        setFalseToDefaults(actionKey: Constants.UserDefaultsKeys.AudiobooksSubscriptionActive)
    }
    
    public static func isAudiobooksSubscriptionActive() -> Bool {
        LoggingUtils.logMessage(message: "Returning audiobooks subscription state - \(getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.AudiobooksSubscriptionActive))")
        return getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.AudiobooksSubscriptionActive)
    }
    
    //MARK: Analytics
    public static func areAnalyticsEnabled() -> Bool {
        return getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.AnalyticsEnabled)
    }
    
    public static func setAnalyticsAsEnabled() {
        setTrueToDefaults(actionKey: Constants.UserDefaultsKeys.AnalyticsEnabled)
    }
    
    public static func setAnalyticsAsDisabled() {
        setFalseToDefaults(actionKey: Constants.UserDefaultsKeys.AnalyticsEnabled)
    }
    
    public static func setAnalyticsSettingsAsOverriden() {
        setTrueToDefaults(actionKey: Constants.UserDefaultsKeys.AnalyticsSettingsOverridenByUser)
    }
    
    public static func wereAnalyticsSettingsAlreadyOverriden() -> Bool {
        return getBoolFromDefaults(actionKey: Constants.UserDefaultsKeys.AnalyticsSettingsOverridenByUser)
    }
    
    //MARK: Playback
    public static func getUnfinishedPlaybackAudiobookId() -> Int {
        return UserDefaults.standard.integer(forKey: Constants.UserDefaultsKeys.UnfinishedPlaybackAudiobookId)
    }
    
    public static func setUnfinishedPlaybackAudiobookId(bookId: Int) {
        UserDefaults.standard.set(bookId, forKey: Constants.UserDefaultsKeys.UnfinishedPlaybackAudiobookId)
    }
    
    //There's no book with ID 0, therefore receiving 0 here means there's no unfinished playback. We are also deliberately storing "0" here when the app goes into background without any ongoing playback.
    public static func isThereAnyUnfinishedAudiobookPlayback() -> Bool {
        return !(getUnfinishedPlaybackAudiobookId() == Constants.Misc.NoUnfinishedPlaybackAudiobookId)
    }
    
    //MARK: Base functions
    private static func setTrueToDefaults(actionKey: String) {
        UserDefaults.standard.set(true, forKey: actionKey)
    }
    
    private static func setFalseToDefaults(actionKey: String) {
        UserDefaults.standard.set(false, forKey: actionKey)
    }
    
    private static func getBoolFromDefaults(actionKey: String) -> Bool {
        return UserDefaults.standard.bool(forKey: actionKey)
    }
}
