//
//  DeviceUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 06/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class DeviceUtils: NSObject {
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6_7        = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P_7P      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_XR         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        static let IS_IPHONE_XS_MAX      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO_11       = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1194.0
        static let IS_IPAD_PRO_10_5       = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1112.0
        static let IS_IPAD_PRO_12_9       = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    }
    
    public static func isIpad() -> Bool {
        return (DeviceType.IS_IPAD_PRO_12_9 || DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO_11 || DeviceType.IS_IPAD_PRO_10_5)
    }
    
    private static func isIpadPro3Gen() -> Bool {
        return UIDevice.modelName.contains("iPad Pro (12.9-inch) (3rd generation)")
    }
    
    //Bezeless device needs Player's miniature to be bigger so it covers the bottom safe area
    public static func isBezelessDevice() -> Bool {
        return (DeviceUtils.DeviceType.IS_IPHONE_X || DeviceUtils.DeviceType.IS_IPHONE_XR || DeviceUtils.DeviceType.IS_IPHONE_XS_MAX || DeviceUtils.DeviceType.IS_IPAD_PRO_11 || (DeviceUtils.DeviceType.IS_IPAD_PRO_12_9 && DeviceUtils.isIpadPro3Gen()))
    }
}
