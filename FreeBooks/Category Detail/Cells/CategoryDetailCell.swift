//
//  CategoryDetailCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 01/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class CategoryDetailCell: UICollectionViewCell, SwipeableSelectionControlDelegate, SwipeableSelectionPageVCManagerDelegate {

    @IBOutlet weak private var pageVCContainer: UIView!
    @IBOutlet weak private var swipeableSelectionControlContainer: UIView!
    
    private var selectionPageVCManager: SwipeableSelectionPageVCManager?
    private var control: SwipeableSelectionControl?
    
    private var selectionVCs = [SwipeableSelectionVC]()
    private var bookSelectionCallback: ((Book) -> ())?
    
    func initWithCategory(category: Category, bookSelectionCallback: @escaping (Book) -> ()) {
        self.bookSelectionCallback = bookSelectionCallback
        
        setupSelectionVCs(category: category)
        selectionPageVCManager = SwipeableSelectionPageVCManager(viewControllers: selectionVCs, containerView: pageVCContainer)
        selectionPageVCManager?.delegate = self

        control = SwipeableSelectionControl(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 50), optionTitles: ["BOOKS", "AUDIOBOOKS"])
        control?.delegate = self
        
        swipeableSelectionControlContainer.addSubview(control!)
    }
    
    private func setupSelectionVCs(category: Category) {
        let booksVC = UIStoryboard.init(name: Constants.StoryboardConstants.StoryboardMain, bundle: nil).instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.CategoryDetailBooksVC) as! CategoryDetailBooksVC
        booksVC.initWithCategoryId(categoryId: category.bookCategoryId, bookType: BookType.Book, bookSelectionCallback: bookSelected(book:))
      
        let audiobooksVC = UIStoryboard.init(name: Constants.StoryboardConstants.StoryboardMain, bundle: nil).instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.CategoryDetailBooksVC) as! CategoryDetailBooksVC
        audiobooksVC.initWithCategoryId(categoryId: category.audioCategoryId, bookType: BookType.Audiobook, bookSelectionCallback: bookSelected(book:))
        selectionVCs.append(contentsOf: [booksVC, audiobooksVC])
    }
    
    private func bookSelected(book: Book) {
        bookSelectionCallback?(book)
    }
    
    func disableSelectionVCsInteraction() {
        selectionVCs.forEach({($0 as? CategoryDetailBooksVC)?.disableCollectionViewInteraction()})
    }
    
    func enableSelectionVCsInteraction() {
        selectionVCs.forEach({($0 as? CategoryDetailBooksVC)?.enableColletionViewInteraction()})
    }
    
    func sortBooks(sorting: CategoryBooksSorting) {
        selectionVCs.forEach({($0 as? CategoryDetailBooksVC)?.sort(sorting: sorting)})
    }
    
    //MARK: Swipeable Selection Page VC Delegate
    internal func scrollingCompletion(completedPercentage: CGFloat) {
        control?.moveSelectionView(completedPercentage: completedPercentage)
    }
    
    internal func scrollingToIndex(index: Int) {
        control?.setDestinationIndex(destinationIndex: index)
    }
    
    internal func scrolledToIndex(index: Int) {
        control?.setSelectedVisual(itemIndex: index)
    }
    
    //MARK: Swipeable Selection Control delegate
    internal func itemSelected(itemIndex: Int) {
        selectionPageVCManager?.scrollToIndex(index: itemIndex)
    }
}
