//
//  CategoryDetailVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 01/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class CategoryDetailVM: NSObject {
    
    let categoryId: Int
    let bookType: BookType
    let count = 30
    
    private var books = [Book]()
    
    private var successCallback: (() -> ())!
    private var failureCallback: (() -> ())!
    
    init(categoryId: Int, bookType: BookType, successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        self.categoryId = categoryId
        self.bookType = bookType
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }

    func getBooks(sorting: CategoryBooksSorting = .popularity) {
        CategoryDetailRequester().getBooksForCategory(categoryId: categoryId, offset: books.count, count: count, sort: sorting.rawValue, completionHandler: { success, books in
            if let obtainedBooks = books, success == true {
                self.books.append(contentsOf: obtainedBooks)
                self.successCallback()
            } else {
                self.failureCallback()
            }
        })
    }
    
    func removeBooks() {
        self.books.removeAll()
    }
    
    func getBooksCount() -> Int {
        return books.count
    }
    
    func getBook(index: Int) -> Book? {
        guard books.indices.contains(index) else {
            return nil
        }
        return books[index]
    }
}
