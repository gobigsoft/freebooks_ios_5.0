//
//  CategoryDetailBooksVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 01/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class CategoryDetailBooksVC: SwipeableSelectionVC {
    
    @IBOutlet weak private var booksCollection: UICollectionView!
    @IBOutlet weak private var emptyView: UIView!
    @IBOutlet weak private var emptyIcon: UIImageView!
    
    private var bookType: BookType?
    private var categoryId: Int?
    private var viewModel: CategoryDetailVM?
    
    private var bookSelectionCallback: ((Book) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyIcon.tintColor = Constants.Colors.GrayLight
        
        self.booksCollection.dataSource = self
        self.booksCollection.delegate = self
        
        viewModel?.getBooks()
        
        disableCollectionViewInteraction()
    }
    
    func initWithCategoryId(categoryId: Int, bookType: BookType, bookSelectionCallback: @escaping (Book) -> ()) {
        self.bookSelectionCallback = bookSelectionCallback
        self.categoryId = categoryId
        self.bookType = bookType
        self.viewModel = CategoryDetailVM(categoryId: categoryId, bookType: bookType, successCallback: categoryBooksObtained, failureCallback: obtainingCategoryBooksFailed)
    }
    
    func disableCollectionViewInteraction() {
        self.booksCollection?.isScrollEnabled = false
    }
    
    func enableColletionViewInteraction() {
        self.booksCollection?.isScrollEnabled = true
    }
    
    func sort(sorting: CategoryBooksSorting) {
        scrollToTopIfAvailable()
        self.viewModel?.removeBooks()
        self.viewModel?.getBooks(sorting: sorting)
    }
    
    private func scrollToTopIfAvailable() {
        guard let viewModel = self.viewModel, viewModel.getBooksCount() > 0 else {
            return
        }
        self.booksCollection?.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
    }
    
    private func categoryBooksObtained() {
        self.booksCollection?.reloadData()
        handleEmpty()
    }
    
    private func obtainingCategoryBooksFailed() {
        
    }
    
    private func handleEmpty() {
        booksCollection?.isHidden = (viewModel?.getBooksCount() ?? 0) == 0
        emptyView?.isHidden = (viewModel?.getBooksCount() ?? 0) > 0
    }
}

extension CategoryDetailBooksVC: UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailBook", for: indexPath) as! BookCell
        cell.updateSeparator(color: Constants.Colors.Brown_10, offsetX: self.view.frame.size.width/4)
        
        if let book = self.viewModel?.getBook(index: indexPath.row) {
            cell.initWithViewModel(viewModel: BookCellVM(book: book), iconsTint: Constants.Colors.ReddishBrown, labelsColor: UIColor.black)
        }
        
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel?.getBooksCount() ?? 0
    }
}

extension CategoryDetailBooksVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate  {
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 150)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedBook = viewModel?.getBook(index: indexPath.row) else {
            return
        }
        
        bookSelectionCallback?(selectedBook)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let viewModel = self.viewModel else {
            return
        }
        
        if indexPath.row == (viewModel.getBooksCount()-1){
            self.viewModel?.getBooks()
        }
    }

    //MARK: Scroll view delegates
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.booksCollection?.isScrollEnabled = (scrollView.contentOffset.y > 0)
    }
}

