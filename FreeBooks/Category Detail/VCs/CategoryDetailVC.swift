//
//  CategoryDetailVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 01/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

enum CategoryBooksSorting: Int {
    case popularity = 1
    case rating = 2
    case author = 3
}

class CategoryDetailVC: BaseVC {
    
    private let CategoryDetailCellIdentifier = "CategoryDetailCell"
    
    private let DefaultIconScaleTranslation = CGFloat(1.5)
    private let DefaultYTranslation = CGFloat(-150)
    private let FinalOffset = CGFloat(75)
    
    @IBOutlet weak private var categoryDetailCollection: UICollectionView!
    @IBOutlet weak private var bgImage: UIImageView!
    @IBOutlet weak private var bgImageContainer: UIView!
    @IBOutlet weak private var icon: UIImageView!
    @IBOutlet weak private var name: UILabel!
    
    private var category: Category?
    private var categoryDetailCell: CategoryDetailCell?
    
    private var animator: UIViewPropertyAnimator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        categoryDetailCollection.delegate = self
        categoryDetailCollection.dataSource = self
        
        categoryDetailCell?.disableSelectionVCsInteraction()
        setupAnimator()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setup() {
        bgImage.image = UIImage(named: category?.bgName ?? "")
        icon.image = UIImage(named: category?.iconName ?? "")
        name.text = category?.name
        name.sizeToFit()
    }
    
    func initWithCategory(category: Category) {
        self.category = category
    }
    
    private func setupAnimator() {
        animator = UIViewPropertyAnimator(duration: 1, curve: .linear) {
            self.bgImageContainer.alpha = 0
            self.icon.transform = CGAffineTransform(scaleX: self.DefaultIconScaleTranslation, y: self.DefaultIconScaleTranslation).translatedBy(x: 0, y: self.DefaultYTranslation)
            
            LoggingUtils.logMessage(message: "Translation x: \(60-self.name.frame.origin.x) translation y: \(36-self.name.frame.origin.y)")
            self.name.transform = CGAffineTransform(translationX: 60-self.name.frame.origin.x, y: 58-self.name.frame.origin.y)
        }
        animator?.pausesOnCompletion = true
    }
    
    internal func handleAnimationCompletionForOffset(offset: CGFloat) {
        let fractionComplete = offset/FinalOffset
        animator?.fractionComplete = fractionComplete
        LoggingUtils.logMessage(message: "Category detail animaction fraction complete: \(fractionComplete)")
        if fractionComplete == 1 {
            categoryDetailCell?.enableSelectionVCsInteraction()
        } else {
            categoryDetailCell?.disableSelectionVCsInteraction()
        }
    }
    
    internal func handleDefaultPositionScroll(offset: CGFloat) {
        if offset > FinalOffset/2 {
            categoryDetailCollection?.setContentOffset(CGPoint(x: 0, y: FinalOffset), animated: true)
        } else {
            categoryDetailCollection?.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
    
    internal func bookSelected(book: Book) {
        showBookDetail(book: book)
    }
    
    @IBAction override func backPressed() {
        //Category icon stays visible during dismissing making the whole dismiss a little clunky, we'll rather hide it
        icon.isHidden = true
        super.backPressed()
    }
    
    @IBAction private func changeSorting(sender: UIButton) {
        let popularitySortingAction = UIAlertAction(title: "Popularity", style: .default, handler: { action in
            self.categoryDetailCell?.sortBooks(sorting: .popularity)
        })
        let ratingSortingAction = UIAlertAction(title: "Rating", style: .default, handler: { action in
            self.categoryDetailCell?.sortBooks(sorting: .rating)
        })
        let authorSortingAction = UIAlertAction(title: "Author", style: .default, handler: { action in
            self.categoryDetailCell?.sortBooks(sorting: .author)
        })
        
        AlertUtils.showActionSheet(title: "Sort by", message: nil, actions: [popularitySortingAction, ratingSortingAction, authorSortingAction], inVC: self, inRect: sender.frame)
    }
}

extension CategoryDetailVC: UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryDetailCellIdentifier, for: indexPath) as! CategoryDetailCell

        if let category = self.category {
            cell.initWithCategory(category: category, bookSelectionCallback: bookSelected(book:))
        }
        if categoryDetailCell == nil {
            categoryDetailCell = cell
        }
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
}

extension CategoryDetailVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: collectionView.frame.size.height)
    }
    
    //MARK: Scroll view delegates
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        handleAnimationCompletionForOffset(offset: scrollView.contentOffset.y)
    }
    
    //MARK: Default position scrolling
    internal func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if categoryDetailCollection?.isDecelerating == false {
            handleDefaultPositionScroll(offset: scrollView.contentOffset.y)
        }
    }
    
    internal func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        handleDefaultPositionScroll(offset: scrollView.contentOffset.y)
    }
}
