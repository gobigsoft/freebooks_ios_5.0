//
//  MyLibraryBookCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class MyLibraryBookCell: SeparatorCell, ChapterDownloaderDelegate {
    
    @IBOutlet private weak var image: UIImageView!
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var author: UILabel!
    @IBOutlet private weak var progress: UIProgressView!
    
    @IBOutlet private weak var downloadStateIcon: UIImageView!
    @IBOutlet private weak var topBookIcon: UIImageView!
    @IBOutlet private weak var audiobookIcon: UIImageView!
    
    @IBOutlet weak var actionsBtn: UIButton!
    
    @IBOutlet weak var loadingContainer: UIView!
    var loadingIndicator: LoadingIndicator?
    
    private var chapterDownloader: ChapterDownloader?
    private var bookId: Int?
    
    override func awakeFromNib() {
        addSeparator(color: Constants.Colors.Brown_10)
        self.progress.transform = self.progress.transform.scaledBy(x: 1, y: 2)
    }
    
    func initWithViewModel(viewModel: MyLibraryBookCellVM) {
        self.image.kf.setImage(with: URL(string: viewModel.getBookUrl()), options: [.transition(.fade(0.2))])
        UIUtils.dropShadow(view: self.image)
        
        self.title.text = viewModel.getBookName()
        self.author.text = viewModel.getAuthorsLongNames()
        
        if viewModel.getBookType() == BookType.Audiobook {
            self.progress.progress = viewModel.getAudiobookProgress()
            self.bookId = viewModel.getBookId()
            searchForCurrentlyDownloadedAudiobookChapters(bookId: self.bookId ?? 0)
        } else {
            self.progress.progress = viewModel.getBookProgress()
        }
        
        setupIcons(viewModel: viewModel)
    }
    
    private func searchForCurrentlyDownloadedAudiobookChapters(bookId: Int) {
        if let chapterDownloader = PlayerEngine.sharedEngine.getChapterDownloader(bookId: bookId) {
            self.chapterDownloader = chapterDownloader
            self.chapterDownloader?.delegate = self
            showChapterLoadingIndicator()
        } else {
            hideChapterLoadingIndicator()
        }
    }
    
    private func setupIcons(viewModel: MyLibraryBookCellVM) {
        self.downloadStateIcon.tintColor = Constants.Colors.ReddishBrown
        self.topBookIcon.tintColor = Constants.Colors.ReddishBrown
        self.audiobookIcon.tintColor = Constants.Colors.ReddishBrown
        
        self.downloadStateIcon.isHidden = false
        self.topBookIcon.isHidden = !viewModel.isTop()
        self.audiobookIcon.isHidden = !(viewModel.getBookType() == .Audiobook)
    }
    
    private func showChapterLoadingIndicator() {
        if loadingIndicator == nil {
             loadingIndicator = LoadingIndicator(frame: CGRect(x: 0, y: 2, width: 10, height: 10), color: Constants.Colors.ReddishBrown, lineWidth: 2)
             loadingContainer.addSubview(loadingIndicator!)
        }
        
        if loadingContainer.isHidden {
            loadingContainer.isHidden = false
            loadingIndicator?.startAnimating()
        }
    }
    
    private func hideChapterLoadingIndicator() {
        if !loadingContainer.isHidden {
            loadingContainer.isHidden = true
            loadingIndicator?.stopAnimating()
        }
    }
    
    internal func chapterDownloadUpdated(percentageProgress: Int) {
        //We don't track a progress here
    }
    
    internal func chapterDownloaded() {
        if let bookId = self.bookId {
            //Since this callback is returned only for a single chapter, we need to make another check, to make sure there are no other chapters being downloaded
            searchForCurrentlyDownloadedAudiobookChapters(bookId: bookId)
        }
    }
    
    internal func chapterDownloadFailed() {
        if let bookId = self.bookId {
            //Since this callback is returned only for a single chapter, we need to make another check, to make sure there are no other chapters being downloaded
            searchForCurrentlyDownloadedAudiobookChapters(bookId: bookId)
        }
    }
}
