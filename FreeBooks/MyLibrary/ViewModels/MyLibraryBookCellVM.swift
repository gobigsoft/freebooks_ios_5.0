//
//  MyLibraryBookCellVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class MyLibraryBookCellVM: BookCellVM {
    
    let storedBook: BookDetail
    
    init(storedBook: BookDetail) {
        self.storedBook = storedBook
        super.init(book: storedBook)
    }
    
    func getBookProgress() -> Float {
        return storedBook.progress
    }
    
    func getAudiobookProgress() -> Float {
        return Float((storedBook.currentAudiobookChapter ?? 0)+1)/Float(storedBook.audiobookChapters.count)
    }
    
    override func getAuthorsLongNames() -> String {
        return Utils.getAuthorLongNames(authors: storedBook.authorsFull)
    }
}
