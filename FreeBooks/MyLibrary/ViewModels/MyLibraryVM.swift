//
//  MyLibraryVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class MyLibraryVM: NSObject {
    
    var storedBooks: [BookDetail]
    
    init(storedBooks: [BookDetail]) {
        self.storedBooks = storedBooks
    }
    
    func getStoredBook(index: Int) -> BookDetail {
        return storedBooks[index]
    }
    
    func deleteBook(book: BookDetail) {
        storedBooks.remove(object: book)
    }
    
    func getStoredBooksCount() -> Int {
        return storedBooks.count
    }
}
