//
//  MyLibraryBooksDataSource.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension MyLibraryBooksVC: UICollectionViewDataSource   {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyLibraryBookCellIdentifier, for: indexPath) as! MyLibraryBookCell
        cell.updateSeparator(color: Constants.Colors.Brown_10, offsetX: self.view.frame.size.width/4)

        if let viewModel = self.viewModel {
            cell.initWithViewModel(viewModel: MyLibraryBookCellVM(storedBook: viewModel.getStoredBook(index: indexPath.row)))
        }
        cell.actionsBtn.tag = indexPath.row
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getStoredBooksCount() ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AdHeaderIdentifier, for: indexPath) as! AdCell
            if let adProvider = self.adProvider, let ad = adProvider.getNextAd() {
                headerView.initWithAd(ad: ad)
            }
            return headerView
        default:
            return UICollectionReusableView()
        }
    }
}
