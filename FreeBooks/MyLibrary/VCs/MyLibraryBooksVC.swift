//
//  MyLibraryBooksVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

protocol MyLibraryBooksDelegate {
    func bookSelected(bookDetail: BookDetail)
    func viewDetailsPressed(bookDetail: BookDetail)
    func browseMorePressed()
}

import UIKit

class MyLibraryBooksVC: SwipeableSelectionVC, UIGestureRecognizerDelegate {
    
    let MyLibraryBookCellIdentifier = "MyLibraryBook"
    let AdHeaderIdentifier = "AdHeader"
    
    @IBOutlet weak private var myLibraryCollection: UICollectionView!
    @IBOutlet weak private var emptyView: UIView!
    @IBOutlet weak private var emptyIcon: UIImageView!
    @IBOutlet weak private var emptyLabel: UILabel!
    
    var bookType: BookType?
    var viewModel: MyLibraryVM?
    var delegate: MyLibraryBooksDelegate?
    
    internal var adProvider: AdProvider?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerScrollToTopNotificationObserver(notificationName: Constants.NotificationConstants.TabBarSelectionNotifications.MyLibrarySelected)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadContents), name: NSNotification.Name(Constants.NotificationConstants.PlayerClosed), object: nil)
        
        emptyIcon.tintColor = Constants.Colors.GrayLight
        emptyLabel.text = (bookType == .Book) ? "No books in your library" : "No audiobooks in your library"
        
        myLibraryCollection.delegate = self
        myLibraryCollection.dataSource = self
        setupGestureRecognizer()
        
        if !UserDefaultsUtils.areAdsRemoved() {
            adProvider = AdProvider(inVC: self, type: .myLibrary, adReceivedCallback: adReceived)
            adProvider?.requestAd()
            
            NotificationCenter.default.addObserver(self, selector: #selector(handleAdsRemoval), name: NSNotification.Name(rawValue: Constants.NotificationConstants.AdsRemoved), object: nil)
        }
    }
    
    private func adReceived() {
        myLibraryCollection.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadContents()
    }
    
    private func registerScrollToTopNotificationObserver(notificationName: String) {
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToTop), name: Notification.Name(notificationName), object: nil)
    }
    
    @objc private func scrollToTop() {
        myLibraryCollection.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func initWithType(bookType: BookType) {
        self.bookType = bookType
    }
    
    private func setupGestureRecognizer() {
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGestureRecognizer.delegate = self
        longPressGestureRecognizer.delaysTouchesBegan = true
        myLibraryCollection.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    @objc private func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        if gesture.state != .ended {
            return
        }
        let p = gesture.location(in: myLibraryCollection)
        if let indexPath = myLibraryCollection.indexPathForItem(at: p), let viewModel = self.viewModel {
            let attributes = myLibraryCollection.layoutAttributesForItem(at: indexPath)
            showActions(book: viewModel.getStoredBook(index: indexPath.row), rect: attributes?.frame)
        } else {
            print("Index Path can't be recognized for this long press.")
        }
    }
    
    @objc private func reloadContents() {
        if let bookType = self.bookType {
            viewModel = MyLibraryVM(storedBooks: RealmUtils.getStoredBookDetails(bookType: bookType))
        }
        myLibraryCollection.reloadData()
        handleEmpty()
    }
    
    private func handleEmpty() {
        emptyView.isHidden = !(viewModel?.getStoredBooksCount() == 0)
        myLibraryCollection.isHidden = (viewModel?.getStoredBooksCount() == 0)
    }
    
    @IBAction private func actionsBtnPressed(sender: UIButton) {
        if let viewModel = self.viewModel {
            let attributes = myLibraryCollection.layoutAttributesForItem(at: IndexPath(row: sender.tag, section: 0))
            showActions(book: viewModel.getStoredBook(index: sender.tag), rect: attributes?.frame)
        }
    }
    
    private func showActions(book: BookDetail, rect: CGRect? = nil) {
        let removeAction = UIAlertAction(title: "Remove", style: .default, handler: { action in
            RealmUtils.removeStoredBook(bookId: book.bookId)
            FileUtils.removeStoredBook(bookId: book.bookId)
            self.viewModel?.deleteBook(book: book)
            self.myLibraryCollection.reloadData()
            self.handleEmpty()
        })
        let viewDetailsAction = UIAlertAction(title: "View details", style: .default, handler: { action in
            self.delegate?.viewDetailsPressed(bookDetail: book)
        })
        AlertUtils.showActionSheet(actions: [removeAction, viewDetailsAction], inVC: self, inRect: rect)
    }
    
    @IBAction private func browseMorePressed() {
        self.delegate?.browseMorePressed()
    }
    
    @objc private func handleAdsRemoval() {
        myLibraryCollection.reloadData()
    }
}

extension MyLibraryBooksVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewModel = self.viewModel {
        
            let book = viewModel.getStoredBook(index: indexPath.row)
            //Before we open the audiobook's player, we need to check whether there's an active subscription, if there's not we'll offer it
            if book.type == BookType.Audiobook.rawValue && !UserDefaultsUtils.isAudiobooksSubscriptionActive() {
                let subscribeToAudiobooksAction = UIAlertAction.init(title: "Subscribe to audiobooks", style: UIAlertAction.Style.default, handler: { action in
                    PurchaseEngine.sharedEngine.purchaseSubscription()
                })
                let attributes = myLibraryCollection.layoutAttributesForItem(at: indexPath)
                AlertUtils.showActionSheet(actions: [subscribeToAudiobooksAction], inVC: self, inRect: attributes?.frame)
            } else {
                self.delegate?.bookSelected(bookDetail: viewModel.getStoredBook(index: indexPath.row))
            }
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 144)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return UserDefaultsUtils.areAdsRemoved() ? CGSize(width: self.view.frame.size.width, height: 0) : CGSize(width: self.view.frame.size.width, height: 144)
    }
}
