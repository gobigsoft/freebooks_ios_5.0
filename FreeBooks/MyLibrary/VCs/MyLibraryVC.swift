//
//  MyLibraryVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class MyLibraryVC: BaseSwipeableSelectionVC, MyLibraryBooksDelegate {
    
    @IBOutlet weak private var fakeNavigationBarDivider: UIView!
    @IBOutlet weak private var topBarConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {   
        UIUtils.dropBottomShadow(view: fakeNavigationBarDivider)
        setTopBarConstraint()
        
        //We init PageVC Manager and Control and the rest is handled by parent class after calling view did load
        selectionPageVCManager = SwipeableSelectionPageVCManager(viewControllers: createSelectionVCs(), containerView: pageVCContainer)
        control = SwipeableSelectionControl(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50), selectedColor: Constants.Colors.ReddishBrown, unselectedColor: Constants.Colors.DarkerBrown, selectionViewColor: Constants.Colors.ReddishBrown, optionTitles: ["BOOKS", "AUDIOBOOKS"])
        
        super.viewDidLoad()
    }
    
    private func setTopBarConstraint() {
        if (DeviceUtils.DeviceType.IS_IPHONE_X || DeviceUtils.DeviceType.IS_IPHONE_XR || DeviceUtils.DeviceType.IS_IPHONE_XS_MAX) {
            topBarConstraint.constant = -40
        }
    }
    
    private func createSelectionVCs() -> [SwipeableSelectionVC] {
        let booksVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.MyLibraryBooksVC) as! MyLibraryBooksVC
        booksVC.initWithType(bookType: .Book)
        booksVC.delegate = self
        
        let audiobooksVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.MyLibraryBooksVC) as! MyLibraryBooksVC
        audiobooksVC.initWithType(bookType: .Audiobook)
        audiobooksVC.delegate = self
        
        return [booksVC, audiobooksVC]
    }
    
    internal func bookSelected(bookDetail: BookDetail) {
        bookDetail.isAudiobook() ? openPlayer(bookDetail: bookDetail) : openReader(bookDetail: bookDetail)
    }
    
    private func openReader(bookDetail: BookDetail) {
        if let ePUB = FileUtils.getStoredEPUB(bookId: bookDetail.bookId) {
            
            let readerVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.ReaderVC) as! ReaderVC
            
            //Load stored style
            ReaderStyleProvider.sharedProvider.loadStyleFromRealm()
            
            //Init engine
            let readerEngine = ReaderEngine(bookDetail: bookDetail, ePUB: ePUB)
            
            //If we have already cached page counts for current text size, let's setup engine with them so we skip page counts calculation
            if let cache = readerEngine.getCache(textSize: ReaderStyleProvider.sharedProvider.getCurrentTextSize()) {
                readerEngine.setupWithPageCounts(pageCounts: cache.getPageCounts(), tableOfContentsIndexItems: cache.getTableOfContentsIndexItems())
            }
            
            readerVC.initWithReaderEngine(readerEngine: readerEngine)
            
            readerVC.interstitialAdDelegate = self
            self.present(readerVC, animated: true, completion: nil)
        }
    }
    
    private func openPlayer(bookDetail: BookDetail) {
        InterstitialAdProvider.sharedProvider.showInterstitial(inVC: self)
        (self.tabBarController as! TabBarController).openPlayer(bookDetail: bookDetail)
    }
    
    internal func viewDetailsPressed(bookDetail: BookDetail) {
        let bookDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookDetailVC) as! BookDetailVC
        bookDetailVC.initWithBook(book: bookDetail)
        self.present(bookDetailVC, animated: true, completion: nil)
    }
    
    internal func browseMorePressed() {
        goToBrowse()
    }
}

extension MyLibraryVC: ReaderInterstitialAdDelegate {
    internal func readerClosed() {
        InterstitialAdProvider.sharedProvider.showInterstitial(inVC: self)
    }
}
