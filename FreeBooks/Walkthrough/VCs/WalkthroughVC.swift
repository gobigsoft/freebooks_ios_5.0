//
//  ViewController.swift
//  FreeBooks
//
//  Created by Karol Grulling on 12/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class WalkthroughVC: BaseVC {
    
    let LeadingTrailingImageConstantiPhone = CGFloat(87)
    let LeadingTrailingImageConstantiPad = CGFloat(190)
    
    @IBOutlet private weak var walkthroughImage: UIImageView!
    @IBOutlet private weak var walkthroughTitle: UILabel!
    @IBOutlet private weak var walkthroughDesc: UILabel!
    
    @IBOutlet private weak var leadingImageConstraint: NSLayoutConstraint!
    @IBOutlet private weak var trailingImageConstraint: NSLayoutConstraint!

    private var walkthrough: Walkthrough?
    private var pageIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        walkthroughImage.image = walkthrough?.image
        walkthroughTitle.text = walkthrough?.title
        walkthroughDesc.text = walkthrough?.description
        
        leadingImageConstraint.constant = DeviceUtils.isIpad() ? LeadingTrailingImageConstantiPad : LeadingTrailingImageConstantiPhone
        trailingImageConstraint.constant = DeviceUtils.isIpad() ? LeadingTrailingImageConstantiPad : LeadingTrailingImageConstantiPhone
    }

    internal func setupWithWalkthrough(walkthrough: Walkthrough) {
        self.walkthrough = walkthrough
    }
    
    internal func getWalkthroughPosition() -> Int {
        return self.walkthrough?.position ?? 0
    }
}
