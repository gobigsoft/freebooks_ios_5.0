//
//  WalkthroughContainerVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

private extension UIButton {
    func setDoneState() {
        self.titleLabel?.font = .boldSystemFont(ofSize: 15)
        self.setTitle("DONE", for: UIControl.State.normal)
    }
    
    func setNextState()  {
        self.titleLabel?.font = .systemFont(ofSize: 15)
        self.setTitle("NEXT", for: UIControl.State.normal)
    }
}

class WalkthroughContainerVC: BaseVC, WalkthroughDelegate {
    
    @IBOutlet private weak var doneButton: UIButton!
    @IBOutlet private weak var walkthroughContainer: UIView!
    @IBOutlet private weak var pageControl: UIPageControl!
    
    private let walkthroughViewModel = WalkthroughVM()
    private var walkthroughManager: WalkthroughManager?
    private var animator: UIViewPropertyAnimator?
    
    private var openedFromAbout = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNextButton()
        walkthroughManager = WalkthroughManager(viewModel: walkthroughViewModel)
        walkthroughManager?.setupPageVC(container: walkthroughContainer, pageControl: pageControl)
        walkthroughManager?.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupPropertyAnimator(walkthroughIndex: Int) {
        animator?.stopAnimation(false)
        animator?.finishAnimation(at: UIViewAnimatingPosition.current)
        animator = UIViewPropertyAnimator(duration: 0.4, curve: .linear) {
            self.walkthroughContainer.backgroundColor = self.walkthroughViewModel.getWalkthroughForPosition(position: walkthroughIndex).color
        }
    }
    
    //MARK: Walkthrough callbacks
    internal func scrollingToIndex(index: Int) {
        LoggingUtils.logMessage(message: "Scrolling to index: \(index)")
        setupPropertyAnimator(walkthroughIndex: index)
        animator?.startAnimation()
        //Pausing animation allows it to be controlled by fraction complete property
        animator?.pauseAnimation()
    }
    
    internal func scrolledToIndex(index: Int) {
        LoggingUtils.logMessage(message: "Scrolled to index: \(index)")
        animator?.stopAnimation(false)
        animator?.finishAnimation(at: UIViewAnimatingPosition.end)
        index == walkthroughViewModel.getWalkthroughItemsCount()-1 ? setDoneButton() : setNextButton()
    }
    
    internal func scrollingCompletion(completedPercentage: CGFloat) {
        LoggingUtils.logMessage(message: "Completed percentage: \(completedPercentage)")
        if completedPercentage == 0 {
            //If scrolling got back to its origin, stop the animation and finish with the current - origin - state
            animator?.stopAnimation(false)
            animator?.finishAnimation(at: UIViewAnimatingPosition.current)
        } else {
            animator?.fractionComplete = completedPercentage
        }
    }
    
    //MARK: Done/Next buttons
    private func setDoneButton() {
        doneButton.removeTarget(self, action: #selector(nextPressed), for: UIControl.Event.allEvents)
        doneButton.addTarget(self, action: #selector(donePressed), for: UIControl.Event.touchUpInside)
        doneButton.setDoneState()
    }
    
    private func setNextButton() {
        doneButton.removeTarget(self, action: #selector(donePressed), for: UIControl.Event.allEvents)
        doneButton.addTarget(self, action: #selector(nextPressed), for: UIControl.Event.touchUpInside)
        doneButton.setNextState()
    }
    
    @objc private func nextPressed() {
        walkthroughManager?.scrollToNext()
    }   
    
    @objc private func donePressed() {
        (self.openedFromAbout == true) ? self.dismiss(animated: true, completion: nil) : enterTheApp()
    }
    
    private func enterTheApp() {
        if let mainTabBar = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.TabControllersIdentifiers.MainTabController) {
            self.present(mainTabBar, animated: true, completion: nil)
            UserDefaultsUtils.setWalkthroughAsDisplayed()
        }
    }
    
    func setOpenedFromAbout() {
        self.openedFromAbout = true
    }
}


