//
//  WalkthroughViewModel.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

struct Walkthrough {
    let title: String
    let description: String
    let image: UIImage
    let color: UIColor
    let position: Int
}

class WalkthroughVM: NSObject {
    
    private let walkthroughItems =
        [Walkthrough(title: "All timeless classics", description: "We’ve got all the best classics that touch your heart every time you start reading! And good news, now there’s more than 50,000 titles in total!", image: UIImage(named: "walkthrough_books")!, color: Constants.Colors.AppleRed, position: 0),
         Walkthrough(title: "Featured content", description: "We deliver fresh content, offer reading recommendations and make sure you always get something what suits you", image: UIImage(named: "walkthrough_featured")!, color: Constants.Colors.HuskGreen, position: 1),
         Walkthrough(title: "Audiobooks", description: "Try our great feature! Get access to 5,000 audiobooks. Now it’s not even a problem to read and listen at one time for those who learn English!", image: UIImage(named: "walkthrough_audiobooks")!, color: Constants.Colors.AppleGreen, position: 2),
         Walkthrough(title: "New design", description: "We’ve rebuilt this legendary app to serve you even better! New interface, new books, new functions… Enjoy!", image: UIImage(named: "walkthrough_design")!, color: Constants.Colors.HippieBlue, position: 3)]
    
    func getWalkthroughForPosition(position: Int) -> Walkthrough {
        return walkthroughItems.filter{$0.position == position}.first!
    }
    
    func getWalkthroughItemsCount() -> Int {
        return walkthroughItems.count
    }
}
