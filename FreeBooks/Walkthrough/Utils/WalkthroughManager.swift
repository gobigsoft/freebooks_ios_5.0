//
//  WalkthroughManager.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension UIPageViewController {
    var scrollView: UIScrollView? {
        return view.subviews.filter { $0 is UIScrollView }.first as? UIScrollView
    }
}

protocol WalkthroughDelegate {
    func scrollingCompletion(completedPercentage: CGFloat)
    func scrollingToIndex(index: Int)
    func scrolledToIndex(index: Int)
}

class WalkthroughManager: NSObject, UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate {
    
    private let pageViewController = UIPageViewController(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
    private let viewModel: WalkthroughVM
    private var pageControl: UIPageControl?
    
    private var pendingIndex = 0
    private var currentIndex = 0
    
    var delegate: WalkthroughDelegate?
    
    init(viewModel: WalkthroughVM) {
        self.viewModel = viewModel
    }
    
    //MARK: PageVC setup
    func setupPageVC(container: UIView, pageControl: UIPageControl) {
        currentIndex = 0
        self.pageControl = pageControl
        self.pageControl?.numberOfPages = viewModel.getWalkthroughItemsCount()
        pageViewController.delegate = self
        pageViewController.dataSource = self
        pageViewController.scrollView?.delegate = self
        pageViewController.setViewControllers([getViewControllerAtIndex(index: 0) as WalkthroughVC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        container.addSubview(pageViewController.view)
        if let view = pageViewController.view.superview {
            pageViewController.view.frame = view.bounds
        }
    }
    
    //MARK: Delegate functions
    internal func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        let pendingVC = pendingViewControllers.first as! WalkthroughVC
        pendingIndex = pendingVC.getWalkthroughPosition()
        delegate?.scrollingToIndex(index: pendingIndex)
    }
    
    internal func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            let finishedVC = pageViewController.viewControllers!.first as! WalkthroughVC
            currentIndex = finishedVC.getWalkthroughPosition()
            pageControl?.currentPage = currentIndex
            delegate?.scrolledToIndex(index: currentIndex)
        }
    }
    
    //MARK: Datasource functions
    internal func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageContent = viewController as! WalkthroughVC
        if ((pageContent.getWalkthroughPosition() == 0) || (pageContent.getWalkthroughPosition() == NSNotFound)) {
            return nil
        }
        return getViewControllerAtIndex(index: pageContent.getWalkthroughPosition()-1)
    }
    
    internal func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let pageContent = viewController as! WalkthroughVC
        if (pageContent.getWalkthroughPosition() == viewModel.getWalkthroughItemsCount()-1)
        {
            return nil;
        }
        return getViewControllerAtIndex(index: pageContent.getWalkthroughPosition()+1)
    }
    
    private func getViewControllerAtIndex(index: NSInteger) -> WalkthroughVC {
        let walkthroughVC = UIStoryboard.init(name: Constants.StoryboardConstants.StoryboardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.WalkthroughVC) as! WalkthroughVC
        walkthroughVC.setupWithWalkthrough(walkthrough: viewModel.getWalkthroughForPosition(position: index))
        return walkthroughVC
    }
    
    private func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return viewModel.getWalkthroughItemsCount()
    }
    
    //MARK: Scrollview delegate
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let point = scrollView.contentOffset
        var percentComplete: CGFloat
        percentComplete = abs((point.x - UIScreen.main.bounds.size.width)/UIScreen.main.bounds.size.width)
        delegate?.scrollingCompletion(completedPercentage: percentComplete)
    }
    
    func scrollToNext() {
        if (currentIndex+1 == viewModel.getWalkthroughItemsCount()) {
            return;
        }
        currentIndex+=1
        pageControl?.currentPage = currentIndex
        pageViewController.setViewControllers([getViewControllerAtIndex(index: currentIndex)], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion:  nil)
        delegate?.scrollingToIndex(index: currentIndex)
        delegate?.scrolledToIndex(index: currentIndex)
    }
}
