//
//  LoggingUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class LoggingUtils: NSObject {
    public static func logMessage(message: String) {
        if Config.AppMode.Debug == true {
            print(message)
        }
        
        //For internal testing only
        //self.sharedUtils.collectLog(log: message)
    }
    
    static let sharedUtils = LoggingUtils()
    private var collectedLog: String = ""
    
    func collectLog(log: String) {
        collectedLog = collectedLog+"\n\(Date()) \(log)"
    }
    
    func retrieveLog() -> String {
        return collectedLog
    }
}
