//
//  AppDelegate.swift
//  FreeBooks
//
//  Created by Karol Grulling on 12/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import Fabric
import Crashlytics
import GoogleMobileAds
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        PurchaseEngine.sharedEngine.setPaymentQueueObserver()
        
        //Default title position is too low, doesn't apply for iPads
        if !DeviceUtils.isIpad() {
            UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
        }
        
        UITabBar.appearance().unselectedItemTintColor = UIColor.init(white: 1, alpha: 0.45)
        
        RealmUtils.configureRealm()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        if UserDefaultsUtils.wasWalkthroughDisplayed() {
            startWithTabBar()
        }
        
        if !UserDefaultsUtils.areAdsRemoved() {
            InterstitialAdProvider.sharedProvider.prepareInterstitial()
        }
                
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        
        verifySubcriptionStateIfEligible()
        
        return true
    }
    
    private func startWithTabBar() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: Constants.StoryboardConstants.StoryboardMain, bundle: nil)
        let tabBar = storyboard.instantiateViewController(withIdentifier: Constants.StoryboardConstants.TabControllersIdentifiers.MainTabController) as! TabBarController
        self.window?.rootViewController = tabBar
        self.window?.makeKeyAndVisible()
    }
    
    func registerForNotifications(application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        LoggingUtils.logMessage(message: "\(userInfo)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Print full message.
        LoggingUtils.logMessage(message: "\(userInfo)")
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        //We want to capture the current playback state and if there's an ongoing playback, we want to remember it and restore when user opens the app again. 
        guard let currentPlaybackAudiobook = PlayerEngine.sharedEngine.getAudiobookDetail() else {
            return
        }
        
        if !UserDefaultsUtils.isAudiobooksSubscriptionActive() {
            return
        }
        
        if PlayerEngine.sharedEngine.getState() == .playing || PlayerEngine.sharedEngine.getState() == .paused {
            UserDefaultsUtils.setUnfinishedPlaybackAudiobookId(bookId: currentPlaybackAudiobook.bookId)
        } else {
            //If there's no ongoing playback we set a constant for "No unfinished playback" as book Id 
            UserDefaultsUtils.setUnfinishedPlaybackAudiobookId(bookId: Constants.Misc.NoUnfinishedPlaybackAudiobookId)
        }
        
        PlayerEngine.sharedEngine.setupNowPlayingInfo()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        verifySubcriptionStateIfEligible()
    }
    
    private func verifySubcriptionStateIfEligible() {
        //We want to verify subscription state every time user gets back into the app, this applies for cases when
        // a) there's an active subscription - we want to check it hasn't expired
        // b) audiobooks aren't already purchased - a complete audiobooks bundle from the older app
        // c) this isn't the first startup of the app
        if UserDefaultsUtils.wasWalkthroughDisplayed() && (!UserDefaultsUtils.areAudiobooksPurchased() && UserDefaultsUtils.isAudiobooksSubscriptionActive()) {
            PurchaseEngine.sharedEngine.verifyAudiobooksSubscriptionState()
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        
        let container = NSPersistentContainer(name: "FreeBooks")
        
        //We expect to find old app's database in .../Application Support/FreeBooks/FreeBooks.sqlite
        if let storeDirectory = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first {
            let url = storeDirectory.appendingPathComponent("FreeBooks", isDirectory: true).appendingPathComponent("FreeBooks.sqlite") 
            let storeDescription = NSPersistentStoreDescription(url: url)
            storeDescription.shouldInferMappingModelAutomatically = true
            storeDescription.shouldMigrateStoreAutomatically = true
            container.persistentStoreDescriptions = [storeDescription]
        }
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

