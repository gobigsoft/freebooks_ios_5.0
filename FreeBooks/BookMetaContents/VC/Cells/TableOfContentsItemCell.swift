//
//  TableOfContentsItemCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 14/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class TableOfContentsItemCell: SeparatorCell {
    
    @IBOutlet weak private var chapterName: UILabel!
    @IBOutlet weak private var page: UILabel!
    
    override func awakeFromNib() {
        setStyle()
    }
    
    private func setStyle() {
        self.chapterName.textColor = AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? UIColor.white : UIColor.black
        self.page.textColor = AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? UIColor.init(white: 1, alpha: 0.45) : Constants.Colors.Brown
        AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? addSeparator() : addSeparator(color: Constants.Colors.Gray_10)
    }
    
    func initWithTableOfContentsItem(tableOfContentsItem: TableOfContentsItem) {
        self.chapterName.text = tableOfContentsItem.getChapterName()
        self.page.text = "Page \(tableOfContentsItem.getPageIndex() + 1)"
    }
}
