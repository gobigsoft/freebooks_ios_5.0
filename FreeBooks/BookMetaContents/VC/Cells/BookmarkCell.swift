//
//  BookmarkCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BookmarkCell: SeparatorCell {
    
    @IBOutlet weak private var snippet: UILabel!
    @IBOutlet weak private var pageDateAdded: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        setStyle()
    }
    
    func initWithBookmark(bookmark: Bookmark, engine: ReaderEngine) {
        self.snippet.text = "\"\(bookmark.snippet)...\""
        self.pageDateAdded.text = "Page \(engine.calculateCurrentPageIndexForProgress(progress: bookmark.progress)+1) · \(dateDifferenceFromNow(date: bookmark.dateAdded))"
    }
    
    private func setStyle() {
        self.snippet.textColor = AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? UIColor.white : UIColor.black
        self.pageDateAdded.textColor = AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? UIColor.init(white: 1, alpha: 0.45) : Constants.Colors.Brown
        self.deleteButton.tintColor = AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? UIColor.init(white: 1, alpha: 0.45) : Constants.Colors.Brown
        AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? addSeparator() : addSeparator(color: Constants.Colors.Gray_10)
    }
    
    private func dateDifferenceFromNow(date: Date) -> String {
        let dateRangeStart = Date()
        let dateRangeEnd = date
        let components = Calendar.current.dateComponents([.weekOfYear, .month, .day, .hour, .minute], from: dateRangeEnd, to: dateRangeStart)
        let months = components.month ?? 0
        let weeks = components.weekOfYear ?? 0
        let days = components.day ?? 0
        let hours = components.hour ?? 0
        let minutes = components.minute ?? 0
        if months > 0 {
            return months == 1 ? "\(months) month ago" : "\(months) months ago"
        } else if weeks > 0 {
            return weeks == 1 ? "\(weeks) week ago" : "\(weeks) weeks ago"
        } else if days > 0 {
            return days == 1 ? "\(days) day ago" : "\(days) days ago"
        } else if hours > 0 {
            return hours == 1 ? "\(hours) hour ago" : "\(hours) hours ago"
        } else if minutes > 0 {
            return minutes == 1 ? "\(minutes) minute ago" : "\(minutes) minutes ago"
        } else {
            return "now"
        }
    }
}
