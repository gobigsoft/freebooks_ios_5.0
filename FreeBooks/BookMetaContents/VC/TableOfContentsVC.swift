//
//  TableOfContentsVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 30/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol TableOfContentsSelectionDelegate {
    func tableOfContentsItemSelected()
}

class TableOfContentsVC: BookMetaContentsBaseVC, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let TableOfContentsItemCellIdentifier = "TableOfContentsItem"
    @IBOutlet weak private var tableOfContentsCollection: UICollectionView!

    private var tableOfContentsItems = [TableOfContentsItem]()
    var delegate: TableOfContentsSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let engine = self.engine {
            self.tableOfContentsItems = engine.getTableOfContentsItems()
            handleEmptyScreen()
            
            tableOfContentsCollection.delegate = self
            tableOfContentsCollection.dataSource = self
        }
    }
    
    private func handleEmptyScreen() {
        noContentsContainer.isHidden = tableOfContentsItems.count == 0 ? false : true
        tableOfContentsCollection.isHidden = tableOfContentsItems.count == 0 ? true : false
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let tableOfContentsItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: TableOfContentsItemCellIdentifier, for: indexPath) as! TableOfContentsItemCell
        tableOfContentsItemCell.initWithTableOfContentsItem(tableOfContentsItem: tableOfContentsItems[indexPath.row])
        return tableOfContentsItemCell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableOfContentsItems.count
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.engine?.tableOfContentsItemSelected(tableOfContentsItem: tableOfContentsItems[indexPath.row])
        self.delegate?.tableOfContentsItemSelected()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 70)
    }
}
