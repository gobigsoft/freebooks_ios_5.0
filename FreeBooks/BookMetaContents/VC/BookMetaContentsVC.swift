//
//  BookMetaContentsVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 29/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

//We do not subclass StyleableView since Book Meta has different background rules as all the other components
class BookMetaTopSelectionBar: UIView {
    
    @IBOutlet weak private var backButton: UIButton!
    @IBOutlet weak private var bookTitle: UILabel!
    
    func setStyle(style: ReaderStyle) {
        if AppearanceUtils.isDarkTheme(themeType: style.theme.themeType) {
            self.backgroundColor = Constants.Colors.Gray_95
        } else {
            self.backgroundColor = Constants.Colors.Brown
        }
    }
    
    func setBookTitle(title: String) {
        self.bookTitle.text = title
    }
}

//Parent class providing styling used by both Book Meta Swipeable VCs - Bookmarks/Contents
class BookMetaContentsBaseVC: SwipeableSelectionVC {
    
    @IBOutlet weak internal var noContentsContainer: UIView!
    @IBOutlet weak internal var noContentsImage: UIImageView!
    @IBOutlet weak internal var noContentsLabel: UILabel!
    
    var engine: ReaderEngine?
    
    override func viewDidLoad() {
        setStyle()
    }
    
    func initWithEngine(engine: ReaderEngine) {
        self.engine = engine
    }
    
    private func setStyle() {
        noContentsImage.tintColor = AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? Constants.Colors.Brown_10 : Constants.Colors.Gray_70
        noContentsLabel.textColor = AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getStyle().theme.themeType) ? Constants.Colors.Gold : Constants.Colors.Gray_70
    }
}

class BookMetaContentsVC: BaseSwipeableSelectionVC, BookmarkSelectionDelegate, TableOfContentsSelectionDelegate {
    
    @IBOutlet weak private var topBar: BookMetaTopSelectionBar!
    private var engine: ReaderEngine?
    
    func initWithEngine(engine: ReaderEngine) {
        self.engine = engine
    }
    
    override func viewDidLoad() {
        topBar.setStyle(style: ReaderStyleProvider.sharedProvider.getStyle())
        (pageVCContainer as! StyleableView).setPresetBgColor(style: ReaderStyleProvider.sharedProvider.getStyle())
        
        selectionPageVCManager = SwipeableSelectionPageVCManager(viewControllers: createSelectionVCs(), containerView: pageVCContainer)
        control = SwipeableSelectionControl(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        
        super.viewDidLoad()
    }
    
    private func createSelectionVCs() -> [SwipeableSelectionVC] {
        let tableOfContentsVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.TableOfContentsVC) as! TableOfContentsVC
        let bookmarksVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookmarksVC) as! BookmarksVC
        
        //Setting the delegate allows us to dismiss VCs when bookmark/table of contents item is selected
        tableOfContentsVC.delegate = self
        bookmarksVC.delegate = self
        
        if let engine = self.engine {
            tableOfContentsVC.initWithEngine(engine: engine)
            bookmarksVC.initWithEngine(engine: engine)
        }

        return [tableOfContentsVC, bookmarksVC]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let engine = self.engine {
            topBar.setBookTitle(title: engine.getBookTitle())
        }
    }
    
    internal func bookmarkSelected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    internal func tableOfContentsItemSelected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
