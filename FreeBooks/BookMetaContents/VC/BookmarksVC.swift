//
//  BookmarksVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 30/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol BookmarkSelectionDelegate {
    func bookmarkSelected()
}

class BookmarksVC: BookMetaContentsBaseVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let BookmarkCellIdentifier = "Bookmark"
    @IBOutlet weak private var bookmarksCollection: UICollectionView!
    
    private var bookmarks = [Bookmark]()
    var delegate: BookmarkSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let engine = self.engine {
            self.bookmarks = engine.getBookmarks()
            handleEmptyScreen()
            bookmarksCollection.delegate = self
            bookmarksCollection.dataSource = self
            
            LoggingUtils.logMessage(message: "Found \(bookmarks.count) bookmarks")
        }
    }
    
    private func handleEmptyScreen() {
        noContentsContainer.isHidden = bookmarks.count == 0 ? false : true
        bookmarksCollection.isHidden = bookmarks.count == 0 ? true : false
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bookmarkCell = collectionView.dequeueReusableCell(withReuseIdentifier: BookmarkCellIdentifier, for: indexPath) as! BookmarkCell
        if let engine = self.engine {
            bookmarkCell.initWithBookmark(bookmark: bookmarks[indexPath.row], engine: engine)
            bookmarkCell.deleteButton.tag = indexPath.row
        }
        return bookmarkCell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bookmarks.count
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.engine?.bookmarkSelected(bookmark: bookmarks[indexPath.row])
        self.delegate?.bookmarkSelected()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 105)
    }
    
    @IBAction internal func deleteBookmarkPressed(sender: UIButton) {
        let action = UIAlertAction(title: "Remove bookmark", style: .default, handler: { action in
            if let engine = self.engine {
                engine.deleteBookmark(progress: self.bookmarks[sender.tag].progress)
                self.bookmarks.remove(at: sender.tag)
                self.handleEmptyScreen()
                self.bookmarksCollection.reloadData()
            }
        })
        AlertUtils.showActionSheet(actions: [action], inVC: self, inRect: sender.frame)
    }
}
