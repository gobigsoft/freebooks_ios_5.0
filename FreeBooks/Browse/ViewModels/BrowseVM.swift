//
//  BrowseVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 10/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

struct Category {
    let name: String
    let croppedBgName: String
    let bgName: String
    let iconName: String
    let bookCategoryId: Int
    let audioCategoryId: Int
}

class BrowseVM: NSObject {
    
    private let categories = [Category(name: "Best Of", croppedBgName: "category_bg_best_of_sq", bgName: "category_bg_best_of", iconName: "category_icon_best_of", bookCategoryId: 24, audioCategoryId: 72),
                      Category(name: "Adventure", croppedBgName: "category_bg_adventure_sq", bgName: "category_bg_adventure", iconName: "category_icon_adventure", bookCategoryId: 1, audioCategoryId: 69),
                      Category(name: "Autos & Bios", croppedBgName: "category_bg_autos_bios_sq", bgName: "category_bg_autos_bios", iconName: "category_icon_autos_bios", bookCategoryId: 11, audioCategoryId: 69),
                      Category(name: "Banned", croppedBgName: "category_bg_banned_sq", bgName: "category_bg_banned", iconName: "category_icon_banned", bookCategoryId: 12, audioCategoryId: -1),
                      Category(name: "Drama", croppedBgName: "category_bg_drama_sq", bgName: "category_bg_drama", iconName: "category_icon_drama", bookCategoryId: 2, audioCategoryId: 73),
                      Category(name: "English 101", croppedBgName: "category_bg_english_101_sq", bgName: "category_bg_english_101", iconName: "category_icon_english_101", bookCategoryId: 13, audioCategoryId: -1),
                      Category(name: "Epic", croppedBgName: "category_bg_epic_sq", bgName: "category_bg_epic", iconName: "category_icon_epic", bookCategoryId: 14, audioCategoryId: 71),
                      Category(name: "Fantasy", croppedBgName: "category_bg_fantasy_sq", bgName: "category_bg_fantasy", iconName: "category_icon_fantasy", bookCategoryId: 3, audioCategoryId: 68),
                      Category(name: "For children", croppedBgName: "category_bg_for_children_sq", bgName: "category_bg_for_children", iconName: "category_icon_for_children", bookCategoryId: -1, audioCategoryId: 74),
                      Category(name: "Ghost stories", croppedBgName: "category_bg_ghost_stories_sq", bgName: "category_bg_ghost_stories", iconName: "category_icon_ghost_stories", bookCategoryId: 15, audioCategoryId: -1),
                      Category(name: "History", croppedBgName: "category_bg_history_sq", bgName: "category_bg_history", iconName: "category_icon_history", bookCategoryId: 4, audioCategoryId: 66),
                      Category(name: "Horror", croppedBgName: "category_bg_horror_sq", bgName: "category_bg_horror", iconName: "category_icon_horror", bookCategoryId: 5, audioCategoryId: 65),
                      Category(name: "Mystery", croppedBgName: "category_bg_mystery_sq", bgName: "category_bg_mystery", iconName: "category_icon_mystery", bookCategoryId: 6, audioCategoryId: 64),
                      Category(name: "Myths & Legends", croppedBgName: "category_bg_myths_legends_sq", bgName: "category_bg_myths_legends", iconName: "category_icon_myths_legends", bookCategoryId: 16, audioCategoryId: -1),
                      Category(name: "Philosophy", croppedBgName: "category_bg_philosophy_sq", bgName: "category_bg_philosophy", iconName: "category_icon_philosophy", bookCategoryId: 17, audioCategoryId: 78),
                      Category(name: "Poetry", croppedBgName: "category_bg_poetry_sq", bgName: "category_bg_poetry", iconName: "category_icon_poetry", bookCategoryId: 7, audioCategoryId: 75),
                      Category(name: "Politics", croppedBgName: "category_bg_politics_sq", bgName: "category_bg_politics", iconName: "category_icon_politics", bookCategoryId: 8, audioCategoryId: 76),
                      Category(name: "Psychology", croppedBgName: "category_bg_psychology_sq", bgName: "category_bg_psychology", iconName: "category_icon_psychology", bookCategoryId: 25, audioCategoryId: -1),
                      Category(name: "Pulp", croppedBgName: "category_bg_pulp_sq", bgName: "category_bg_pulp", iconName: "category_icon_pulp", bookCategoryId: 28, audioCategoryId: -1),
                      Category(name: "Romance", croppedBgName: "category_bg_romance_sq", bgName: "category_bg_romance", iconName: "category_icon_romance", bookCategoryId: 27, audioCategoryId: -1),
                      Category(name: "Science fiction", croppedBgName: "category_bg_science_fiction_sq", bgName: "category_bg_science_fiction", iconName: "category_icon_science_fiction", bookCategoryId: 9, audioCategoryId: 67),
                      Category(name: "Theology", croppedBgName: "category_bg_theology_sq", bgName: "category_bg_theology", iconName: "category_icon_theology", bookCategoryId: 26, audioCategoryId: -1),
                      Category(name: "Thriller", croppedBgName: "category_bg_thriller_sq", bgName: "category_bg_thriller", iconName: "category_icon_thriller", bookCategoryId: 10, audioCategoryId: -1),
                      Category(name: "Travel", croppedBgName: "category_bg_travel_sq", bgName: "category_bg_travel", iconName: "category_icon_travel", bookCategoryId: 20, audioCategoryId: 77),
                      Category(name: "US presidents", croppedBgName: "category_bg_us_presidents_sq", bgName: "category_bg_us_presidents", iconName: "category_icon_us_presidents", bookCategoryId: 18, audioCategoryId: -1),
                      Category(name: "War", croppedBgName: "category_bg_war_sq", bgName: "category_bg_war", iconName: "category_icon_war", bookCategoryId: 21, audioCategoryId: 80),
                      Category(name: "Western", croppedBgName: "category_bg_western_sq", bgName: "category_bg_western", iconName: "category_icon_western", bookCategoryId: 22, audioCategoryId: -1),
                      Category(name: "Young adult", croppedBgName: "category_bg_young_adult_sq", bgName: "category_bg_young_adult", iconName: "category_icon_young_adult", bookCategoryId: 23, audioCategoryId: -1)]
    
    private var authors: [Author]?
    
    private var successCallback: (() -> ())!
    private var failureCallback: (() -> ())!
    
    init(successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    func downloadAuthors() {
        BrowseRequester().getAuthors(completionHandler: { success, authors in
            if success {
                self.authors = authors
                self.successCallback()
            } else {
                self.failureCallback()
            }
        })
    }
    
    func getAuthorsCount() -> Int {
        return self.authors?.count ?? 0
    }

    func getAuthorName(index: Int) -> String {
        guard let authors = self.authors else {
            return ""
        }
        return Utils.getAuthorNamesWithFirstNameInitials(author: authors[index])
    }
    
    func getAuthorImage(index: Int) -> String {
        return self.authors?[index].image ?? ""
    }
    
    func getCategoriesCount() -> Int {
        return self.categories.count
    }
    
    func getCategory(index: Int) -> Category {
        return self.categories[index]
    }
    
    func getAuthor(index: Int) -> Author? {
        return self.authors?[index]
    }
}


