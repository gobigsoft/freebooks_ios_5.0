//
//  CategoryCellVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 11/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class CategoryCellVM: NSObject {
    
    private let category: Category
    
    init(category: Category) {
        self.category = category
    }
    
    func getBackground() -> UIImage? {
        return UIImage.init(named: category.croppedBgName)
    }
    
    func getIcon() -> UIImage? {
        return UIImage.init(named: category.iconName)
    }
    
    func getName() -> String {
        return category.name
    }
}
