//
//  BrowseHeader.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BrowseHeader: UICollectionReusableView {
    
    @IBOutlet weak private var title: UILabel!
    @IBOutlet weak private var showMore: UIButton!
    
    func initWithTitle(title: String) {
        self.title.text = title
    }
    
    func hideActionButton() {
        showMore.isHidden = true
    }
}
