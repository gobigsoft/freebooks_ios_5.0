//
//  CategoryCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 11/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak private var name: UILabel!
    @IBOutlet weak private var background: UIImageView!
    @IBOutlet weak private var icon: UIImageView!
    
    func initWithViewModel(viewModel: CategoryCellVM) {
        name.text = viewModel.getName()
        background.image = viewModel.getBackground()
        icon.image = viewModel.getIcon()
    }
}

