//
//  BrowseAuthorsCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 10/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol AuthorSelectionDelegate {
    func authorSelected(author: Author)
}

class BrowseAuthorsCell: UICollectionViewCell {

    @IBOutlet weak private var authorsCollection: UICollectionView!
    private var viewModel: BrowseVM?
    internal var delegate: AuthorSelectionDelegate?
    
    func initWithViewModel(viewModel: BrowseVM, delegate: AuthorSelectionDelegate) {
        self.viewModel = viewModel
        self.delegate = delegate
        authorsCollection.dataSource = self
        authorsCollection.delegate = self
    }
}

extension BrowseAuthorsCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let authorCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AuthorCell", for: indexPath) as! AuthorCell
        authorCell.initWithImageUrl(imageUrl: viewModel?.getAuthorImage(index: indexPath.row) ?? "", name: viewModel?.getAuthorName(index: indexPath.row) ?? "")
        return authorCell
    }

    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getAuthorsCount() ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let author = viewModel?.getAuthor(index: indexPath.row) {
            self.delegate?.authorSelected(author: author)
        }
    }
}
