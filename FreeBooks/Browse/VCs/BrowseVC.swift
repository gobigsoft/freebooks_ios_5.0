//
//  BrowseVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 10/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BrowseVC: BaseVC, AuthorSelectionDelegate {
    @IBOutlet private weak var browseCollection: UICollectionView!
    internal var viewModel: BrowseVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerScrollToTopNotificationObserver(notificationName: Constants.NotificationConstants.TabBarSelectionNotifications.BrowseSelected)

        showLoading()
        viewModel = BrowseVM(successCallback: authorsObtained, failureCallback: obtainingAuthorsFailed)
        viewModel?.downloadAuthors()
    }
    
    private func authorsObtained() {
        hideLoading()
        browseCollection.delegate = self
        browseCollection.dataSource = self
    }
    
    private func obtainingAuthorsFailed() {
        hideLoading()
        showNoConnectionView()
    }
    
    @IBAction private func retryPressed() {
        hideNoConnectionView()
        showLoading()
        
        viewModel?.downloadAuthors()
    }
    
    @IBAction private func showMorePressed() {
        goToSearch()
    }
    
    internal func authorSelected(author: Author) {
        showAuthorDetail(author: author)
    }
    
    override internal func scrollToTop() {
        browseCollection.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
}
