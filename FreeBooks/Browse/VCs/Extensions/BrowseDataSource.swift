//
//  BrowseDataSource.swift
//  FreeBooks
//
//  Created by Karol Grulling on 11/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

internal enum BrowseSections: String {
    case Authors = "Authors"
    case Categories = "Category"
    
    static let allValues = [Authors, Categories]
}

extension BrowseVC: UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let browseSection = BrowseSections.allValues[indexPath.section]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: browseSection.rawValue, for: indexPath)
        guard let browseViewModel = viewModel else {
            return cell
        }
        
        switch browseSection {
        case .Authors:
            (cell as! BrowseAuthorsCell).initWithViewModel(viewModel: browseViewModel, delegate: self)
        case .Categories:
            (cell as! CategoryCell).initWithViewModel(viewModel: CategoryCellVM(category: browseViewModel.getCategory(index: indexPath.row)))
        }
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch BrowseSections.allValues[section] {
        case .Authors:
            return 1
        case .Categories:
            return viewModel?.getCategoriesCount() ?? 0
        }
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return BrowseSections.allValues.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "BrowseHeader", for: indexPath) as! BrowseHeader
            switch BrowseSections.allValues[indexPath.section] {
            case .Authors:
                headerView.initWithTitle(title: "Authors")
            case .Categories:
                headerView.initWithTitle(title: "Categories")
                headerView.hideActionButton()
            }
            return headerView
        default:
            return UICollectionReusableView()
        }
    }
}
