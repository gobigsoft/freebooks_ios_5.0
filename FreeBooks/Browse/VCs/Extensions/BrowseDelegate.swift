//
//  BrowseDelegate.swift
//  FreeBooks
//
//  Created by Karol Grulling on 11/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension BrowseVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch BrowseSections.allValues[indexPath.section] {
        case .Authors:
            return CGSize(width: collectionView.frame.size.width, height: 150)
        case .Categories:
            let squareSide = collectionView.frame.size.width/2-15
            return CGSize(width: squareSide, height: squareSide)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch BrowseSections.allValues[section] {
        case .Authors:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        case .Categories:
            return UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let browseViewModel = viewModel else {
            return
        }
        
        let categoryDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.CategoryDetailVC) as! CategoryDetailVC
        categoryDetailVC.initWithCategory(category: browseViewModel.getCategory(index: indexPath.row))
        self.present(categoryDetailVC, animated: true, completion: nil)
    }
}
