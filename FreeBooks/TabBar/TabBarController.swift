//
//  TabBarController.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UIGestureRecognizerDelegate, PlayerVCDelegate, PlayerAnimatorDelegate {
    
    private lazy var PlayerOffset: CGFloat = {
        let tabBarHeight = self.tabBar.frame.size.height
        let playerVCMiniatureHeight = DeviceUtils.isBezelessDevice() ? Constants.Misc.PlayerMiniatureBezelessDeviceSizeHeight : Constants.Misc.PlayerMiniatureSizeHeight
        let calculatedOffset = tabBarHeight+playerVCMiniatureHeight
        return calculatedOffset
    }()
    
    private var playerContainer: UIView?
    private var playerVC: PlayerVC?
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(playerPanned(recognizer:)))
        recognizer.delegate = self
        return recognizer
    }()
    
    private lazy var tapRecognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer()
        recognizer.addTarget(self, action: #selector(showPlayer))
        recognizer.delegate = self
        return recognizer
    }()
    
    private var playerAnimator: PlayerAnimator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGoldBadgeAtIndex(index: 1)
        initPlayer()
        NotificationCenter.default.addObserver(self, selector: #selector(hidePlayer), name: NSNotification.Name(Constants.NotificationConstants.PlayerCancelled), object: nil)
        
        //If there's any unfinished playback from the last session, we'll recover it, show the player and allow user to proceed with his listening
        if UserDefaultsUtils.isThereAnyUnfinishedAudiobookPlayback() && playerAnimator?.getCurrentState() != .open && UserDefaultsUtils.isAudiobooksSubscriptionActive() {
            PlayerEngine.sharedEngine.recoverFromBookId(bookId: UserDefaultsUtils.getUnfinishedPlaybackAudiobookId())
            playerVC!.setup()
            playerVC!.setMiniatureVisible()
            playerContainer!.frame.origin.y = self.view.frame.size.height-PlayerOffset
        }
    }
    
    private func setAudiobooksBadge() {
        if let audiobooksTabBarItem = self.tabBar.items?[1] {
            audiobooksTabBarItem.badgeColor = UIColor.green
            audiobooksTabBarItem.badgeValue = ""
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //If Player Engine isn't closed we know there's a playback going on and therefore we need to display the player
        if PlayerEngine.sharedEngine.getState() != .closed && playerAnimator?.getCurrentState() != .open  {
            playerVC!.setup()
            playerVC!.setMiniatureVisible()
            playerContainer!.frame.origin.y = self.view.frame.size.height-PlayerOffset
        }
    }
    
    private func initPlayer() {
        playerVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.PlayerVC) as? PlayerVC
        playerVC?.delegate = self
        initPlayerView()
    }
    
    private func initPlayerView() {
        //Place player container into view
        playerContainer = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        playerContainer?.isOpaque = true 
        
        //Put PlayerVC its view into container
        playerContainer?.addSubview(playerVC!.view)
        
        //Add gesture recognizers
        playerContainer?.addGestureRecognizer(panRecognizer)
        
        //Player miniature is initialized after the Player VC is added as a subview, therefore this is the earliest place we can add a recognizer to it
        playerVC?.playerMiniature.addGestureRecognizer(tapRecognizer)
        
        //Add container with Player into parent view
        self.view.addSubview(playerContainer!)
        
        //Make sure tab bar is visible
        self.view.bringSubviewToFront(self.tabBar)
        
        //Setup animator which handles all the Pan gestures
        playerAnimator = PlayerAnimator(playerContainer: playerContainer!, parentView: self.view, playerMiniature: playerVC!.playerMiniature, offset: PlayerOffset, tabBar: self.tabBar)
        playerAnimator?.delegate = self
    }
    
    func openPlayer(bookDetail: BookDetail) {
        playerAnimator?.animateTransitionIfNeeded(to: .open, duration: Constants.Misc.PlayerDisplayAnimationDuration)
        
        PlayerEngine.sharedEngine.setupWithAudiobookDetail(audiobookDetail: bookDetail)
        PlayerEngine.sharedEngine.continuePlayback()
    }
    
    @objc func showPlayer() {
        playerAnimator?.animateTransitionIfNeeded(to: .open, duration: Constants.Misc.PlayerDisplayAnimationDuration)
    }
    
    //Pan gesture recognizer which is taken over by player animator
    @objc private func playerPanned(recognizer: UIPanGestureRecognizer) {
        playerAnimator?.playerViewPanned(recognizer: recognizer)
    }
    
    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        //Recognizer should be disabled for buttons/sliders as well as when there's a playback speed selection view visible
        if touch.view is UIControl || playerVC?.playbackSpeedDarkView.isHidden == false {
            return false
        }
        return true
    }
    
    internal func cancelPlayerPressed() {
        NotificationCenter.default.post(name: NSNotification.Name(Constants.NotificationConstants.PlayerCancelled), object: nil)
    }
    
    @objc private func hidePlayer() {
        playerAnimator?.hide()
        playerAnimator?.showTabBarIfRequired()
    }
    
    internal func playerOpened() {
        //So far we don't need to handle opening of the player
    }
    
    internal func playerClosed() {
        //Every time a player is closed, we need to let My Library Books VC know to update its current progress, since there's no direct relation between Tab Bar Controller and My Library VC, we need to post notification
        NotificationCenter.default.post(name: NSNotification.Name(Constants.NotificationConstants.PlayerClosed), object: nil)
    }
    
    internal func showChaptersPressed() {
        if let audiobookDetail = PlayerEngine.sharedEngine.getAudiobookDetail() {
            let audiobookChaptersVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.AudiobookChaptersVC) as! AudiobookChaptersVC
            audiobookChaptersVC.initWithChapters(audiobookChapters: audiobookDetail.audiobookChapters, audiobookDetail: audiobookDetail)
            self.present(audiobookChaptersVC, animated: true, completion: nil)
        }
    }
    
    internal func showAudiobookDetailPressed() {
        if let audiobookDetail = PlayerEngine.sharedEngine.getAudiobookDetail() {
            let bookDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookDetailVC) as! BookDetailVC
            bookDetailVC.initWithBook(book: audiobookDetail)
            self.present(bookDetailVC, animated: true, completion: nil)
        }
    }
    
    internal func hidePlayerPressed() {
        playerAnimator?.animateTransitionIfNeeded(to: .closed, duration: Constants.Misc.PlayerDisplayAnimationDuration)
    }
}

extension TabBarController: UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        LoggingUtils.logMessage(message: "Tab Bar item index selected \(item.tag)")
        switch item.tag {
        case 0:
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationConstants.TabBarSelectionNotifications.FeaturedSelected), object: nil)
            break
        case 1:
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationConstants.TabBarSelectionNotifications.AudiobooksSelected), object: nil)
            break
        case 2:
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationConstants.TabBarSelectionNotifications.BrowseSelected), object: nil)
            break
        case 3:
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationConstants.TabBarSelectionNotifications.MyLibrarySelected), object: nil)
            break
        case 4:
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationConstants.TabBarSelectionNotifications.SearchSelected), object: nil)
            break
        default:
            break
        }
        InterstitialAdProvider.sharedProvider.showFirstActionInterstitial(inVC: self)
    }
}

//Default badge is a bit bigger than we want, we need to create a custom one so it fits in and looks good
extension TabBarController {
    internal func addGoldBadgeAtIndex(index: Int) {
        for subview in self.tabBar.subviews {
            if subview.tag == 9999 {
                subview.removeFromSuperview()
                break
            }
        }
        
        let screenSize = UIScreen.main.bounds
        let TabBarItemCount = CGFloat(self.tabBar.items!.count)
        
        let BadgeRadius: CGFloat = 4
        let BadgeDiameter = BadgeRadius * 2
        
        let TopMargin:CGFloat = DeviceUtils.isIpad() ? 14 : 7
        let HalfItemWidth = (screenSize.width) / (TabBarItemCount * 2)
        let imageHalfWidth: CGFloat = (self.tabBar.items![index]).selectedImage!.size.width / 2
        
        let xOffset = HalfItemWidth * CGFloat(index * 2 + 1)
        var xPosition: CGFloat = 0
        
        if DeviceUtils.isIpad() {
            if DeviceUtils.DeviceType.IS_IPAD_PRO_12_9 {
                xPosition = (xOffset + imageHalfWidth - 17)
            } else if DeviceUtils.DeviceType.IS_IPAD_PRO_11 || DeviceUtils.DeviceType.IS_IPAD_PRO_10_5 {
                xPosition = (xOffset + imageHalfWidth - 24)
            } else if DeviceUtils.DeviceType.IS_IPAD {
                xPosition = (xOffset + imageHalfWidth - 26)
            }
        } else {
            xPosition = (xOffset + imageHalfWidth - 7)
        }
        
        let badge = UIView(frame: CGRect(x:xPosition, y: TopMargin, width: BadgeDiameter, height: BadgeDiameter))
        
        badge.tag = 9999
        badge.backgroundColor = Constants.Colors.Gold
        badge.layer.cornerRadius = BadgeRadius
        
        self.tabBar.addSubview(badge)
    }
}
