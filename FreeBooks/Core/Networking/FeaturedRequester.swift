//
//  FeaturedRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FeaturedRequester: CoreRequester {
    func getFeatured(completionHandler: @escaping(_ success: Bool, _ featured: Featured?) -> Void) {
        get(url: Config.API.Routes.Featured, completionHandler: { response in
            return self.handleFeaturedResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    private func handleFeaturedResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ featured: Featured?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            do {
                completionHandler(true, try Featured(json: JSON(response)))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse featured response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Featured response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
