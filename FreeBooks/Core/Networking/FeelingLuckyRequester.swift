//
//  FeelingLuckyRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 20/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FeelingLuckyRequester: CoreRequester {

    func feelingLucky(libraryBookIds: [Int], completionHandler: @escaping(_ success: Bool, _ book: Book?) -> Void) {
        let request = JSON(["library_book_ids": libraryBookIds])
        
        LoggingUtils.logMessage(message: "Feeling lucky request: \(String(describing: request.rawString()))")
        post(url: Config.API.Routes.FeelingLucky, request: request, completionHandler: { response in
            return self.handleFeelingLuckyResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    private func handleFeelingLuckyResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ book: Book?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            do {
                completionHandler(true, try Book(json: JSON(response)))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse feeling lucky response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Feeling lucky response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
