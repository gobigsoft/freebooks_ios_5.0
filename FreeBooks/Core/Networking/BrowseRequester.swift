//
//  BrowseRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 10/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BrowseRequester: CoreRequester {
    func getAuthors(completionHandler: @escaping(_ success: Bool, _ authors: [Author]?) -> Void) {
        get(url: Config.API.Routes.Browse, completionHandler: { response in
            return self.handleBrowseResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    private func handleBrowseResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ authors: [Author]?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            do {
                var authors = [Author]()
                for author in JSON(response)["authors"].arrayValue {
                    authors.append(try Author(json: author))
                }
                completionHandler(true, authors)
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse browse response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Browse response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
