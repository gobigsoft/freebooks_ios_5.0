//
//  SearchRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 02/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class SearchRequester: CoreRequester {
    
    //We want to have a single instance of search request since these can be spammed quite easily by quick typing, by doing this we make sure there's only a single request currently processed
    private var searchRequest: Request?
    
    func search(searchTerm: String, offset: Int, page: Int, completionHandler: @escaping(_ success: Bool, _ books: [Book]?) -> Void) {
        let request = JSON(["query": searchTerm, "type": "books", "count": Constants.Misc.DefaultSearchResultsPageCount, "offset": offset, "page": page])
        
        LoggingUtils.logMessage(message: "Search request: \(String(describing: request.rawString()))")
        post(url: Config.API.Routes.Search, request: request, completionHandler: { response in
            return self.handleBooksResponse(response: response, completionHandler:  completionHandler)
        })
    }
    
    override func post(url: String, request: JSON, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        searchRequest?.cancel()
        searchRequest = Alamofire.request(getUrl()+url, method: .post, parameters: request.dictionaryObject, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            completionHandler(response)
        }
    }
    
    private func handleBooksResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ books: [Book]?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            do {
                var books = [Book]()
                for book in JSON(response)["items"].arrayValue {
                    books.append(try Book(json: book))
                }
                completionHandler(true, books)
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse searched books response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Search books response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
