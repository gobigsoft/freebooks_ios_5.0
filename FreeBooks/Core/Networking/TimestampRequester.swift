//
//  TimestampRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 20/06/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TimestampRequester: CoreRequester {
    func getCurrentTimestamp(completionHandler: @escaping(_ success: Bool, _ timestamp: Date?) -> Void) {
        getWithoutCache(url: Config.API.Routes.Timestamp, completionHandler: { response in
            return self.handleTimestampResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    private func handleTimestampResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ timestamp: Date?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            let timestamp = JSON(response)["timestamp"].numberValue
            let date = Date(timeIntervalSince1970: timestamp.doubleValue)
            completionHandler(true, date)
        } else {
            LoggingUtils.logMessage(message: "Timestamp response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
