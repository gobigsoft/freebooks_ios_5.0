//
//  CategoryDetailRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 01/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CategoryDetailRequester: CoreRequester {

    func getBooksForCategory(categoryId: Int, offset: Int, count: Int, sort: Int, completionHandler: @escaping(_ success: Bool, _ books: [Book]?) -> Void) {
       let url = Config.API.Routes.Category + "?id=\(categoryId)" + "&offset=\(offset)" + "&count=\(count)" + "&sort=\(sort)"
        get(url: url, completionHandler: { response in
            return self.handleBooksResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    private func handleBooksResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ books: [Book]?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            do {
                var books = [Book]()
                for book in JSON(response)["books"].arrayValue {
                    books.append(try Book(json: book))
                }
                completionHandler(true, books)
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse category books response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Category books response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
