//
//  AudiobookContentRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire

class AudiobookContentRequester: CoreRequester {
    
    private var downloadRequest: Request?
    
    func downloadChapter(chapterId: Int, progressHandler: @escaping(_ progress: Double, _ estimatedSize: Int64) -> Void, completionHandler: @escaping(_ success: Bool, _ bookData: Data?) -> Void) {
        downloadRequest = download(url: String(format: Config.API.Routes.DownloadAudiobookChapter, chapterId), progressHandler: { progress, estimatedSize in
            LoggingUtils.logMessage(message: "Audiobook chapter progress: \(progress)")
            progressHandler(progress, estimatedSize)
        }, completionHandler: { response in
            self.handleChapterDownloadResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    func cancelDownload() {
        downloadRequest?.cancel()
    }
    
    private func handleChapterDownloadResponse(response: DataResponse<Data>, completionHandler: (_ success: Bool, _ chapterData: Data?) -> Void) {
        let handledResponse = handleDataResponse(response: response)
        if let responseData = handledResponse.responseData {
            completionHandler(true, responseData)
        } else {
            LoggingUtils.logMessage(message: "No chapter response data returned, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}

