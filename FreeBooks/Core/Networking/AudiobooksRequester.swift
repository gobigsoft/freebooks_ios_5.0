//
//  AudiobooksRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AudiobooksRequester: CoreRequester {
    
    func getAudiobooks(completionHandler: @escaping(_ success: Bool, _ audiobooks: [Book]?) -> Void) {
        get(url: Config.API.Routes.Audiobooks, completionHandler: { response in
            return self.handleAudiobooksResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    private func handleAudiobooksResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ audiobooks: [Book]?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            do {
                var audiobooks = [Book]()
                for audiobook in JSON(response)["items"].arrayValue {
                    audiobooks.append(try Book(json: audiobook))
                }
                completionHandler(true, audiobooks)
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse audiobooks response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Audiobooks response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
