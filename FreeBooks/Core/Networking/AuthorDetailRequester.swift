//
//  AuthorDetailRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AuthorDetailRequester: CoreRequester {
    
    func getAuthorDetail(authorId: Int, completionHandler: @escaping(_ success: Bool, _ authorDetail: AuthorDetail?) -> Void) {
        get(url: String(format: Config.API.Routes.AuthorDetail, authorId), completionHandler: { response in
            return self.handleAuthorDetailResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    private func handleAuthorDetailResponse(response: DataResponse<Any>, completionHandler: (_ success: Bool, _ authorDetail: AuthorDetail?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            do {
                completionHandler(true, try AuthorDetail(json: JSON(response)))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse author detail response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Author detail response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
