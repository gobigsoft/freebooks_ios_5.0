//
//  BookContentRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 23/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire

class BookContentRequester: CoreRequester {
    
    private var downloadRequest: Request?
    
    func downloadBook(bookId: Int, progressHandler: @escaping(_ progress: Double, _ estimatedSize: Int64) -> Void, completionHandler: @escaping(_ success: Bool, _ bookData: Data?) -> Void) {
        downloadRequest = download(url: String(format: Config.API.Routes.DownloadBook, bookId), progressHandler: { progress, totalUnitCount in
            LoggingUtils.logMessage(message: "Download progress: \(progress)")
            progressHandler(progress, totalUnitCount)
        }, completionHandler: { response in
            self.handleBookDownloadResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    func cancelDownload() {
        downloadRequest?.cancel()
    }
    
    private func handleBookDownloadResponse(response: DataResponse<Data>, completionHandler: (_ success: Bool, _ bookData: Data?) -> Void) {
        let handledResponse = handleDataResponse(response: response)
        if let responseData = handledResponse.responseData {
            completionHandler(true, responseData)
        } else {
            LoggingUtils.logMessage(message: "No response data returned, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}



