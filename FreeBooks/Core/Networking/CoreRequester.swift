//
//  CoreRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/01/2019.
//  Copyright © 2019 Karol Grulling. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CoreRequester: NSObject {
    
    internal func getUrl() -> String {
        if Config.AppMode.ProductionAPI == false {
            return Config.API.BaseUrl.Testing
        } else {
            return Config.API.BaseUrl.Production
        }
    }
    
    internal func get(url: String, completionHandler: @escaping(_ response: DataResponse<Any>) -> Void) {
        Alamofire.request(getUrl()+url).validate(statusCode: [Config.API.StatusCode.Success]).responseJSON { response in
            completionHandler(response)
        }
    }
    
    internal func getWithoutCache(url: String, completionHandler: @escaping(_ response: DataResponse<Any>) -> Void) {
        //Since we always need to have the most recent timestamp, we need to avoid Alamofire's caching by appending a random number as requests argument, making it always considered as a new request  
        Alamofire.request(getUrl()+url+"?\(arc4random())").validate(statusCode: [Config.API.StatusCode.Success]).responseJSON { response in
            completionHandler(response)
        }
    }
    
    internal func post(url: String, request: JSON, completionHandler: @escaping(_ response: DataResponse<Any>) -> Void) {
        Alamofire.request(getUrl()+url, method: .post, parameters: request.dictionaryObject, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            completionHandler(response)
        }
    }
    
    internal func download(url: String, progressHandler: @escaping(_ progress: Double, _ expectedSize: Int64) -> Void, completionHandler: @escaping(_ data: DataResponse<Data>) -> Void) -> Request {
        return Alamofire.request(getUrl()+url).downloadProgress(closure: { progress in
            progressHandler(progress.fractionCompleted, progress.totalUnitCount)
        }).responseData(completionHandler: { response  in
            completionHandler(response)
        })
    }
    
    internal func handleResponse(response: DataResponse<Any>) -> (statusCode: Int, response: [String: Any]?) {
        let responseStatus = getResponseStatus(statusCode: response.response?.statusCode)
        switch response.result {
        case .success(let value):
            let json = value as? [String:Any]
            LoggingUtils.logMessage(message: String(describing: value))
            return (responseStatus, json)
        case .failure(_):
            return (responseStatus, nil)
        }
    }
    
    internal func handleArrayResponse(response: DataResponse<Any>) -> (statusCode: Int, response: [[String: Any]]?) {
        let responseStatus = getResponseStatus(statusCode: response.response?.statusCode)
        switch response.result {
        case .success(let value):
            let json = value as? [[String:Any]]
            LoggingUtils.logMessage(message: String(describing: value))
            return (responseStatus, json)
        case .failure(_):
            return (responseStatus, nil)
        }
    }
    
    internal func handleDataResponse(response: DataResponse<Data>) -> (statusCode: Int, responseData: Data?) {
        let responseStatus = getResponseStatus(statusCode: response.response?.statusCode)
        LoggingUtils.logMessage(message: "Data downloaded with status \(responseStatus)")
        switch response.result {
        case .success(let value):
            return (responseStatus, value)
        case .failure(_):
            return (responseStatus, nil)
        }
    }
    
    func getResponseStatus(statusCode: Int?) -> Int {
        return statusCode ?? Config.API.StatusCode.UnknownError
    }
}
