//
//  BookDetailRequester.swift
//  FreeBooks
//
//  Created by Karol Grulling on 26/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BookDetailRequester: CoreRequester {
    
    func getBookDetail(bookId: Int, completionHandler: @escaping(_ success: Bool, _ featured: BookDetail?) -> Void) {
        get(url: String(format: Config.API.Routes.BookDetail, bookId), completionHandler: { response in
            return self.handleBookDetailResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    func getBookDetailFromLegacyId(legacyId: Int, completionHandler: @escaping(_ success: Bool, _ featured: BookDetail?) -> Void) {
        get(url: String(format: Config.API.Routes.LegacyBooks, legacyId), completionHandler: { response in
            return self.handleLegacyBookDetailResponse(response: response, completionHandler: completionHandler)
        })
        
        //        get(url: String(format: Config.API.Routes.BookDetail, legacyId), completionHandler: { response in
        //            return self.handleBookDetailResponse(response: response, completionHandler: completionHandler)
        //        })
    }
    
    private func handleLegacyBookDetailResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ bookDetail: BookDetail?) -> Void) {
        let handledResponse = handleArrayResponse(response: response)
        if let response = handledResponse.response, let bookDetailJson = response.first {
            do {
                completionHandler(true, try BookDetail(json: JSON(bookDetailJson)))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse legacy book detail response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Legacy book detail response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
    
    private func handleBookDetailResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ bookDetail: BookDetail?) -> Void) {
        let handledResponse = handleResponse(response: response)
        if let response = handledResponse.response {
            do {
                completionHandler(true, try BookDetail(json: JSON(response)))
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse book detail response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Book detail response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
    
    func getBookReviews(bookId: Int, completionHandler: @escaping(_ success: Bool, _ reviews: [Review]?) -> Void) {
        get(url: String(format: Config.API.Routes.BookReviews, bookId), completionHandler: { response in
            return self.handleBookReviewsResponse(response: response, completionHandler: completionHandler)
        })
    }
    
    private func handleBookReviewsResponse(response: DataResponse<Any>, completionHandler: @escaping(_ success: Bool, _ reviews: [Review]?) -> Void) {
        let handledResponse = handleArrayResponse(response: response)
        if let response = handledResponse.response {
            do {
                var reviews = [Review]()
                for review in JSON(response).arrayValue {
                    reviews.append(try Review(json: review))
                }
                completionHandler(true, reviews)
            } catch let error as NSError {
                LoggingUtils.logMessage(message: "Couldn't parse book reviews response, error: \(error.description)")
                completionHandler(false, nil)
            }
        } else {
            LoggingUtils.logMessage(message: "Book reviews response not present, status code, error: \(handledResponse.statusCode) returned")
            completionHandler(false, nil)
        }
    }
}
