//
//  BookCollectionCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

internal protocol BookSectionSelectionDelegate {
    func bookSelected(book: Book)
}

class BaseBookSectionCell: UICollectionViewCell {
    
    fileprivate let BookCellIdentifier = "BookCell"
    
    private var viewModel: BaseBookSectionVM?
    private var bookCellIconsTintColor: UIColor?
    private var bookCellLabelsColor: UIColor?

    @IBOutlet weak internal var booksCollection: UICollectionView!
    private var delegate: BookSectionSelectionDelegate?

    //Reddish brown is an icon tint color in featured screen, therefore it's considered as default
    func initWithViewModel(viewModel: BaseBookSectionVM, delegate: BookSectionSelectionDelegate, bookCellIconsTintColor: UIColor = Constants.Colors.ReddishBrown, bookCellLabelsColor: UIColor = UIColor.black) {
        self.bookCellIconsTintColor = bookCellIconsTintColor
        self.bookCellLabelsColor = bookCellLabelsColor
        
        self.viewModel = viewModel
        self.delegate = delegate
        
        booksCollection.register(UINib(nibName: BookCellIdentifier, bundle: nil), forCellWithReuseIdentifier: BookCellIdentifier)
        booksCollection.dataSource = self
        booksCollection.delegate = self
    }
}

extension BaseBookSectionCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    internal func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getTotalBooksCount() ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bookCell = collectionView.dequeueReusableCell(withReuseIdentifier: BookCellIdentifier, for: indexPath) as! BookCell
        if let book = viewModel?.getBook(index: indexPath.row) {
            bookCell.initWithViewModel(viewModel: BookCellVM(book: book), iconsTint: bookCellIconsTintColor!, labelsColor: bookCellLabelsColor!)
        }
        return bookCell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let book = viewModel?.getBook(index: indexPath.row) {
            delegate?.bookSelected(book: book)
        }
    }
}
