//
//  BaseSeparatorCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 08/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BaseSeparatorCell: UICollectionViewCell {
    
    var separator: UIView!
    
    func addSeparator(color: UIColor = UIColor.init(white: 1, alpha: 0.05), offsetX: CGFloat = 0) {
        separator = UIView(frame: CGRect(x: offsetX, y: self.frame.size.height-1, width: UIScreen.main.bounds.size.width, height: 1))
        separator?.backgroundColor = color
        self.contentView.addSubview(separator)
    }
    
    func removeSeparator() {
        separator?.removeFromSuperview()
    }
    
    func updateSeparator(color: UIColor = UIColor.init(white: 1, alpha: 0.05), offsetX: CGFloat = 0) {
        removeSeparator()
        addSeparator(color: color, offsetX: offsetX)
    }
}
