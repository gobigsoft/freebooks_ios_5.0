//
//  BaseRoundedBorderedCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BaseRoundedBorderedCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        self.setRoundedCorners(radius: 5)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
}
