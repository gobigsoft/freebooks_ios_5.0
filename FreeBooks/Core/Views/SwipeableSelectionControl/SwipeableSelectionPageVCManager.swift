//
//  SwipeableSelectionPageVCManager.swift
//  FreeBooks
//
//  Created by Karol Grulling on 30/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class SwipeableSelectionVC: UIViewController {
    var pageIndex = 0
}

protocol SwipeableSelectionPageVCManagerDelegate {
    func scrollingCompletion(completedPercentage: CGFloat)
    func scrollingToIndex(index: Int)
    func scrolledToIndex(index: Int)
}

class SwipeableSelectionPageVCManager: NSObject, UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate {
    
    private let viewControllers: [SwipeableSelectionVC]
    private let pageViewController = UIPageViewController(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
    private var currentIndex = 0
    var delegate: SwipeableSelectionPageVCManagerDelegate?
    
    init(viewControllers: [SwipeableSelectionVC], containerView: UIView) {
        self.viewControllers = viewControllers
        for (index, element) in self.viewControllers.enumerated() {
            element.pageIndex = index
        }
        super.init()
        setupPageVC(container: containerView)
    }
    
    func scrollToIndex(index: Int) {
        if index >= viewControllers.count {
            return
        }
        let navigationDirection = index > currentIndex ? UIPageViewController.NavigationDirection.forward : UIPageViewController.NavigationDirection.reverse
        pageViewController.setViewControllers([getViewControllerAtIndex(index: index)], direction: navigationDirection, animated: true, completion:  nil)
        delegate?.scrollingToIndex(index: index)
        delegate?.scrolledToIndex(index: index)
        currentIndex = index
    }
    
    func getCurrentIndex() -> Int {
        return currentIndex
    }
    
    private func setupPageVC(container: UIView) {
        pageViewController.delegate = self
        pageViewController.dataSource = self
        pageViewController.scrollView?.delegate = self
        pageViewController.scrollView?.bounces = true
        pageViewController.setViewControllers([getViewControllerAtIndex(index: 0) as SwipeableSelectionVC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        container.addSubview(pageViewController.view)
        if let view = pageViewController.view.superview {
            pageViewController.view.frame = view.bounds
        }
    }
    
    //MARK: Datasource functions
    internal func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageContent = viewController as! SwipeableSelectionVC
        if ((pageContent.pageIndex == 0) || (pageContent.pageIndex == NSNotFound)) {
            return nil
        }
        return getViewControllerAtIndex(index: pageContent.pageIndex-1)
    }
    
    internal func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let pageContent = viewController as! SwipeableSelectionVC
        if (pageContent.pageIndex == viewControllers.count-1)
        {
            return nil;
        }
        return getViewControllerAtIndex(index: pageContent.pageIndex+1)
    }
    
    internal func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return viewControllers.count
    }
    
    private func getViewControllerAtIndex(index: NSInteger) -> SwipeableSelectionVC {
        return viewControllers[index]
    }
    
    //MARK: Delegate functions
    internal func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        let pendingVC = pendingViewControllers.first as! SwipeableSelectionVC
        delegate?.scrollingToIndex(index: pendingVC.pageIndex)
    }
    
    internal func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            let finishedVC = pageViewController.viewControllers!.first as! SwipeableSelectionVC
            currentIndex = finishedVC.pageIndex
            delegate?.scrolledToIndex(index: finishedVC.pageIndex)
        }
    }
    
    //MARK: Scrollview delegate
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let point = scrollView.contentOffset
        var percentComplete: CGFloat
        
        if point.x == UIScreen.main.bounds.size.width {
            return
        }
        
        percentComplete = abs((point.x - UIScreen.main.bounds.size.width)/UIScreen.main.bounds.size.width)
        delegate?.scrollingCompletion(completedPercentage: percentComplete)
    }
}
