//
//  SwipeableSelectionControl.swift
//  FreeBooks
//
//  Created by Karol Grulling on 31/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol SwipeableSelectionControlDelegate {
    func itemSelected(itemIndex: Int)
}

class SwipeableSelectionControl: UIView {
    
    private let SwipeableSelectionControlNibName = "SwipeableSelectionControl"
    private var view: UIView!
    
    //These are internal so we can style them via extensions
    @IBOutlet weak internal var buttonStackView: UIStackView!
    @IBOutlet weak internal var selectionView: UIView!
    @IBOutlet weak internal var selectionViewWidthConstraint: NSLayoutConstraint!
    
    private var animator: UIViewPropertyAnimator?
    private var destinationIndex = 1
    
    private let selectedColor: UIColor
    private let unselectedColor: UIColor
    private let selectionViewColor: UIColor
    
    var delegate: SwipeableSelectionControlDelegate?
    
    init(frame: CGRect, selectedColor: UIColor = UIColor.white, unselectedColor: UIColor =  UIColor.init(white: 1, alpha: 0.45), selectionViewColor: UIColor = UIColor.white, optionTitles: [String]? = nil) {
        //Since this is reusable component we want to be able to specify colors at init
        self.selectedColor = selectedColor
        self.unselectedColor = unselectedColor
        self.selectionViewColor = selectionViewColor
        
        super.init(frame: frame)
        xibSetup()
        
        if let customTitles = optionTitles {
            setOptionTitles(titles: customTitles)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        //we need to populate these with default colors - init(coder:) needs to be provided for UIView even though is not used in our case - we're initialising via init(frame:)
        self.selectedColor = UIColor.white
        self.unselectedColor = UIColor.init(white: 1, alpha: 0.45)
        self.selectionViewColor = UIColor.white

        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
        
        setupAnimator(destinationIndex: 1)
        setSelectedVisual(itemIndex: 0)
        
        selectionViewWidthConstraint.constant = self.view.frame.size.width/2
        self.selectionView.backgroundColor = self.selectionViewColor
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: SwipeableSelectionControlNibName, bundle: bundle)
        let views = nib.instantiate(withOwner: self, options: nil)
        let appearanceSettingsView = views[0] as! UIView
        return appearanceSettingsView
    }
    
    private func setOptionTitles(titles: [String]) {
        for (index, element) in buttonStackView.arrangedSubviews.enumerated() {
            (element as! UIButton).setTitle(titles[index], for: UIControl.State.normal)
        }
    }
 
    func moveSelectionView(completedPercentage: CGFloat) {
        animator?.fractionComplete = completedPercentage
        if completedPercentage == 1 {
            destinationIndex == 1 ? setupAnimator(destinationIndex: 0) : setupAnimator(destinationIndex: 1)
        }
    }
    
    func setDestinationIndex(destinationIndex: Int) {
        self.destinationIndex = destinationIndex
    }
    
    func setSelectedVisual(itemIndex: Int) {
        for (index, element) in buttonStackView.arrangedSubviews.enumerated() {
            index == itemIndex ? (element as! UIButton).setTitleColor(selectedColor, for: UIControl.State.normal) : (element as! UIButton).setTitleColor(unselectedColor, for: UIControl.State.normal)
        }
    }
    
    private func setupAnimator(destinationIndex: Int) {
        animator?.stopAnimation(false)
        animator?.finishAnimation(at: UIViewAnimatingPosition.current)
        animator = UIViewPropertyAnimator(duration: 1, curve: .easeIn) {
            self.selectionView.transform = destinationIndex == 0 ? CGAffineTransform(translationX: 0, y: 0) : CGAffineTransform(translationX: self.view.frame.size.width-self.selectionView.frame.size.width, y: 0)
        }
        animator?.pausesOnCompletion = true
    }
    
    @IBAction private func actionSelected(sender: UIButton) {
        self.delegate?.itemSelected(itemIndex: sender.tag)
    }
}
