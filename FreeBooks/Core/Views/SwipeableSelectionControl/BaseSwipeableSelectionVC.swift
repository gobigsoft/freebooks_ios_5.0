//
//  BaseSwipeableSelectionVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 21/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit



class BaseSwipeableSelectionVC: BaseVC, SwipeableSelectionControlDelegate, SwipeableSelectionPageVCManagerDelegate {
    @IBOutlet weak internal var pageVCContainer: UIView!
    @IBOutlet weak internal var controlsContainer: UIView!
    internal var selectionPageVCManager: SwipeableSelectionPageVCManager?
    internal var control: SwipeableSelectionControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        selectionPageVCManager?.delegate = self
        control?.delegate = self
        
        if let control = self.control {
            controlsContainer.addSubview(control)
        }
    }
    
    //MARK: Swipeable Selection Page VC Delegate
    internal func scrollingCompletion(completedPercentage: CGFloat) {
        control?.moveSelectionView(completedPercentage: completedPercentage)
    }
    
    internal func scrollingToIndex(index: Int) {
        control?.setDestinationIndex(destinationIndex: index)
    }
    
    internal func scrolledToIndex(index: Int) {
        control?.setSelectedVisual(itemIndex: index)
    }
    
    //MARK: Swipeable Selection Control delegate
    internal func itemSelected(itemIndex: Int) {
        selectionPageVCManager?.scrollToIndex(index: itemIndex)
    }
}
