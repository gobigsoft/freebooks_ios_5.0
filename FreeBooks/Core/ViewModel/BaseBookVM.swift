//
//  BaseBookVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 31/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BaseBookVM: NSObject {
    
    internal let book: Book
    
    init(book: Book) {
        self.book = book
    }
    
    func isTop() -> Bool {
        return self.book.isTop
    }
    
    func getBookType() -> BookType {
        if self.book.type == BookType.Book.rawValue {
            return BookType.Book
        } else {
            return BookType.Audiobook
        }
    }
    
    func getRating() -> Double {
        return Utils.getBookRating(book: book)
    }
    
    func getRating() -> String {
        return String(format: "%.1f", Utils.getBookRating(book: book))
    }
    
    func getBook() -> Book {
        return book
    }
}
