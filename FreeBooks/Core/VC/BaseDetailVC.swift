//
//  BaseDetailVCViewController.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BaseDetailVC: BaseVC, UIGestureRecognizerDelegate, PlayerVCDelegate, PlayerAnimatorDelegate {
    
    private var playerContainer: UIView?
    private lazy var playerVC: PlayerVC = {
        let playerVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.PlayerVC) as! PlayerVC
        playerVC.delegate = self
        return playerVC
    }()
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(playerPanned(recognizer:)))
        recognizer.delegate = self
        return recognizer
    }()
    
    private lazy var tapRecognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer()
        recognizer.addTarget(self, action: #selector(showPlayer))
        recognizer.delegate = self
        return recognizer
    }()
    
    private lazy var playerHeight: CGFloat = {
        if DeviceUtils.isBezelessDevice() {
            return Constants.Misc.PlayerMiniatureBezelessDeviceSizeHeight
        } else {
            return Constants.Misc.PlayerMiniatureSizeHeight
        }
    }()
    
    private var playerAnimator: PlayerAnimator?
    
    func initPlayerView() {
        //Place player container into view
        playerContainer = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height-playerHeight, width: self.view.frame.size.width, height: self.view.frame.size.height))
        
        //Add gesture recognizer
        playerContainer?.addGestureRecognizer(panRecognizer)
        
        //Put PLayerVC its view into container
        playerContainer?.addSubview(playerVC.view)
        
        //Player miniature is initialized after the Player VC is added as a subview, therefore this is the earliest place we can add a recognizer to it
        playerVC.playerMiniature.addGestureRecognizer(tapRecognizer)
        
        //Add container with Player into parent view
        self.view.addSubview(playerContainer!)
        self.view.bringSubviewToFront(playerContainer!)
        
        //Setup animator which handles all the Pan gestures
        playerAnimator = PlayerAnimator(playerContainer: playerContainer!, parentView: self.view, playerMiniature: playerVC.playerMiniature, offset: playerHeight, tabBar: nil)
        playerAnimator?.delegate = self
    }
    
    func openPlayer(bookDetail: BookDetail) {
        if playerContainer == nil {
            initPlayerView()
        }
        playerAnimator?.animateTransitionIfNeeded(to: .open, duration: Constants.Misc.PlayerDisplayAnimationDuration)
        
        PlayerEngine.sharedEngine.setupWithAudiobookDetail(audiobookDetail: bookDetail)
        PlayerEngine.sharedEngine.continuePlayback()
    }
    
    @objc func showPlayer() {
        playerAnimator?.animateTransitionIfNeeded(to: .open, duration: Constants.Misc.PlayerDisplayAnimationDuration)
    }
    
    //Pan gesture recognizer which is taken over by player animator
    @objc private func playerPanned(recognizer: UIPanGestureRecognizer) {
        playerAnimator?.playerViewPanned(recognizer: recognizer)
    }
    
    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIControl || playerVC.playbackSpeedDarkView.isHidden == false {
            return false
        }
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(hidePlayer), name: NSNotification.Name(Constants.NotificationConstants.PlayerCancelled), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LoggingUtils.logMessage(message: "Book Detail Will Appear")
        
        //If Player Engine isn't closed we know there's a playback going on and therefore we need to display the player
        if PlayerEngine.sharedEngine.getState() != .closed {
            if playerContainer == nil {
                initPlayerView()
                playerVC.setup()
                playerVC.setMiniatureVisible()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        InterstitialAdProvider.sharedProvider.showFirstActionInterstitial(inVC: self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    internal func showAbout(aboutTitle: String, about: String) {
        let aboutVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.AboutVC) as! AboutVC
        aboutVC.initWithContents(aboutTitle: aboutTitle, about: about)
        self.present(aboutVC, animated: true, completion: nil)
    }
    
    internal func cancelPlayerPressed() {
        NotificationCenter.default.post(name: NSNotification.Name(Constants.NotificationConstants.PlayerCancelled), object: nil)
    }
    
    @objc private func hidePlayer() {
        playerAnimator?.hide()
    }
    
    internal func playerOpened() {
        //We don't need to handle this callback 
    }
    
    func hidePlayerPressed() {
        playerAnimator?.animateTransitionIfNeeded(to: .closed, duration: Constants.Misc.PlayerDisplayAnimationDuration)
    }
    
    internal func playerClosed() {
        //Every time a player is closed, we need to let My Library Books VC know to update its current progress, since there's no direct relation between Tab Bar Controller and My Library Books VC, we need to post notification
        NotificationCenter.default.post(name: NSNotification.Name(Constants.NotificationConstants.PlayerClosed), object: nil)
    }
    
    internal func showChaptersPressed() {
        if let audiobookDetail = PlayerEngine.sharedEngine.getAudiobookDetail() {
            let audiobookChaptersVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.AudiobookChaptersVC) as! AudiobookChaptersVC
            audiobookChaptersVC.initWithChapters(audiobookChapters: audiobookDetail.audiobookChapters, audiobookDetail: audiobookDetail)
            self.present(audiobookChaptersVC, animated: true, completion: nil)
        }
    }
    
    internal func showAudiobookDetailPressed() {
        if let audiobookDetail = PlayerEngine.sharedEngine.getAudiobookDetail() {
            let bookDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookDetailVC) as! BookDetailVC
            bookDetailVC.initWithBook(book: audiobookDetail)
            self.present(bookDetailVC, animated: true, completion: nil)
        }
    }
}
