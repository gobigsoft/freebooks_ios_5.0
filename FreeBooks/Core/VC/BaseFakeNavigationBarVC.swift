//
//  BaseFakeNavigationBarVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 08/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BaseFakeNavigationBarVC: BaseVC {
    
    @IBOutlet internal weak var screenTitle: UILabel!
    @IBOutlet private weak var fakeNavigationBarDivider: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIUtils.dropBottomShadow(view: fakeNavigationBarDivider)
    }
    
    @IBAction func closePressed() {
        self.dismiss(animated: true, completion: nil)
    }
}
