//
//  BaseIndexableVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 05/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BaseIndexableVC: UIViewController {
    
    var pageIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
