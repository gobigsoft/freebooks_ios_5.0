//
//  BaseVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension UIView {
    func setRoundedCorners(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
}

class BaseVC: UIViewController {
    
    @IBOutlet weak private var noConnectionView: UIView?
    @IBOutlet weak internal var noConnectionIcon: UIImageView?
    
    var indicator: LoadingIndicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noConnectionIcon?.tintColor = Constants.Colors.GrayLight
    }
    
    internal func showBookDetail(book: Book) {
        if let bookDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookDetailVC) as? BookDetailVC {
            bookDetailVC.initWithBook(book: book)
            self.present(bookDetailVC, animated: true, completion: nil)
        }
    }
    
    internal func showBookDetail(bookId: Int) {
        if let bookDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookDetailVC) as? BookDetailVC {
            bookDetailVC.initWithBookId(bookId: bookId)
            self.present(bookDetailVC, animated: true, completion: nil)
        }
    }
    
    internal func showAuthorDetail(author: Author) {
        if let authorDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.AuthorDetailVC) as? AuthorDetailVC {
            authorDetailVC.initWithAuthor(author: author)
            self.present(authorDetailVC, animated: true, completion: nil)
        }
    }
    
    internal func shareBook(title: String, authorName: String) {
        let textToShare = String(format: "%@ \nby %@ \n \nShared via My Books app \n%@", title, authorName, Constants.Misc.AppStoreUrl)
        let activityVC = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    internal func setupLoading(color: UIColor) {
        if indicator == nil {
            indicator = LoadingIndicator(frame: CGRect(x: 0, y: 0, width: 40, height: 40), color: color)
        }
    }
    
    internal func showLoading(color: UIColor = Constants.Colors.ReddishBrown, centerOffset: CGFloat = 0) {
        setupLoading(color: color)
        if let loadingIndicator = indicator {
            loadingIndicator.center = CGPoint.init(x: self.view.center.x, y: self.view.center.y+centerOffset)
            self.view.addSubview(loadingIndicator)
            loadingIndicator.startAnimating()
        }
    }
    
    internal func hideLoading() {
        if let loadingIndicator = indicator {
            loadingIndicator.removeFromSuperview()
            loadingIndicator.stopAnimating()
        }
    }
    
    @IBAction internal func goToSearch() {
        //Search screen is on tab bar's 4th index
        self.tabBarController?.selectedIndex = 4
    }
    
    internal func goToBrowse() {
        //Search screen is on tab bar's 2nd index
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction internal func backPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction internal func backPressed(completion: (() -> ())? = nil) {
        self.dismiss(animated: true, completion: completion)
    }
    
    internal func showNoConnectionView() {
        noConnectionView?.isHidden = false
    }
    
    internal func hideNoConnectionView() {
        noConnectionView?.isHidden = true
    }
    
    internal func registerScrollToTopNotificationObserver(notificationName: String) {
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToTop), name: Notification.Name(notificationName), object: nil)
    }
    
    @objc internal func scrollToTop() {
        //To be overriden by particular VC
    }
}

extension BaseVC {
    override func present(_ viewControllerToPresent: UIViewController,
                          animated flag: Bool,
                          completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = .fullScreen
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}
