//
//  PurchaseEngine.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import StoreKit

class PurchaseEngine: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver, SKRequestDelegate {
    
    static let sharedEngine = PurchaseEngine()
 
    private var adFreeModeProduct: SKProduct?
    private var audiobooksSubscriptionProduct: SKProduct?
    
    func setPaymentQueueObserver() {
        LoggingUtils.logMessage(message: "Setting payment queue observer")
        SKPaymentQueue.default().add(self)
    }
    
    func requestProducts() {
        let productsRequest = SKProductsRequest(productIdentifiers: [Config.InAppPurchases.PurchaseItemRemoveAds, Config.InAppPurchases.PurchaseItemAudiobooksSubscription])
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        LoggingUtils.logMessage(message: "PurchaseEngine: SKRequest did fail with error, error: \(error.localizedDescription)")
    }
    
    internal func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let products = response.products
        for product in products {
            if product.productIdentifier == Config.InAppPurchases.PurchaseItemRemoveAds {
                adFreeModeProduct = product
            } else if product.productIdentifier == Config.InAppPurchases.PurchaseItemAudiobooksSubscription {
                audiobooksSubscriptionProduct = product
            }
        }
    }
    
    func purchaseRemoveAds() {
        if let product = self.adFreeModeProduct {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
            LoggingUtils.logMessage(message: "Purchasing ad removal with id \(product.productIdentifier)...")
        }
    }
    
    func purchaseSubscription() {
        if let product = self.audiobooksSubscriptionProduct {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
            LoggingUtils.logMessage(message: "Purchasing subscription with id \(product.productIdentifier)...")
        }
    }
    
    func restorePurchases() {
        LoggingUtils.logMessage(message: "PurchaseEngine: Restore purchases called")
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    func finishTransactions() {
        for transaction in SKPaymentQueue.default().transactions {
            LoggingUtils.logMessage(message: "PurchaseEngine: Finishing transaction: \(String(describing: transaction.original?.payment.productIdentifier))")
            SKPaymentQueue.default().finishTransaction(transaction)
        }
    }
}

extension PurchaseEngine {
    internal func paymentQueue(_ queue: SKPaymentQueue,
                               updatedTransactions transactions: [SKPaymentTransaction]) {
        LoggingUtils.logMessage(message: "PurchaseEngine: PaymentQueue updatedTransactions")
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                restore(transaction: transaction)
                break
            case .deferred:
                fail(transaction: transaction)
                break
            case .purchasing:
                break
            }
        }
    }
    
    internal func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        LoggingUtils.logMessage(message: "PurchaseEngine: Subscription: Restoring completed transactions successful, validating subscription status.")
        if !UserDefaultsUtils.areAudiobooksPurchased() {
            //At this point the restoration of purchases has finished, therefore we can proceed with verifying the subscription state
            //Applies only for cases when user doesn't bring audiobooks purchase from the former app
            verifyAudiobooksSubscriptionState()
        }
        
        if !UserDefaultsUtils.areAdsRemoved() {
            //Purchase restoration restores non-consumable items only after the very first restore, for further restores we need to use receipt in order to verify ad free purchase state
            verifyAdFreePurchase()
        }
    }
    
    internal func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        LoggingUtils.logMessage(message: "PurchaseEngine: Subscription: Restoring completed transactions failed, validating subscription status.")
        if !UserDefaultsUtils.areAudiobooksPurchased() {
            //At this point the restoration of purchases has finished therefore we can proceed with verifying the subscription state. Despite the fact that the restoration failed, we can still look for the current App Store receipt or, if not present, try to retrieve it again. If the receipt can't be retrieved after all of these attempts, we'll consider the subscription as inactive.
            //Applies only for cases when user doesn't bring audiobooks purchase from the former app
            verifyAudiobooksSubscriptionState()
        }
        
        if !UserDefaultsUtils.areAdsRemoved() {
            //Purchase restoration restores non-consumable items only after the very first restore, for further restores we need to use receipt in order to verify ad free purchase state
            verifyAdFreePurchase()
        }
    }
    
    private func complete(transaction: SKPaymentTransaction) {
        handlePaymentTransaction(transaction: transaction)
    }
    
    private func restore(transaction: SKPaymentTransaction) {
        handlePaymentTransaction(transaction: transaction, isRestore: true)
    }
    
    func verifyAudiobooksSubscriptionState() {
        SubscriptionValidator().isSubscriptionValid(completionHandler: {
            success in
            LoggingUtils.logMessage(message: "PurchaseEngine: Subscription: Audiobooks subscription validation ended with result \(success)")
            if success {
                self.sendSubscriptionActiveNotification()
                UserDefaultsUtils.setAudiobooksSubscriptionActive()
            } else {
                self.sendSubscriptionInactiveNotification()
                UserDefaultsUtils.setAudiobooksSubscriptionInactive()
            }
        })
    }
    
    func verifyAdFreePurchase() {
        LoggingUtils.logMessage(message: "PurchaseEngine: Verifying ad free purchase")
        AdFreePurchaseValidator().isAdFreePurchased(completionHandler: {
            success in
            if success {
                LoggingUtils.logMessage(message: "PurchaseEngine: Ad free was already purchased, removing ads")
                UserDefaultsUtils.setAdsRemovedPurchased()
                self.sendAdsRemovedNotification()
            }
        })
    }
    
    private func handlePaymentTransaction(transaction: SKPaymentTransaction, isRestore: Bool = false) {
        LoggingUtils.logMessage(message: "PurchaseEngine: handlePaymentTransaction, isRestore: \(isRestore)")
        guard let productIdentifier = isRestore == true ? transaction.original?.payment.productIdentifier : transaction.payment.productIdentifier else {
            return
        }
        
        LoggingUtils.logMessage(message: "PurchaseEngine: Handling completed/restored transaction with id... \(productIdentifier)")

        if productIdentifier == Config.InAppPurchases.PurchaseItemRemoveAds {
            UserDefaultsUtils.setAdsRemovedPurchased()
            sendAdsRemovedNotification()
        } else if (productIdentifier == Config.InAppPurchases.PurchaseItemAudiobooksSubscription) {
            self.sendSubscriptionActiveNotification()
            UserDefaultsUtils.setAudiobooksSubscriptionActive()
        } else if (productIdentifier == Config.InAppPurchases.PurchaseItemAudiobooks || productIdentifier == Config.InAppPurchases.PurchaseItemUnlockAudiobooks) {
            //PurchaseItemAudiobooks // PurchasItemUnlockAudiobooks refers to audiobooks purchase from the former app, if this has been already purchased, we set the subscription as active and don't perform any further subscription validity checks
            UserDefaultsUtils.setAudiobooksPurchased()
            UserDefaultsUtils.setAudiobooksSubscriptionActive()
            self.sendSubscriptionActiveNotification()
        }
        
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func fail(transaction: SKPaymentTransaction) {
        LoggingUtils.logMessage(message: "PurchaseEngine: transaction failed")
        if let transactionError = transaction.error as NSError?,
            let localizedDescription = transaction.error?.localizedDescription,
            transactionError.code != SKError.paymentCancelled.rawValue {
            LoggingUtils.logMessage(message: "PurchaseEngine: An error occurred while performing SKPaymentTransaction: \(localizedDescription)")
        }
        
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    //After successful ad removal we need to make sure UI is immediately updated and ads are removed - to give a sense that the purchase was successful 
    private func sendAdsRemovedNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotificationConstants.AdsRemoved), object: nil)
    }
    
    //We need to notify VCs that the subscription is not active so they can update their UI, e.g. hide player that was opened from the previous app's session
    private func sendSubscriptionInactiveNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotificationConstants.AudiobooksSubscriptionInactive), object: nil)
    }
    
    //We need to notify VCs that the subscription is active so they can update their UI, e.g. book detail VC update it's action button from SUBSCRIBE to PLAY
    private func sendSubscriptionActiveNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotificationConstants.AudiobooksSubscriptionActive), object: nil)
    }
}
