//
//  SubscriptionValidator.swift
//  FreeBooks
//
//  Created by Karol Grulling on 11/06/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import StoreKit

class SubscriptionValidator: NSObject, SKRequestDelegate {
    
    var subscriptionCompletionHandler: ((Bool) -> ())?
    
    func isSubscriptionValid(completionHandler: @escaping(_ valid: Bool) -> Void) {
        subscriptionCompletionHandler = completionHandler
        
        //If we have receipt, we'll validate it, if we don't we'll try to retrieve it, if we can't retrieve it, we consider subscription as invalid 
        if ReceiptValidator.isReceiptPresent() {
            LoggingUtils.logMessage(message: "Subscription: Receipt is present")
            validateExpirationDate(completionHandler: completionHandler)
        } else {
            LoggingUtils.logMessage(message: "Subscription: Receipt is not present, retrieving receipt")
            refreshReceipt()
        }
    }
    
    func refreshReceipt() {
        let refreshRequest = SKReceiptRefreshRequest()
        refreshRequest.delegate = self
        refreshRequest.start()
    }
    
    internal func requestDidFinish(_ request: SKRequest) {
        LoggingUtils.logMessage(message: "Subscription: Receipt request finished successfully")
        if let subscriptionCompletionHandler = self.subscriptionCompletionHandler {
            validateExpirationDate(completionHandler: subscriptionCompletionHandler)
        }
    }
    
    internal func validateExpirationDate(completionHandler: @escaping(_ valid: Bool) -> Void) {
        getCurrentDate(completionHandler: { date in
            if let date = date {
                LoggingUtils.logMessage(message: "Receipt validation: current timestamp received from API: \(date)")
                completionHandler(self.isSubscriptionReceiptValid(currentDate: date))
            } else {
                LoggingUtils.logMessage(message: "Receipt validation: current timestamp not received from backend, using local timestamp: \(String(describing: Date()))")
                completionHandler(self.isSubscriptionReceiptValid(currentDate: Date()))
            }
        })
    }
    
    internal func getCurrentDate(completionHandler: @escaping(_ currentDate: Date?) -> Void) {
        TimestampRequester().getCurrentTimestamp(completionHandler: { success, date in
            if let date = date, success == true {
                completionHandler(date)
            } else {
                completionHandler(nil)
            }
        })
    }
    
    internal func request(_ request: SKRequest, didFailWithError error: Error) {
        LoggingUtils.logMessage(message: "Subscription: Receipt couldn't be obtained and there's no receipt stored. Cancelling subscription.")
        subscriptionCompletionHandler?(false)
    }
    
    private func isSubscriptionReceiptValid(currentDate: Date) -> Bool {
        var isValid = false
        
        //If there is subscription receipt in place, we'll validate it with the current date
        if let subscriptionReceipt = ReceiptValidator.getSubscriptionReceipt(), subscriptionReceipt.subscriptionCancellationDate == nil, let subscriptionExpirationDate = subscriptionReceipt.subscriptionExpirationDate, subscriptionExpirationDate > currentDate {
            isValid = true
        }
        
        //If there is a receipt coming from non consumable purchase - from the older app, we consider it as valid too
        if let _ = ReceiptValidator.getAudiobooksNonConsumableReceipt() {
            isValid = true
        }
        
        return isValid
    }
}

class AdFreePurchaseValidator: NSObject, SKRequestDelegate {
    var adFreePurchaseCompletionHandler: ((Bool) -> ())?

    func isAdFreePurchased(completionHandler: @escaping(_ wasPurchased: Bool) -> Void) {
        adFreePurchaseCompletionHandler = completionHandler
        
        //If we have receipt, we'll validate it, if we don't we'll try to retrieve it, if we can't retrieve it, we consider ad free as not purchased
        if ReceiptValidator.isReceiptPresent() {
            ReceiptValidator.getAdFreeReceipt() != nil ? completionHandler(true) : completionHandler(false)
        } else {
            refreshReceipt()
        }
    }
    
    func refreshReceipt() {
        let refreshRequest = SKReceiptRefreshRequest()
        refreshRequest.delegate = self
        refreshRequest.start()
    }
    
    internal func requestDidFinish(_ request: SKRequest) {
        ReceiptValidator.getAdFreeReceipt() != nil ? adFreePurchaseCompletionHandler?(true) : adFreePurchaseCompletionHandler?(false)
    }
    
    internal func request(_ request: SKRequest, didFailWithError error: Error) {
        LoggingUtils.logMessage(message: "Subscription: Receipt couldn't be obtained and there's no receipt stored. Ad free is not purchased.")
        adFreePurchaseCompletionHandler?(false)
    }
}

enum ReceiptStatus: String {
    case validationSuccess = "This receipt is valid."
    case noReceiptPresent = "A receipt was not found on this device."
    case unknownFailure = "An unexpected failure occurred during verification."
    case unknownReceiptFormat = "The receipt is not in PKCS7 format."
    case invalidPKCS7Signature = "Invalid PKCS7 Signature."
    case invalidPKCS7Type = "Invalid PKCS7 Type."
    case invalidAppleRootCertificate = "Public Apple root certificate not found."
    case failedAppleSignature = "Receipt not signed by Apple."
    case unexpectedASN1Type = "Unexpected ASN1 Type."
    case missingComponent = "Expected component was not found."
    case invalidBundleIdentifier = "Receipt bundle identifier does not match application bundle identifier."
    case invalidVersionIdentifier = "Receipt version identifier does not match application version."
    case invalidHash = "Receipt failed hash check."
    case invalidExpired = "Receipt has expired."
}

class ReceiptValidator: NSObject {
    static public func isReceiptPresent() -> Bool {
        if let receiptUrl = Bundle.main.appStoreReceiptURL, let canReach = try? receiptUrl.checkResourceIsReachable(), canReach {
            return true
        }
        return false
    }
    
    static public func getSubscriptionReceipt() -> PurchaseReceipt? {
        if let subscriptionReceipt = getPurchaseReceipts()?.filter({$0.productIdentifier == Config.InAppPurchases.PurchaseItemAudiobooksSubscription}).last {
            return subscriptionReceipt
        } else {
            return nil
        }
    }
    
    static public func getAdFreeReceipt() -> PurchaseReceipt? {
        if let adFreeReceipt = getPurchaseReceipts()?.filter({$0.productIdentifier == Config.InAppPurchases.PurchaseItemRemoveAds}).last {
            return adFreeReceipt
        } else {
            return nil
        }
    }
    
    static public func getAudiobooksNonConsumableReceipt() -> PurchaseReceipt? {
        if let adFreeReceipt = getPurchaseReceipts()?.filter({$0.productIdentifier == Config.InAppPurchases.PurchaseItemAudiobooks || $0.productIdentifier == Config.InAppPurchases.PurchaseItemUnlockAudiobooks}).last {
            return adFreeReceipt
        } else {
            return nil
        }
    }
    
    static public func getPurchaseReceipts() -> [PurchaseReceipt]? {
        let receipt = MainReceipt()
        guard receipt.receiptStatus == .validationSuccess else {
            return nil
        }
        let inAppReceipts = receipt.inAppReceipts
        
        LoggingUtils.logMessage(message: receipt.getReceiptContents())
        inAppReceipts.forEach({
            LoggingUtils.logMessage(message: $0.getReceiptContents())
        })
        
        return inAppReceipts
    }
}
