//
//  BookCollectionCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

internal protocol BookSectionSelectionDelegate {
    func bookSelected(book: Book)
}

class BaseBookSectionCell: BaseRoundedBorderedCell {
    
    fileprivate var viewModel: BaseBookSectionVM?
    @IBOutlet weak internal var booksCollection: UICollectionView!
    internal var delegate: BookSectionSelectionDelegate?

    func initWithViewModel(viewModel: BaseBookSectionVM, delegate: BookSectionSelectionDelegate) {
        self.viewModel = viewModel
        self.delegate = delegate
        booksCollection.dataSource = self
        booksCollection.delegate = self
    }
}

extension BaseBookSectionCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    internal func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getTotalBooksCount() ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bookCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCell", for: indexPath) as! BookCell
        guard let editorsPickViewModel = viewModel else {
            return bookCell
        }
        bookCell.initWithViewModel(viewModel: BookCellVM(book: editorsPickViewModel.getBook(index: indexPath.row)))
        return bookCell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let book = viewModel?.getBook(index: indexPath.row) {
            delegate?.featuredBookSelected(book: book)
        }
    }
}
