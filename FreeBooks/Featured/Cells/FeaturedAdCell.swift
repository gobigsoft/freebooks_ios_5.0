//
//  FeaturedAdCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class FeaturedAdCell: AdCell {
    //Here we'd want to inherit AdCell & BaseRoundedBorderedCell, which we can't do in Swift, therefore we need to have this awake from nib again here
    override func awakeFromNib() {
        self.setRoundedCorners(radius: 5)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
}
