//
//  QuotesCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class QuotesCell: BaseRoundedBorderedCell {
    
    @IBOutlet private var authorNames: [UILabel]!
    @IBOutlet private var quotes: [UILabel]!
    @IBOutlet private var authorImages: [UIImageView]!
    @IBOutlet private weak var quote1container: UIView!
    @IBOutlet private weak var quote2container: UIView!
    @IBOutlet private weak var quote3container: UIView!

    func initWithViewModel(viewModel: QuotesVM) {
        let smallestCount = min(authorNames.count, viewModel.getQuotesCount())
        for index in 0...smallestCount-1 {
            authorNames[index].text = viewModel.getAuthorNameForQuote(index: index)
            quotes[index].text = viewModel.getQuote(index: index)
            
            let authorImage = authorImages[index]
            authorImage.setRoundedCorners(radius: authorImage.frame.height/2)
            authorImages[index].kf.setImage(with: URL(string: viewModel.getAuthorImageForQuote(index: index)), options: [.transition(.fade(0.2))])
        }
    }
    
    @IBAction func showMorePressed() {
        quote2container.isHidden && quote3container.isHidden ? setQuoteContainersHidden(hidden: false) : setQuoteContainersHidden(hidden: true)
    }
    
    func setQuoteContainersHidden(hidden: Bool) {
        quote2container.isHidden = hidden
        quote3container.isHidden = hidden
    }
}

