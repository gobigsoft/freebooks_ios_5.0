//
//  BookCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import Kingfisher

class BookCell: BaseSeparatorCell {

    @IBOutlet private weak var image: UIImageView!
    @IBOutlet internal weak var title: UILabel!
    @IBOutlet internal weak var author: UILabel!
    @IBOutlet private weak var rating: UILabel!
    @IBOutlet private weak var isTopIcon: UIImageView!
    @IBOutlet private weak var ratingIcon: UIImageView!
    @IBOutlet private weak var audioBooksIcon: UIImageView!
    
    func initWithViewModel(viewModel: BookCellVM, iconsTint: UIColor, labelsColor: UIColor, authorLongNames: Bool = false) {
        self.image.kf.setImage(with: URL(string: viewModel.getBookUrl()), options: [.transition(.fade(0.2))])
        UIUtils.dropShadow(view: self.image)
        
        self.title.text = viewModel.getBookName()
        self.author.text = (authorLongNames == true) ? viewModel.getAuthorsLongNames() : viewModel.getAuthors()
        self.rating.text = viewModel.getRating()
        if viewModel.isTop() {
            isTopIcon.isHidden = false
        } else {
            isTopIcon.isHidden = true
        }
        
        setLabelsColor(color: labelsColor)
        
        //Rating has the same color as icons
        rating.textColor = iconsTint
        UIUtils.tintImageView(imageView: isTopIcon, tintColor: iconsTint)
        UIUtils.tintImageView(imageView: ratingIcon, tintColor: iconsTint)
        
        if viewModel.getBookType() == BookType.Audiobook {
            UIUtils.tintImageView(imageView: audioBooksIcon, tintColor: iconsTint)
            audioBooksIcon?.isHidden = false
        } else {
            audioBooksIcon?.isHidden = true
        }
    }
    
    private func setLabelsColor(color: UIColor) {
        title.textColor = color
        author.textColor = color
    }
}
