//
//  EditorsPickCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit


class EditorsPickCell: RoundedBookSectionCell {
    
    private let ThresholdOffsetForFade = CGFloat(75)
    private let DefaultFadeOutAutomaticScrollOffset = CGFloat(75)
    private let DefaultFadeOutOffset = CGFloat(150)
    private let DefaultFadeInOffset = CGFloat(0)
    
    @IBOutlet private weak var fadeInView: UIView!
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var image: UIImageView!
    @IBOutlet private weak var editorsPickBackground: UIView!
    
    private var animator: UIViewPropertyAnimator?
    
    func initWithViewModel(viewModel: EditorsPickCellVM, delegate: BookSectionSelectionDelegate) {
        super.initWithViewModel(viewModel: viewModel, delegate: delegate)
        
        image.kf.setImage(with: URL(string: viewModel.getImageUrl()))
        image.setRoundedCorners(radius: image.frame.height/2)
        image.layer.borderWidth = 2
        image.layer.borderColor = Constants.Colors.ReddishBrown.cgColor
        
        editorsPickBackground.setRoundedCorners(radius: 2)
        title.text = viewModel.getFeaturedTitle()
        
        booksCollection.delegate = self
        booksCollection.bounces = false
        
        setupAnimator()
    }
    
    func resetScroll() {
        booksCollection.setContentOffset(CGPoint(x: DefaultFadeInOffset,y: 0), animated: false)
    }
    
    private func setupAnimator() {
        animator = UIViewPropertyAnimator(duration: 0.4, curve: .linear) {
            self.fadeInView.alpha = 0
        }
        animator?.pausesOnCompletion = true
        animator?.startAnimation()
        animator?.pauseAnimation()
    }
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x >= DefaultFadeInOffset {
            let fractionComplete = getAlphaForOffset(offset: scrollView.contentOffset.x)
            animator?.fractionComplete = fractionComplete
        }
    }
    
    private func getAlphaForOffset(offset: CGFloat) -> CGFloat {
        return offset/ThresholdOffsetForFade
    }
    
    //MARK: Default position scrolling
    internal func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if booksCollection.isDecelerating == false {
            handleDefaultPositionScroll(offset: scrollView.contentOffset.x)
        }
    }
    
    internal func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        handleDefaultPositionScroll(offset: scrollView.contentOffset.x)
    }
    
    private func handleDefaultPositionScroll(offset: CGFloat) {
        if (offset > DefaultFadeOutAutomaticScrollOffset && offset < DefaultFadeOutOffset) {
            booksCollection.setContentOffset(CGPoint(x: DefaultFadeOutOffset, y: 0), animated: true)
        } else if (offset > DefaultFadeInOffset && offset < DefaultFadeOutAutomaticScrollOffset){
            booksCollection.setContentOffset(CGPoint(x: DefaultFadeInOffset, y: 0), animated: true)
        }
    }
}
