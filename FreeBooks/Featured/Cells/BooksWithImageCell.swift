//
//  BooksWithImageCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BooksWithImageCell: RoundedBookSectionCell {

    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var subtitle: UILabel!
    
    func initWithViewModel(viewModel: BooksWithImageCellVM, delegate: BookSectionSelectionDelegate) {
        super.initWithViewModel(viewModel: viewModel, delegate: delegate)
        self.title.text = viewModel.getFeaturedTitle()
        self.subtitle.text = viewModel.getFeaturedSubtitle()
    }
}
