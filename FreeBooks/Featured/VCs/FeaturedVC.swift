//
//  FeaturedVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class FeaturedVC: BaseVC, BookSectionSelectionDelegate, MigrationDelegate {
    
    //Animation constants
    private let ThresholdOffsetForFade = CGFloat(80)
    private let DefaultFadeOutAutomaticScrollOffset = CGFloat(25)
    private let DefaultFadeOutOffset = CGFloat(90)
    private let DefaultFadeInOffset = CGFloat(0)
    
    private let DefaultYTranslation = CGFloat(-100)
    private let DefaultHeaderImageScaleTranslation = CGFloat(1.5)
    private let DefaultHeaderTextScaleTranslation = CGFloat(0.8)
    
    @IBOutlet internal var featuredCollection: UICollectionView!
    @IBOutlet internal weak var fakeNavigationBarDivider: UIView!
    
    internal var isExpanded = false
    
    @IBOutlet private weak var headerImage: UIImageView!
    @IBOutlet private weak var headerTitle: UIImageView!
    
    //Editor's pick cell needs to belong to VC so it's not reused by colletion view, otherwise the current state of its animation is forgotten while reusing
    internal var editorsPickCell: EditorsPickCell!
    internal var viewModel: FeaturedVCVM?
    
    private var animator: UIViewPropertyAnimator?
    
    internal var adProvider: AdProvider?
    private var migrationLoadingAlert: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle.tintColor = Constants.Colors.DarkerBrown
        headerImage.tintColor = Constants.Colors.DarkerBrown
        
        self.registerScrollToTopNotificationObserver(notificationName: Constants.NotificationConstants.TabBarSelectionNotifications.FeaturedSelected)
    
        viewModel = FeaturedVCVM(successCallback: featuredObtained, failureCallback: obtainingFeaturedFailed)
        viewModel?.downloadFeatured()
        
        UIUtils.dropBottomShadow(view: fakeNavigationBarDivider)
        
        setupAnimator()
        showLoading()
        
        if !UserDefaultsUtils.areAdsRemoved() {
            adProvider = AdProvider(inVC: self, type: .featured, adReceivedCallback: adReceived)
            adProvider?.requestAd()
            NotificationCenter.default.addObserver(self, selector: #selector(handleAdsRemoval), name: NSNotification.Name(rawValue: Constants.NotificationConstants.AdsRemoved), object: nil)
        }
        
        PurchaseEngine.sharedEngine.requestProducts()
        
        //Migration
        Migrator.sharedMigrator.delegate = self
        let booksToMigrate = Migrator.sharedMigrator.numberOfBooksToMigrate()
        if booksToMigrate > 0 {
            LoggingUtils.logMessage(message: "Migration required, there are \(booksToMigrate) to migrate")
            Migrator.sharedMigrator.startMigration()
            migrationLoadingAlert = AlertUtils.createMigrationLoadingAlert(booksToMigrateCount: booksToMigrate)
            self.present(migrationLoadingAlert!, animated: true, completion: nil)
        }
        
        (UIApplication.shared.delegate as! AppDelegate).registerForNotifications(application: UIApplication.shared)
    }

    internal func migrationProgressUpdated(downloaded: Int, total: Int) {
        LoggingUtils.logMessage(message: "Migration progress: \(downloaded)/\(total)")
        migrationLoadingAlert?.message = "Please don't turn off the network. \n \(downloaded) of \(total)"   
    }
    
    internal func migrationCompleted() {
        LoggingUtils.logMessage(message: "Migration completed")
        migrationLoadingAlert?.dismiss(animated: true, completion: nil)
    }
    
    private func adReceived() {
        LoggingUtils.logMessage(message: "Ad received")
        featuredCollection.reloadData()
    }
    
    private func featuredObtained() {
        featuredCollection.dataSource = self
        featuredCollection.delegate = self
        hideLoading()
    }
    
    private func obtainingFeaturedFailed() {
        hideLoading()
        showNoConnectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        featuredCollection.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        featuredCollection.reloadData()
    }
    
    @IBAction private func retryPressed() {
        hideNoConnectionView()
        showLoading()
        
        viewModel?.downloadFeatured()
        
        //If we need to retry the download, it's likely we haven't received ad either, in order to make sure we have the ad, we'll request it again
        if !UserDefaultsUtils.areAdsRemoved() {
            adProvider?.requestAd()
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        featuredCollection.reloadData()
//    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //Reset editor's pick back to initial scroll
        editorsPickCell?.resetScroll()
    }
    
    internal func bookSelected(book: Book) {
        showBookDetail(book: book)
    }
    
    @IBAction private func showMorePressed(sender: UIButton) {
        if isExpanded {
            sender.setTitle("SHOW MORE", for: .normal)
            isExpanded = false
        } else {
            sender.setTitle("SHOW LESS", for: .normal)
            isExpanded = true
        }
        featuredCollection.performBatchUpdates({ }, completion: { success in })
        featuredCollection.reloadData()
    }
    
    @IBAction func quoteSelected(sender: UITapGestureRecognizer) {
        if let senderTag = sender.view?.tag, let featuredViewModel = viewModel, let featuredQuotes = featuredViewModel.getQuotes(), let author = featuredQuotes.quotes[senderTag].author {
            showAuthorDetail(author: author)
        }
    }
    
    //MARK: Header animation
    private func setupAnimator() {
        animator = UIViewPropertyAnimator(duration: 1, curve: .linear) {
            self.headerImage.alpha = 0
            self.headerImage.transform = CGAffineTransform(scaleX: self.DefaultHeaderImageScaleTranslation, y: self.DefaultHeaderImageScaleTranslation).translatedBy(x: 0, y: self.DefaultYTranslation)
            self.headerTitle.transform = CGAffineTransform(scaleX: self.DefaultHeaderTextScaleTranslation, y: self.DefaultHeaderTextScaleTranslation).translatedBy(x: 0, y: self.DefaultYTranslation)
        }
        animator?.pausesOnCompletion = true
    }
    
    internal func handleAnimationCompletionForOffset(offset: CGFloat) {
        let fractionComplete = offset/ThresholdOffsetForFade
        if (fractionComplete > 1) {
            fakeNavigationBarDivider.isHidden = false
        } else {
            fakeNavigationBarDivider.isHidden = true
        }
        animator?.fractionComplete = fractionComplete
    }
    
    internal func handleDefaultPositionScroll(offset: CGFloat) {
        LoggingUtils.logMessage(message: "Handling default position scroll offset: \(offset)")
        if (offset >= DefaultFadeOutAutomaticScrollOffset && offset <= DefaultFadeOutOffset) {
            featuredCollection.setContentOffset(CGPoint(x: 0, y: DefaultFadeOutOffset), animated: true)
        } else if (offset >= DefaultFadeInOffset && offset <= ThresholdOffsetForFade){
            featuredCollection.setContentOffset(CGPoint(x: 0, y: DefaultFadeInOffset), animated: true)
        }
    }
    
    @IBAction private func menuPressed(sender: UIButton) {
        var menuActions = [UIAlertAction]()
        
        if !UserDefaultsUtils.areAdsRemoved() {
            let removeAdsAction = UIAlertAction.init(title: "Remove ads", style: UIAlertAction.Style.default, handler: { action in
                PurchaseEngine.sharedEngine.purchaseRemoveAds()
            })
            menuActions.append(removeAdsAction)
        }
        
        //If audiobooks are already purchased from the former app, we don't offer any audiobooks related action
        if !UserDefaultsUtils.areAudiobooksPurchased() {
            if !UserDefaultsUtils.isAudiobooksSubscriptionActive() {
                let subscribeToAudiobooksAction = UIAlertAction.init(title: "Subscribe to audiobooks", style: UIAlertAction.Style.default, handler: { action in
                    self.showSubscriptionInfoAlert()
                })
                menuActions.append(subscribeToAudiobooksAction)
            } else {
                let manageSubscriptionsAction = UIAlertAction.init(title: "Manage subscriptions", style: UIAlertAction.Style.default, handler: { action in
                    UIApplication.shared.open(URL(string: Constants.Misc.ManageSubscriptionsUrl)!, options: [:], completionHandler: nil)
                })
                menuActions.append(manageSubscriptionsAction)
            }
        }
        
        let aboutAction = UIAlertAction.init(title: "About", style: UIAlertAction.Style.default, handler: {
            action in
            if let aboutAppVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.AboutAppVC) {
                self.present(aboutAppVC, animated: true, completion: nil)
            }
        })
        
        let settingsAction = UIAlertAction.init(title: "Settings", style: UIAlertAction.Style.default, handler: {
            action in
            if let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.SettingsVC) {
                self.present(settingsVC, animated: true, completion: nil)
            }
        })
        
        let restorePurchasesAction = UIAlertAction.init(title: "Restore purchases", style: UIAlertAction.Style.default, handler: {
            action in
            PurchaseEngine.sharedEngine.restorePurchases()
        })
        
        menuActions.append(contentsOf: [aboutAction, settingsAction, restorePurchasesAction])
        AlertUtils.showActionSheet(actions: menuActions, inVC: self, inRect: sender.frame)
    }
    
    private func showSubscriptionInfoAlert() {
        let subscribeToAudiobooksAction = UIAlertAction.init(title: "Subscribe", style: UIAlertAction.Style.default, handler: { action in
            PurchaseEngine.sharedEngine.purchaseSubscription()
        })
        
        let termsOfUseAction = UIAlertAction.init(title: "Terms of Use", style: UIAlertAction.Style.default, handler: { action in
            guard let url = URL(string: Constants.AboutApp.URLs.WebTermsOfUse) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
        
        let privacyPolicyAction = UIAlertAction.init(title: "Privacy Policy", style: UIAlertAction.Style.default, handler: { action in
            guard let url = URL(string: Constants.AboutApp.URLs.WebPrivacyPolicy) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
    
        AlertUtils.showAlert(title: "Subscribe to audiobooks", message: Constants.Misc.SubscriptionInfoText, actions: [subscribeToAudiobooksAction, termsOfUseAction, privacyPolicyAction], cancelAction: UIAlertAction(title: "Cancel", style: .cancel, handler: nil), inVC: self)
    }
    
    @objc private func handleAdsRemoval() {
        featuredCollection.reloadData()
    }
    
    @IBAction private func feelingLucky() {
        if let viewModel = self.viewModel {
            showLoading()
            viewModel.feelingLucky(completionHandler: { book in
                self.hideLoading()
                if let luckyBook = book {
                    DispatchQueue.main.async {
                        let bookDetailVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookDetailVC) as! BookDetailVC
                        bookDetailVC.initWithBook(book: luckyBook)
                        self.present(bookDetailVC, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    override internal func scrollToTop() {
        featuredCollection.setContentOffset(CGPoint(x: 0, y: DefaultFadeInOffset), animated: true)
    }
}
