//
//  FeaturedDelegate.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension FeaturedVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: heightForFeaturedSection(section: FeaturedSections.allValues[indexPath.section]))
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            //Collection view needs to have an initial y inset of 100 px so fade / scale animations on elements that belong to another "layer" of the UI View Controller's hierarchy are visible
            return UIEdgeInsets(top: 110, left: 0, bottom: 10, right: 0)
        case 1...FeaturedSections.allValues.count-1:
            return UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        default:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    private func heightForFeaturedSection(section: FeaturedSections) -> CGFloat {
        switch section {
        case .EditorsPick:
            return 250.0
        case .Ads:
            return UserDefaultsUtils.areAdsRemoved() ? 0 : 270.0
        case .WhatToRead:
            return 62.0
        case .BooksWithImage:
            return 310.0
        case .Quotes:
            if (isExpanded == true) {
                return 330.0
            } else {
                return 160.0
            }
        case .Audiobooks:
            return 272.0
        }
    }
    
    //MARK: Scroll view delegates
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        handleAnimationCompletionForOffset(offset: scrollView.contentOffset.y)
    }
    
    //MARK: Default position scrolling
    internal func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if featuredCollection.isDecelerating == false {
            handleDefaultPositionScroll(offset: scrollView.contentOffset.y)
        }
    }
    
    internal func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        handleDefaultPositionScroll(offset: scrollView.contentOffset.y)
    }
}
