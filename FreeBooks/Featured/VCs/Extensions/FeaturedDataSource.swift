//
//  FeaturedDataSource.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

internal enum FeaturedSections: String {
    case EditorsPick = "EditorsPick"
    case Ads = "Ads"
    case WhatToRead = "WhatToRead"
    case BooksWithImage = "BooksWithImage"
    case Quotes = "Quotes"
    case Audiobooks = "Audiobooks"
    
    static let allValues = [EditorsPick, Ads, WhatToRead, BooksWithImage, Quotes, Audiobooks]
}

extension FeaturedVC: UICollectionViewDataSource {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let featuredSection = FeaturedSections.allValues[indexPath.section]
        
        switch featuredSection {
        case .EditorsPick:
            //Prevent reusing of the cell to keep the current state of animation
            if editorsPickCell == nil {
                editorsPickCell = collectionView.dequeueReusableCell(withReuseIdentifier: featuredSection.rawValue, for: indexPath) as? EditorsPickCell
                guard let featuredBooks = viewModel?.getFeaturedBooks() else {
                    return editorsPickCell!
                }
                editorsPickCell?.initWithViewModel(viewModel: EditorsPickCellVM(featuredBooks: featuredBooks), delegate: self)
            }
            return editorsPickCell!
        case .Ads:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: featuredSection.rawValue, for: indexPath) as! FeaturedAdCell
            if let adProvider = self.adProvider, let ad = adProvider.getNextAd() {
                cell.initWithAd(ad: ad)
            }
            return cell
        case .WhatToRead:
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: featuredSection.rawValue, for: indexPath)
             return cell
        case .BooksWithImage:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: featuredSection.rawValue, for: indexPath) as! BooksWithImageCell
            guard let featuredBooksWithImage = viewModel?.getBooksWithImage() else {
                return cell
            }
            cell.initWithViewModel(viewModel: BooksWithImageCellVM(featuredBooksWithImage: featuredBooksWithImage), delegate: self)
            return cell
        case .Quotes:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: featuredSection.rawValue, for: indexPath) as! QuotesCell
            guard let featuredQuotes = viewModel?.getQuotes() else {
                return cell
            }
            cell.initWithViewModel(viewModel: QuotesVM(featuredQuotes: featuredQuotes))
            return cell
        case .Audiobooks:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: featuredSection.rawValue, for: indexPath) as! RoundedBookSectionCell
            guard let featuredAudiobooks = viewModel?.getAudioBooks() else {
                return cell
            }
            cell.initWithViewModel(viewModel: BaseBookSectionVM(books: featuredAudiobooks.books), delegate: self)
            return cell
        }
    }
        
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return FeaturedSections.allValues.count
    }
}
