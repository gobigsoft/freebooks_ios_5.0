//
//  BaseFeaturedViewModel.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BaseBookSectionVM: NSObject {
    
    private let books: [Book]
    
    init(books: [Book]) {
        self.books = books
    }
    
    func getTotalBooksCount() -> Int {
        return self.books.count
    }
    
    func getBook(index: Int) -> Book {
        return self.books[index]
    }
}
