//
//  BooksWithImageVM.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BooksWithImageCellVM: BaseBookSectionVM {

    private let featuredBooksWithImage: FeaturedBooksWithImage
    
    init(featuredBooksWithImage: FeaturedBooksWithImage) {
        self.featuredBooksWithImage = featuredBooksWithImage
        super.init(books: featuredBooksWithImage.books)
    }
    
    func getFeaturedTitle() -> String {
        return self.featuredBooksWithImage.title ?? ""
    }
    
    func getFeaturedSubtitle() -> String {
        return self.featuredBooksWithImage.subtitle ?? ""
    }
}
