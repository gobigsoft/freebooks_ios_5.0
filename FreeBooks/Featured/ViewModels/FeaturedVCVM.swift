//
//  FeaturedViewModel.swift
//  FreeBooks
//
//  Created by Karol Grulling on 16/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class FeaturedVCVM: NSObject {
    
    private var featured: Featured?
    private var successCallback: (() -> ())!
    private var failureCallback: (() -> ())!
    
    init(successCallback: @escaping () -> (), failureCallback: @escaping () -> ()) {
        self.successCallback = successCallback
        self.failureCallback = failureCallback
    }
    
    func downloadFeatured() {
        FeaturedRequester().getFeatured(completionHandler: { success, featured in
            if success {
                self.featured = featured
                self.successCallback()
            } else {
                self.failureCallback()
            }
        })
    }
    
    func getFeaturedBooks() -> FeaturedBooks? {
        return featured?.featuredBooks
    }
    
    func getAudioBooks() -> FeaturedAudiobooks? {
        return featured?.audiobooks
    }
    
    func getBooksWithImage() -> FeaturedBooksWithImage? {
        return featured?.booksWithImage
    }
    
    func getQuotes() -> FeaturedQuotes? {
        return featured?.quotes
    }
    
    func feelingLucky(completionHandler: @escaping (Book?) -> ()) {
        let libraryBooks = RealmUtils.getStoredBookDetails()
        var bookIds = [Int]()
        
        libraryBooks.forEach({ bookIds.append($0.bookId) })
        
        FeelingLuckyRequester().feelingLucky(libraryBookIds: bookIds, completionHandler: {
            success, book in
            success == true ? completionHandler(book) : completionHandler(nil)
        })
    }
}
