//
//  BookViewModel.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BookCellVM: BaseBookVM {
    
    func getBookUrl() -> String {
        return self.book.coverUrl
    }
    
    func getBookName() -> String {
        return self.book.title
    }
    
    func getAuthors() -> String {
        return Utils.getAuthorNamesWithFirstNameInitials(book: self.book)
    }
    
    func getAuthorsLongNames() -> String {
        return Utils.getAuthorLongNames(book: self.book)
    }
    
    func getBookId() -> Int {
        return self.book.bookId
    }
}
