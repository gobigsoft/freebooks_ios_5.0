//
//  QuotesViewModel.swift
//  FreeBooks
//
//  Created by Karol Grulling on 20/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class QuotesVM: NSObject {
    
    private let featuredQuotes: FeaturedQuotes
    
    init(featuredQuotes: FeaturedQuotes) {
        self.featuredQuotes = featuredQuotes
    }
    
    func getTitle() -> String {
        return self.featuredQuotes.title ?? ""
    }
    
    func getQuotesCount() -> Int {
        return self.featuredQuotes.quotes.count
    }
    
    func getQuote(index: Int) -> String {
        return self.featuredQuotes.quotes[index].quote
    }
    
    func getAuthorNameForQuote(index: Int) -> String {
        guard let authorName = self.featuredQuotes.quotes[index].author?.name else {
            return ""
        }
        return "- " + authorName
    }
    
    func getAuthorImageForQuote(index: Int) -> String {
        return self.featuredQuotes.quotes[index].author?.image ?? ""
    }
}
