//
//  EditorsPickViewModel.swift
//  FreeBooks
//
//  Created by Karol Grulling on 20/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class EditorsPickCellVM: BaseBookSectionVM {
    
    private let featuredBooks: FeaturedBooks
    
    init(featuredBooks: FeaturedBooks) {
        self.featuredBooks = featuredBooks
        super.init(books: featuredBooks.books)
    }
    
    func getFeaturedTitle() -> String {
        return self.featuredBooks.title ?? ""
    }
    
    func getBackgroundImageUrl() -> String {
        return self.featuredBooks.bgImageUrl ?? ""
    }
    
    func getImageUrl() -> String {
        return self.featuredBooks.imageUrl ?? ""
    }
}
