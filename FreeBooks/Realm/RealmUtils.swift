//
//  RealmProvider.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import RealmSwift

class RealmUtils: NSObject {
    
    public static func configureRealm() {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: Config.Realm.SchemaVersion,
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        Realm.Configuration.defaultConfiguration = config
    }
    
    //MARK: Books
    public static func storeBookDetail(bookDetail: BookDetail) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(bookDetail.getRealmBookDetailObject(), update: .all)
        }
    }
    
    public static func getStoredBookDetail(bookId: Int) -> BookDetail? {
        guard let realmBookDetail = getStoredRealmBookDetail(bookId: bookId) else {
            return nil
        }
        return RealmConverters.convertBookDetail(realmBookDetail: realmBookDetail)
    }
    
    public static func getStoredBookDetails(bookType: BookType) -> [BookDetail] {
        let realm = try! Realm()
        var foundBookDetails = [BookDetail]()
        let realmBookDetails = realm.objects(RealmBookDetail.self).filter("type = '\(bookType.rawValue)'").toArray(ofType: RealmBookDetail.self)
        realmBookDetails.forEach({ foundBookDetails.append(RealmConverters.convertBookDetail(realmBookDetail: $0)) })
        return foundBookDetails
    }

    public static func getStoredBookDetails() -> [BookDetail]{
        let realm = try! Realm()
        let storedBookDetails = realm.objects(RealmBookDetail.self)
        var foundBookDetails = [BookDetail]()
        storedBookDetails.forEach( { foundBookDetails.append(RealmConverters.convertBookDetail(realmBookDetail: $0)) })
        return foundBookDetails
    }
    
    public static func getStoredRealmBookDetail(bookId: Int) -> RealmBookDetail? {
        let realm = try! Realm()
        guard let bookDetail = realm.objects(RealmBookDetail.self).filter("id = \(bookId)").first else {
            return nil
        }
        return bookDetail
    }
    
    public static func removeStoredBook(bookId: Int) {
        if let storedBook = getStoredRealmBookDetail(bookId: bookId) {
            let realm = try! Realm()
            try! realm.write {
                realm.delete(storedBook)
            }
        }
    }
    
    public static func storeSearchedBook(searchedBook: Book) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(searchedBook.getSearchedBookRealmobject(), update: .all)
        }
    }
    
    public static func getStoredSearchedBooks() -> [Book]{
        let realm = try! Realm()
        let storedSearchedBooks = realm.objects(RealmSearchedBook.self)
        var foundBooks = [Book]()
        storedSearchedBooks.forEach( {
            foundBooks.append(RealmConverters.convertSearchedBook(realmSearchedBook: $0))
        })
        return foundBooks
    }
    
    public static func removeStoredSearchBooks() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(RealmSearchedBook.self))
        }
    }
    
    //MARK: Audiobooks
    public static func setChapterAsDownloaded(chapterId: Int) {
        let realm = try! Realm()
        guard let chapter = realm.objects(RealmAudiobookChapter.self).filter("id = \(chapterId)").first else {
            return
        }
        
        try! realm.write {
            chapter.isDownloaded = true
        }
    }
    
    //MARK: Styles
    public static func storeCurrentStyle() {
        let realm = try! Realm()
        try! realm.write {
            let currentStyle = realm.objects(RealmReaderStyle.self)
            realm.delete(currentStyle)
            realm.add(ReaderStyleProvider.sharedProvider.getStyle().getRealmObject())
        }
    }
    
    public static func getStoredStyle() -> RealmReaderStyle? {
        let realm = try! Realm()
        return realm.objects(RealmReaderStyle.self).first
    }
    
    //MARK: Cache
    public static func cacheReader(pageCounts: [Int], tableOfContentsIndexItems: [TableOfContentsIndexItem], totalPages: Int, fontSize: Int, bookId: Int) {
        let pageCountsString = (pageCounts.map{String($0)}).joined(separator: ",")
        let realmReaderCache = RealmReaderCache(value: ["pageCounts": pageCountsString, "totalPages": totalPages, "fontSize": fontSize])
        
        tableOfContentsIndexItems.forEach({
            realmReaderCache.tableOfContentsIndexItems.append($0.getRealmObject())
        })
        
        let realm = try! Realm()
        
        guard let realmBookDetail = getStoredRealmBookDetail(bookId: bookId) else {
            return
        }
        
        try! realm.write {
            realmBookDetail.cache.append(realmReaderCache)
            realm.add(realmReaderCache)
        }
    }
    
    public static func updateProgress(progress: Float, bookId: Int, currentAudiobookChapter: Int? = nil) {
        guard let realmBookDetail = getStoredRealmBookDetail(bookId: bookId) else {
            return
        }
        
        let realm = try! Realm()
        try! realm.write {
            realmBookDetail.progress = progress
            
            //If we update audiobook's progress, we need to set also the current chapter, otherwise it's not needed 
            if let currentChapter = currentAudiobookChapter {
                realmBookDetail.currentAudiobookChapter = currentChapter
            }
        }
    }
    
    //MARK: Bookmarks
    public static func storeBookmark(bookId: Int, bookmark: Bookmark) {
        guard let realmBookDetail = getStoredRealmBookDetail(bookId: bookId) else {
            return
        }
        
        let realmBookmark = bookmark.getRealmObject()
        let realm = try! Realm()
        try! realm.write {
            realmBookDetail.bookmarks.append(realmBookmark)
            realm.add(realmBookmark)
        }
    }
    
    public static func removeBookmark(bookId: Int, progress: Float) {
        guard let realmBookDetail = getStoredRealmBookDetail(bookId: bookId) else {
            return
        }
        if let realmBookmarkIndex = realmBookDetail.bookmarks.firstIndex(where: { $0.progress == progress }) {
            let realm = try! Realm()
            try! realm.write {
                realmBookDetail.bookmarks.remove(at: realmBookmarkIndex)
            }
        }
    }
}

class RealmConverters: NSObject {
    public static func convertBookDetail(realmBookDetail: RealmBookDetail) -> BookDetail {
        return BookDetail(realmBookDetail: realmBookDetail)
    }
    
    public static func convertReaderStyle(realmReaderStyle: RealmReaderStyle) -> ReaderStyle {
        return ReaderStyle(realmReaderStyle: realmReaderStyle)
    }
    
    public static func convertSearchedBook(realmSearchedBook: RealmSearchedBook) -> Book {
        return Book(realmBook: realmSearchedBook)
    }
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        return array
    }
}
