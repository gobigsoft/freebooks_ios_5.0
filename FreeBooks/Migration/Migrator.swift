//
//  Migrator.swift
//  FreeBooks
//
//  Created by Karol Grulling on 20/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import CoreData

protocol MigrationDelegate {
    func migrationCompleted()
    func migrationProgressUpdated(downloaded: Int, total: Int)
}

class Migrator: NSObject {
    static let sharedMigrator = Migrator()
    
    private let context: NSManagedObjectContext
    
    private var bookIdsToMigrate = [Int]()
    private var totalBooksToMigrate = 0
    
    var delegate: MigrationDelegate?
    
    private override init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
    }
    
    func startMigration() {
        self.bookIdsToMigrate = retrieveBookIdsToMigrate()
        self.totalBooksToMigrate = self.bookIdsToMigrate.count
        
        for legacyBookId in bookIdsToMigrate {
            LoggingUtils.logMessage(message: "Migrating book with id: \(legacyBookId)")
            
            downloadBookDetail(legacyBookId: legacyBookId, completion: { bookId in
                //If we couldn't retrieve book id let's move to another book and consider the failed one as migrated - to keep it simple
                guard let bookId = bookId else {
                    LoggingUtils.logMessage(message: "Couldn't migrate book with legacy id: \(legacyBookId)")
                    self.handleMigrationOfBookWithLegacyId(legacyBookId: legacyBookId)
                    return
                }
                
                self.downloadEpub(bookId: bookId, completion: {
                    self.handleMigrationOfBookWithLegacyId(legacyBookId: legacyBookId)
                }) 
            })
        }
    }
    
    private func handleMigrationOfBookWithLegacyId(legacyBookId: Int) {
        self.bookIdsToMigrate.remove(object: legacyBookId)
        self.delegate?.migrationProgressUpdated(downloaded: self.totalBooksToMigrate-self.bookIdsToMigrate.count, total: self.totalBooksToMigrate)
        if self.bookIdsToMigrate.isEmpty {
            self.deleteAllOldBooks()
            self.delegate?.migrationCompleted()
        }
    }
    
    private func downloadBookDetail(legacyBookId: Int, completion: @escaping (Int?) -> ()) {
        BookDetailRequester().getBookDetailFromLegacyId(legacyId: legacyBookId, completionHandler: { success, bookDetail in
            if let downloadedBookDetail = bookDetail, success {
                RealmUtils.storeBookDetail(bookDetail: downloadedBookDetail)
                completion(downloadedBookDetail.bookId)
            } else {
                completion(nil)
            }
        })
    }
    
    func numberOfBooksToMigrate() -> Int {
        return retrieveBookIdsToMigrate().count
    }
    
    private func retrieveBookIdsToMigrate() -> [Int] {
        var bookIdsToMigrate = [Int]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Book")
        do {
            let storedBookIds = try self.context.fetch(fetchRequest) as! [NSManagedObject]
            storedBookIds.forEach({
                if let bookId = $0.value(forKey: "bookId") as? Int {
                    //We need to make sure we're migrating only those books that aren't already downloaded
                    if RealmUtils.getStoredBookDetail(bookId: bookId) == nil  {
                        bookIdsToMigrate.append(bookId)
                    }
                }
            })
        } catch _ as NSError {
            LoggingUtils.logMessage(message: "Migration error. An error ocurred while retrieving stored book IDs")
            return bookIdsToMigrate
        }
        return bookIdsToMigrate
    }
    
    private func deleteAllOldBooks() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Book")
        do {
            let storedBookIds = try self.context.fetch(fetchRequest) as! [NSManagedObject]
            storedBookIds.forEach({ self.context.delete($0) })
            try context.save()
        } catch _ as NSError {
            LoggingUtils.logMessage(message: "Couldn't delete old books from core data")
        }
    }
    
    func downloadEpub(bookId: Int, completion: @escaping () -> ()) {
        BookContentRequester().downloadBook(bookId: bookId, progressHandler: { progress, estimatedSize in }, completionHandler: {
            success, data in
            if let returnedBookData = data, success {
                _ = FileUtils.parseEPUBFromData(bookData: returnedBookData, bookId: bookId)
            }
            completion()
        })
    }
}
