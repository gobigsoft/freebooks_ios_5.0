//
//  SettingsVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 20/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import UserNotifications

class SettingsVC: BaseFakeNavigationBarVC {
    
    @IBOutlet weak private var analyticsSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaultsUtils.wereAnalyticsSettingsAlreadyOverriden() {
            analyticsSwitch.isOn = UserDefaultsUtils.areAnalyticsEnabled()
        } else {
            analyticsSwitch.isOn = true
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction private func setAnalytics() {
        UserDefaultsUtils.setAnalyticsSettingsAsOverriden()
        analyticsSwitch.isOn ? enableAnalytics() : disableAnalytics()
    }
    
    private func enableAnalytics() {
        LoggingUtils.logMessage(message: "Enabling analytics")
        UserDefaultsUtils.setAnalyticsAsEnabled()
        Analytics.setAnalyticsCollectionEnabled(true)
    }
    
    private func disableAnalytics() {
        LoggingUtils.logMessage(message: "Disabling analytics")
        UserDefaultsUtils.setAnalyticsAsDisabled()
        Analytics.setAnalyticsCollectionEnabled(false)
    }
    
    @IBAction private func openNotificationSettingsPressed() {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
}
