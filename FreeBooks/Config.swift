//
//  Config.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/01/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

struct Config {
    
    struct API {
        struct BaseUrl {
            static let Testing = "http://ec2-18-195-226-222.eu-central-1.compute.amazonaws.com"
            static let Production = "http://ec2-52-0-20-5.compute-1.amazonaws.com"
        }
        
        struct Routes {
            static let Featured = "/3.2/book/featured"
            static let Audiobooks = "/3.2/audiobooks/top"
            static let Browse = "/3.0/author/browse"
            static let Search = "/3.2/book/search"
            static let Synchronize = "/v2/synchronize"
            static let BookDetail = "/3.2/book/%@/detail"
            static let BookReviews = "/3.0/book/%@/reviews/%@"
            static let UserReview = "/userReview/%@"
            static let AuthorDetail = "/3.0/author/%@/detail"
            static let Category = "/3.0/category"
            static let Login = "/login"
            static let FacebookLogin = "/login/facebook"
            static let Register = "/register"
            static let Logout = "/logout"
            static let RecoverPassword = "/recoverPassword"
            static let Statistics = "/statistics"
            static let DownloadBook = "/3.0/book/%@/download"
            static let DownloadAudiobookChapter = "/3.0/book/chapter/%@"
            static let OldBooks = "/3.0/oldbooks"
            static let FeelingLucky = "/3.0/feelingLucky"
            static let Archive = "/3.0/archive/%@"
        }
    }
}
