//
//  BookAdVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 05/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import GoogleMobileAds

class BookAdVC: BaseIndexableVC {
    
    @IBOutlet weak private var adView: GADUnifiedNativeAdView!
    @IBOutlet weak private var iconContainer: UIView?
    @IBOutlet weak private var adFlag: UIView!
    
    private var adProvider: AdProvider?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        adProvider = AdProvider(inVC: self, type: .myLibrary, adReceivedCallback: setup)
        adProvider?.requestAd()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        if let ad = adProvider?.getNextAd() {
            setupWithAd(ad: ad)
        } else {
            adProvider?.requestAd()
        }
    }
    
    private func setupWithAd(ad: GADUnifiedNativeAd) {
        guard let adView = self.adView else {
            return
        }
        
        adView.nativeAd = ad
        
        adView.mediaView?.contentMode = UIView.ContentMode.scaleAspectFill
        adView.mediaView?.mediaContent = ad.mediaContent
        
        (adView.headlineView as? UILabel)?.text = ad.headline
        (adView.bodyView as? UILabel)?.text = ad.body
        
        (adView.iconView as? UIImageView)?.image = ad.icon?.image
        
        if let iconContainer = self.iconContainer {
            iconContainer.setRoundedCorners(radius: iconContainer.frame.size.width/2)
        }
        
        adFlag.setRoundedCorners(radius: 3)
        adView.iconView?.isHidden = ad.icon == nil
    }
}
