
//
//  AdCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 05/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdCell: UICollectionViewCell {
    
    @IBOutlet weak private var adView: GADUnifiedNativeAdView!
    @IBOutlet weak private var iconContainer: UIView?
    @IBOutlet weak private var adFlag: UIView!
    
    func initWithAd(ad: GADUnifiedNativeAd, roundedIcon: Bool = false) {
        adView.nativeAd = ad
        
        adView.mediaView?.contentMode = UIView.ContentMode.scaleAspectFill
        adView.mediaView?.mediaContent = ad.mediaContent
        
        (adView.headlineView as? UILabel)?.text = ad.headline
        (adView.bodyView as? UILabel)?.text = ad.body
        
        (adView.iconView as? UIImageView)?.image = ad.icon?.image
        
        if let iconContainer = self.iconContainer {
            iconContainer.setRoundedCorners(radius: iconContainer.frame.size.width/2)
        }
        
        adFlag.setRoundedCorners(radius: 3)
        adView.iconView?.isHidden = ad.icon == nil
        
        //On some occasions ad view is not in front, having it in front makes sure the ad is clickable
        self.bringSubviewToFront(adView)
    }
}

