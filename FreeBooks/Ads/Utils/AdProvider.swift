//
//  AdProvider.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import GoogleMobileAds

enum AdType {
    case featured
    case myLibrary
}

class AdProvider: NSObject, GADAdLoaderDelegate, GADUnifiedNativeAdLoaderDelegate {
    
    private let type: AdType
    private var adLoader: GADAdLoader?
    private var ads = [GADUnifiedNativeAd]()
    private var currentIndex = 0
    private var adReceivedCallback: (() -> ())!

    init(inVC: UIViewController, type: AdType, adReceivedCallback: @escaping () -> ()) {
        self.type = type
        self.adReceivedCallback = adReceivedCallback
        
        let adViewOptions = GADNativeAdViewAdOptions()
        adViewOptions.preferredAdChoicesPosition = .topRightCorner 
        
        adLoader = GADAdLoader(adUnitID: type == .featured ? Config.Ads.AdMob.AdIdentifiers.Featured : Config.Ads.AdMob.AdIdentifiers.MyLibrary,
                               rootViewController: inVC,
                               adTypes: [GADAdLoaderAdType.unifiedNative],
                               options: [adViewOptions])
        super.init()
        adLoader?.delegate = self
    }
    
    func requestAd() {
        adLoader?.load(GADRequest())
    }
    
    func getNextAd() -> GADUnifiedNativeAd? {
        return ads.indices.contains(0) ? ads[currentIndex] : nil
    }
    
    internal func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        
    }
    
    internal func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
        ads.append(nativeAd)
        adReceivedCallback()
    }
    
    internal func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        
    }
}
