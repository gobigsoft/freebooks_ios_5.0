//
//  InterstitialAdProvider.swift
//  FreeBooks
//
//  Created by Karol Grulling on 04/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import GoogleMobileAds

extension Date {
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
}

class InterstitialAdProvider: NSObject, GADInterstitialDelegate {
    
    static let sharedProvider = InterstitialAdProvider()
    private var interstitial: DFPInterstitial?
    
    private var lastInterstitialShown: Date?
    
    //An interstitial that's supposed to be displayed after the first action in the app (tab switching, book detail opening etc.)
    private var firstActionInterstitialAlreadyDisplayed = false
    
    func showInterstitial(inVC: UIViewController) {
        LoggingUtils.logMessage(message: "Show interstitial called")
        guard canShowInterstitial(), !UserDefaultsUtils.areAdsRemoved() else {
            return
        }
        
        if interstitial?.isReady ?? false {
            lastInterstitialShown = Date()
            interstitial?.present(fromRootViewController: inVC)
        }
    }
    
    func showFirstActionInterstitial(inVC: UIViewController) {
        guard firstActionInterstitialAlreadyDisplayed == false, !UserDefaultsUtils.areAdsRemoved() else {
            return
        }
        firstActionInterstitialAlreadyDisplayed = true
        showInterstitial(inVC: inVC)
    }
    
    func canShowInterstitial() -> Bool {
        guard lastInterstitialShown != nil else {
            return true
        }
        
        let timeDifferenceBetweenLastInterstitialAndNow = Int(Date() - lastInterstitialShown!)
        LoggingUtils.logMessage(message: "Time difference between last interstitial and now is : \(timeDifferenceBetweenLastInterstitialAndNow)")
        
        return timeDifferenceBetweenLastInterstitialAndNow > Config.Ads.MinimumSpanBetweenInterstitialsSeconds
    }
    
    func prepareInterstitial() {
        LoggingUtils.logMessage(message: "Prepare interstitial called")
        interstitial = DFPInterstitial(adUnitID: Config.Ads.AdMob.AdIdentifiers.ReaderInterstitial)
        let request = DFPRequest()
        interstitial?.delegate = self
        interstitial?.load(request)
    }
    
    internal func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        LoggingUtils.logMessage(message: "Interstitial receive ad")
    }
    
    internal func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        LoggingUtils.logMessage(message: "Interstitial did fail to receive ad with error, error: \(error.description)")
    }
    
    internal func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        LoggingUtils.logMessage(message: "Interstitial did fail to present ad")
    }
    
    internal func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        prepareInterstitial()
    }
}
