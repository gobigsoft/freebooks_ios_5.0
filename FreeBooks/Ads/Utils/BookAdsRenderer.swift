//
//  BookAdsRenderer.swift
//  FreeBooks
//
//  Created by Karol Grulling on 05/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class BookAdsRenderer: NSObject {
    
    private let NumberOfAdsToRenderInOneDirection = 2
    
    private var currentPage: Int
    private var totalPages: Int
    private var renderedAds = [BookAdVC]()
    
    init(currentPage: Int, totalPages: Int) {
        self.currentPage = currentPage
        self.totalPages = totalPages
        super.init()
        renderAds()
    }
    
    func updatePages(currentPage: Int, totalPages: Int) {
        self.currentPage = currentPage
        self.totalPages = totalPages
        
        //When we get to the bounds of currently rendered ads, we'll rerender ads
        if let maxRenderedPage = self.renderedAds.map({$0.pageIndex}).max(), maxRenderedPage < currentPage {
            renderAds()
        }
        
        //If the lowest page index for which we have an ad is higher than current page + our ad frequency, we know we're missing an ad and should re-render them
        if let minRenderedPage = self.renderedAds.map({$0.pageIndex}).min(), minRenderedPage > currentPage+Config.Ads.ReaderAdFrequencyPages {
            renderAds()
        }
    }
    
    private func renderAds() {
        renderedAds.removeAll()
        renderNextAds()
        renderPreviousAds()
        LoggingUtils.logMessage(message: "Rendering ads")
    }
    
    private func renderNextAds() {
        for i in 1...NumberOfAdsToRenderInOneDirection {
            let pageIndex = currentPage+i*Config.Ads.ReaderAdFrequencyPages
            if pageIndex < totalPages {
                LoggingUtils.logMessage(message: "Rendering next ad with page index: \(pageIndex)")
                renderedAds.append(createBookAdVC(index: pageIndex))
            }
        }
    }
    
    private func renderPreviousAds() {
        for i in 1...NumberOfAdsToRenderInOneDirection {
            let pageIndex = currentPage-i*Config.Ads.ReaderAdFrequencyPages
            if pageIndex > 0 {
                LoggingUtils.logMessage(message: "Rendering previous ad with page index: \(pageIndex)")
                renderedAds.append(createBookAdVC(index: pageIndex))
            }
        }
    }
    
    private func createBookAdVC(index: Int) -> BookAdVC {
        let adVC = UIStoryboard(name: Constants.StoryboardConstants.StoryboardMain, bundle: nil).instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookAdVC) as! BookAdVC
        adVC.pageIndex = index
        return adVC
    }
    
    func getAd(bookIndex: Int) -> BookAdVC? {
        return renderedAds.first(where: { $0.pageIndex == bookIndex })
    }
}
