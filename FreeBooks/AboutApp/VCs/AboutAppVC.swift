//
//  AboutAppVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import MessageUI

internal enum AboutAppSections: String {
    case DevelopedBy = "Developed by"
    case Help = "Help"
    case Legal = "Legal"
    case SupportApp = "Support app"
    
    static let allValues = [DevelopedBy, Help, Legal, SupportApp]
}

internal enum AboutItemType: String {
    case DeveloperName = "Karol Grülling"
    case Digiply = "Digiply"
    case ReportAProblem = "Report a problem"
    case Forum = "Forum"
    case ShowGuide = "Show guide"
    case WhatsNew = "What's New"
    case PrivacyPolicy = "Privacy Policy"
    case OpenSourceLicenses = "Open source licenses"
    case ShareAppWithFriends = "Share app with friends"
    case RateOnAppStore = "Rate on App Store"
}

internal struct AboutItem {
    let type: AboutItemType
    let subTitle: String?
    let url: String?
}

class AboutAppVC: BaseFakeNavigationBarVC, MFMailComposeViewControllerDelegate, WhatsNewViewDelegate {
    
    internal let SingleLabelCellIdentifier = "SingleLabelCell"
    internal let MultiLabelCellIdentifier = "MultiLabelCell"
    
    internal let AboutAppVersionHeaderIdentifier = "AboutAppVersionHeader"
    internal let AboutAppTitleHeaderIdentifier = "AboutAppTitleHeader"
    
    internal let DevelopedByItems = [AboutItem(type: .DeveloperName, subTitle: "www.krl.sk", url: Constants.AboutApp.URLs.WebKrl),
                                    AboutItem(type: .Digiply, subTitle: "www.digiply.io", url: Constants.AboutApp.URLs.WebDigiply)]
    
    internal let HelpItems = [AboutItem(type: .ReportAProblem, subTitle: nil, url: nil),
                             AboutItem(type: .Forum, subTitle: nil, url: Constants.AboutApp.URLs.WebForum),
                             AboutItem(type: .ShowGuide, subTitle: nil, url: nil),
                             AboutItem(type: .WhatsNew, subTitle: nil, url: nil)]
    
    internal let LegalItems = [AboutItem(type: .PrivacyPolicy, subTitle: nil, url: Constants.AboutApp.URLs.WebPrivacyPolicy),
                              AboutItem(type: .OpenSourceLicenses, subTitle: nil, url: nil)]
    
    internal let SupportAppItems = [AboutItem(type: .ShareAppWithFriends, subTitle: nil, url: nil),
                                   AboutItem(type: .RateOnAppStore, subTitle: nil, url: nil)]
    
    @IBOutlet private weak var aboutAppCollection: UICollectionView!
    
    @IBOutlet internal weak var whatsNewView: WhatsNewView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        aboutAppCollection.register(UINib(nibName: AboutAppTitleHeaderIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: AboutAppTitleHeaderIdentifier)
        
        aboutAppCollection.delegate = self
        aboutAppCollection.dataSource = self
        
        whatsNewView.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    internal func openReportAProblemEmail() {
        LoggingUtils.logMessage(message: "Show report a problem")
        guard MFMailComposeViewController.canSendMail() else {
            AlertUtils.showAlert(title: "Email account is not set in the device", message: "In order to send a report to us you need to set up an email account, or contact us directly at \(Constants.AboutApp.SupportEmail)", actions: [], inVC: self)
            return
        }
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([Constants.AboutApp.SupportEmail])
        composeVC.setSubject("Problem in My Books")
        
        if let diagnosticsData = DiagnosticsUtils.generateDiagnosticsReportData() {
            composeVC.addAttachmentData(diagnosticsData, mimeType: "text/txt", fileName: "diagnostics.txt")
        }
        
        self.present(composeVC, animated: true, completion: nil)
    }
    
    internal func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    internal func shareApp() {
        let textToShare = String(format: "My Books - \(Constants.Misc.AppStoreUrl)")
        let activityVC = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    internal func closeWhatsNewView() {
        whatsNewView.hide(completion: {
            self.whatsNewView.isHidden = true
        })
    }
}


