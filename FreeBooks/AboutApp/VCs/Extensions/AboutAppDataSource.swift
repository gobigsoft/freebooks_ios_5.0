//
//  AboutAppDataSource.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension AboutAppVC: UICollectionViewDataSource {
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let aboutAppSection = AboutAppSections.allValues[indexPath.section]
        let cell = ((aboutAppSection == .DevelopedBy) ? collectionView.dequeueReusableCell(withReuseIdentifier: MultiLabelCellIdentifier, for: indexPath) : collectionView.dequeueReusableCell(withReuseIdentifier: SingleLabelCellIdentifier, for: indexPath)) as! AboutAppCell
        cell.updateSeparator(color: Constants.Colors.Brown_10)

        switch aboutAppSection {
        case .DevelopedBy:
            let developedByItem = DevelopedByItems[indexPath.row]
            cell.initWithTitle(titleText:developedByItem.type.rawValue, subTitleText: developedByItem.subTitle ?? "")
            break
        case .Help:
            cell.initWithTitle(titleText: HelpItems[indexPath.row].type.rawValue)
            break
        case .Legal:
            cell.initWithTitle(titleText: LegalItems[indexPath.row].type.rawValue)
            break
        case .SupportApp:
            cell.initWithTitle(titleText: SupportAppItems[indexPath.row].type.rawValue)
            break
        }
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let aboutAppSection = AboutAppSections.allValues[section]
        
        switch aboutAppSection {
        case .DevelopedBy:
            return DevelopedByItems.count
        case .Help:
            return HelpItems.count
        case .Legal:
            return LegalItems.count
        case .SupportApp:
            return SupportAppItems.count
        }
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return AboutAppSections.allValues.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let aboutAppSection = AboutAppSections.allValues[indexPath.section]
            
            switch aboutAppSection {
            case .DevelopedBy:
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AboutAppVersionHeaderIdentifier, for: indexPath) as! AboutAppVersionHeader
                headerView.initWithVersion(version: Bundle.main.releaseVersionNumberPretty)
                return headerView
            default:
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AboutAppTitleHeaderIdentifier, for: indexPath) as! AboutAppTitleHeader
                headerView.initWithTitle(title: aboutAppSection.rawValue)
                return headerView
            }
        default:
            return UICollectionReusableView()
        }
    }
}
