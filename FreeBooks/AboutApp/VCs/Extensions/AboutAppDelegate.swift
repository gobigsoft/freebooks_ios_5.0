//
//  AboutAppDelegate.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension AboutAppVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let aboutAppSection = AboutAppSections.allValues[indexPath.section]
        
        switch aboutAppSection {
        case .DevelopedBy:
            openUrlIfPossible(urlString: DevelopedByItems[indexPath.row].url)
        case .Help:
            let helpItem = HelpItems[indexPath.row]
            
            if helpItem.type == .ReportAProblem {
                openReportAProblemEmail()
            } else if helpItem.type == .Forum {
                openUrlIfPossible(urlString: HelpItems[indexPath.row].url)
            } else if helpItem.type == .ShowGuide {
                showWalkthrough()
            } else if helpItem.type == .WhatsNew {
                showWhatsNew()
            }
        case .Legal:
            let legalItem = LegalItems[indexPath.row]
            
            if legalItem.type == .PrivacyPolicy {
                openUrlIfPossible(urlString: LegalItems[indexPath.row].url)
            } else if legalItem.type == .OpenSourceLicenses {
                showOpenSourceLicenses()
            }
        case .SupportApp:
            let supportAppItem = SupportAppItems[indexPath.row]
            
            if supportAppItem.type == .ShareAppWithFriends {
                shareApp()
            } else if supportAppItem.type == .RateOnAppStore {
                openUrlIfPossible(urlString: Constants.Misc.AppStoreUrl)
            }
        }
    }
    
    private func openUrlIfPossible(urlString: String?) {
        guard let urlString = urlString, let url = URL(string: urlString) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func showWalkthrough() {
        if let walkthroughContainer = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.WalkthroughContainerVC) as? WalkthroughContainerVC {
            walkthroughContainer.setOpenedFromAbout()
            self.present(walkthroughContainer, animated: true, completion: nil)
        }
    }
    
    private func showOpenSourceLicenses() {
        if let openSourceLicensesVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.OpenSourceLicensesVC) as? OpenSourceLicensesVC {
            self.present(openSourceLicensesVC, animated: true, completion: nil)
        }
    }
    
    private func showWhatsNew() {
        whatsNewView.isHidden = false
        whatsNewView.show()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 50)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if AboutAppSections.allValues[section] == .DevelopedBy {
            return CGSize(width: self.view.frame.size.width, height: 189)
        } else {
            return CGSize(width: self.view.frame.size.width, height: 64)
        }
    }
}
