//
//  OpenSourceLicensesVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import WebKit

class OpenSourceLicensesVC: BaseFakeNavigationBarVC {
    
    @IBOutlet weak private var licensesWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = Bundle.main.url(forResource: Constants.AboutApp.LicensesFileName, withExtension: "html", subdirectory: nil) {
            licensesWebView.loadFileURL(url, allowingReadAccessTo: url)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
