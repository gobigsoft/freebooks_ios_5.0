//
//  DiagnosticsUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class DiagnosticsUtils: NSObject {
    
    public static func generateDiagnosticsReportData() -> Data? {
        let data = createDiagnosticsReportString().data(using: String.Encoding.utf8)
        return data
    }
    
    private static func createDiagnosticsReportString() -> String {
        var diagnosticReport = ""
        diagnosticReport.append("--- GENERAL \n")
        diagnosticReport.stringWithAppendedDiagnostic(key: "Bundle Id", value: obtainBundleId())
        diagnosticReport.stringWithAppendedDiagnostic(key: "Version", value: obtainVersion())
        diagnosticReport.stringWithAppendedDiagnostic(key: "Device", value: obtainDeviceType())
        diagnosticReport.stringWithAppendedDiagnostic(key: "Display", value: obtainResolution())
        diagnosticReport.stringWithAppendedDiagnostic(key: "iOS Version", value: obtainSystemVersion())
        diagnosticReport.stringWithAppendedDiagnostic(key: "Generated", value: obtainGeneratedTimestamp())
        diagnosticReport.append("\n")
        
        diagnosticReport.append("--- PERMISSIONS \n")
        diagnosticReport.stringWithAppendedDiagnostic(key: "Voice search", value: getVoiceSearchPermissionStatus())
        diagnosticReport.append("\n")
        
        diagnosticReport.append("--- USER DEFAULTS STATE \n")
        diagnosticReport.append(getUserDefaultsState())
        diagnosticReport.append("\n")
        
        diagnosticReport.append("--- DATABASE \n")
        diagnosticReport.stringWithAppendedDiagnostic(key: "DB Version", value: getRealmVersion())
        diagnosticReport.append("\n")
        
        diagnosticReport.append("--- LIBRARY BOOKS \n")
        diagnosticReport.append(getBookLibrarySate())
        diagnosticReport.append("\n")
        
        diagnosticReport.append("--- LIBRARY AUDIOBOOKS \n")
        diagnosticReport.append(getAudiobookLibraryState())
        diagnosticReport.append("\n")
        
        diagnosticReport.append("--- IN APP PURCHASES \n")
        diagnosticReport.append(getInAppPurchasesState())
        
        //Used for internal testing, returns all the logs gathered from the current session of the app
//        diagnosticReport.append("--- APP LOG \n")
//        diagnosticReport.append(LoggingUtils.sharedUtils.retrieveLog())
        
        return diagnosticReport
    }
    
    private static func obtainBundleId() -> String {
        return Bundle.main.bundleIdentifier ?? "Unknown"
    }
    
    private static func obtainVersion() -> String {
        return "\(Bundle.main.releaseVersionNumber ?? "Unknown") build \(Bundle.main.buildVersionNumber ?? "unknown")"
    }
    
    private static func obtainDeviceType() -> String {
        return UIDevice.modelName
    }
    
    private static func obtainResolution() -> String {
        return "\(DeviceUtils.ScreenSize.SCREEN_WIDTH)x\(DeviceUtils.ScreenSize.SCREEN_HEIGHT)"
    }
    
    private static func obtainSystemVersion() -> String {
        return UIDevice.current.systemVersion
    }
    
    private static func obtainGeneratedTimestamp() -> String {
        return "\(Date())"
    }
    
    private static func getRealmVersion() -> String {
        return "\(Config.Realm.SchemaVersion)"
    }
    
    private static func getUserDefaultsState() -> String {
        var userDefaultsState = ""
        userDefaultsState.stringWithAppendedDiagnostic(key: "walkthrough_displayed", value: "\(UserDefaultsUtils.wasWalkthroughDisplayed())")
        userDefaultsState.stringWithAppendedDiagnostic(key: "search_hint_displayed", value: "\(UserDefaultsUtils.wasSearchHintDisplayed())")
        userDefaultsState.stringWithAppendedDiagnostic(key: "reader_controls_hint_displayed", value: "\(UserDefaultsUtils.wasReaderControlsHintDisplayed())")
        userDefaultsState.stringWithAppendedDiagnostic(key: "ads_removed", value: "\(UserDefaultsUtils.areAdsRemoved())")
        userDefaultsState.stringWithAppendedDiagnostic(key: "analytics_default_settings_overriden", value: "\(UserDefaultsUtils.wereAnalyticsSettingsAlreadyOverriden())")
        userDefaultsState.stringWithAppendedDiagnostic(key: "analytics_enabled", value: "\(UserDefaultsUtils.areAnalyticsEnabled())")
        userDefaultsState.stringWithAppendedDiagnostic(key: "unfinished_playback_audiobook_id", value: "\(UserDefaultsUtils.getUnfinishedPlaybackAudiobookId())")
        userDefaultsState.stringWithAppendedDiagnostic(key: "audiobooks_subscription_active", value: "\(UserDefaultsUtils.isAudiobooksSubscriptionActive())")
        
        return userDefaultsState
    }
    
    private static func getBookLibrarySate() -> String {
        var bookLibraryState = ""
        let books = RealmUtils.getStoredBookDetails(bookType: .Book)
        
        for i in 0..<books.count {
            bookLibraryState.append("[\(i)] \n")
            let book = books[i]
            bookLibraryState.stringWithAppendedDiagnostic(key: "id", value: "\(book.bookId)")
            bookLibraryState.stringWithAppendedDiagnostic(key: "title", value: book.title)
            bookLibraryState.stringWithAppendedDiagnostic(key: "progress", value: "\(book.progress)")
            bookLibraryState.stringWithAppendedDiagnostic(key: "epub_downloaded", value: "\(isEpubDownloaded(bookId: book.bookId))")
        }
        
        return bookLibraryState
    }
    
    private static func getAudiobookLibraryState() -> String {
        var audiobookLibraryState = ""
        let audiobooks = RealmUtils.getStoredBookDetails(bookType: .Audiobook)
        
        for i in 0..<audiobooks.count {
            audiobookLibraryState.append("[\(i)] \n")
            let audiobook = audiobooks[i]
            audiobookLibraryState.stringWithAppendedDiagnostic(key: "id", value: "\(audiobook.bookId)")
            audiobookLibraryState.stringWithAppendedDiagnostic(key: "title", value: audiobook.title)
            audiobookLibraryState.stringWithAppendedDiagnostic(key: "progress", value: "\(audiobook.progress)")
            audiobookLibraryState.stringWithAppendedDiagnostic(key: "downloaded_chapters", value: getDownloadedChaptersString(bookDetail: audiobook))
        }
        
        return audiobookLibraryState
    }
    
    private static func getInAppPurchasesState() -> String {
        var inAppPurchasesState = ""
        if let inAppPurchasesReceipts = ReceiptValidator.getPurchaseReceipts() {
            for i in 0..<inAppPurchasesReceipts.count {
                inAppPurchasesState.append("[\(i)] \n")
                let receipt = inAppPurchasesReceipts[i]
                
                if let productIdentifier = receipt.productIdentifier {
                    inAppPurchasesState.stringWithAppendedDiagnostic(key: "product", value: productIdentifier)
                }
                
                if let purchaseDate = receipt.purchaseDate {
                    inAppPurchasesState.stringWithAppendedDiagnostic(key: "purchase_date", value: "\(String(describing: purchaseDate))")
                }
                
                if let originalPurchaseDate = receipt.originalPurchaseDate {
                    inAppPurchasesState.stringWithAppendedDiagnostic(key: "original_purchase_date", value: "\(String(describing: originalPurchaseDate))")
                }
                
                if let expirationDate = receipt.subscriptionExpirationDate {
                    inAppPurchasesState.stringWithAppendedDiagnostic(key: "subscription_expiration_date", value: "\(String(describing: expirationDate))")
                }
                
                if let cancellationDate = receipt.subscriptionCancellationDate {
                    inAppPurchasesState.stringWithAppendedDiagnostic(key: "subscription_cancellation_date", value: "\(String(describing: cancellationDate))")
                }
            }
        }
        return inAppPurchasesState
    }
    
    private static func isEpubDownloaded(bookId: Int) -> Bool {
        return (FileUtils.getStoredEPUB(bookId: bookId) != nil)
    }
    
    private static func getDownloadedChaptersString(bookDetail: BookDetail) -> String {
        var downloadedChaptersString = "\n"
        
        let downloadedChapters = bookDetail.audiobookChapters.filter({ $0.isDownloaded == true })
        for i in 0..<downloadedChapters.count {
            let chapter = downloadedChapters[i]
            if chapter.isDownloaded == true {
                downloadedChaptersString.append("id: \(chapter.chapterId), title: \(chapter.name) \n")
            }
        }
        return downloadedChaptersString
    }
    
    private static func getVoiceSearchPermissionStatus() -> String {
        switch VoiceSearchManager.sharedManager.getVoiceSearchPermissionsStatus() {
        case .Microphone_NotAuthorized:
            return "Microphone Not Authorized"
        case .SpeechRecognizer_NotAuthorized:
            return "Speech recognizer not authorized"
        case .SpeechRecognizer_Microphone_NotAuthorized:
            return "Speech recognizer and microphone not authorized"
        case .Unknown:
            return "Unknown"
        case .Ok:
            return "Ok"
        }
    }
}

extension String {
    mutating func stringWithAppendedDiagnostic(key: String, value: String) {
        self.append("\(key): \(value) \n")
    }
}

