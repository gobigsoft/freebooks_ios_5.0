//
//  AboutAppTitleHeader.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AboutAppTitleHeader: UICollectionReusableView {
    
    @IBOutlet weak private var title: UILabel!
    
    func initWithTitle(title: String) {
        self.title.text = title
    }
}

