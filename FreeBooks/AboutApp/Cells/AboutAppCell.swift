//
//  AboutAppCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 18/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AboutAppCell: SeparatorCell {
    
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var subTitle: UILabel?
    
    func initWithTitle(titleText: String, subTitleText: String? = nil) {
        self.title.text = titleText
    
        if let subTitle = self.subTitle, let subTitleText = subTitleText {
            subTitle.text = subTitleText
        }
    }
}
