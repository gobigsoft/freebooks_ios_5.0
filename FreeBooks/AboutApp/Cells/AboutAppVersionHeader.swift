//
//  AboutAppVersionHeader.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AboutAppVersionHeader: UICollectionReusableView {
    
    @IBOutlet weak private var version: UILabel!
    
    func initWithVersion(version: String) {
        self.version.text = version
    }
}


