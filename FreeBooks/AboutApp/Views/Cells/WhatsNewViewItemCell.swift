//
//  WhatsNewViewItemCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class WhatsNewViewItemCell: UICollectionViewCell {

    private let termsToHiglight = ["UI redesign", "unified search"]
    @IBOutlet weak private var title: UILabel!
    
    func initWithWhatsNewTitle(whatsNewTitle: String) {
        if let termToBeHighlighted = findHiglightedTermInTitle(title: whatsNewTitle) {
            self.title.attributedText = NSMutableAttributedString(fullString: "•    \(whatsNewTitle)", fullStringColor: UIColor.black, subString: termToBeHighlighted, subStringColor: UIColor.black, substringFontSize: self.title.font.pointSize)
        } else {
            self.title.text = "•    \(whatsNewTitle)"
        }
    }
    
    private func findHiglightedTermInTitle(title: String) -> String? {
        var foundTerm: String?
        termsToHiglight.forEach( {
            if title.contains($0) {
                foundTerm = $0
            }
        })
        return foundTerm
    }
}
