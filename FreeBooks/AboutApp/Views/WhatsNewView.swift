//
//  WhatsNew.swift
//  FreeBooks
//
//  Created by Karol Grulling on 19/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol WhatsNewViewDelegate {
    func closeWhatsNewView()
}

class WhatsNewView: UIView {
    
    internal let WhatsNewItems = ["Fresh new UI redesign", "Smarter unified search", "Feeling lucky button", "Added metadata about books in \n      book detail", "Matching books button in book detail", "New audiobooks tab"]
    
    private let WhatsNewViewNibName = "WhatsNewView"
    private let WhatsNewViewItemCellIdentifier = "WhatsNewViewItemCell"

    private var view: UIView!
    
    @IBOutlet weak private var backgroundView: UIView!
    
    @IBOutlet weak private var whatsNewCollectionContainer: UIView!
    @IBOutlet private var whatsNewViewCollection: UICollectionView!

    private var whatsNewViewGestureRecognizer: UITapGestureRecognizer?

    var delegate: WhatsNewViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        setupGestureRecognizer()
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: WhatsNewViewNibName, bundle: bundle)
        let views = nib.instantiate(withOwner: self, options: nil)
        let appearanceSettingsView = views[0] as! UIView
        return appearanceSettingsView
    }
    
    override func awakeFromNib() {
        whatsNewViewCollection.register(UINib(nibName: WhatsNewViewItemCellIdentifier, bundle: nil), forCellWithReuseIdentifier: WhatsNewViewItemCellIdentifier)
        whatsNewViewCollection.dataSource = self
        whatsNewViewCollection.delegate = self
        
        whatsNewCollectionContainer.setRoundedCorners(radius: 2)
        UIUtils.dropShadow(view: whatsNewCollectionContainer)
    }
    
    private func setupGestureRecognizer() {
        self.whatsNewViewGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeWhatsNewView))
        self.backgroundView.addGestureRecognizer(self.whatsNewViewGestureRecognizer!)
    }
    
    func show() {
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1
        }
    }
    
    func hide(completion: @escaping (() -> Void)) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { success in
            completion()
        })
    }
    
    @IBAction private func closeWhatsNewView() {
        self.delegate?.closeWhatsNewView()
    }
}

extension WhatsNewView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return WhatsNewItems.count
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WhatsNewViewItemCellIdentifier, for: indexPath) as! WhatsNewViewItemCell
        cell.initWithWhatsNewTitle(whatsNewTitle: WhatsNewItems[indexPath.row])
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 45)
    }
}
