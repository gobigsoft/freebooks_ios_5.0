//
//  ReaderPageConfig.swift
//  FreeBooks
//
//  Created by Karol Grulling on 05/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReaderPageConfig: NSObject {
    
    private let bgColor: String
    private let textColor: String
    private let textColorSecondary: String
    private let fontName: String
    private let fontSize: Float
    private let textAlignment: Int
    
    private let localPageIndex: Int
    private let globalPageIndex: Int
    private let showBookmark: Bool
    private let shouldCalculateTotalPages: Bool
    private let spineElementsCount: Int
    private let bookmarkColor: String
    private let tocTargetIds: [String]
 
    init(visualConfig: ReaderVisualConfig,
         localPageIndex: Int,
         globalPageIndex: Int,
         showBookmark: Bool,
         shouldCalculateTotalPages: Bool,
         spineElementsCount: Int,
         bookmarkColor: String,
         tocTargetIds: [String])
    {
        self.bgColor = visualConfig.getBgColor()
        self.textColor = visualConfig.getTextColor()
        self.textColorSecondary = visualConfig.getTextColorSecondary()
        self.fontName = visualConfig.getFontName()
        self.fontSize = visualConfig.getFontSize()
        self.textAlignment = visualConfig.getTextAlignment()
        
        self.localPageIndex = localPageIndex
        self.globalPageIndex = globalPageIndex
        self.showBookmark = showBookmark
        self.shouldCalculateTotalPages = shouldCalculateTotalPages
        self.spineElementsCount = spineElementsCount
        self.bookmarkColor = bookmarkColor
        self.tocTargetIds = tocTargetIds
    }
    
    func getJsonString() -> String {
        let jsonString = JSON(["bgColor": bgColor, "textColor": textColor, "textColorSecondary": textColorSecondary, "fontName": fontName, "fontSize": fontSize, "textAlignment": textAlignment, "localPageIndex": localPageIndex,
                               "globalPageIndex": globalPageIndex, "showBookmark": showBookmark, "shouldCalculateTotalPages": shouldCalculateTotalPages, "spineElementsCount": spineElementsCount, "bookmarkColor": bookmarkColor, "tocTargetIds" : tocTargetIds]).rawString(options: []) ?? ""
        return jsonString
    }
}
