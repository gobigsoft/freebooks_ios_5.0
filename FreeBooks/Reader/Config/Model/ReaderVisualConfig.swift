//
//  ReaderVisualConfig.swift
//  FreeBooks
//
//  Created by Karol Grulling on 11/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class ReaderVisualConfig: NSObject {
    
    private let bgColor: String
    private let textColor: String
    private let textColorSecondary: String
    private let fontName: String
    private let fontSize: Float
    private let textAlignment: Int
    
    init(bgColor: String,
         textColor: String,
         textColorSecondary: String,
         fontName: String,
         fontSize: Float,
         textAlignment: Int) {
        
        self.bgColor = bgColor
        self.textColor = textColor
        self.textColorSecondary = textColorSecondary
        self.fontName = fontName
        self.fontSize = fontSize
        self.textAlignment = textAlignment
    }
    
    func getBgColor() -> String {
        return bgColor
    }
    
    func getTextColor() -> String {
        return textColor
    }
    
    func getTextColorSecondary() -> String {
        return textColorSecondary
    }
    
    func getFontName() -> String {
        return fontName
    }
    
    func getFontSize() -> Float {
        return fontSize
    }
    
    func getTextAlignment() -> Int {
        return textAlignment
    }
}
