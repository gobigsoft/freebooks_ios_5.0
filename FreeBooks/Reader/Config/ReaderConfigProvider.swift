//
//  ReaderConfigProvider.swift
//  FreeBooks
//
//  Created by Karol Grulling on 11/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit


class ReaderConfigProvider: NSObject {
    
    static let sharedProvider = ReaderConfigProvider()

    func getConfig(pageIndex: Int, htmlPagesCount: Int, localPageIndex: Int, shouldCalculateTotalPages: Bool, tableOfContentsChapterIds: [String], readerStyle: ReaderStyle) -> ReaderPageConfig {
        let visualConfig = ReaderVisualConfig(bgColor:readerStyle.theme.backgroundColorHex, textColor: readerStyle.theme.textColorHex, textColorSecondary: readerStyle.theme.textSecondaryColorHex, fontName: readerStyle.font.rawValue, fontSize: readerStyle.htmlFontSize, textAlignment: TextAlignments.allValues.index(of: readerStyle.textAlignment) ?? 0)
        return ReaderPageConfig(visualConfig: visualConfig, localPageIndex: localPageIndex, globalPageIndex: pageIndex, showBookmark: false, shouldCalculateTotalPages: shouldCalculateTotalPages, spineElementsCount: htmlPagesCount, bookmarkColor: readerStyle.theme.bookmarkColorHex, tocTargetIds: tableOfContentsChapterIds)
    }
}
