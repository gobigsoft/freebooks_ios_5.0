//
//  ReaderEngine.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import EPUBKit
import SwiftSoup

protocol ReaderEngineDelegate {
    func bookmarkAdded()
    func bookmarkRemoved()
    
    func bookmarkSelected(bookmarkPage: Int)
    
    func tableOfContentsItemSelected(page: Int)
    
    func adPageDisplayed()
    func adPageNotDisplayed()
}

class ReaderEngine: NSObject {
    
    private let bookDetail: BookDetail
    private let ePUB: EPUBDocument
    
    private var pageCounts: [Int]?
    private var totalPages: Int?
    
    private var currentPage: ReaderSinglePageVC?
    private var currentReadingProgress: Float
    
    private var tableOfContentsItems = [TableOfContentsItem]()
    
    var delegate: ReaderEngineDelegate?
    
    //Setup
    init(bookDetail: BookDetail, ePUB: EPUBDocument) {
        self.bookDetail = bookDetail
        self.currentReadingProgress = bookDetail.progress
        self.ePUB = ePUB
        super.init()
        
        fillTableOfContentsFromEpub()
    }
    
    func getCache(textSize: Int) -> ReaderCache? {
        return self.bookDetail.readerCache.first(where: {$0.getFontSize() == textSize})
    }
    
    func setupWithPageCounts(pageCounts: [Int], tableOfContentsIndexItems: [TableOfContentsIndexItem]) {
        self.pageCounts = pageCounts
        self.totalPages = ReaderEngineUtils.countPages(countArray: pageCounts)
        
        self.fillTableOfContentsWithIndices(indices: tableOfContentsIndexItems)
        
        RealmUtils.cacheReader(pageCounts: pageCounts, tableOfContentsIndexItems: tableOfContentsIndexItems, totalPages: self.totalPages ?? 0, fontSize: ReaderStyleProvider.sharedProvider.getCurrentTextSize(), bookId: self.bookDetail.bookId)
    }
    
    func getCoverUrl() -> String {
        return self.bookDetail.coverUrl
    }
    
    func getBookTitle() -> String {
        return self.bookDetail.title
    }
    
    func getBookId() -> Int {
        return self.bookDetail.bookId
    }
    
    func getAuthorFullName() -> String {
        return Utils.getAuthorLongNames(book: bookDetail)
    }
    
    func getPageContent(pageIndex: Int) -> String {
        guard let pageCounts = self.pageCounts else {
            let htmlIndex = ReaderEngineUtils.getHtmlAndLocalPageIndex(globalPageIndex: pageIndex, pageCounts: [Int]()).htmlIndex
            return ReaderEngineUtils.postProcessHtmlContent(ePUB: ePUB, htmlIndex: htmlIndex) ?? ""
        }
        return ReaderEngineUtils.postProcessHtmlContent(ePUB: ePUB, htmlIndex: ReaderEngineUtils.getHtmlAndLocalPageIndex(globalPageIndex: pageIndex, pageCounts: pageCounts).htmlIndex) ?? ""
    }
    
    func getHtmlPageContent(htmlPageIndex: Int) -> String {
        return ReaderEngineUtils.postProcessHtmlContent(ePUB: ePUB, htmlIndex: htmlPageIndex) ?? ""
    }
    
    func getHtmlPagesCount() -> Int {
        return ePUB.spine.items.count
    }
    
    func getTotalPagesCount() -> Int {
        guard let pageCounts = self.pageCounts else {
            //If there're no page counts yet, display at least 1 page
            return 1
        }
        return ReaderEngineUtils.countPages(countArray: pageCounts)
    }
    
    func getLocalPageIndex(globalPageIndex: Int) -> Int {
        guard let pageCounts = self.pageCounts else {
            return 0
        }
        return ReaderEngineUtils.getHtmlAndLocalPageIndex(globalPageIndex: globalPageIndex, pageCounts: pageCounts).localPageIndex
    }
    
    func getReaderStyle() -> ReaderStyle {
        return ReaderStyleProvider.sharedProvider.getStyle()
    }
    
    func shouldCalculateTotalPages() -> Bool {
        return totalPages == nil
    }
    
    func setCurrentPage(page: ReaderSinglePageVC) {
        self.currentPage = page
    }
    
    func setCurrentReadingProgress(pageIndex: Int) {
        if let totalPages = self.totalPages {
            currentReadingProgress = calculateReadingProress(currentPage: pageIndex, totalPages: totalPages)
            LoggingUtils.logMessage(message: "Current reading progress calculated as \(String(describing: currentReadingProgress))")
            
            self.bookDetail.progress = Float(currentReadingProgress)
            RealmUtils.updateProgress(progress: Float(currentReadingProgress), bookId: self.bookDetail.bookId)
        }
    }
    
    func getPageIndexForCurrentProgress() -> Int {
        let pageIndex = calculateCurrentPageIndexForProgress(progress: currentReadingProgress)
        LoggingUtils.logMessage(message: "Calculated page index for current progress is \(pageIndex)")
        return pageIndex
    }
    
    func calculateReadingProress(currentPage: Int, totalPages: Int) -> Float {
        return Float(currentPage)/Float(totalPages)
    }
    
    func calculateCurrentPageIndexForProgress(progress: Float) -> Int {
        guard let totalPages = self.totalPages else {
            return 0
        }
        return Int(Float(totalPages)*progress)
    }
    
    func requestBookmark() {
        self.currentPage?.getPageSnippet()
    }
    
    func createBookmark(pageIndex: Int, snippet: String) {
        if let totalPages = self.totalPages {
            let bookmark = Bookmark(progress: calculateReadingProress(currentPage: pageIndex, totalPages: totalPages), snippet: snippet)
            self.bookDetail.bookmarks.append(bookmark)
            RealmUtils.storeBookmark(bookId: self.bookDetail.bookId, bookmark: bookmark)
            LoggingUtils.logMessage(message: "Storing bookmark for progress: \(bookmark.progress)")
            delegate?.bookmarkAdded()
        }
    }
    
    func isPageBoomarked(pageIndex: Int) -> Bool {
        //Since we have progress stored in book detail, we need to calculate it's page index with regards to current font size and compare it with the current page index we're on
        return self.bookDetail.bookmarks.contains(where: { calculateCurrentPageIndexForProgress(progress: $0.progress) == pageIndex })
    }
    
    func isCurrentPageBookmarked() -> Bool {        
        //Since we have progress stored in book detail, we need to calculate it's page index with regards to current font size and compare it with the current page index we're on
        if let currentIndex = currentPage?.pageIndex {
            return isPageBoomarked(pageIndex: currentIndex)
        } else {
            return false
        }
    }
    
    //Called from Single Page VC, therefore we need to inform Reader VC to change the Top bar
    func deleteBookmark(pageIndex: Int){
        if let totalPages = self.totalPages {
            let bookmarkReadingProgress = calculateReadingProress(currentPage: pageIndex, totalPages: totalPages)
            
            RealmUtils.removeBookmark(bookId: bookDetail.bookId, progress: bookmarkReadingProgress)
            self.bookDetail.bookmarks.removeAll(where: {$0.progress == bookmarkReadingProgress})
            self.delegate?.bookmarkRemoved()
        }
    }
    
    //Called outside of Single Page VC, therefore we need to hide bookmark from it
    func deleteBookmark(progress: Float) {
        RealmUtils.removeBookmark(bookId: bookDetail.bookId, progress: progress)
        self.bookDetail.bookmarks.removeAll(where: {$0.progress == progress})
    }
    
    func deleteBookmarkOnCurrentPage() {
        self.deleteBookmark(progress: currentReadingProgress)
        self.currentPage?.hideBookmark()
    }
    
    func getBookmarks() -> [Bookmark] {
        return self.bookDetail.bookmarks
    }
    
    func bookmarkSelected(bookmark: Bookmark) {
        self.delegate?.bookmarkSelected(bookmarkPage: calculateCurrentPageIndexForProgress(progress: bookmark.progress))
    }
    
    func resetContents() {
        pageCounts = nil
        totalPages = nil
    }
    
    func adPageDisplayed() {
        self.delegate?.adPageDisplayed()
    }
    
    func adPageNotDisplayed() {
        self.delegate?.adPageNotDisplayed()
    }
    
    private func fillTableOfContentsFromEpub() {
        if let tableOfContents = self.ePUB.tableOfContents.subTable {
            tableOfContents.forEach({
                guard let itemId = $0.item, let hashtagIndex = itemId.range(of: "#")?.upperBound else {
                    return
                }
                self.tableOfContentsItems.append(TableOfContentsItem(chapterName: $0.label, chapterId: String(itemId.suffix(from: hashtagIndex))))
            })
        }
    }
    
    func getTableOfContentChapterIds() -> [String] {
        var tableOfContentChapterIds = [String]()
        self.tableOfContentsItems.forEach({
            tableOfContentChapterIds.append($0.getChapterId())
        })
        return tableOfContentChapterIds
    }
    
    private func fillTableOfContentsWithIndices(indices: [TableOfContentsIndexItem]) {
        for tableOfContentItem in self.tableOfContentsItems {
            if let chapterIndexItem = indices.filter({ $0.chapterId == tableOfContentItem.getChapterId()}).first {
                tableOfContentItem.setPageIndex(index: chapterIndexItem.getPageIndex())
            }
        }
    }
    
    func getTableOfContentsItems() -> [TableOfContentsItem] {
        return self.tableOfContentsItems
    }
    
    func tableOfContentsItemSelected(tableOfContentsItem: TableOfContentsItem) {
        self.delegate?.tableOfContentsItemSelected(page: tableOfContentsItem.getPageIndex())
    }
    
    func getChapterNameForPageIndex(index: Int) -> String {
        LoggingUtils.logMessage(message: "Retrieving chapter name for index \(index)")
        
        let sortedTableOfContents = tableOfContentsItems.sorted(by: {$0.getPageIndex() > $1.getPageIndex()})
        guard sortedTableOfContents.count > 0 else {
            return getBookTitle()
        }
    
        for i in 0...sortedTableOfContents.count-1 {
            if index >= sortedTableOfContents[i].getPageIndex() {
                return sortedTableOfContents[i].getChapterName()
            }
        }
    
        return getBookTitle()
    }
}
