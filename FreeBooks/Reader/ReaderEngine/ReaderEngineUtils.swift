//
//  ReaderEngineUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 13/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import EPUBKit
import SwiftSoup

class ReaderEngineUtils: NSObject {
    
    public static func postProcessHtmlContent(ePUB: EPUBDocument, htmlIndex: Int) -> String? {
        let resource = ePUB.spine.items[htmlIndex]
        let pageUrl = ePUB.contentDirectory.absoluteString+(resource.idref)
        
        do {
            let document = try SwiftSoup.parse(String(contentsOf: URL(string: pageUrl)! ))
            let head = document.head()
            if let cssElement = HTMLGenerator.createCssElement(), let viewPortMeta = HTMLGenerator.createViewportMeta(), let scriptPromisesBackPort = HTMLGenerator.createScriptPromisesBackport(), let scriptLink = HTMLGenerator.createScriptLink(), let scriptOnLoad = HTMLGenerator.createScriptOnLoad() {
                try head?.appendChild(cssElement)
                try head?.appendChild(viewPortMeta)
                try head?.appendChild(scriptPromisesBackPort)
                try head?.appendChild(scriptLink)
                try head?.appendChild(scriptOnLoad)
            }
            
            if let body = document.body(), let bodyWithNewOuterAndInnerDiv = HTMLGenerator.createOuterAndInnerDivAndMoveBodyThere(body: body), let bookmarkWrapper = HTMLGenerator.createBookmarkWrapper(), let pageNumberWrapper = HTMLGenerator.createPageNumberWrapper() {
                try body.appendChild(bodyWithNewOuterAndInnerDiv)
                try body.appendChild(bookmarkWrapper)
                try body.appendChild(pageNumberWrapper)
            }
            
            let editedHtml = try document.outerHtml()
            return editedHtml
        } catch let error as NSError {
            print("Couldn't post process EPUB, error: \(error.description)")
            return nil
        }
    }
    
    public static func getHtmlAndLocalPageIndex(globalPageIndex: Int, pageCounts:[Int]) -> (htmlIndex: Int, localPageIndex: Int) {
        var counts = 0
        for i in 0..<pageCounts.count {
            let htmlPageCount = pageCounts[i]
            counts = counts+htmlPageCount
            if globalPageIndex < counts {
                let localPageIndex = globalPageIndex - (counts - htmlPageCount)
//                LoggingUtils.logMessage(message: "Local page count = \(globalPageIndex) - (\(counts) - \(htmlPageCount)")
                return (i, localPageIndex)
            }
        }
        
        
        return (0,0)
    }
    
    //Count all pages in book, calculated from page counts returned by reader.js (every number in array represents total pages in a single HTML)
    public static func countPages(countArray: [Int]) -> Int {
        let countOfPageCountsArray = countArray.count
        var totalPages = 0
        for i in 0..<countOfPageCountsArray {
            totalPages = totalPages+countArray[i]
        }
        return totalPages
    }

}
