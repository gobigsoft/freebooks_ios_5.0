//
//  JSHandler.swift
//  FreeBooks
//
//  Created by Karol Grulling on 15/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON

protocol JSHandlerDelegate {
    func onInit()
    func onPagesRenderResult(pageCounts: [Int], tableOfContentsIndexItems: [TableOfContentsIndexItem])
    func onPagesRenderProgress(progress: Int)
    func onPageSnippetResult(pageSnippet: String)
}

struct JSCallbacks {
    static let OnInitCallback = "onInit"
    static let OnSetupWithConfigCallback = "onSetupWithConfig"
    static let OnPagesRenderResultCallback = "onPagesRenderResult"
    static let OnPagesRenderProgressCallback = "onPagesRenderProgress"
    static let OnBodyClickedCallback = "onBodyClicked"
    static let OnBookmarkClickedCallback = "onBookmarkClicked"
    static let OnAnchorClickedCallback = "onAnchorClicked"
    static let OnPageSnippetResultCallback = "onPageSnippetResult"
    static let Test = "test"
}

class JSHandler: NSObject, WKScriptMessageHandler, UIWebViewDelegate {
    
    private let readerEngine: ReaderEngine
    private var delegate: JSHandlerDelegate?
    
    init(readerEngine: ReaderEngine, delegate: JSHandlerDelegate) {
        self.readerEngine = readerEngine
        self.delegate = delegate
        super.init()
    }
    
    func createWebViewConfiguration() -> WKWebViewConfiguration {
        let webViewConfig = WKWebViewConfiguration()
        let contentController = WKUserContentController();
        
        contentController.add(self, name: JSCallbacks.Test)
        contentController.add(self, name: JSCallbacks.OnInitCallback)
        contentController.add(self, name: JSCallbacks.OnSetupWithConfigCallback)
        contentController.add(self, name: JSCallbacks.OnPagesRenderResultCallback)
        contentController.add(self, name: JSCallbacks.OnPagesRenderProgressCallback)
        contentController.add(self, name: JSCallbacks.OnBodyClickedCallback)
        contentController.add(self, name: JSCallbacks.OnBookmarkClickedCallback)
        contentController.add(self, name: JSCallbacks.OnAnchorClickedCallback)
        contentController.add(self, name: JSCallbacks.OnPageSnippetResultCallback)
        
        webViewConfig.userContentController = contentController
        
        return webViewConfig
    }
    
    internal func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == JSCallbacks.OnInitCallback {
            delegate?.onInit()
        } else if message.name == JSCallbacks.OnPagesRenderResultCallback {
            if let body = message.body as? String {
                LoggingUtils.logMessage(message: "Page counts callback: \(body)")
                let bodyJson = JSON(parseJSON: body)
                let pageCountsArray = bodyJson["pageCounts"].arrayValue
                var pageCounts = [Int]()
                pageCountsArray.forEach({ pageCounts.append($0.intValue) })
                
                var tableOfContentsIndexItems = [TableOfContentsIndexItem]()
                for (key, value) in bodyJson["anchorTargetIndices"] {
                    tableOfContentsIndexItems.append(TableOfContentsIndexItem(chapterId: key, pageIndex: value.intValue))
                }
            
                delegate?.onPagesRenderResult(pageCounts: pageCounts, tableOfContentsIndexItems: tableOfContentsIndexItems)
            }
        } else if message.name == JSCallbacks.OnPagesRenderProgressCallback {
            if let body = message.body as? Int {
                delegate?.onPagesRenderProgress(progress: body)
            }
        } else if message.name == JSCallbacks.OnPageSnippetResultCallback {
            if let body = message.body as? String {
                delegate?.onPageSnippetResult(pageSnippet: body)
            }
        } else if message.name == JSCallbacks.Test {
            if let body = message.body as? String {
                LoggingUtils.logMessage(message: "Test JS message received: \(body)")
            }
        }
    }
}




