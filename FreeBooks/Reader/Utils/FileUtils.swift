//
//  EPUBParser.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import EPUBKit

class FileUtils: NSObject {
    
    public static func parseEPUBFromData(bookData: Data, bookId: Int) -> EPUBDocument? {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let epubURL = documentsURL.appendingPathComponent(String(describing: bookId)).appendingPathExtension("epub")
        do {
            try bookData.write(to: epubURL)
        } catch {
            print("Couldn't create EPUB document from book data.")
        }
        guard let document = EPUBDocument(url: epubURL) else {
            return nil
        }
        return document
    }
    
    public static func getStoredEPUB(bookId: Int) -> EPUBDocument? {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let epubURL = documentsURL.appendingPathComponent(String(describing: bookId)).appendingPathExtension("epub")
        guard let ePUB = EPUBDocument(url: epubURL) else {
            return nil
        }
        return ePUB
    }
    
    public static func removeStoredBook(bookId: Int, isAudiobook: Bool = false) {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let htmlDirURL = documentsURL.appendingPathComponent(String(describing: bookId))
        try? FileManager.default.removeItem(at: htmlDirURL)
        
        //Regular books have EPUB, Audiobook has its contents only in folder which has been deleted above
        if isAudiobook == false {
            let epubURL = documentsURL.appendingPathComponent(String(describing: bookId)).appendingPathExtension("epub")
            try? FileManager.default.removeItem(at: epubURL)
        }
    }
}
