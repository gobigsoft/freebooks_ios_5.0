//
//  ReaderStyleProvider.swift
//  FreeBooks
//
//  Created by Karol Grulling on 22/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import RealmSwift

class ReaderStyleProvider: NSObject {
    static let sharedProvider = ReaderStyleProvider()
    
    private var currentTheme: Theme?
    private var currentThemeType: ThemeTypes = ThemeTypes.Sepia
    private var currentTextAlignment: TextAlignments = TextAlignments.Justify
    private var currentFont: Fonts = Fonts.Neuton
    private var currentTextSize: Int = 100
    
    private let themes = [
        Theme(themeType: .Sepia,
              textColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.SepiaDark),
              textColorHex: Constants.ThemeConstants.ColorsHex.SepiaDark,
              textSecondaryColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.SepiaDarker),
              textSecondaryColorHex: Constants.ThemeConstants.ColorsHex.SepiaDarker,
              backgroundColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.Sepia),
              backgroundColorHex: Constants.ThemeConstants.ColorsHex.Sepia,
              bookmarkColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.SepiaBookmarkColor),
              bookmarkColorHex: Constants.ThemeConstants.ColorsHex.SepiaBookmarkColor),
        
        Theme(themeType: .White,
              textColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.White_Gray90),
              textColorHex: Constants.ThemeConstants.ColorsHex.White_Gray90,
              textSecondaryColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.White_Gray20),
              textSecondaryColorHex: Constants.ThemeConstants.ColorsHex.White_Gray20,
              backgroundColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.White),
              backgroundColorHex: Constants.ThemeConstants.ColorsHex.White,
              bookmarkColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.WhiteBookmarkColor),
              bookmarkColorHex: Constants.ThemeConstants.ColorsHex.WhiteBookmarkColor),
        
        Theme(themeType: .Dark,
              textColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.Dark_Gray20),
              textColorHex: Constants.ThemeConstants.ColorsHex.Dark_Gray20,
              textSecondaryColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.Dark_Gray80),
              textSecondaryColorHex: Constants.ThemeConstants.ColorsHex.Dark_Gray80,
              backgroundColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.Dark_Gray90),
              backgroundColorHex: Constants.ThemeConstants.ColorsHex.Dark_Gray90,
              bookmarkColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.DarkBookmarkColor),
              bookmarkColorHex: Constants.ThemeConstants.ColorsHex.DarkBookmarkColor),

        Theme(themeType: .Sunset,
              textColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.Sunset_RedDark),
              textColorHex: Constants.ThemeConstants.ColorsHex.Sunset_RedDark,
              textSecondaryColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.Sunset_RedDarker),
              textSecondaryColorHex: Constants.ThemeConstants.ColorsHex.Sunset_RedDarker,
              backgroundColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.Sunset_Red),
              backgroundColorHex: Constants.ThemeConstants.ColorsHex.Sunset_Red,
              bookmarkColor: UIColor.init(hexString: Constants.ThemeConstants.ColorsHex.SunsetBookmarkColor),
              bookmarkColorHex: Constants.ThemeConstants.ColorsHex.SunsetBookmarkColor)]
    
    func getStyle() -> ReaderStyle {
        return ReaderStyle(theme: currentTheme ?? getTheme(themeType: ThemeTypes.allValues[0]), font: getCurrentFont(), textAlignment: getCurrentTextAlignment(), htmlFontSize: AppearanceUtils.getHTMLFontSizeForPercentage(sizePercentage: getCurrentTextSize()))
    }
    
    func getTheme(themeType: ThemeTypes) -> Theme {
        return themes.filter{$0.themeType == themeType}.first ?? themes[0]
    }
    
    func setThemeType(themeType: ThemeTypes) {
        self.currentThemeType = themeType
        self.currentTheme = getTheme(themeType: themeType)
    }
    
    func setFont(fontType: Fonts) {
        self.currentFont = fontType
    }
    
    func setTextAlignment(alignment: TextAlignments) {
        self.currentTextAlignment = alignment
    }
    
    func setTextSize(size: Int) {
        self.currentTextSize = size
    }
    
    func getCurrentThemeType() -> ThemeTypes {
        return self.currentThemeType
    }
    
    func getCurrentFont() -> Fonts {
        return self.currentFont
    }
    
    func getCurrentTextAlignment() -> TextAlignments {
        return self.currentTextAlignment
    }
    
    func getCurrentTextSize() -> Int {
        return self.currentTextSize
    }
    
    func loadStyleFromRealm() {
        guard let storedStyle = RealmUtils.getStoredStyle() else {
            return
        }
        let style = RealmConverters.convertReaderStyle(realmReaderStyle: storedStyle)
        
        setThemeType(themeType: style.theme.themeType)
        setFont(fontType: style.font)
        setTextAlignment(alignment: style.textAlignment)
        
        //Realm stores % size, therefore we take it from there
        setTextSize(size: storedStyle.textSize)
    }
}

class ReaderStyle {
    let theme: Theme
    let font: Fonts
    let textAlignment: TextAlignments
    let htmlFontSize: Float
    
    init(theme: Theme, font: Fonts, textAlignment: TextAlignments, htmlFontSize: Float) {
        self.theme = theme
        self.font = font
        self.textAlignment = textAlignment
        self.htmlFontSize = htmlFontSize
    }
    
    init(realmReaderStyle: RealmReaderStyle) {
        if let realmReaderStyleTheme = ThemeTypes.allValues.first(where: {$0.rawValue == realmReaderStyle.theme}) {
            self.theme = ReaderStyleProvider.sharedProvider.getTheme(themeType: realmReaderStyleTheme)
        } else {
            self.theme = ReaderStyleProvider.sharedProvider.getTheme(themeType: ThemeTypes.White)
        }
   
        if let realmReaderStyleFont = Fonts.allValues.first(where: {$0.rawValue == realmReaderStyle.font}) {
            self.font = realmReaderStyleFont
        } else {
            self.font = ReaderStyleProvider.sharedProvider.getCurrentFont()
        }
        
        if let realmReaderStyleTextAlignment = TextAlignments.allValues.first(where: {$0.rawValue == realmReaderStyle.textAlignment}) {
            self.textAlignment = realmReaderStyleTextAlignment
        } else {
            self.textAlignment = ReaderStyleProvider.sharedProvider.getCurrentTextAlignment()
        }
        
        //We've stored % size, therefore we need to compute the real font size from there
        self.htmlFontSize = AppearanceUtils.getHTMLFontSizeForPercentage(sizePercentage: realmReaderStyle.textSize)
    }
    
    func getRealmObject() -> RealmReaderStyle {
        //We want to store % size rather than computed size, therefore we current text size in populating text size into realm object
        return RealmReaderStyle(value: ["theme" : self.theme.themeType.rawValue, "font": self.font.rawValue, "textAlignment": self.textAlignment.rawValue, "textSize": ReaderStyleProvider.sharedProvider.getCurrentTextSize()])
    }
}

class RealmReaderStyle: Object {
    @objc dynamic var theme: String?
    @objc dynamic var font: String?
    @objc dynamic var textAlignment: String?
    @objc dynamic var textSize: Int = 100
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
