//
//  Renderer.swift
//  FreeBooks
//
//  Created by Karol Grulling on 03/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol RendererDelegate {
    func renderingStarted()
    func renderingFinished()
}

class Renderer: NSObject, ReaderPageRenderDelegate {
    
    let engine: ReaderEngine
    let pageVCManager: ReaderPageVCManager
    
    var currentPagesRendered = [Int]()
    var renderingInProgress = false
    var rendererDisabled = true
    
    let delegate: RendererDelegate
    
    init(engine: ReaderEngine, pageVCManager: ReaderPageVCManager, delegate: RendererDelegate) {
        self.engine = engine
        self.pageVCManager = pageVCManager
        self.delegate = delegate
    }
    
    func enable() {
        rendererDisabled = false
    }
    
    func disable() {
        rendererDisabled = true 
    }
    
    func renderPages(pages: [Int]) -> [ReaderSinglePageVC] {
        var renderedPages = [ReaderSinglePageVC]()
        addPagesToRendering(pages: pages)
        pages.forEach({renderedPages.append(createReaderPageVC(pageIndex: $0))})
        return renderedPages
    }
    
    func renderNextPages(startingIndex: Int, numberOfPages: Int) -> [ReaderSinglePageVC] {
        var renderedPages = [ReaderSinglePageVC]()
        for i in startingIndex...startingIndex+numberOfPages {
            addPageToRendering(pageIndex: i)
            renderedPages.append(createReaderPageVC(pageIndex: i))
        }
        return renderedPages
    }
    
    func renderPreviousPages(startingIndex: Int, numberOfPages: Int) -> [ReaderSinglePageVC] {
        var renderedPages = [ReaderSinglePageVC]()
        
        //We need to make sure we don't render negative pages
        let pageCountToRender = startingIndex < numberOfPages ? startingIndex : numberOfPages
        
        for i in startingIndex-pageCountToRender...startingIndex {
            addPageToRendering(pageIndex: i)
            renderedPages.append(createReaderPageVC(pageIndex: i))
        }
        return renderedPages
    }
    
    func createReaderPageVC(pageIndex: Int) -> ReaderSinglePageVC {
        let readerPageVC = UIStoryboard.init(name: Constants.StoryboardConstants.StoryboardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.ReaderSinglePageVC) as! ReaderSinglePageVC
        readerPageVC.initWithBookPage(pageIndex:pageIndex, engine: engine)
        readerPageVC.pageIndex = pageIndex
        readerPageVC.delegate = self.pageVCManager
        readerPageVC.renderDelegate = self
        
        //WKWebView will not be fully loaded until it's visible/in the view hierarchy, calling view did load forcefully helps pre-loading
        readerPageVC.viewDidLoad()
        return readerPageVC
    }
    
    private func addPagesToRendering(pages: [Int]) {
        currentPagesRendered.append(contentsOf: pages)
        
        if rendererDisabled == false {
            startRendering()
        }
    }
    
    private func addPageToRendering(pageIndex: Int) {
        currentPagesRendered.append(pageIndex)
        
        if rendererDisabled == false {
            startRendering()
        }
    }
    
    private func startRendering() {
        if !renderingInProgress {
            renderingInProgress = true
            self.delegate.renderingStarted()
        }
    }
    
    private func stopRendering() {
        renderingInProgress = false
        
        if rendererDisabled == false {
            self.delegate.renderingFinished()
        }
    }
    
    internal func pageRendered(pageIndex: Int) {
        currentPagesRendered.remove(object: pageIndex)
        if currentPagesRendered.count == 0 {
            stopRendering()
        }
    }
}

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        guard let index = index(of: object) else {return}
        remove(at: index)
    }
}
