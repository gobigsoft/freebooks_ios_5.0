//
//  ReaderPageVCManager.swift
//  FreeBooks
//
//  Created by Karol Grulling on 01/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol ReaderPageVCManagerDelegate {
    func pageTurned(pageIndex: Int)
}

class ReaderPageVCManager: NSObject, UIPageViewControllerDelegate, UIPageViewControllerDataSource, ReaderPageDelegate, RendererDelegate {
    
    private let DefaultRenderedPageCount = 5
    private let MaxPageToHold = 12
    private let engine: ReaderEngine
    
    private let pageViewController = UIPageViewController(transitionStyle: UIPageViewController.TransitionStyle.pageCurl, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
    private var currentIndex = 0
    
    private var currentRenderedPages = [ReaderSinglePageVC]()
    
    private let renderingProgressHandler: (_ progress: Int) -> ()
    private let reRenderingFinishedHandler: () -> ()
    private let pagesRenderingStartedHandler: () -> ()
    private let pagesRenderingFinishedHandler: () -> ()
    
    private var renderer: Renderer?
    
    var delegate: ReaderPageVCManagerDelegate?
    
    private var adRenderer: BookAdsRenderer?
    
    //MARK: PageVC setup
    init(readerEngine: ReaderEngine, renderingProgressHandler: @escaping (Int) -> (), reRenderingFinishedHandler: @escaping () -> (), pagesRenderingStartedHandler: @escaping () -> (), pagesRenderingFinishedHandler: @escaping () -> ()) {
        self.engine = readerEngine
        
        self.renderingProgressHandler = renderingProgressHandler
        self.reRenderingFinishedHandler = reRenderingFinishedHandler
        self.pagesRenderingStartedHandler = pagesRenderingStartedHandler
        self.pagesRenderingFinishedHandler = pagesRenderingFinishedHandler
        super.init()
        
        self.renderer = Renderer(engine: self.engine, pageVCManager: self, delegate: self)
        
        //If user hasn't purchased ad free mode, we'll create ad renderer to generate ads
        if !UserDefaultsUtils.areAdsRemoved() {
            self.adRenderer = BookAdsRenderer(currentPage: self.engine.getPageIndexForCurrentProgress(), totalPages: self.engine.getTotalPagesCount())
        }
    }
    
    func setupPageVC(container: UIView, startingIndex: Int = 0) {
        //Start with the first page which would obtain page counts
        
        currentRenderedPages.append(self.renderer!.createReaderPageVC(pageIndex: startingIndex))
        
        //if we already know total page counts we can feel confident of rendering further pages
        if !self.engine.shouldCalculateTotalPages() {
            currentRenderedPages.append(contentsOf: self.renderer!.renderNextPages(startingIndex: startingIndex, numberOfPages: DefaultRenderedPageCount))
            currentRenderedPages.append(contentsOf: self.renderer!.renderPreviousPages(startingIndex: startingIndex, numberOfPages: DefaultRenderedPageCount))
        }
        
        pageViewController.delegate = self
        pageViewController.dataSource = self
        pageViewController.setViewControllers([getViewControllerAtIndex(index: startingIndex) as ReaderSinglePageVC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        
        container.addSubview(pageViewController.view)
        if let view = pageViewController.view.superview {
            pageViewController.view.frame = view.bounds
        }
    }
    
    func setCurrentPage(index: Int) {
        currentIndex = index
        
        currentRenderedPages.append(contentsOf: self.renderer!.renderNextPages(startingIndex: currentIndex, numberOfPages: DefaultRenderedPageCount))
        currentRenderedPages.append(contentsOf: self.renderer!.renderPreviousPages(startingIndex: currentIndex, numberOfPages: DefaultRenderedPageCount))
        resetViewControllers()
        
        engine.adPageNotDisplayed()
    }
    
    func cleanUpPages() {
        LoggingUtils.logMessage(message: "Cleaning up pages from Page VC Manager")
        currentRenderedPages.forEach({$0.cleanUp()})
        currentRenderedPages.removeAll()
    }
    
    //MARK: Delegate function
    internal func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let finishedVC = pageViewController.viewControllers!.first as? ReaderSinglePageVC {
                currentIndex = finishedVC.pageIndex
                adRenderer?.updatePages(currentPage: currentIndex, totalPages: self.engine.getTotalPagesCount())
                self.delegate?.pageTurned(pageIndex: currentIndex)
                self.engine.adPageNotDisplayed()
            } else if let _ = pageViewController.viewControllers!.first as? BookAdVC {
                self.engine.adPageDisplayed()
            }
        }
    }
    
    //MARK: Datasource functions
    internal func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageContent = viewController as! BaseIndexableVC
        if ((pageContent.pageIndex == 0) || (pageContent.pageIndex == NSNotFound)) {
            return nil
        }
        
        //If we don't have previous pages, we need to render them as well as make sure we don't render pages below 0
        if (currentRenderedPages.filter({$0.pageIndex == pageContent.pageIndex-2}).count == 0 && pageContent.pageIndex-2 > 0){
            currentRenderedPages.append(contentsOf: self.renderer!.renderPreviousPages(startingIndex: pageContent.pageIndex-2, numberOfPages: DefaultRenderedPageCount))
        }
        
        if !isPageAlreadyRendered(index: pageContent.pageIndex-1) {
            return nil
        }
        
        //If we determine a previous page is supposed to be Ad and we're currently on a regular book page, then display ad, otherwise just proceed backwards
        if let adRenderer = self.adRenderer, let adVC = adRenderer.getAd(bookIndex: pageContent.pageIndex), let _ = viewController as? ReaderSinglePageVC {
            return adVC
        }
        
        return getViewControllerAtIndex(index: pageContent.pageIndex-1)
    }
    
    internal func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let pageContent = viewController as! BaseIndexableVC
        if (pageContent.pageIndex == engine.getTotalPagesCount()-1)
        {
            return nil;
        }
        
        //If we don't have next pages, we need to render them as well as make sure we don't render any further than until the last page
        if (currentRenderedPages.filter({$0.pageIndex == pageContent.pageIndex+2}).count == 0 && (pageContent.pageIndex+2 < self.engine.getTotalPagesCount())){
            currentRenderedPages.append(contentsOf: self.renderer!.renderNextPages(startingIndex: pageContent.pageIndex+2, numberOfPages: DefaultRenderedPageCount))
        }
        
        if !isPageAlreadyRendered(index: pageContent.pageIndex+1) {
            return nil
        }
        
        if let adRenderer = self.adRenderer, let adVC = adRenderer.getAd(bookIndex: pageContent.pageIndex+1) {
            return adVC
        }
        
        //If the current page displayed is Book Ad, we want to display real page with the same index since ad is holding that index at the moment, otherwise we just proceed forward +1
        if let _ = viewController as? BookAdVC {
            return getViewControllerAtIndex(index: pageContent.pageIndex)
        } else {
            return getViewControllerAtIndex(index: pageContent.pageIndex+1)
        }
    }
    
    private func getViewControllerAtIndex(index: NSInteger) -> ReaderSinglePageVC {
        if currentRenderedPages.count > MaxPageToHold {
            removeDistantPages(currentIndex: index)
        }
        return currentRenderedPages.filter({$0.pageIndex == index}).first!
    }
    
    private func removeDistantPages(currentIndex: Int) {
        let startingActiveRangeIndex = currentIndex <= MaxPageToHold/2 ? 0 : currentIndex-MaxPageToHold/2
        let activeRange = startingActiveRangeIndex...startingActiveRangeIndex+20
        var indexesToRemove = [Int]()
        
        currentRenderedPages.forEach({
            if !activeRange.contains($0.pageIndex) {
                indexesToRemove.append($0.pageIndex)
                $0.cleanUp()
                LoggingUtils.logMessage(message: "Adding page \($0.pageIndex) for removal")
            }})
        
        for index in indexesToRemove {
            LoggingUtils.logMessage(message: "Removing page \(index) from renderedPages")
            currentRenderedPages.removeAll(where: {$0.pageIndex == index})
        }
    }
    
    private func isPageAlreadyRendered(index: NSInteger) -> Bool {
        return currentRenderedPages.filter({$0.pageIndex == index}).first != nil ? true : false
    }
    
    private func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return engine.getTotalPagesCount()
    }
    
    private func resetViewControllers() {
        pageViewController.setViewControllers([getViewControllerAtIndex(index: currentIndex) as ReaderSinglePageVC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    }
    
    //MARK: Reader Page Delegate
    internal func pageCountsRetrieved(pageCounts: [Int], tableOfContentsIndexItems: [TableOfContentsIndexItem]) {
        self.engine.setupWithPageCounts(pageCounts: pageCounts, tableOfContentsIndexItems: tableOfContentsIndexItems)
        //When we have page counts we can render further pages of the book with confidence
        if pageCounts.count > 0 {
            currentRenderedPages.append(contentsOf: self.renderer!.renderNextPages(startingIndex: currentIndex+1, numberOfPages: DefaultRenderedPageCount))
        }
        reRenderingFinishedHandler()        
    }
    
    internal func pageCountsCalculationProgress(progress: Int) {
        self.renderingProgressHandler(progress)
    }
    
    //MARK: Page renderer handling
    func enableRenderer() {
        self.renderer!.enable()
    }
    
    func disableRenderer() {
        self.renderer!.disable()
    }
    
    internal func renderingStarted() {
        self.pagesRenderingStartedHandler()
    }
    
    internal func renderingFinished() {
        self.pagesRenderingFinishedHandler()
    }
    
    deinit {
        self.cleanUpPages()
    }
}
