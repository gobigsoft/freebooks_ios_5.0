//
//  HTMLGenerator.swift
//  FreeBooks
//
//  Created by Karol Grulling on 01/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import SwiftSoup

class HTMLGenerator: NSObject {
    
    public static func createOuterAndInnerDivAndMoveBodyThere(body: Element) -> Element? {
        do {
            let outerWrapperDiv = try Element(Tag("div"), "").attr("id", "outer-wrapper")
            let innerWrapperDiv = try Element(Tag("div"), "").attr("id", "inner-wrapper")
            try outerWrapperDiv.appendChild(innerWrapperDiv)
            moveElements(oldParent: body, newParent: innerWrapperDiv)
            return outerWrapperDiv
        } catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't create outer and inner div, error: \(error.description)")
        }
        return nil
    }
    
    private static func moveElements(oldParent: Element, newParent: Element) {
        do {
            while oldParent.children().size() > 0 {
                try newParent.appendChild(oldParent.child(0))
            }
        } catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't move elements, error: \(error.description)")
        }
    }
    
    public static func createBookmarkWrapper() -> Element? {
        do {
            return try Element(Tag("div"), "").attr("id", "bookmark")
        }
        catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't create Bookmark wrapper element, error: \(error.description)")
            return nil
        }
    }
    
    public static func createPageNumberWrapper() -> Element? {
        do {
            let div = try Element(Tag("div"), "").attr("id", "page-number-wrapper")
            let span = try Element(Tag("span"), "").attr("id", "page-number")
            try div.appendChild(span)
            return div
        }
        catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't create Bookmark wrapper element, error: \(error.description)")
            return nil
        }
    }
    
    public static func createCssElement() -> Element? {
        do {
            return try Element(Tag("link"), "").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "reader.css")
        }
        catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't create CSS element, error: \(error.description)")
            return nil
        }
    }
    
    public static func createViewportMeta() -> Element? {
        do {
            return try Element(Tag("meta"), "").attr("name", "viewport").attr("content", "initial-scale=1 width=device-width, user-scalable=no")
        }
        catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't create Viewport Meta element, error: \(error.description)")
            return nil
        }
    }
    
    public static func createScriptPromisesBackport() -> Element? {
        do {
            return try Element(Tag("script"), "").attr("src", "promiscuous-browser.js")
        }
        catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't create Viewport Meta element, error: \(error.description)")
            return nil
        }
    }
    
    public static func createScriptLink() -> Element? {
        do {
            return try Element(Tag("script"), "").attr("src", "reader.js")
        }
        catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't create Script Link element, error: \(error.description)")
            return nil
        }
    }
    
    public static func createScriptOnLoad() -> Element? {
        do {
            return try Element(Tag("script"), "").appendText("window.onload = function(){ init(); };")
        }
        catch let error as NSError {
            LoggingUtils.logMessage(message: "Couldn't create Script Link element, error: \(error.description)")
            return nil
        }
    }
}
