//
//  ReaderVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import MaterialShowcase

protocol ReaderInterstitialAdDelegate {
    func readerClosed()
}

class ReaderVC: BaseVC, AppearanceSettingsDelegate, ReaderPageVCManagerDelegate, ReaderEngineDelegate, ReaderTopBarDelegate, UIGestureRecognizerDelegate {
    
    let TopBarVisibleConstraint = CGFloat(-44)
    let TopBarNotVisibleConstraint = CGFloat(-133)
    let BottomSnackBarNotVisibleConstraint = CGFloat(-129)
    let BottomSnackVisible = CGFloat(-40)
    
    @IBOutlet weak internal var coverContainer: UIView!
    @IBOutlet weak private var coverImage: UIImageView!
    
    @IBOutlet weak private var bookContainer: StyleableView!
    @IBOutlet weak private var bookContainerTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak internal var coverProgressView: UIProgressView!
    
    internal var readerEngine: ReaderEngine?
    internal var readerPageVCManager: ReaderPageVCManager?
    
    private var readerControlsDisplayed = false
    private var tapGestureRecognizer: UITapGestureRecognizer?
    
    @IBOutlet weak private var topBarConstraint: NSLayoutConstraint!
    @IBOutlet weak private var topBar: TopBar!
    @IBOutlet weak internal var bottomBar: BottomBar!
    @IBOutlet weak internal var bottomSnackbarConstraint: NSLayoutConstraint!
    
    @IBOutlet weak internal var appearanceSettings: AppearanceSettings!
    
    @IBOutlet weak internal var loadingView: LoadingView!
    internal let reloadIndicator = LoadingIndicator(frame: CGRect(x: 0, y: 0, width: 40, height: 40), color: UIColor.gray)
    
    private var interstitialDisplayed = false
    
    var interstitialAdDelegate: ReaderInterstitialAdDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        styleTopAndBottomBars()
        
        self.coverProgressView.transform = self.coverProgressView.transform.scaledBy(x: 1, y: 2)
        
        self.loadingView.addSubview(reloadIndicator)
        
        self.topBar.delegate = self
        self.bottomBar.delegate = self
        
        if let readerEngine = self.readerEngine {
            readerEngine.delegate = self
            
            //This means we don't know the page counts yet, therefore we need to calculate them and go with a different flow
            if readerEngine.shouldCalculateTotalPages() {
                setCover(coverUrl: readerEngine.getCoverUrl())
                resetPageVCManager()
            } else {
                showLoadingView()
                self.coverContainer?.removeFromSuperview()
                resetPageVCManagerWithEnabledRenderer()
                bottomBar.initWithBookTitle(title: readerEngine.getBookTitle(), totalPagesCount: readerEngine.getTotalPagesCount(), currentPage: readerEngine.getPageIndexForCurrentProgress(), engine: readerEngine)
                bottomBar.setPage(pageIndex: readerEngine.getPageIndexForCurrentProgress())
            }
        }
        
        setupGestureRecognizers()
        appearanceSettings.delegate = self
    }
    
    private func showReaderControlsHint() {
        UIUtils.showHint(inView: bottomBar, title: "Show controls", subtitle: "Tap anywhere on the screen to show your UI controls", completion: {
            UserDefaultsUtils.setReaderControlsHintAsDisplayed()
        })
    }
    
    internal func showHintIfNecessary() {
        if !UserDefaultsUtils.wasReaderControlsHintDisplayed() {
            showReaderControlsHint()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        LoggingUtils.logMessage(message: "Interstitial - Reader VC did appear")
        if interstitialDisplayed == false && !UserDefaultsUtils.areAdsRemoved() {
            InterstitialAdProvider.sharedProvider.showInterstitial(inVC: self)
            interstitialDisplayed = true
        }
        
        //We want to keep the screen from locking
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //Leaving the reader means we can enable screen locking
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupBookmark()
    }
    
    func initWithReaderEngine(readerEngine: ReaderEngine) {
        self.readerEngine = readerEngine
    }
    
    private func setCover(coverUrl: String) {
        coverImage.kf.setImage(with: URL(string: coverUrl), options: [.transition(.fade(0.2))])
    }
    
    private func setupGestureRecognizers() {
        self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(readerTapped(sender:)))
        self.tapGestureRecognizer?.delegate = self
        self.bookContainer.addGestureRecognizer(self.tapGestureRecognizer!)
    }
    
    @objc private func readerTapped(sender: UITapGestureRecognizer) {
        handleReaderControlsDisplay()
    }
    
    func handleReaderControlsDisplay() {
        readerControlsDisplayed = !readerControlsDisplayed
        
        var finalAlpha: CGFloat?
        if readerControlsDisplayed {
            self.topBarConstraint?.constant = TopBarVisibleConstraint
            finalAlpha = CGFloat(1)
        } else {
            self.topBarConstraint?.constant = TopBarNotVisibleConstraint
            finalAlpha = CGFloat(0)
        }
        
        UIView.animate(withDuration: 0.25) {
            self.setNeedsStatusBarAppearanceUpdate()
            self.bottomBar.alpha = finalAlpha!
            self.topBar.alpha = finalAlpha!
            self.view.layoutIfNeeded()
        }
    }
    
    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return !readerControlsDisplayed
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getCurrentThemeType()) ? .lightContent : .default
    }
    
    private func reloadReader(recalculatePageCounts: Bool) {
        //Progress container is a deterministic progress view which is displayed in cases when we need to recalculate page counts
        recalculatePageCounts ? loadingView.showProgressContainer() : loadingView.hideProgressContainer()
        
        self.readerPageVCManager?.disableRenderer()
        showLoadingView()
        handleReaderControlsDisplay()
        
        //Let's have a smooth looking reload
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            if recalculatePageCounts {
                self.readerEngine!.resetContents()
                self.resetPageVCManager()
            } else {
                self.resetPageVCManagerWithEnabledRenderer()
            }
        })
    }
    
    internal func closeAppearanceSettingsPressed() {
        appearanceSettings.hide(completion: {
            self.appearanceSettings.isHidden = true
        })
    }
    
    internal func appearanceOptionSelected(requiresPageRecalculation: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        styleTopAndBottomBars()
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationConstants.ThemeChanged), object: nil)
        appearanceSettings.hide(completion: {
            self.appearanceSettings.isHidden = true
            self.reloadReader(recalculatePageCounts: requiresPageRecalculation)
        })
    }
    
    private func styleTopAndBottomBars() {
        topBar.setStyle(style: ReaderStyleProvider.sharedProvider.getStyle())
        bottomBar.setStyle(style: ReaderStyleProvider.sharedProvider.getStyle())
    }
    
    internal func resetPageVCManager() {
        self.cleanUpReader()
        setupReaderPageVCManager(readerEngine: self.readerEngine!)
    }
    
    private func cleanUpReader() {
        self.bookContainer.subviews.forEach({ $0.removeFromSuperview() })
        self.readerPageVCManager?.cleanUpPages()
    }
    
    private func setupReaderPageVCManager(readerEngine: ReaderEngine) {
        self.readerPageVCManager = createReaderPageVCManager(readerEngine: readerEngine)
        //Reader engine holds current progress of the book therefore we'll use it
        self.readerPageVCManager?.setupPageVC(container: self.bookContainer, startingIndex: self.readerEngine?.getPageIndexForCurrentProgress() ?? 0)
    }
    
    //Function used for in-app appearance changes where we don't need to recalculate pages - we throw away page vc manager, create new one but keep the renderer progress tracker enabled, which turns off the loading after everything is rendered with new style
    private func resetPageVCManagerWithEnabledRenderer() {
        self.cleanUpReader()
        
        self.readerPageVCManager = createReaderPageVCManager(readerEngine: self.readerEngine!)
        self.readerPageVCManager?.enableRenderer()
        self.readerPageVCManager?.setupPageVC(container: self.bookContainer, startingIndex: self.readerEngine?.getPageIndexForCurrentProgress() ?? 0)
    }
    
    private func createReaderPageVCManager(readerEngine: ReaderEngine) -> ReaderPageVCManager {
        let readerPageVC = ReaderPageVCManager(readerEngine: readerEngine, renderingProgressHandler: self.pagesRenderingProgress, reRenderingFinishedHandler: self.reRenderingFinishedHandler, pagesRenderingStartedHandler: self.pagesRenderingStarted, pagesRenderingFinishedHandler: self.pagesRenderingFinished)
        readerPageVC.delegate = self
        return readerPageVC
    }
    
    internal func pageTurned(pageIndex: Int) {
        bottomBar.setPage(pageIndex: pageIndex)
        self.readerEngine?.setCurrentReadingProgress(pageIndex: pageIndex)
        setupBookmark()
    }
    
    override func backPressed(completion: (() -> ())?) {
        super.backPressed(completion: {
            self.interstitialAdDelegate?.readerClosed()
        })
        self.readerPageVCManager?.cleanUpPages()
    }
    
    internal func setupBookmark() {
        if let engine = self.readerEngine, engine.isCurrentPageBookmarked() {
            topBar.setBookmarkSelected(selected: true)
        } else {
            topBar.setBookmarkSelected(selected: false)
        }
    }
    
    internal func bookmarkAdded() {
        topBar.setBookmarkSelected(selected: true)
    }
    
    internal func bookmarkRemoved() {
        topBar.setBookmarkSelected(selected: false)
    }
    
    internal func bookmarkSelected(bookmarkPage: Int) {
        self.bottomBar.setPage(pageIndex: bookmarkPage)
        pageChanged(page: bookmarkPage)
    }
    
    internal func tableOfContentsItemSelected(page: Int) {
        self.bottomBar.setPage(pageIndex: page)
        pageChanged(page: page)
    }
    
    internal func adPageDisplayed() {
        guard self.topBar.bookmarkButton.isHidden == false else {
            return
        }
        self.topBar.bookmarkButton.isHidden = true
    }
    
    internal func adPageNotDisplayed() {
        guard self.topBar.bookmarkButton.isHidden == true else {
            return
        }
        self.topBar.bookmarkButton.isHidden = false
    }
}

extension ReaderVC: BottomBarPageSliderDelegate {
    func pageChanged(page: Int) {
        self.readerEngine?.setCurrentReadingProgress(pageIndex: page)
        showLoadingView()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1200), execute: {
            //Turning renderer back on allows us to receive loading completed callback
            self.readerPageVCManager?.setCurrentPage(index: page)
        })
    }
}
