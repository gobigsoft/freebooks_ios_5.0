//
//  ReaderVCRenderingCallbacks.swift
//  FreeBooks
//
//  Created by Karol Grulling on 17/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension ReaderVC {

    internal func pagesRenderingProgress(progress: Int) {
        LoggingUtils.logMessage(message: "Pages rendering progress: \(progress)")
        
        //Having cover container in place tells us that we're loading a book for the very first time. Such flow has a different view hierarchy (has cover displayed), therefore we should update progress in "coverProgressView" - which is a linear deterministic progress view that's a subview of cover container
        if self.coverContainer != nil {
            coverProgressView.setProgress(Float(progress/100), animated: true)
        //If we don't have cover container, we know we should update progress in a regular loading view
        } else {
            loadingView.setProgress(progress: Float(progress/100))
        }
    }
    
    internal func pagesRenderingStarted() {
        showLoadingView()
    }
    
    internal func pagesRenderingFinished() {
        hideLoadingView()
        
        //After rendering we could end up on totally different page, therefore we need to make sure we check for bookmark as well
        setupBookmark()
    }
    
    internal func reRenderingFinishedHandler() {
        bottomBar.initWithBookTitle(title: self.readerEngine?.getBookTitle() ?? "", totalPagesCount: self.readerEngine?.getTotalPagesCount() ?? 0, currentPage: 1, engine: self.readerEngine)
        
        //Having cover container in place tells us that this book is opened for the first time therefore we need to handle progress bar which is on the cover
        if self.coverContainer != nil {
            //Reset view after we have page counts
            resetPageVCManager()
            //Hide cover after delay so it stays visible and recognizable
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
                UIView.animate(withDuration: 1, animations: {
                    self.coverContainer?.alpha = 0
                }, completion: { success in
                    self.coverContainer?.removeFromSuperview()
                    
                    //This is the last thing of initial rendering, we expect everything to be loaded and ready for reading, therefore we can enable renderer with confidence
                    self.readerPageVCManager?.enableRenderer()
                    
                    //We display UI controls hint if it's for the very first time a book has been loaded 
                    self.showHintIfNecessary()
                })
            })
        } else {
            resetPageVCManager()
            //We need to scroll the reader into the same progress of the book as before the rerendering
            self.bottomBar.setPage(pageIndex: self.readerEngine?.getPageIndexForCurrentProgress() ?? 0)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                //After we're finished we need to hide progress container as well as reset rendering progress back to 0
                self.loadingView.hideProgressContainer()

                self.hideLoadingView()
                //This is the last thing of re-rendering, we expect everything to be loaded and ready for reading, therefore we can enable renderer with confidence
                self.readerPageVCManager?.enableRenderer()
            })
        }
    }
}
