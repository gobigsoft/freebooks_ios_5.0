//
//  ReaderVCProgressHandlers.swift
//  FreeBooks
//
//  Created by Karol Grulling on 03/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension ReaderVC {
    
    internal func showLoadingView() {
        if self.loadingView.isHidden == true {
            reloadIndicator.center = CGPoint.init(x: self.view.center.x, y: self.view.center.y)
            reloadIndicator.startAnimating()

            self.loadingView.setThemeStyle(style: ReaderStyleProvider.sharedProvider.getStyle())
            self.loadingView.isHidden = false
            UIView.animate(withDuration: 0.7, animations: {
                self.loadingView.alpha = 1
            })
        }
    }
    
    internal func hideLoadingView() {
        if self.loadingView.isHidden == false {
            UIView.animate(withDuration: 0.7, animations: {
                self.loadingView.alpha = 0
            }, completion: { success in
                self.reloadIndicator.stopAnimating()
                self.loadingView.isHidden = true
            })
        }
    }
    
    internal func showSnackbar() {
        self.bottomSnackbarConstraint.constant = BottomSnackVisible
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        }, completion: { success in
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                self.hideSnackbar()
            })
        })
    }
    
    internal func hideSnackbar() {
        self.bottomSnackbarConstraint.constant = self.BottomSnackBarNotVisibleConstraint
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
