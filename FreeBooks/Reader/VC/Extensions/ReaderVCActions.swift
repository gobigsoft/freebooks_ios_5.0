//
//  ReaderVCActions.swift
//  FreeBooks
//
//  Created by Karol Grulling on 17/04/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

extension ReaderVC {
    
    internal func addBookmarkPressed() {
        self.readerEngine?.requestBookmark()
        showSnackbar()
    }
    
    internal func removeBookmarkPressed() {
        self.readerEngine?.deleteBookmarkOnCurrentPage()
    }
    
    @IBAction internal func showAppearanceSettings() {
        appearanceSettings.isHidden = false
        appearanceSettings.show()
    }
    
    @IBAction internal func removeBookmarkFromSnackbarPressed() {
        removeBookmarkPressed()
        setupBookmark()
        hideSnackbar()
    }
    
    @IBAction internal func bookMetaContentsPressed() {
        if let bookMetaContentsVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.VCIdentifiers.BookMetaContentsVC) as? BookMetaContentsVC, let engine = self.readerEngine {
            bookMetaContentsVC.initWithEngine(engine: engine)
            self.present(bookMetaContentsVC, animated: true, completion: nil)
        }
    }
    
    @IBAction internal func sharePressed() {
        if let engine = readerEngine {
            shareBook(title: engine.getBookTitle(), authorName: engine.getAuthorFullName())
        }
    }
}
