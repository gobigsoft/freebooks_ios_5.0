//
//  LoadingView.swift
//  FreeBooks
//
//  Created by Karol Grulling on 23/05/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class LoadingView: StyleableView {
    
    @IBOutlet weak private var loadingProgressContainer: UIView!
    @IBOutlet weak private var loadingProgressView: UIProgressView!
    @IBOutlet weak private var processingLabel: UILabel!
    
    override func awakeFromNib() {
        self.loadingProgressView.transform = self.loadingProgressView.transform.scaledBy(x: 1, y: 2)
    }
    
    override func setThemeStyle(style: ReaderStyle) {
        super.setThemeStyle(style: style)
 
        processingLabel.textColor = AppearanceUtils.isDarkTheme(themeType: ReaderStyleProvider.sharedProvider.getCurrentThemeType()) ? UIColor.white : Constants.Colors.DarkerBrown
    }
    
    func setProgress(progress: Float) {
        loadingProgressView.setProgress(progress, animated: true)
    }
    
    func hideProgressContainer() {
        loadingProgressView.progress = 0 
        loadingProgressContainer.isHidden = true
    }
    
    func showProgressContainer() {
        loadingProgressContainer.isHidden = false
    }
}
