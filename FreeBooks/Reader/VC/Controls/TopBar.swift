//
//  TopBar.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol ReaderTopBarDelegate {
    func addBookmarkPressed()
    func removeBookmarkPressed()
}

class TopBar: StyleableView {
    
    @IBOutlet private var actionButtons: [UIButton]!
    @IBOutlet private var fakeNavigationBarDivider: UIView!
    @IBOutlet var bookmarkButton: UIButton!
    var delegate: ReaderTopBarDelegate?
    
    override func setStyle(style: ReaderStyle) {
        setPresetBgColor(style: style)
        
        switch style.theme.themeType {
        case ThemeTypes.Dark, ThemeTypes.Sunset:
            setButtonsTint(color: UIColor.white)
            fakeNavigationBarDivider.isHidden = true
            break
        case ThemeTypes.White, ThemeTypes.Sepia:
            setButtonsTint(color: Constants.Colors.DarkerBrown)
            fakeNavigationBarDivider.isHidden = false
            UIUtils.dropBottomShadow(view: fakeNavigationBarDivider)
            break
        }
    }
    
    func setButtonsTint(color: UIColor) {
        for button in actionButtons {
            button.tintColor = color
        }
    }

    func setBookmarkSelected(selected: Bool) {
        selected ? setBookmarkSelectedState() : setBookmarkUnselectedState()
    }
    
    @objc @IBAction private func addBookmark() {
        //We don't want to show the bookmark was successfully added before we received snippet from reader.js, we'll modify the button's state after we have a result -- call will come from ReaderVC calling setBookmarkSelected
        self.delegate?.addBookmarkPressed()
    }
    
    @objc @IBAction private func removeBookmark() {
        //Here we're not waiting for anything so we can confidently change the button's state and remove bookmark from Realm
        setBookmarkUnselectedState()
        self.delegate?.removeBookmarkPressed()
    }
    
    private func setBookmarkSelectedState() {
        bookmarkButton.removeTarget(self, action: #selector(addBookmark), for: UIControl.Event.allEvents)
        bookmarkButton.addTarget(self, action: #selector(removeBookmark), for: UIControl.Event.touchUpInside)
        bookmarkButton.setImage(UIImage(named: "icon_bookmark_selected"), for: UIControl.State.normal)
    }
    
    private func setBookmarkUnselectedState() {
        bookmarkButton.removeTarget(self, action: #selector(removeBookmark), for: UIControl.Event.allEvents)
        bookmarkButton.addTarget(self, action: #selector(addBookmark), for: UIControl.Event.touchUpInside)
        bookmarkButton.setImage(UIImage(named: "icon_bookmark_unselected"), for: UIControl.State.normal)
    }
}
