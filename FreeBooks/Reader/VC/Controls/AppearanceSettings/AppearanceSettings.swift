//
//  AppearanceSettings.swift
//  FreeBooks
//
//  Created by Karol Grulling on 22/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

struct Theme {
    let themeType: ThemeTypes
    let textColor: UIColor
    let textColorHex: String
    let textSecondaryColor: UIColor
    let textSecondaryColorHex: String
    let backgroundColor: UIColor
    let backgroundColorHex: String
    let bookmarkColor: UIColor
    let bookmarkColorHex: String
}

enum ThemeTypes: String {
    case Sepia = "Sepia"
    case White = "White"
    case Dark = "Dark"
    case Sunset = "Sunset"
    static let allValues =  [Sepia, White, Dark, Sunset]
}

enum AppearanceOptions: String {
    case Theme = "Theme"
    case Font = "Font"
    case TextAlignment = "Text Alignment"
    case TextSize = "Text Size"
    static let allValues = [Theme, Font, TextAlignment, TextSize]
}

enum Fonts: String {
    case Georgia = "Georgia"
    case Neuton = "Neuton"
    case ATTypewriter = "AT Typewriter"
    case Roboto = "Roboto"
    static let allValues = [Georgia, Neuton, ATTypewriter, Roboto]
}

enum TextAlignments: String {
    case Left = "Left"
    case Justify = "Justify"
    case Right = "Right"
    static let allValues = [Left, Justify, Right]
}

protocol AppearanceSettingsDelegate {
    func closeAppearanceSettingsPressed()
    func appearanceOptionSelected(requiresPageRecalculation: Bool)
}

//Extension for registering reusable collection view components (cells/headers) so they can react to theme changes
extension UICollectionReusableView {
    func registerThemeChangedHandler(themeChangedHandler: Selector) {
        NotificationCenter.default.addObserver(self, selector: themeChangedHandler, name: Notification.Name(rawValue: Constants.NotificationConstants.ThemeChanged), object: nil)
    }
}

class AppearanceSettings: UIView {

    private let AppearanceSettingsNibName = "AppearanceSettings"
    private let AppearanceOptionTypeCellIdentifier = "AppearanceOptionTypeCell"
    private let AppearanceOptionTypeHeaderIdentifier = "AppearanceOptionTypeHeader"

    let HiddenConstraint = CGFloat(500)
    let VisibleConstraint = CGFloat(-20)

    private var view: UIView!
    
    @IBOutlet var appearanceSettingsCollection: UICollectionView!
    @IBOutlet weak private var backgroundView: UIView!
    
    private var appearanceGestureRecognizer: UITapGestureRecognizer?
    
    @IBOutlet weak private var bottomConstraint: NSLayoutConstraint!
    
    var delegate: AppearanceSettingsDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        setupGestureRecognizer()
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: AppearanceSettingsNibName, bundle: bundle)
        let views = nib.instantiate(withOwner: self, options: nil)
        let appearanceSettingsView = views[0] as! UIView
        return appearanceSettingsView
    }
    
    private func setupGestureRecognizer() {
        self.appearanceGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeAppearanceSettings))
        self.backgroundView.addGestureRecognizer(self.appearanceGestureRecognizer!)
    }
    
    func show() {
        bottomConstraint.constant = VisibleConstraint
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1
            self.view.layoutIfNeeded()
        }
        
        //We want to let the world know that we're displaying appearance settings - and especially the AppearanceOptionTypeCell that displays Text Sizes, so it knows it needs to scroll to the currently active text size
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationConstants.ReaderAppearanceSettingsDisplayed), object: nil)
    }
    
    func hide(completion: @escaping (() -> Void)) {
        bottomConstraint.constant = HiddenConstraint
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { success in
            completion()
        })
    }
    
    @objc private func closeAppearanceSettings() {
        self.delegate?.closeAppearanceSettingsPressed()
    }
    
    override func awakeFromNib() {
        appearanceSettingsCollection.register(UINib(nibName: AppearanceOptionTypeCellIdentifier, bundle: nil), forCellWithReuseIdentifier: AppearanceOptionTypeCellIdentifier)
        appearanceSettingsCollection.register(UINib(nibName: AppearanceOptionTypeHeaderIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: AppearanceOptionTypeHeaderIdentifier)
        
        appearanceSettingsCollection.dataSource = self
        appearanceSettingsCollection.delegate = self
    }
    
    internal func changeAppearance(requiresPageRecalculation: Bool) {
        self.appearanceSettingsCollection.backgroundColor = AppearanceUtils.getColorForElement(element: StyleableElementTypes.OptionTypeCellBackground)
        self.delegate?.appearanceOptionSelected(requiresPageRecalculation: requiresPageRecalculation)
    }
}

extension AppearanceSettings: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppearanceOptionTypeCellIdentifier, for: indexPath) as! AppearanceOptionTypeCell
        cell.initWithAppearanceOption(option: AppearanceOptions.allValues[indexPath.section], optionSelectedCallback: changeAppearance)
        //We don't want to display the separator after the latest item
        indexPath.section == AppearanceOptions.allValues.count-1 ? cell.removeSeparator() : cell.addSeparator(color: Constants.Colors.Brown_10)
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return AppearanceOptions.allValues.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 70)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AppearanceOptionTypeHeaderIdentifier, for: indexPath) as! AppearanceOptionTypeHeader
            headerView.initWithApperanceOption(option: AppearanceOptions.allValues[indexPath.section])
            return headerView
        default:
            return UICollectionReusableView()
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 45)
    }
}

class AppearanceOptionTypeHeader: UICollectionReusableView {
    
    @IBOutlet weak private var title: UILabel!
    
    func initWithApperanceOption(option: AppearanceOptions) {
        registerThemeChangedHandler(themeChangedHandler: #selector(setStyle))
        self.title.text = option.rawValue
        setStyle()
    }
    
    @objc private func setStyle() {
        self.backgroundColor = AppearanceUtils.getColorForElement(element: StyleableElementTypes.OptionTypeHeaderBackground)
        self.title.textColor = AppearanceUtils.getColorForElement(element: StyleableElementTypes.OptionTypeHeaderTitle)
    }
}
