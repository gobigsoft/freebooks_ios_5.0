//
//  AppearanceUtils.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

enum StyleableElementTypes {
    case OptionTypeHeaderTitle
    case OptionTypeHeaderBackground
    case OptionTypeCellBackground
    case OptionCellTitle
    case OptionCellBackground
    case OptionCellBorder
}

class AppearanceUtils: NSObject {
    //Returns appropriate color according to current style
    public static func getColorForElement(element: StyleableElementTypes, selected: Bool = false) -> UIColor {
        
        let themeType = ReaderStyleProvider.sharedProvider.getStyle().theme.themeType
        
        switch element {
        case .OptionTypeCellBackground, .OptionTypeHeaderBackground:
            return isDarkTheme(themeType: themeType) ? Constants.Colors.Gray_95 : UIColor.white
        case .OptionTypeHeaderTitle:
            return isDarkTheme(themeType: themeType) ? Constants.Colors.Gold : Constants.Colors.ReddishBrown
        case .OptionCellTitle:
            if selected {
                return isDarkTheme(themeType: themeType) ? Constants.Colors.Gold : Constants.Colors.ReddishBrown
            } else {
                return isDarkTheme(themeType: themeType) ? UIColor.white : Constants.Colors.Gray_70
            }
        case .OptionCellBackground:
            return isDarkTheme(themeType: themeType) ? Constants.Colors.Gray_80 : Constants.Colors.Gray_10
        case .OptionCellBorder:
            if selected {
                return isDarkTheme(themeType: themeType) ? Constants.Colors.Gold : Constants.Colors.ReddishBrown
            } else {
                return isDarkTheme(themeType: themeType) ? UIColor.clear : Constants.Colors.Gray_70
            }
        }
    }
    
    public static func getThemeColorForElement(element: StyleableElementTypes, theme: Theme, selected: Bool = false) -> UIColor {
        
        let themeType = ReaderStyleProvider.sharedProvider.getStyle().theme.themeType
        
        switch element {
        case .OptionCellTitle:
            if selected {
                return isDarkTheme(themeType: themeType) ? Constants.Colors.Gold : Constants.Colors.ReddishBrown
            } else {
                return theme.textColor
            }
        case .OptionCellBackground:
            return theme.backgroundColor
        //Others are not theme specific
        default:
            return UIColor.clear
        }
    }
    
    public static func isDarkTheme(themeType: ThemeTypes) -> Bool {
        return (themeType == ThemeTypes.Dark || themeType == ThemeTypes.Sunset)
    }
    
    public static func getHTMLFontSizeForPercentage(sizePercentage: Int) -> Float {
        switch sizePercentage {
        case 50:
            return 9
        case 60:
            return 10.5
        case 70:
            return 12.5
        case 80:
            return 14.5
        case 90:
            return 16.5
        case 100:
            return 18
        case 110:
            return 19.5
        case 120:
            return 21.5
        case 130:
            return 23.5
        case 140:
            return 25
        case 150:
            return 27
        case 175:
            return 31.5
        case 200:
            return 36
        default:
            return 18
        }
    }
    
    public static func getFontForFontType(fontType: Fonts, size: CGFloat) -> UIFont? {
        switch fontType {
        case .Georgia:
            return UIFont(name: "Georgia", size: size)
        case .Neuton:
            return UIFont(name: "Neuton-Regular", size: size)
        case .ATTypewriter:
            return UIFont(name: "AmericanTypewriter-Semibold", size: size)
        case .Roboto:
            return UIFont(name: "Roboto-Regular", size: size)
        }
    }
}
