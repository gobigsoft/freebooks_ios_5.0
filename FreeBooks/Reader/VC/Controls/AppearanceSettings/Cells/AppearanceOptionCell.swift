//
//  AppearanceOptionCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 22/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AppearanceOptionCell: UICollectionViewCell {
    
    @IBOutlet weak private var title: UILabel!
    var isSelectedOption: Bool = false
    var theme: Theme?
    
    func initWithName(name: String) {
        registerThemeChangedHandler(themeChangedHandler: #selector(setStyle))
        self.layer.cornerRadius = 5
        self.title.text = name
        setStyle()
    }
    
    //Setting theme will impact styling of the cell in case of theme change
    func initWithName(name: String, theme: Theme) {
        self.initWithName(name: name)
        self.theme = theme
    }
    
    func initWithName(name: String, fontType: Fonts) {
        self.initWithName(name: name)
        if let font = AppearanceUtils.getFontForFontType(fontType: fontType, size: self.title.font.pointSize) {
            self.title.font = font
        }
    }
    
    @objc func setStyle() {
        if let cellTheme = theme {
            self.title.textColor = AppearanceUtils.getThemeColorForElement(element: StyleableElementTypes.OptionCellTitle, theme: cellTheme, selected: isSelectedOption)
            self.contentView.backgroundColor = AppearanceUtils.getThemeColorForElement(element: StyleableElementTypes.OptionCellBackground, theme: cellTheme, selected: isSelectedOption)
        } else {
            self.title.textColor = AppearanceUtils.getColorForElement(element: StyleableElementTypes.OptionCellTitle, selected: isSelectedOption)
            self.contentView.backgroundColor = AppearanceUtils.getColorForElement(element: StyleableElementTypes.OptionCellBackground)
        }
        
        self.layer.borderColor = AppearanceUtils.getColorForElement(element: StyleableElementTypes.OptionCellBorder, selected: isSelectedOption).cgColor
    }
    
    func setSelected(selected: Bool) {
        self.isSelectedOption = selected
        setSelectedBorder(selected: selected)
        setStyle()
    }
    
    private func setSelectedBorder(selected: Bool) {
        if selected {
            self.layer.borderWidth = 3
        } else {
            self.layer.masksToBounds = true
            self.layer.borderWidth = (theme == nil) ? 0 : 0.5
        }
    }
}
