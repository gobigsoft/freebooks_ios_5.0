//
//  AppearanceOptionTypeCell.swift
//  FreeBooks
//
//  Created by Karol Grulling on 22/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class AppearanceOptionTypeCell: BaseSeparatorCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let SmallestSize = 50
    let BiggestSize = 200
    let AppearanceSettingsNibName = "AppearanceSettings"
    let AppearanceOptionCellIdentifier = "AppearanceOptionCell"
    
    @IBOutlet weak private var optionsCollection: UICollectionView!
    var appearanceOption: AppearanceOptions?
    var textSizes = [Int]()
    
    var optionSelectedCallback: ((_ requiresPageRecalculation: Bool) -> ())?
    
    func initWithAppearanceOption(option: AppearanceOptions, optionSelectedCallback: @escaping (Bool) -> ()) {
        self.optionSelectedCallback = optionSelectedCallback
        registerThemeChangedHandler(themeChangedHandler: #selector(setStyle))
        setStyle()
        appearanceOption = option
        optionsCollection.register(UINib(nibName: AppearanceOptionCellIdentifier, bundle: nil), forCellWithReuseIdentifier: AppearanceOptionCellIdentifier)
        optionsCollection.delegate = self
        optionsCollection.dataSource = self
        
        if option == .TextSize {
            initTextSizes()
            scrollToSelectedTextSize()
            
            NotificationCenter.default.addObserver(self, selector: #selector(scrollToSelectedTextSize), name: Notification.Name(Constants.NotificationConstants.ReaderAppearanceSettingsDisplayed), object: nil)
        }  
    }
    
    @objc private func setStyle() {
        self.backgroundColor = AppearanceUtils.getColorForElement(element: StyleableElementTypes.OptionTypeCellBackground)
    }
    
    @objc private func scrollToSelectedTextSize() {
        if let selectedTextSizeIndex = self.textSizes.firstIndex(of: ReaderStyleProvider.sharedProvider.getCurrentTextSize()) {
            self.optionsCollection.scrollToItem(at: IndexPath(row: selectedTextSizeIndex, section: 0), at: UICollectionView.ScrollPosition.left, animated: false)
            self.optionsCollection.layoutSubviews()
        }
    }
    
    private func initTextSizes() {
        let sizeCount = ((BiggestSize-SmallestSize)/10)-5 //skip 160 - 200
        for i in 0...sizeCount {
            textSizes.append(SmallestSize+(i*10))
        }
        textSizes.append(contentsOf: [175, 200]) //add 2 more sizes - 175% & 200%
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let appearanceOption = self.appearanceOption else {
            return 0
        }
        
        switch appearanceOption {
        case .Theme:
            return ThemeTypes.allValues.count
        case .Font:
            return Fonts.allValues.count
        case .TextAlignment:
            return TextAlignments.allValues.count
        case .TextSize:
            return textSizes.count
        }
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let appearanceOption = self.appearanceOption else {
            return UICollectionViewCell()
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppearanceOptionCellIdentifier, for: indexPath) as! AppearanceOptionCell
        
        switch appearanceOption {
        case .Theme:
            let themeType = ThemeTypes.allValues[indexPath.row]
            let theme = ReaderStyleProvider.sharedProvider.getTheme(themeType: themeType)
            cell.initWithName(name: themeType.rawValue, theme: theme)
            //Unselected theme option has theme text color
            themeType == ReaderStyleProvider.sharedProvider.getCurrentThemeType() ? cell.setSelected(selected: true) : cell.setSelected(selected: false)
            break
        case .Font:
            let fontType = Fonts.allValues[indexPath.row]
            cell.initWithName(name: fontType.rawValue, fontType: fontType)
            fontType == ReaderStyleProvider.sharedProvider.getCurrentFont() ? cell.setSelected(selected: true) : cell.setSelected(selected: false)
            break
        case .TextAlignment:
            let textAlignment = TextAlignments.allValues[indexPath.row]
            cell.initWithName(name: textAlignment.rawValue)
            textAlignment == ReaderStyleProvider.sharedProvider.getCurrentTextAlignment() ? cell.setSelected(selected: true) : cell.setSelected(selected: false)
            break
        case .TextSize:
            let textSize = textSizes[indexPath.row]
            cell.initWithName(name: "\(textSize)%")
            textSize == ReaderStyleProvider.sharedProvider.getCurrentTextSize() ? cell.setSelected(selected: true) : cell.setSelected(selected: false)
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let appearanceOption = self.appearanceOption else {
            return
        }
        
        switch appearanceOption {
        case .Theme:
            ReaderStyleProvider.sharedProvider.setThemeType(themeType: ThemeTypes.allValues[indexPath.row])
            break
        case .Font:
            ReaderStyleProvider.sharedProvider.setFont(fontType: Fonts.allValues[indexPath.row])
            break
        case .TextAlignment:
            ReaderStyleProvider.sharedProvider.setTextAlignment(alignment: TextAlignments.allValues[indexPath.row])
            break
        case .TextSize:
            ReaderStyleProvider.sharedProvider.setTextSize(size: textSizes[indexPath.row])
            break
        }
        
        RealmUtils.storeCurrentStyle()
        
        optionsCollection.reloadData()
        (appearanceOption == .TextSize) ? optionSelectedCallback?(true) : optionSelectedCallback?(false)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let apperanceOption = self.appearanceOption else {
            return CGSize(width: collectionView.frame.size.width, height: 0)
        }
        
        switch apperanceOption {
        case .Theme:
            return CGSize(width: collectionView.frame.size.width/4-10, height: 40)
        case .Font:
            return CGSize(width: collectionView.frame.size.width/4-10, height: 40)
        case .TextAlignment:
            return CGSize(width: collectionView.frame.size.width/3-10, height: 40)
        case .TextSize:
            return CGSize(width: collectionView.frame.size.width/6, height: 40)
        }
    }
}
