//
//  BottomBar.swift
//  FreeBooks
//
//  Created by Karol Grulling on 22/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

protocol BottomBarPageSliderDelegate {
    func pageChanged(page: Int)
}

class BottomBar: StyleableView {
    
    @IBOutlet weak private var bookTitle: UILabel!
    @IBOutlet weak private var currentBookPage: UILabel!
    @IBOutlet weak private var pageSelectionSlider: UISlider!
    @IBOutlet weak private var bottomBarDivider: UIView!
    var totalPagesCount = 0
    
    var engine: ReaderEngine?
    var delegate: BottomBarPageSliderDelegate?
    
    func initWithBookTitle(title: String, totalPagesCount: Int, currentPage: Int, engine: ReaderEngine?) {
        self.engine = engine
        
        self.totalPagesCount = totalPagesCount
        bookTitle.text = title
        currentBookPage.text = "\(currentPage)/\(totalPagesCount)"
        setChapterTitle(pageIndex: currentPage)
        
        pageSelectionSlider.isContinuous = true
        pageSelectionSlider.maximumValue = Float(totalPagesCount)
        pageSelectionSlider.minimumValue = 1
    }
    
    override func setStyle(style: ReaderStyle) {
        super.setPresetBgColor(style: style)
        switch style.theme.themeType {
        case ThemeTypes.Dark, ThemeTypes.Sunset:
            self.bookTitle.textColor = Constants.Colors.Gold
            self.currentBookPage.textColor = Constants.Colors.Gold
            self.pageSelectionSlider.thumbTintColor = Constants.Colors.Gold
            self.pageSelectionSlider.tintColor = Constants.Colors.Gold
            self.bottomBarDivider.isHidden = true
            break
        case ThemeTypes.White, ThemeTypes.Sepia:
            self.bookTitle.textColor = Constants.Colors.DarkerBrown
            self.currentBookPage.textColor = Constants.Colors.DarkerBrown
            self.pageSelectionSlider.thumbTintColor = Constants.Colors.DarkerBrown
            self.pageSelectionSlider.tintColor = Constants.Colors.DarkerBrown
            self.bottomBarDivider.isHidden = false
            UIUtils.dropTopShadow(view: bottomBarDivider)
            break
        }
    }
    
    func setPage(pageIndex: Int) {
        self.pageSelectionSlider.value = Float(pageIndex+1)
        currentBookPage.text = "\(Int(pageSelectionSlider.value))/\(totalPagesCount)"
        setChapterTitle(pageIndex: pageIndex)
    }
    
    private func setChapterTitle(pageIndex: Int) {
        bookTitle.text = engine?.getChapterNameForPageIndex(index: pageIndex)
    }
    
    @IBAction private func pageSelectionSliderValueChanged(_ slider: UISlider, _ event: UIEvent) {
        currentBookPage.text = "\(Int(pageSelectionSlider.value))/\(totalPagesCount)"
        setChapterTitle(pageIndex: Int(pageSelectionSlider.value-1))
        
        //Page needs to be changed at the end of sliding 
        guard let touch = event.allTouches?.first, touch.phase != .ended else {
            LoggingUtils.logMessage(message: "Bottom Bar Page Changed, page: \(Int(pageSelectionSlider.value-1))")
            self.delegate?.pageChanged(page: Int(pageSelectionSlider.value-1))
            return
        }
    }
}   	
