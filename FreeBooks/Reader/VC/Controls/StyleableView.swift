//
//  StyleableView.swift
//  FreeBooks
//
//  Created by Karol Grulling on 24/03/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit

class StyleableView: UIView {
    
    internal func setStyle(style: ReaderStyle) {
        self.backgroundColor = AppearanceUtils.isDarkTheme(themeType: style.theme.themeType) ? UIColor.black : UIColor.white
    }
    
    internal func setPresetBgColor(style: ReaderStyle) {
        self.backgroundColor = AppearanceUtils.isDarkTheme(themeType: style.theme.themeType) ? Constants.Colors.Gray_95 : UIColor.white
    }
    
    internal func setThemeStyle(style: ReaderStyle) {
        self.backgroundColor = style.theme.backgroundColor
    }
}
