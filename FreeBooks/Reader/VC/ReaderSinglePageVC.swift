//
//  ReaderSinglePageVC.swift
//  FreeBooks
//
//  Created by Karol Grulling on 28/02/2019.
//  Copyright © 2019 spreadsong. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON

protocol ReaderPageDelegate {
    func pageCountsRetrieved(pageCounts: [Int], tableOfContentsIndexItems: [TableOfContentsIndexItem])
    func pageCountsCalculationProgress(progress: Int)
}

protocol ReaderPageRenderDelegate {
    func pageRendered(pageIndex: Int)
}

class ReaderSinglePageVC: BaseIndexableVC, WKNavigationDelegate, WKUIDelegate, JSHandlerDelegate {
    
    let BookmarkNotVisibleTopConstraint = CGFloat(100)
    let BookmarkVisibleTopConstraint = CGFloat(0)
    let SpineIndexPrefix = "SpineIndex-"
    
    private var pageContent: String?
    private var pageWebView: WKWebView?
    
    private var engine: ReaderEngine?
    private var jsHandler: JSHandler?
    
    var delegate: ReaderPageDelegate?
    var renderDelegate: ReaderPageRenderDelegate?
    
    private var config: String?
    
    @IBOutlet weak private var configButton: UIButton!
    @IBOutlet weak private var bookmarkConstraint: NSLayoutConstraint!
    @IBOutlet weak private var bookmarkBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = ReaderStyleProvider.sharedProvider.getStyle().theme.backgroundColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupBookmark()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Setting current page into engine allows anyone who has access to it (especially parent VC - ReaderVC which holds top/bottom control bars) to interract with current page
        self.engine?.setCurrentPage(page: self)
    }

    func initWithBookPage(pageIndex: Int, engine: ReaderEngine) {
        self.engine = engine
        self.jsHandler = JSHandler(readerEngine: self.engine!, delegate: self)
        self.pageContent = self.engine!.getPageContent(pageIndex: pageIndex)
        
        setupWebView()
        
        //Testing button for displaying current configuration
        self.view.bringSubviewToFront(configButton)
        self.view.bringSubviewToFront(bookmarkBtn)
    }
    
    private func setupWebView() {
        let configuration = jsHandler!.createWebViewConfiguration()
        self.pageWebView = WKWebView(frame: self.view.bounds, configuration: configuration)
        
        //Sort of hack to force web view to fully load and avoid flickering after turning the page, wkwebviews won't load until they aren't in the active hierarchy, the only way to achieve it is by adding it into the window in an unnoticeable way
        UIApplication.shared.keyWindow?.addSubview(self.pageWebView!)
        self.pageWebView?.removeFromSuperview()

        //Web view needs to be added as a subview before its content is loaded
        self.view.addSubview(self.pageWebView!)
        
        self.pageWebView?.navigationDelegate = self
        self.pageWebView?.uiDelegate = self
        self.pageWebView?.scrollView.bounces = false
        self.pageWebView?.scrollView.isScrollEnabled = false
        self.pageWebView?.isOpaque = false
        self.pageWebView?.backgroundColor = UIColor.clear
        self.pageWebView?.loadHTMLString(pageContent!, baseURL: Bundle.main.bundleURL)
    }
    
    //WKUIDelegate method providing synchronous communication between JS <> WKWebView
    internal func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        LoggingUtils.logMessage(message: "Content of page is \(prompt)")
        
        guard let engine = self.engine else {
            return
        }
        
        if prompt.contains(SpineIndexPrefix) {
            let promptValue = prompt.replacingOccurrences(of: SpineIndexPrefix, with: "")
            let spineElementIndex = Int(promptValue)
            LoggingUtils.logMessage(message: "Content of page \(String(describing: spineElementIndex)) is \(engine.getHtmlPageContent(htmlPageIndex: spineElementIndex!))")
            completionHandler(engine.getHtmlPageContent(htmlPageIndex: spineElementIndex!))
        }
    }
    
    internal func onInit() {
        guard let engine = self.engine else {
            return
        }
        config = ReaderConfigProvider.sharedProvider.getConfig(pageIndex: pageIndex, htmlPagesCount: engine.getHtmlPagesCount(), localPageIndex: engine.getLocalPageIndex(globalPageIndex: pageIndex), shouldCalculateTotalPages: engine.shouldCalculateTotalPages(), tableOfContentsChapterIds: engine.getTableOfContentChapterIds(), readerStyle: engine.getReaderStyle()).getJsonString()
        pageWebView?.evaluateJavaScript("setupWithConfig('\(String(describing: config!))')", completionHandler: { completion, error in
            self.renderDelegate?.pageRendered(pageIndex: self.pageIndex)
        })
    }
    
    internal func onPagesRenderResult(pageCounts: [Int], tableOfContentsIndexItems: [TableOfContentsIndexItem]) {
        delegate?.pageCountsRetrieved(pageCounts: pageCounts, tableOfContentsIndexItems: tableOfContentsIndexItems)
    }
    
    internal func onPagesRenderProgress(progress: Int) {
        delegate?.pageCountsCalculationProgress(progress: progress)
    }
    
    private func setupBookmark() {
        LoggingUtils.logMessage(message: "Setup bookmark for page called with index \(self.pageIndex)")
        bookmarkBtn.tintColor = ReaderStyleProvider.sharedProvider.getStyle().theme.bookmarkColor
        if let engine = self.engine, engine.isPageBoomarked(pageIndex: self.pageIndex) {
            showBookmark()
        } else {
            hideBookmark()
        }
    }

    private func showBookmark() {
        bookmarkConstraint.constant = BookmarkVisibleTopConstraint
        bookmarkBtn.isHidden = false
        self.view.layoutIfNeeded()
    }

    //We need to be able to hide bookmark from reader engine, in case when it was removed from top bar instead of tapping on the bookmark itself --> which we already have under control here
    func hideBookmark() {
        bookmarkConstraint.constant = BookmarkNotVisibleTopConstraint
        bookmarkBtn.isHidden = true
        self.view.layoutIfNeeded()
    }
    
    @IBAction private func bookmarkTapped() {
        hideBookmarkAnimated()
        if let engine = self.engine {
            engine.deleteBookmark(pageIndex: self.pageIndex)
        }
    }
    
    @objc private func hideBookmarkAnimated() {
        bookmarkConstraint.constant = BookmarkNotVisibleTopConstraint
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }, completion: { success in
            self.bookmarkBtn.isHidden = true
        })
    }
    
    func getPageSnippet() {
        pageWebView?.evaluateJavaScript("getCurrentPageSnippet()", completionHandler: { completion, error in
            
        })
    }
    
    internal func onPageSnippetResult(pageSnippet: String) {
        LoggingUtils.logMessage(message: "Page snippet: \(pageSnippet)")
        if let engine = self.engine {
            engine.createBookmark(pageIndex: self.pageIndex, snippet: pageSnippet)
            
            //We've successfully obtained page snippet, therefore we're ready to show bookmark
            showBookmark()
        }
    }
    
    @IBAction private func showConfig() {
        LoggingUtils.logMessage(message: "Page: \(pageIndex), config: \(String(describing: config))")
    }
    
    deinit {
        LoggingUtils.logMessage(message: "De initing page \(pageIndex)")
        cleanUp()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        LoggingUtils.logMessage(message: "Page disappeared \(pageIndex)")
    }
    
    func cleanUp() {
        LoggingUtils.logMessage(message: "Cleaning up page \(pageIndex)")
        self.jsHandler = nil
        self.pageWebView?.stopLoading()
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.Test)
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.OnInitCallback)
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.OnSetupWithConfigCallback)
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.OnPagesRenderResultCallback)
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.OnPagesRenderProgressCallback)
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.OnBodyClickedCallback)
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.OnBookmarkClickedCallback)
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.OnAnchorClickedCallback)
        self.pageWebView?.configuration.userContentController.removeScriptMessageHandler(forName: JSCallbacks.OnPageSnippetResultCallback)
        self.pageWebView = nil     
    }
}
